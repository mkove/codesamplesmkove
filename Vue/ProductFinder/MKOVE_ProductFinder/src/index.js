import Vue from 'vue';
import App from './App.vue';

new Vue({
	el: '#pfApp',
	render: h => h(App),
});