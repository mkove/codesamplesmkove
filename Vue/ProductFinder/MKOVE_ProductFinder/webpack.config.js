const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
	entry: [
		__dirname + '/src/index.js',
		__dirname + '/src/style/layout.scss',
		__dirname + '/src/style/acemarks.scss'
	],
	module: {
		rules: [
			{ test: /\.js$/, use: 'babel-loader' },
			{ test: /\.vue$/, use: 'vue-loader' },
			{ test: /\.css$/, use: ['vue-style-loader', 'css-loader']},
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'file-loader',
						options: { outputPath: 'css/', name: 'pf-[name].min.css'}
					},
					'sass-loader'
				]
			},
			// { test: /\.scss$/i,
			// 	use: [
			// 		"vue-style-loader",
			// 		"style-loader",
			// 		"css-loader",
			// 		"sass-loader",
			// 	],
			// },
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'fonts/'
						}
					}
				]
			}
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html',
		}),
		new VueLoaderPlugin(),
	]
};