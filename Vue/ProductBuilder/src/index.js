
const URL = "https://qa.allumandsidaway.co.uk/brown-and-newirth-ring-builder/app/";


import Vue from 'vue';
import App from './ProductBuilderApp.vue';
import VueResource from 'vue-resource';
import '@trevoreyre/autocomplete-vue/dist/style.css'
// Vue.use(Autocomplete)
import './branding.css';
Vue.use(VueResource);
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const router = new VueRouter();
//
// export default new Router({
// 	routes: [
// 		{
// 			path: '/',
// 			name: 'ProductBuilder',
// 			component: App
// 		}
// 	]
// })

new Vue({
	el: '#productBuilderApp',
	router,
	render: h => h(App),
});
// console.log("LOADED APP");