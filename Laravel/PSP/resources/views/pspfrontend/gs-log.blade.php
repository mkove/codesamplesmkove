@extends('pspfrontend.layout.main')
@section('content')
    @include('pspfrontend.user.partials.nav',['active' => 'pools'])
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient border-white">
                        <h3 class="text-white text-uppercase ">GS LOG</h3>
                    </div>
                    <div class="card-body pb-5">
                        <div id="gsLog">
                            <log-table :messages.sync="getMessages"></log-table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @push('custom-js')
        <!-- <script src="{{asset('js/common-echo.js')}}?v={{rand(1,334434)}}"></script> -->
        <script src="/js/logger.min.js"></script>
    @endpush
@endsection
