<nav class="navbar navbar-default navbar-account">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed text-white" style="color:white !important;" data-toggle="collapse" data-target="#navbar-account" aria-expanded="false" aria-controls="navbar-account">
                <span class="sr-only">Toggle navigation</span>
                <i style="color:white;" class="fa fa-bars"></i> USER MENU
            </button>
        </div>
        <div id="navbar-account" class="navbar-collapse collapse" style="background: #000;">
            <ul class="nav navbar-nav navbar-right top-right-links">
                <li><a href="#">{{date('M j, Y G:i A T')}}</a></li>
                <li><a href="#"><i class="fa fa-phone"></i> Contact Us</a></li>
            </ul>
        </div>
    </div>
</nav>
