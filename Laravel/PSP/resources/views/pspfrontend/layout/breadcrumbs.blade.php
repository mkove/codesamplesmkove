<div class=" margin-t-20">
    @php
        if(isset($crumbs->obj)){
            $crumbs = $crumbs->get();
            $cnt = $crumbs->count();
        } else {
            $cnt = (isset($crumbs) && is_array($crumbs)) ? count($crumbs) : 0;
        }
    @endphp
    @if(isset($crumbs) && $cnt && $crumbs)
        <div class="grid-breadcrumbs text-uppercase mb-3 pl-4">
            <div class="col-xs-12">
                <nav aria-label="breadcrumbs">
                    <div style="display: inline-block;white-space: nowrap;">
                        <ol class="breadcrumbs bg-transparent pl-0 pr-0 pt-0 pb-0 mt-0 mb-0" >
                            @foreach($crumbs as $crumb)
                                <li class=" {{($loop->first) ? "" : ''}}  breadcrumbs-item {{(isset($crumb['active']) && $crumb['active']) ? 'active-bc' : ''}}">
                                    @if(isset($crumb['active']) && !$crumb['active'] && isset($crumb['link']))
                                        <a href="{{$crumb['link']}}">{!!  $crumb['label'] !!} </a>
                                    @else
                                        <a href="#">{!!  str_replace(' ', '&nbsp;', $crumb['label']) !!}</a>
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                    </div>
                   
                </nav>
            </div>
        </div>
    @endif
</div>
