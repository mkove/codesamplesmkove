<div class="navbar-wrapper" id="main-menu">
    <div class="container-fluid">

        <nav class="navbar navbar-static-top navbar-main">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle text-center collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="color:white !important;">
                        <span class="sr-only">Toggle navigation</span>
                        <i style="color:white;" class="fa fa-bars text-white"></i> <span class="" style="font-weight: bold;">MAIN MENU</span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/">HOME</a></li>
                        <li class="divider-vertical" style="color:#e3f5ff;border-left:inset 1px white;"><span style="width:100%;"></span></li>
                        <li class="dropdown">
                            <a href="#" class="fe-dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">POOLS <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/pool/create">Create Pool</a></li>
                                <li><a href="/pool">Join Pool</a></li>
                                <li class="dropdown-submenu">
                                    <a class="test" tabindex="-1" href="#">Available Pools <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a tabindex="-1" href="/pool">View All</a></li>
                                        <li><a tabindex="-1" href="/pool/?filter=squares">Squares</a></li>
                                        <li><a tabindex="-1" href="/pool/?filter=schwindy">Schwindy</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        @if($user = auth()->user())
                            <li><a href="/user/pools">My Pools</a></li>
                            <li><a href="/user">{{$user->global_display_name}}</a></li>
                            <li><a href="/logout">logout</a></li>
                        @else
                            <li><a href="/login">Login</a></li>
                            <li><a href="/signup">Sign Up</a></li>
                            
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
