<!DOCTYPE html>
<html lang="en">
<head>
    @include('pspfrontend.layout.head')
</head>
<body>
@if(App\Helpers\SiteHelper::conf('site.tracking.track','boolean', '1'))
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{__conf('site.tracking.google_tag_manager','text', 'GTM-K4KVVFG')}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
@endif
<div class="wrapper">
    @include('pspfrontend.layout.header')

    <div class="content" role="main">
        @yield('content')
    </div>
    @include('pspfrontend.layout.footer')

</div>
@php $hideChat = (isset($hide_chat)) ? $hide_chat : false; @endphp
@if(isset($pool) && $pool && $pool->chat && !$hideChat && \Auth::user())
    <div id="poolChat" data-id="{{$pool->chat->id}}">
        <chat-container
            :chat.sync ="chat"
            @send-message="sendMessage"
            @toggle-chat="toggleChat"
        />
    </div>
@endif
@stack('modals')
@include('pspfrontend.layout.footerjs')

@if(isset($pool) && $pool && $pool->chat && !$hideChat && \Auth::user())
    <!-- <script src="{{asset('js/common-echo.js')}}?v={{rand(1,334434)}}"></script> -->
    <script src="{{asset('js/chat.js')}}?v={{rand(1,334434)}}"></script>
@endif

@include('pspfrontend.layout.svgs')
</body>
</html>
