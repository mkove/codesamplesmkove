
<div class="modal fade" id="liveEditPopup" tabindex="-1" role="dialog" aria-labelledby="contentEditPopup" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="card bg-transparent border-white">
                <div class="card-header brand-gradient border-white">
                    <h5 class="text-white text-uppercase" id="liveEditTitle"></h5>
                    <div class="card-header-controls">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-brand-darkgrey brand-btn" id="liveEditPopupCancel" aria-label="Close">
                                <i class="fa fa-times"></i> Close
                            </button>
                            <button data-id="" id="liveEditSubmit" data-url="" class="btn btn-brand-blue brand-btn btn-sm"><i class="fa fa-save"></i> Update</button>
                        </div>

                    </div>
                </div>
                <div class="card-body p-3 pb-5 bg-brand-grey">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 id="liveEditFormTitle"></h3>
                            <div class="form-group">
                                <textarea name="description" id="liveEditInput" class="form-control" rows="5"></textarea>
                                <div class="help-block text-white" id="liveEditHelp"></div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-12">Note</label>
                                <div class="col-12" id="liveEditLabel"></div>
                            </div>
                            <div class="form-group">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('custom-css')

    <link rel="stylesheet" href="/psp/css/richtext.min.css">
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote-bs4.css">
@endpush
@push('custom-js')
    <script src="/assets/vendor/summernote/summernote-bs4.js"></script>
    <script>
        $(document).ready(function() {
            $('#liveEditPopup').on('shown.bs.modal', function() {
                $('#liveEditInput').summernote({
                    height: 200,
                    dialogsInBody: true,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ]
                });
            });
            $('#liveEditPopup').on('shown.bs.modal', function() {
                $('#liveEditInput').summernote('destroy');
            });
            // $(‘..’).summernote(‘destroy’);
            $('#liveEditPopupCancel').click(function(){
                $('#liveEditInput').summernote('destroy');
                $('#liveEditPopup').modal('hide');
            });

            $(document).on('click','.edit-content', function(){
                var id = $(this).data('id');
                var content = $(this).data('content');
                var identifier = $(this).data('identifier');
                var title = $(this).data('title');
                var section = "<span class='text-brand-orange'>"+$(this).data('section')+"</span> ";
                var note = $(this).data('note');
                $('#liveEditFormTitle').html(title);
                $('#liveEditTitle').html(section);
                $('#liveeditNote').html(note);
                $('#liveEditInput').val(content);
                $('#liveEditSubmit').data('id', id);
                $('#liveEditHelp').html(identifier);

            });
            $(document).on('click', '#liveEditSubmit', function(){
                var content = $('#liveEditInput').val();
                var id = $('#liveEditSubmit').data('id');
                var reloadable = '.reload-'+id;
                $.ajax({
                    url: '/admin/content/'+id,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        content : content,
                        _method: 'PUT',
                        _token : $('input[name="_token"]').val()
                    },
                })
                    .done(function(jsonData) {
                        if(jsonData.error){
                            alert("Error Update");
                        } else {

                        }
                        $('#liveEditInput').summernote('destroy');
                        $('#liveEditPopup').modal('hide');
                    })
                    .fail(function() {

                    })
                    .always(function() {

                    });

                $(reloadable).each(function() {
                    $( this ).addClass( "bg-brand-green" );
                    $(this).html(content);
                });
            })

            //init summernote

        });

        //click edit trigger


        //submit content edit

    </script>
@endpush
