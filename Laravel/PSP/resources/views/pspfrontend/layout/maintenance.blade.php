<div class="bg-brand-charcoal p-3 text-center border-bottom">
    <p class="text-white m-0">{!! __c("website.content.maintenance_mode_notice",'<span class="text-brand-red">MAINTENANCE ALERT:</span> We will perform website maintenance on <span class="text-brand-red">January 18, 2021 2:00 PM Eastern Time</span>.<br>Site will be unavailable!', [], false) !!}</p>
</div>
