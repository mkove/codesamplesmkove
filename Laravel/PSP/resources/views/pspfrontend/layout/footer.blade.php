<div class="menu-gradient pt-1"></div>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="footer-logo p-1 text-sm-center text-lg-left"><a href="/">
                                    <svg class="logo-svg">
                                        <use xlink:href="#logo"></use>
                                    </svg></a>
                            </div>
                            {{--                            <div class="footer-logo-container">--!!}
{{--                                --!!}
{{--                                <a href="/">--!!}
{{--                                    <img src="/assets/images/logo_white.png" alt="Premier Sports Pools">--}}
{{--                                </a>--}}
{{--                            </div>--}}
                            <div class="credit-cards pt-1 text-sm-center text-lg-left">
                                <img src="/assets/images/credit_card_logos.png" width="250" height="38" alt="We accept Visa, Mastercard, AMEX">
                            </div>

                        </div>
                        <div class="col mx-auto pt-4">
                            <h4>Information</h4>
                            <ul>
                                @foreach(\App\Models\StaticPage::where('active', true)->orderBy('order','asc')->get() as $staticPage)
                                    <li><a class="text-white" href="/page/{{$staticPage->slug}}">{{$staticPage->link_title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-3 pt-4">
                            <h4>Contact Information</h4>
                            <div class="address-container">{!! __c("website.contact_street_address", "Street 123", [] ,true) !!}<br> {!!__c("website.contact_street_address_2", "Suite 233", [] ,true)!!} <br>
                                {!!__c("website.contact_street_city", "Cincinnati", [] ,true)!!}, {!!__c("website.contact_state", "Ohio", [] ,true)!!} - {!!__c("website.contact_zipcode", "43222", [] ,true)!!} <br>
                                <hr>
                                <i class="fa fa-phone"></i> <a href="tel:{!!__c("website.contact_phone", "(123)123-3333", [] ,true)!!}">{!!__c("website.contact_phone", "(123)123-3333", [] ,true)!!}</a>&nbsp;|&nbsp; <i class="fa fa-envelope"></i> <a href="mailto:{!!__c("website.contact_email", "info@premiersportspools.com", [] ,true)!!}">{!!__c("website.contact_email", "info@premiersportspools.com", [] ,true)!!}</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col mx-auto text-center p-3 footer_copy">
                {!!  __c('footer.copyright_text', "Copy", [] ,false) !!}

            </div>
        </div>
    </div>
    <div class="container bg-black">

    </div>

</footer>

<div class="modal fade" id="infoPopup" tabindex="-1" role="dialog" aria-labelledby="infoPopup" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="card bg-transparent">
                <div class="card-header brand-gradient  ">
                    <h5 class="text-white text-uppercase">{!! $help_title ?? "Help Missing" !!}</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="card-body p-3 pb-5 text-white bg-brand-darkgrey">
                    <div class="row">
                        <div class="col-md-12 text-white">
                            @php
                            if(isset($help_content)){
                                $help_content = str_replace("&lt;", "<", $help_content);
                                $help_content = str_replace("&gt;", ">", $help_content);
                            }
                            @endphp
                            {!! $help_content ?? "Help Content Missing" !!} <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php $usr = auth()->user(); @endphp
@if($usr && $usr->is_admin)
    @include('pspfrontend.layout.live-edit')
@endif
