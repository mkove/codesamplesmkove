@if(config('app.env') == 'development')
    <div style="background:red;text-align: center; width:100%;display: block;padding:20px;color:yellow;"><strong>{{config('app.env')}} SERVER</strong></div>
@endif
@if(App\Helpers\SiteHelper::conf('site.maintenance.flag','boolean', 0))
    @include('pspfrontend.layout.maintenance')
@endif
<header class="header text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flexy">
                    <div class="logo-container">
                        <div class="header-logo">
                            <a href="/">
                                <svg class="logo-svg">
                                    <use xlink:href="#logo"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="menu-container">
                        <div class="mainNav">
                            @include('pspfrontend.layout.mainnav')
                        </div>
                        <div class="seconaryNav">
                            @include('pspfrontend.layout.secondarynav')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
