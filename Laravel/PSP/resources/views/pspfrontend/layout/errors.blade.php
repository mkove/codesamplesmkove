@php
    $messages = session('messages') ;
@endphp
@if($messages && count($messages))
    <div class="container pt-2">
        <div class="row">
            <div class="col-md-12">
                <ul style="list-style: none;margin-left:0;padding:0;">
                    @foreach($messages as $message)
                        <li class="alert alert-{{$message['type']}}" style="list-style: none">
                            {!! $message['message'] !!}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
