    <div class="d-none d-lg-block secondary-nav" id="navbarSecondary">
        <ul class="p-0 m-0">
            <li class="text-uppercase nav-item "><a class="nav-link text-white  " style="" href="/pool">{!! __c("website.nav.main_menu.join_pool_item", "Join Pool")!!}</a></li>
            <li class="text-uppercase nav-item "><a style="" class="nav-link text-white  " href="{{route('psp.pool.create')}}">{!! __c("website.nav.main_menu.create_pool_item", "Create Pool")!!}</a></li>
            <li class="text-uppercase nav-item ">
                <a class="nav-link text-white  "  href="{!! route('psp.user.pools')!!}">{!! __c("website.nav.main_menu.my_pools_item", "My Pools")!!}</a>
            </li>
        </ul>
    </div>
