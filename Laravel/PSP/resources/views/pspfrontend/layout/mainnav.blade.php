<button class="openbtn toggle-nav toggle-nav-primary text-white  " data-menu="primaryNav" style="font-size: 16px;right: 0;position: absolute;padding-top: 20px;">&#9776; {!! __c('website.nav.main_menu.menu_toggle_button','MENU') !!}</button>
<div id="primaryNav" class="collapsable-nav collapsable-nav-primary collapsable-nav-dropdown">
    <a href="javascript:void(0)" class="closebtn toggle-nav" data-menu="primaryNav">&times;</a>

    <ul class="pb-0 mb-0">
        <li class="text-uppercase  d-lg-none d-xl-nonе  "><a class="  text-white"  href="{!! route('psp.user.pools')!!}">{!! __c("website.nav.main_menu.my_pools_item", "My Pools")!!}</a></li>
        <li class="text-uppercase  d-lg-none d-xl-nonе  "><div></div><a class=" text-white " style="" href="/pool">{!! __c("website.nav.main_menu.join_pool_item", "Join Pool")!!}</a></li>
        <li class="text-uppercase  d-lg-none d-xl-nonе  "><a style="" class=" text-white " href="/pool/create">{!! __c("website.nav.main_menu.create_pool_item", "Create Pool")!!}</a></li>

        @if($user = Auth::user())

                <li class="text-uppercase  ">
                    <a class=" text-white" href="/user">{!! __c("website.nav.main_menu.my_account", "My Account")!!}</a>
                </li>

                <li class="text-uppercase n  n-list">
                    <a class=" {!! ($user->is_admin == 'yes') ? '' : ' pr-0 mr-0'!!}" href="/logout">{!! __c("website.nav.main_menu.logout", "Logout")!!}</a>
                </li>
                <li>
                    <a href="#">
                        <div id="siteNotificationApp" data-user_id="{!!  (\Auth::user())  ? Auth::user()->id : ''!!}" class="text-uppercase  dropdown dropdown-menu-left show n">
                            <notification-menu :unread="unread" @readall="readAll"></notification-menu>
                            <dropdown-menu :notices.sync="notices"></dropdown-menu>
                            <notifications  position="top left" group="usernotices" />
                        </div>
                    </a>

                </li>
        @else
            <li class="text-uppercase  "><a class="  text-white " href="/login">{!! __c("website.nav.main_menu.login", "Login")!!}</a></li>
            <li class="text-uppercase  "><a class="  text-white " href="{{route('psp.signup')}}">{!! __c("website.nav.main_menu.register", "Register")!!}</a></li>
        @endif
    </ul>
        @if($user && $user->is_admin == 'yes')
            <div class="d-inline-block">
                @php $liveEditOn = __conf('site.amin.live_edit','boolean', '1'); @endphp
                @php $config = \App\Models\Config::where('key', 'site.amin.live_edit')->first(); @endphp
                <form action="{{route('toggle_live_edit')}}" method="post" style="display:inline-block">
                    @csrf
                    <input type="hidden" name="live_edit" value="{{!($config->value)}}">
                    <input type="hidden" name="from" value="{{url()->current()}}">
                    @if($liveEditOn)
                        <button type="submit" class="btn btn-brand-orange btn-xs brand-btn">Live Edit On</button>
                    @else
                        <button type="submit" class="btn btn-brand-charcoal btn-xs brand-btn">Live Edit Off</button>
                    @endif
                </form>
                <form action="{{route('hard_refresh')}}" method="post" style="display: inline-block">
                    @csrf
                    <button type="submit" class="btn btn-brand-charcoal btn-xs brand-btn"><i class="fa fa-reload"></i> Refresh Edits</button>
                </form>
            </div>
            <div class="d-inline-block">
                <a class="btn btn-brand-red btn-xs brand-btn" href="/admin">Admin</a>
            </div>
        @endif
    <div style="clear:right;"></div>
</div>

<!-- <script src="{{asset('js/common-echo.js')}}?v={{rand(1,334434)}}"></script> -->
@if($user && $user->is_admin == 'yes')
    <script>
        window.liveEdit = '{{__conf('site.amin.live_edit','boolean', '1') }}';
    </script>
@else
    <script>
        window.liveEdit = false;
    </script>
@endif
