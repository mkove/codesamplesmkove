<!DOCTYPE html>
<html lang="en">
<head>
    @include('pspfrontend.layout.head')
</head>
<body>
<div class="wrapper">
    <div class="content content-print" role="print" style="background:none;">
        @yield('content')
    </div>
</div>
@include('pspfrontend.layout.footerjs')
@include('pspfrontend.layout.svgs')
</body>
</html>
