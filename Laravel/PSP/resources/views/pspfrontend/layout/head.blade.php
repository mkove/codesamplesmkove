<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ $meta['title'] ?? 'Premier Sports Pools' }}</title>
<meta property="og:title" content="{{ $meta['og_title'] ?? '' }}" />
<meta property="og:image" content="{{asset('assets/images/psp_logo_main.png')}}" />
<meta property="og:description" content="{{ $meta['og_description'] ?? '' }}" /> 
<meta name="description" CONTENT="{{ $meta['description'] ?? '' }}">
<meta name="keywords" CONTENT="{{ $meta['keywords'] ?? '' }}">
<meta name="robots" content="{{ $meta['robots'] ?? '' }}">

<!-- <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800|Quicksand:300,400,700|Titillium+Web:300,400,400i,700,700i,900&display=swap" rel="stylesheet"> -->
<link rel="stylesheet" href=" {{asset('css/fonts.googleapis.com-Nanum-Gothic.css')}}">
<link rel="stylesheet" href=" {{asset('css/font-awesome-5.min.css')}}">
<!-- End Google Tag Manager -->
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}?v={{date('U')}}">
<link rel="stylesheet" href="{{asset('css/main.css')}}?v={{date('U')}}">

@if(App\Helpers\SiteHelper::conf('site.tracking.track','boolean', '1'))
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','{{__conf('site.tracking.google_tag_manager','text', 'GTM-K4KVVFG')}}');</script>
    {!! __gtm_pull() !!}

@endif
@stack('head-js')
@stack('custom-css')
