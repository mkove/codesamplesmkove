@extends('pspfrontend.layout.main')
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-12 text-white">
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">How It Works</h3>
                    </div>
                    <div class="card-body p-3">
                        <p>Premier Sports Pools provides online hosting for your pools.  We offer complete management from setup for commissioners and roster entry and standings for players.  Here’s how it works:</p>
    
                        <h4>Squares Pools</h4>
    
                        <p>Commissioners create a pool and edit the settings to their liking. Next, the commissioner can start inviting players.  Via the My Picks page players can select the squares they want. The commissioner can generate the numbers for their squares grid when ready - payment must be received prior to generating numbers</p>
    
                        <h4>Schwindy Points Pools</h4>
    
                        <p>The goal of the Schwindy Points Pool is to accumulate more points than your opponents over the course of the pro football season.  Points are awarded equal to the number of points a player’s selected teams score in the corresponding period.  For example, if a player selects Atlanta and Arizona in week one of the season they are awarded the number of points that each team scores in week one. Enhance your pool by adding weekly prizes.  The use of each team is permitted only once throughout the season - this makes the strategy of when to use each team very important.</p>
    
                        <p>Commissioners create a pool and edit the settings to their liking.  Next, the commissioner can start inviting players.  Via the My Picks page players can select the teams they wish to use for that week.  Prior to the second week of the pool everything will temporarily lock until the commissioner makes payment for the pool hosting fee.  Use the first day or 2 after week one to remove any rosters that you do not want in your pool.</p>
                    </div>
                </div>
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">PAYMENTS</h3>
                    </div>
                    <div class="card-body p-3">
                        <h4>Squares</h4>
                        <p>The fee for generic or single game football and basketball square boards is $7; the fee for March Tournament Boards is $12(standings provided throughout).  Payment must be made prior to generating numbers.</p>
                        <h4>Schwindy Points Pool</h4>
                        <p>This hosting fee for this game is $3 per player; $15 minimum.</p>
                        <p>Payment information must be put into the system for each pool; it will not be saved at this time.  If your payment information is not provided your pool will be placed in a "locked" state prior to week 2 and no viewing or edits can be made until payment is received.  Use the time prior to week 2 to remove any rosters that you do not wish to have participating as you will be charged for them.</p>
    
                        <p>It is encouraged to prepay for all pools to avoid any delays with payment processing.  You will not be charged until the "lock" state for each pool.  However there will be a $10 initial charge for golf at the start of round 1.  You can simply not enter payment information; your pool will remain in a locked state until a payment is made.</p>
                    </div>
                </div>
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">CANCELLATIONS</h3>
                    </div>
                    <div class="card-body p-3">
                        
                        <p>If you have already submitted a payment but wish to cancel a pool:</p>
                        <h4>Squares</h4>
                        <p>Cancellations and refunds can be accepted until numbers are generated.  Once numbers are generated all payments are final.</p>
                        <h4>Schwindy</h4>
                        <p>You can cancel a payment prior to the 2nd week for a full refund. Once the second week has started (Thursday night game) this cannot be reversed and you will be charged per active player in the pool as of Thursday at 12:01AM Eastern</p>
                        <h4>Golf</h4>
                        <p>The initial $10 payment can be refunded prior to the start of Round 1 play. Due to the format of the golf pools we cannot accept cancellations once play begins and player picking information is public.  Please use the time between Round 1 and round 2 to clean up the pool roster as you will be billed for all active players.</p>
                    </div>
                </div>
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">REFUNDS</h3>
                    </div>
                    <div class="card-body p-3">
                        <p>Refunds will only be considered for instances of site malfunction or failure.  All payments received for pools that have started will not be refunded.  Be sure to take advantage of the "i" buttons on each page for additional help tips and information on how to use each  page.  Please do not hesitate to contact us for help at <a href="maolto:info@PremierSportsPools.com">info@PremierSportsPools.com</a>.</p>

                    </div>
                </div>
               
            </div>
        </div>
    </div>
@endsection
