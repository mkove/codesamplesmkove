@extends('pspfrontend.layout.main')
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-12 text-white">
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">Privacy Statement</h3>
                    </div>
                    <div class="card-body p-3">
{{--                        {!! __c("website.privacy_full") !!}--}}
                        
                        <h2>Premier Hosting Services, LLC - dba Premier Sports Pools</h2>
    
                        <p>It is the policy of Premier Hosting Services, LLC (PremierSportsPools.com) to protect and shield your personal data.  At no time will any of your personal information be shared with any third party entity.  Your data will be shared with your with your pool commissioner in order to contact you for updates, rule changes, or any other reason within a pool in which you are a participant.  You can edit your settings to limit which information commissioners can view.  Your email will always remain visible to the pool commissioner; other information can be hidden at your discretion.  You can edit whether other pool members can view your email and other personal information.  Passwords are encrypted in our database; no staff member at Premier has access to your password.  Credit card information will not be stored by Premier Hosting Services, LLC.</p>
    
                        <h2>What do we share?</h2>
                        <p>Premier Hosting Services, LLC (PremierSportsPools.com) will never share any of you personal data with any third party, including individual selections you make within pools.  We may, however; share macro level pick data with third parties.  For instance, we will never share that user Bob28 selected Detroit in week 3; but we may share that 3,000 of 10,000 users across all pools selected Detroit in week 3.  This data may be broken up into smaller groups based on pool settings.  Some of these pool settings may include, but are not limited to:</p>
    
                        <h4>Survivor, Schwindy:</h4>
                        <ul>
                            <li>Number of selections to be made each week</li>
                            <li>Whether picking the same team is allowed multiple times</li>
                            <li>Number/percentage of users who have selected each team to date/or have not selected</li>
                            <li>Which week/date pool started</li>
                        </ul>
                        
                            <h4>Squares:</h4>
                        <ul>
                            <li>Number of pools ran for a particular contest</li>
                            <li>Macro score results for segments of contests. <br>
                                ***Individual game data is already readily available on various platforms but Premier will not share this.</li>
                        </ul>
                            <h4>Golf:</h4>
                        <ul>
                            <li>Roster % across all pools, never within individual pools</li>
                        </ul>
                            <p>Essentially, we will share sitewide macro data but no data that directly reflects an individual’s picks, nor will we share data from an individual pool.</p>
                            <h2>Your Preferences</h2>
                            <p>Under the “My Account” page you can edit your personal information as well as many of your settings.  This includes which information others will see; commissioners and other pool members.  You can enter all data now, then decide to hide or show it later.  Premier is committed to your privacy and only wishes data displayed that you are comfortable with.</p>
                            <p>You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us at info@premiersportspools.com</p>
                        <ul>
                            <li>See what data we have about you, if any.</li>
                            <li>Change/correct any data we have about you.</li>
                            <li>Have us delete any data we have about you.</li>
                            <li>Express any concern you have about our use of your data.</li>
                        </ul>
                            <h2>Security</h2>
                        <p>We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p>
                            <p>Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page. Credit card information will not be stored by Premier Hosting Services, LLC.</p>
                            <p>While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>
                        <p>If you feel that we are not abiding by this privacy policy, you should contact us immediately via
                                Email at info@premiersportspools.com</p>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
