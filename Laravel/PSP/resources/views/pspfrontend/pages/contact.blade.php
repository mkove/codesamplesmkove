@extends('pspfrontend.layout.main')
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-12 text-white">
                <div class="card bg-transparent border-white mt-5">
                    <div class="card-header brand-gradient border-bottom border-white">
                        <h3 class="text-white text-uppercase mb-0">Contact Us</h3>
                    </div>
                    <div class="card-body p-3">
               
                        <p class="address">
        
                            <strong>Premier Hosting Services, LLC</strong><br>
                            PO Box 462 <br>
                            Miamitown, OH 45041 <br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
