@extends('pspfrontend.layout.main')
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-12 text-white">
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">Terms of use</h3>
                    </div>
                    <div class="card-body p-3">
                        {!! __c("website.terms_full") !!}
                        
                        <p>Premier Hosting Services, LLC(PremierSportsPools)</p>
    
                        <p>We require that any use of Premier software be for lawful purposes, and for entertainment only.  If you plan to financially benefit from the use of Premier’s software you may be in violation of several laws.  You, as the user, are responsible for verifying use is compliant with these laws.
                        </p>
                        <p class="text-brand-blue">**Exceptions may be made for non-profit organizations with proper fundraising licenses; please contact info@PremierSportsPools.com to be granted this exemption.</p>
                        <p>When signing up we only mandate you provide a valid email and birthdate. Any falsification of birth date will be sufficient grounds for account termination and barring from the site.  Legal action may also be pursued against anyone who falsifies birth date information.  We do not involve ourselves in the pools themselves; our software is for rent and participation is at the discretion of pool commissioners.  However, it is the policy of  Premier Hosting Services, LLC(PremierSportsPools) to mandate all United States users be at least 18 years of age. (Age requirement may vary in other jurisdictions)</p>
                        <p>Use of the site requires that it is used in the manner intended by Premier Hosting Services, LLC(PremierSportsPools).  Any use for phishing, scams, hacking, or any other purpose not intended by the site administration is strictly prohibited and violators will be banned and potentially prosecuted.</p>
                        <p>Do you agree to use  Premier Hosting Services, LLC(PremierSportsPools) within the guidelines listed in the terms of use?</p>
    
                        <p>By continuing using "PremierSportsPools" you agree to the terms listed  above.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
