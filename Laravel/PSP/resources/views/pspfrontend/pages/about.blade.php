@extends('pspfrontend.layout.main')
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-12 text-white">
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">About Us</h3>
                    </div>
                    <div class="card-body p-3">
                        <h4>Mission</h4>
                        <p>
                        To enhance sports pools by offering seamless software and integrity of games to commissioners, and enhancing the "sweat" for pool participants.
                        </p>
                        <h4>Vision</h4>
                        <p>
                        Simplify pool games and make them available to a larger demographic.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
