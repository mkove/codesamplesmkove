@extends('pspfrontend.layout.main')
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-12 text-white">
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">{{$staticPage->title}}</h3>
                    </div>
                    <div class="card-body p-3">
                       {!!  $staticPage->content!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
