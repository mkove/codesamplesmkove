@if(!Auth::user())
<form action="/register" id="signupForm" method="post">
    {!! csrf_field() !!}
    <input type="hidden" name="pool_hash" value="{{session('joinPoolHash')}}">
    <div class="row">
        @if (count($errors) > 0)
            <div class="col">
                <div class="error">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group" style="margin-bottom: 5px;">
                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">{!! __c("website.signup.content.username_label", "Create Username") !!}<span class="required">*</span></label>
                <div class="col">
                    <input type="text" required name="username" id="username" class="form-control validateUsername"  value="{{old('username')}}">
                    <span class="validateUsernameOutput"></span>
                    <div class="help-block">{!! __c("website.signup.content.username_description", "Description For Create Username") !!}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group" style="margin-bottom: 5px;">
                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">{!! __c("website.signup.content.email_label", "Email") !!}<span class="required">*</span></label>
                <div class="col">
                    <input type="email"  required value="{{old('email')}}" name="email" class="form-control">
                    <div class="help-block">{!! __c("website.signup.content.email_description", "Description For Email") !!}</div>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="row">--}}
{{--        <div class="col">--}}
{{--            <div class="form-group" style="margin-bottom: 5px;">--}}
{{--                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">{!! __c("website.signup.content.phone_label", "Phone") !!}</label>--}}
{{--                <div class="col">--}}
{{--                    <input type="tel"  required value="{{old('phone')}}" name="phone" class="form-control">--}}
{{--                    <div class="help-block">{!! __c("website.signup.content.phone_description", "Description For Phone") !!}</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-6">--}}
{{--            <div class="form-group" style="margin-bottom: 5px;">--}}
{{--                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">{!! __c("website.signup.content.first_name_label", "First Name") !!}</label>--}}
{{--                <div class="col">--}}
{{--                    <input type="text"  required value="{{old('first_name')}}" name="first_name" class="form-control">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-6">--}}
{{--            <div class="form-group" style="margin-bottom: 5px;">--}}
{{--                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">{!! __c("website.signup.content.last_name_label", "Last Name") !!}</label>--}}
{{--                <div class="col">--}}
{{--                    <input type="text"  required value="{{old('last_name')}}" name="last_name" class="form-control">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-12">--}}
{{--            <div class="form-group" style="margin-bottom: 5px;">--}}
{{--                <div class="col">--}}
{{--                    <div class="help-block">{!! __c("website.signup.content.first_name_description", "Description For First Name") !!}</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="row">
        <div class="col">
            <div class="form-group" style="margin-bottom: 5px;">
                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">{!! __c("website.signup.content.password_label", "Password") !!}<span class="required">*</span></label>
                <div class="col">
                    <input type="password" required name="password" class="form-control">
                    <div class="help-block">{!! __c("website.signup.content.password_description", "Description For Password") !!}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group" style="margin-bottom: 5px;">
                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">{!! __c("website.signup.content.password_confirm_label", "Confirm Password") !!}<span class="required">*</span></label>
                <div class="col">
                    <input type="password" required name="password_confirmation" class="form-control">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="form-group" style="margin-bottom: 1px;margin-right: 4px;">
                <div class="checkbox">
                    <label class="pl-3">
                        <input type="checkbox" id="agree_age" name="agree_age"> {!! __c("website.signup.content.agree_age_consent_checkbox_label", "I consent of being 18 years of age or older.") !!}</a>
                    </label>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="form-group" style="margin-bottom: 5px;margin-right: 4px;">
                <div class="checkbox">
                    <label class="pl-3">
                        <input type="checkbox" id="agree" name="agree"> {!! __c("website.signup.content.agree_checkbox_label", "I agree to ") !!} <a target="_blank" href="/page/terms-conditions">Terms &amp; Conditions</a>
                    </label>
                </div>
            </div>
        </div>
    </div>
    @php
        $configLabel = "website.signup.content.button_landing_".str_replace('psp.', '', \Route::currentRouteName());
    @endphp
    @if(App\Helpers\SiteHelper::conf('site.captcha.enabled','boolean', 0))
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="" class="col">Captcha</label>
                <div class="col">
                    <div class="captcha">
                        <span>{!! captcha_img() !!}</span>
                        <button type="button" class="btn btn-danger" class="reload" id="reload">
                            &#x21bb;
                        </button>
                    </div>
                    <input id="captcha" type="text" class="form-control mt-4" placeholder="Enter Captcha" name="captcha">
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col">
            <div class="form-group" style="margin-bottom: 5px;">
                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col">&nbsp;</label>
                <div class="col">
                    <button class="btn btn-block brand-btn btn-brand-blue  " data-la="{{$configLabel}}"> {!! __c($configLabel, "Sign Up") !!} </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col text-center">
            <div class="form-group" style="margin-bottom: 5px;">
                <label style="font-size:0.85em;margin-bottom: 2px;" for="" class="col"> {!! __c("website.signup.content.alread_member_text", "Already a member?") !!} <a href="/login" class="">{!! __c("website.signup.content.alread_member_login_link", "Login") !!}</a></label>
            </div>
        </div>
    </div>
</form>
<style>
    .required {
        color:red;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $('#reload').click(function() {
        $.ajax({
            type: 'GET',
            url: 'reload-captcha',
            success: function(data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
@else
    <div class="row">
        <div class="col">
            <div class="form-group" style="margin-bottom: 5px;">
                <div class="col text-center">
                    <h3 class="text-white">Welcome!</h3>
                    <a class="btn btn-block brand-btn btn-brand-blue  " href="{{$goto ?? '/pool/create'}}">{{$label ?? 'Start Pool'}}</a>
                </div>
            </div>
        </div>
    </div>
@endif
