@extends('pspfrontend.layout.main')
@section('content')
    <div class="brand-gradient pt-1"></div>
    <div id="home-content" class="site-wrapper">
        <div id="wrap_content" class="page home-page-content">
            <div class="hero-sec">
                <div class="sec-content" style="background: url('assets/images/banner_top.webp');">
                    <div class="container">
                        <div class="hero-sec-">
                            <div class="text-block">
                                <div class="heading">
                                    <h1>{!! __c("home.primary.hero.title", 'Online Management For your Pools') !!}</h1>
                                </div>
                                <div class="desc">
                                    <p class="home-p">{!! __c("home.primary.hero.sub_title_text") !!}</p>
{{--                                    <a href="/page/how-it-works" class="btn brand-btn btn-brand-blue btn-lg">How It Works <i class="fa fa-chevron-right"></i></a>--}}
                                    <a href="{{route('psp.signup')}}" class="btn btn-xl brand-btn btn-brand-blue">{!! __c("home.primary.hero.cta_button", "Start Your First Pool") !!} <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="brand-gradient pt-1"></div>
            <div class="features-sec">
                <div class="sec-content">
                    <div class="container">
                        <div class="bordered-style-heading text-center text-center">
                            <h2>{!! __c("home.primary.regular.index_feature-list_title") !!}</h2>
                        </div>
                        <div class="features-list">
                            <div class="feature-item col-sm-12 text-sm-center text-md-left col-md-4">
                                <div class="feature-item-content">
                                    <h3>{!! __c("home.primary.regular.index_feature-list_title-1") !!}</h3>
                                    <p class="home-p">{!! __c("home.primary.regular.index_feature-list_text-1") !!}</p>
                                </div>
                            </div>
                            <div class="feature-item col-sm-12 text-sm-center text-md-left col-md-4">
                                <div class="feature-item-content">
                                    <h3>{!! __c("home.primary.regular.index_feature-list_title-2") !!}</h3>
                                    <p class="home-p">{!! __c("home.primary.regular.index_feature-list_text-2") !!}</p>
                                </div>
                            </div>
                            <div class="feature-item col-sm-12 text-sm-center text-md-left col-md-4">
                                <div class="feature-item-content">
                                    <h3>{!! __c("home.primary.regular.index_feature-list_title-3") !!}</h3>
                                    <p class="home-p">{!! __c("home.primary.regular.index_feature-list_text-3") !!}</p>
                                </div>
                            </div>

                            <div class="feature-item col-sm-12 text-sm-center text-md-left col-md-4">
                                <div class="feature-item-content">
                                    <h3>{!! __c("home.primary.regular.index_feature-list_title-4") !!}</h3>
                                    <p class="home-p">{!! __c("home.primary.regular.index_feature-list_text-4") !!}</p>
                                </div>
                            </div>
                            <div class="feature-item col-sm-12 text-sm-center text-md-left col-md-4">
                                <div class="feature-item-content">
                                    <h3>{!! __c("home.primary.regular.index_feature-list_title-5") !!}</h3>
                                    <p class="home-p">{!! __c("home.primary.regular.index_feature-list_text-5") !!}</p>
                                </div>
                            </div>
                            <div class="feature-item col-sm-12 text-sm-center text-md-left col-md-4">
                                <div class="feature-item-content">
                                    <h3>{!! __c("home.primary.regular.index_feature-list_title-6") !!}</h3>
                                    <p class="home-p">{!! __c("home.primary.regular.index_feature-list_text-6") !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="feat-aside-live-scoring-sec">
                <div class="sec-content">
                    <div class="container">
                        <div class="bordered-style-heading text-center">
                            <h2>{!! __c("home.primary.regular.index_exclusive-title") !!}</h2>
                        </div>
                        <div class="jumbotron-title">
                            <h2>{!! __c("home.primary.regular.index_premier-title") !!}</h2>
                            <h3>{!! __c("home.primary.regular.index_premier_subtitle") !!}</h3>
                        </div>
                        <div class="feat-aside-desc-block">
                            <div class="row">
                                <div class="feat-block col-sm-12 text-sm-center text-md-left col-md-6 col-xs-12">
                                    <div class="figure">
                                        <img src="assets/images/laptop-feat-img.png" style="width: 100%; height:100%;" alt='{!! __c("home.primary.regular.index_premier-title") !!}'>
                                    </div>
                                </div>
                                <div class="desc-block col-sm-12 text-sm-center text-md-left col-md-6 col-xs-12">
                                    <div class="desc">
                                        <p class="home-p">{!! __c("home.primary.regular.index_premietextr1") !!}</p>
                                        <div class="small-text">
                                            <small>{!! __c("home.primary.regular.index_premietextr2") !!}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="pools-sec">
				<div class="sec-content">
					<div class="container">
						<div class="bordered-style-heading text-center">
							<h2>Featured Pools</h2>
						</div>
						<div class="pools-list">
							<div class="pool-item" style="background: url('assets/images/pool-feat-img1.png')">
								<a href="">
									<div class="item-content">
										<h2>SCHWINDY <sup>TM</sup></h2>
										<span>POINTS POOLS</span>
									</div>
								</a>
							</div>
							<div class="pool-item" style="background: url('assets/images/pool-feat-img2.png')">
								<a href="">
									<div class="item-content">
										<h2>Fantasy </h2>
										<span>Golf More</span>
									</div>
								</a>
							</div>
							<div class="pool-item" style="background: url('assets/images/pool-feat-img3.png')">
								<a href="">
									<div class="item-content">
										<h2>SQUARES</h2>
										<span>FOOTBALL & MORE</span>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>-->



        </div>
    </div>
@endsection
@push('custom-css')
    <style>
        .feature-item-content {
            height: 100%;

        }
    </style>
@endpush
