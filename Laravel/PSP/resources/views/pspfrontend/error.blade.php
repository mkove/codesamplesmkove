@extends('pspfrontend.layout.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card bg-transparent border-white mt-4">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">ERROR: Not Found</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                The Page youre looking for is not found!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
@endsection
