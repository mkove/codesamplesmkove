@extends('pspfrontend.layout.main')
@section('content')
	<div class="brand-gradient pt-1"></div>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 text-white">
                <div class="card bg-transparent mt-4">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">LOGIN</h3>
                    </div>
                    <div class="card-body p-3">
                        @include('pspfrontend.layout.errors')
                        <form action="/login" class="form-horizontal" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="pool_hash" value="{{session('joinPoolHash')}}">
                            <div class="row">
                                @if (count($errors) > 0)
                                    <div class="col">
                                        <div class="error">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="" class="col">Username / Email</label>
                                        <div class="col">
                                            <input type="text" name="login" value="{{old('login')}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="" class="col">Password</label>
                                        <div class="col">
                                            <input type="password" name="password" class="form-control">
                                            <div class="text-right"><a href="/password/reset" >Forgot password?</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="" class="col">&nbsp;</label>
                                        <div class="col">
                                            <button class="btn btn-block brand-btn btn-brand-blue"> Login </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
            
                            <div class="row">
                                <div class="col text-center">
                                    <div class="form-group">
                        
                                        <label for="" class="col">Don't have an account? <a href="/signup">Sign-up</a></label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection
