@extends('pspfrontend.layout.main')
@section('content')
	<div class="brand-gradient pt-1"></div>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 text-white">
                <div class="card bg-transparent mt-4">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">SIGN UP</h3>
                    </div>
                    <div class="card-body p-3 ">
                        @include('pspfrontend.partial.signup-form')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('custom-js')
        <script>

        
        </script>
    @endpush
    
@endsection
