@extends('pspfrontend.layout.main')
    @section('content')
    @php
        $poolTypePrefix = __pool_type_help($pool->pool_type, 'pool'); 
        $amComish = $pool->is_commissioner($user->id);
        $new_created = 1;
    @endphp
    @if(isset($squareboard))
        @include('pspfrontend.pool.squareboard.partials.nav',['active' => 'commissioner'])
    @endif
    @if(isset($schwindy))
        @include('pspfrontend.pool.schwindy.partials.nav',['active' => 'commissioner'])
    @endif
    @if(isset($golf))
        @include('pspfrontend.pool.golf.partial.subnav', ['golf' => $golf, 'golfPool' => $golf, 'golf_pool_rosters' => $golf->rosters])
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if($pool->is_commissioner($user->id))
                <div class="row">
                    <div class="col-12">
                        <div class="bg-brand-charcoal p-3">
                            {!! __c("pool.invites.content.invite_title", "Rejected users will be located in pool roster.") !!}
                            @if(isset(($schwindy->id)))
                                <a href="{{route('psp.schwindy.roster',['pool' => $pool->id,'schwindy' => $schwindy->id])}}">View Pool Roster</a>
                            @else
                                <a href="{{route('psp.pool.roster',['pool' => $pool->id])}}">View Pool Roster</a>
                            @endif
                        </div>
                    </div>
                </div>
                @include('pspfrontend.pool.partials.invite.unapproved-users')
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pl-3 pr-3 pb-3 pt-3"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card bg-transparent mt-1">
                            <div class="card-header brand-gradient bb-brand-grey">
                                <h3 class="text-white text-uppercase mb-0">
                                    {!! __c($poolTypePrefix.".invites.content.invite_title", "Invite Users") !!}
                                </h3>
                                <div class="card-header-controls">
                                    <a class="btn grey-gradient brand-btn btn-sm" href="#" data-toggle="modal" data-target="#infoPopup">Help?</a>
                                </div>
                            </div>
                            <div class="card-body pt-3 pl-3 pr-3 pb-0">
                                <div class="row">
                                    @if( $vidUrl = __c($poolTypePrefix.".invites.invite_video_instructions", 'psskmLkDwgo', [], false))
                                    <div class="col-md-5 col mx-auto">
                                        <iframe width="350" height="" src="https://www.youtube.com/embed/{{$vidUrl}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    @endif
                                    <div class="col-md-7 text-brand-orange">
                                        {!! __c($poolTypePrefix.".invites.invite_general_instructions", "Invite Instructions") !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="accordion" class="accordion">
                    @include('pspfrontend.pool.partials.invite.email-forward')
                    @include('pspfrontend.pool.partials.invite.credentials')
                    @include('pspfrontend.pool.partials.invite.read-only')
                    @include('pspfrontend.pool.partials.invite.friends')
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('pspfrontend.pool.partials.invite.invited-users')
            </div>
        </div>
    </div>
    @push('custom-js')
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    
    <script>
        //hidden
        $('.icon-accordion').on('hidden.bs.collapse', function(ev) {
            var i = $(ev.currentTarget).find('.toggle-icon');
            if ($(i).hasClass('fa-chevron-down')) {
                $(i).removeClass('fa-chevron-down');
                $(i).addClass('fa-chevron-right');
            }
        });

        //visible
        $('.icon-accordion').on('shown.bs.collapse', function(ev) {
            var i = $(ev.currentTarget).find('.toggle-icon');
            if ($(i).hasClass('fa-chevron-right')) {
                $(i).removeClass('fa-chevron-right');
                $(i).addClass('fa-chevron-down');
            }
        })

        $('#inviteByEmailPopup').on('shown.bs.modal', function() {
            $('#addNewEmailInvite').trigger('focus');
            $('#inviteEmail').trigger('focus');
            $('#inviteName').trigger('focus');
        })

        $(document).on('click', '.selectUser', function() {
            var id = $(this).data('id');
            var checked = $(this).prop('checked');
            if (checked) {
                $('#friend-' + id).addClass('bg-brand-darkblue');
            } else {
                $('#friend-' + id).removeClass('bg-brand-darkblue');
            }
        })

        $(document).on('click', '#sendMeLinkButton', function() {
            var url = $(this).data('url');
            $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    data: { 
                    },
                })
                .done(function(jsonData) {
                    if (jsonData.error) {
                        alert("Error");
                    } else {
                        alert('Email was sent');
                    }
                })
                .fail(function() {

                })
                .always(function() {
                    $('#sendMeForwardMessage').val('')
                });
        })

        $(document).on('click', '#sendMeForwardButton', function() {
            var url = $(this).data('url');
            $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    data: {
                        commissioner_message: $('#sendMeForwardMessage').val(),
                    },
                })
                .done(function(jsonData) {
                    if (jsonData.error) {
                        alert("Error");
                    } else {
                        alert('Email was sent');
                    }
                })
                .fail(function() {

                })
                .always(function() {
                    $('#sendMeForwardMessage').val('')
                });
        })

        $(document).ready(function() {

            $('#addFriendPopup').on('keyup', '#friendEmail', function(e) {
                if (e.keyCode === 13) { //Enter keycode
                    var name = $('#friendName').val();
                    var email = $('#friendEmail').val();
                    addFriend(name, email);
                    $('#friendName').val('');
                    $('#friendEmail').val('');
                }
            });

            var columnCount = $('.data-table-sortable thead th').length;

            $(document).on('keyup', '#inviteEmail', function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                    var name = $('#inviteName').val();
                    var email = $('#inviteEmail').val();
                    addItem(name, email);
                    $('#inviteName').val('');
                    $('#inviteEmail').val('');
                }
            });

            $(document).on('click', '#addNewFriendButton', function() {
                var name = $('#friendName').val();
                var email = $('#friendEmail').val();
                addFriend(name, email);
                $('#friendName').val('');
                $('#friendEmail').val('');
            }); 

            $('#inviteList').on('click', '.remove-invite', function() {
                var email = $(this).data('email');
                $('*[data-femail="' + email + '"]').removeClass('bg-brand-darkblue');
                $(this).closest('li').remove(); 
            })

            function reSize() {
                $('#friendsBody').height($('#inviteBody').height());
            }

            function addFriend(name, email) {
                email = email.toLowerCase();
                if (email) {
                    if (name === "") {
                        name = email.substring(0, email.indexOf("@"));
                    }
                    var found = $("#friendsList tr").filter(function() {
                        return ($(this).attr('data-femail') === email);
                    });

                    var id = ~~(Date.now() / 1000);
                    if (found.length === 0) {
                        var html = "<tr id='friend-" + id + "' data-femail='' class='bg-brand-darkblue'><td><label for='friend-" + id + "'><input type='checkbox' checked='checked' class='selectUser ' data-id='' name='email[]' value='" + email + "|" + name + "'><strong class='text-white ml-3'>" + name + "</strong> - <small class='text-brand-gray'>" + email + "</small></label></td></tr>";
                        if ($('#friendsList tr:first').length) {
                            $('#friendsList tr:first').before(html);
                        } else {
                            $('#friendsList').append(html);
                        }
                    } else {
                        alert("already in list");
                    }
                    reSize();
                }
                return false;
            }
        });
    </script>
    @endpush
@endsection