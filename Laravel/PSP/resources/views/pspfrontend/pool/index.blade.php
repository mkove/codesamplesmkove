@php
    $userId = (auth()->user()) ? auth()->user()->id : null;
@endphp
@extends('pspfrontend.layout.main')
@section('content')
    @include('pspfrontend.layout.errors')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">Join By Pool ID</h3>
                        
                        <div id="userNav" class="collapsable-nav collapsable-nav-left">
                            <a href="javascript:void(0)" class="closebtn toggle-nav" data-menu="userNav">&times;</a>
                            <ul style="margin-bottom: 0;">
                                <li class='no-separator'><a class='' href='#' data-toggle="modal" data-target="#infoPopup" >{!! __c("website.content.help.button_label", "Help?") !!}</a></li>
                                <li class='no-separator'><a href="/pool/create" class="btn brand-btn btn-brand-blue pull-right"><i class="fa fa-plus"></i> Start Pool</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{route('psp.pool.join')}}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <legend>Join Pool</legend>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="form-label" class="col-md-12 required">Pool ID</label>
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control " value="" name="puid1" placeholder="ABC" size="3" max="3" maxlength="3" minlength="3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="validationTooltipUsernamePrepend">-</span>
                                                                </div>
                                                                <input type="number" class="form-control " name="puid2" placeholder="1234" size="4">


                                                            </div>

                                                            <div class="help-block">Provided By Commissioner</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="" class="col-md-12 required ">Pool Password</label>
                                                        <div class="col-md-12">
                                                            <input type="text" value="" class="form-control required" required="" name="password">
                                                            <div class="help-block">Provided by commissioner</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-md-3">--}}
                                                {{-- <div class="form-group">--}}
                                                {{-- <label for="" class="col-md-12 required ">Display Name</label>--}}
                                                {{-- <div class="col-md-12">--}}
                                                {{-- <input type="text" value="" class="form-control required" required="" name="display_name">--}}
                                                {{-- <div class="help-block">Your Default Name For This Pool</div>--}}
                                                {{-- </div>--}}
                                                {{-- </div>--}}
                                                {{-- </div>--}}
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="" class="col-md-12 required ">&nbsp;</label>
                                                        <div class="col-md-12">
                                                            <button type="submit" class="btn-block btn brand-btn btn-brand-blue">Join <i class="fa fa-chevron-right"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($pools as $template =>$poolType)
                            @php $poolCollection = $poolType['pools']; @endphp
                            <div class="col-md-6">
                                <div class="card bg-transparent mt-1">
                                    <div class="card-header brand-gradient bb-brand-grey">
                                        <h3 class="text-white text-uppercase mb-0">{{$poolType['prettyName']}}</h3>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-sm">
                                            <thead>
                                                <tr class="text-brand-blue">
                                                    <th class="border-top-0">Pool Name</th>
                                                    <th class="border-top-0" style="width:75px;"><i class="fa fa-users"></i></th>
                                                    <th class="border-top-0" style="width:75px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($poolCollection as $pool)
                                                @include('pspfrontend.pool.partials.index-pool-row', ['pool' => $pool, 'invite' => false])
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        {{$poolCollection->appends(
                                                            [
                                                                'squareboard_page' => $pools['squareboard']['pools']->currentPage(),
                                                                'schwindy_page' => $pools['schwindy']['pools']->currentPage()
                                                            ])->links()}}
                                                    </td>
                                                </tr>

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection