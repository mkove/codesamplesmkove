@extends('pspfrontend.layout.main')
@section('content')
	<?php
			$poolData  = [];
		$poolData['poolGameCount'] = 4;
		$poolData['boardSize'] = 10;
		$poolData['realCols'] = $poolData['boardSize']+$poolData['$poolGameCount'];
		$poolData['realRows'] = $poolData['$boardSize']+$poolData['$poolGameCount'];
	?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="panel-controls pull-right">
			<h2 class="panel-title"><span class="label label-info"><i class="fa fa-star"></i> Username</span> Pool ID/Name</h2>
			<button data-toggle="modal" data-target="#invite-users" class="btn btn primary btn-xs"><i class="fa fa-plus"></i><i class="fa fa-users"></i> <span class=" hidden-sm hidden-xs">Invite Users</span></button>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-info">

					<div class="panel-heading">
						<div class="panel-controls pull-right">
						</div>
						<h2 class="panel-title">{!! __c(\Route::currentRouteName()."_commissioner_settings_title") !!}</h2>
					</div>
					<div class="panel-body">
						<button data-toggle="modal" data-target="#pool-settings"  class="btn btn primary btn-xs"><i class="fa fa-gear"></i> <span class=" hidden-sm hidden-xs">{!! __c(\Route::currentRouteName()."_settings_text") !!}</span></button>
						<button class="btn btn primary btn-xs"><i class="fa fa-random"></i> <span class=" hidden-sm hidden-xs">{!! __c(\Route::currentRouteName()."_randomize_users_text") !!}</span></button>
						<button class="btn btn primary btn-xs" id="generate-random-numbers"><i class="fa fa-sort-alpha-desc"></i> <span class=" hidden-sm hidden-xs">{!! __c(\Route::currentRouteName()."_generate_numbers_text") !!}</span></button>
						<div class="" style="width:150px; display: inline-block;">
							<input type="number" min="0" step="1" id="teama" class="form-control score" >
						</div>:
						<div style="width:150px; display: inline-block">
							<input type="number" min="0" step="1" id="teamb" class="form-control score" >
						</div>
						<div style="width:150px; display: inline-block">
							<select name="" id="game" class="form-control">
								<?php for($s=1;$s<=$poolGameCount;$s++) { ?>
									<option value="<?=$s;?>"><?=$s;?></option>
								<?php } ?>
							</select>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('pool.'$poolType.'.view', $poolTypeData])

			<?php include('elements/table.php');?>
			</div>
		</div>
	</div>

</div>
@stop
