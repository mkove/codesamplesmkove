@extends('pspfrontend.layout.main')
@section('content')
    @php $poolType = $pool->pool_type->template; @endphp
    @switch($poolType)
        @case('squareboard')
            @include('pspfrontend.pool.squareboard.partials.nav',['active' => 'commissioner'])
        @break
        @case('golf')
            @include('pspfrontend.pool.golf.partial.subnav')
        @break
        @case('schwindy')
            @include('pspfrontend.pool.schwindy.partials.nav',['active' => 'commissioner'])
        @break
    @endswitch
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-transparent mt-1">
                    <div class="card-header brand-gradient bb-brand-grey">
                        <h3 class="text-white text-uppercase mb-0">{!! __c('pool.invoice.content.thank_you_title',"Thank you for your payment") !!}</h3>
                    </div>
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                         <h4>{!! __c('pool.invoice.content.thank_you_subtitle',"Your payment was accepted.") !!}</h4>
                                        @switch($poolType)
                                            @case('squareboard')
                                            <p>
                                                {!! __c('pool.invoice.content.squareboard_thank_you_description',"You can now generate numbers or view grid") !!}
                                            </p>
                                                <a href="{{route('psp.squareboard.generate-numbers',['pool' => $pool->id, 'squareboard' => $pool->squareBoard->id])}}"
                                                   class="btn btn-brand-blue brand-btn"
                                                >
                                                    <i class="fab fa-first-order"></i> {!! __c('pool.invoice.content.squareboard_thank_you_generate_numbers_btn',"Generate Numbers") !!}
                                                </a>
                                                <a href="{{route('psp.squareboard.pick',['pool' => $pool->id, 'squareboard' => $pool->squareBoard->id])}}"
                                                   class="btn btn-brand-charcoal brand-btn"
                                                >
                                                    <i class="fa fa-table"></i> {!! __c('pool.invoice.content.square_thank_you_view_picks_btn',"View Picks") !!}
                                                </a>
                                            @break
                                            @case('golf')
                                            <p>
                                                {!! __c('pool.invoice.content.golf_thank_you_description',"You can now view picks") !!}
                                            </p>
                                            <a href="{{route('psp.golf.picks',['golf' => $pool->golf->id])}}"
                                               class="btn btn-brand-blue brand-btn"
                                            >
                                                {!! __c('pool.invoice.content.square_thank_you_view_picks_btn',"View Picks") !!}
                                            </a>
                                            @break
                                            @case('schwindy')
                                            <p>
                                                {!! __c('pool.invoice.content.schwindy_thank_you_description',"You can view picks now") !!}
                                            </p>
                                                <a href="{{route('psp.schwindy-pick.show',['pool' => $pool->id, 'schwindy' => $pool->schwindy->id])}}"
                                                   class="btn btn-brand-blue brand-btn"
                                                >
                                                     {!! __c('pool.invoice.content.schwindy_thank_you_view_picks_btn',"View") !!}
                                                </a>
                                            @break
                                        @endswitch

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('custom-js')
        <script>
            var container = document.getElementsByClassName("cc-number-group")[0];
            container.onkeyup = function(e) {
                var target = e.srcElement || e.target;
                var maxLength = parseInt(target.attributes["maxlength"].value, 10);
                var myLength = target.value.length;
                if (myLength >= maxLength) {
                    var next = target;
                    while (next = next.nextElementSibling) {
                        if (next == null)
                            break;
                        if (next.tagName.toLowerCase() === "input") {
                            next.focus();
                            break;
                        }
                    }
                }
                // Move to previous field if empty (user pressed backspace)
                else if (myLength === 0) {
                    var previous = target;
                    while (previous = previous.previousElementSibling) {
                        if (previous == null)
                            break;
                        if (previous.tagName.toLowerCase() === "input") {
                            previous.focus();
                            break;
                        }
                    }
                }
            }
        </script>
    @endpush
@endsection
