@extends('layout.main')
@section('content')
    <div class="content-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Settings</h3>
                            </div>
                            <div class="panel-body">
                                <form action="/user/update" method="post" class="form-horizontal">
                                    {{csrf_field()}}
                                    <legend>Public Information</legend>
                                    <div class="form-group">
                                        <label for="" class="col-md-4">Short Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{$user->name}}" name="name">
                                            <div class="help-block">Public Information</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-4">Global Display Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{$user->global_display_name}}" name="global_display_name">
                                        </div>
                                    </div>
                                    <legend>Private Information</legend>
                                    <div class="form-group">
                                        <label for="" class="col-md-4">Full Name</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" value="{{$user->first}}" name="first">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" value="{{$user->last}}" name="last">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-4">Phone</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{$user->phone}}" name="phone">
                                        </div>
                                    </div>
                                    <legend>Password</legend>
                                    <hr>
                                    <div class="form-group">
                                        <label for="" class="col-md-4">Update Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" name="current_password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-4">New Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-4">Repeat Password</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="" name="password_confirmation">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 text-right">
                                            <button class="btn btn-primary">Update Your Account</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop