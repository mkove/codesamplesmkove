@php
    $userId = (auth()->user()) ? auth()->user()->id : null;
@endphp
@extends('layout.main')
@section('content')
    <h2 class="page-title"><strong>My Active Pools</strong><br> <small> Dashboard </small></h2>
    <section class="pool-nav">
        @include('user.partials.nav',['active' => 'pools'])
    </section>
    <hr>
    @if($invitedPools->count())
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Invited To Pools</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th class="col-md-1"><i class="fa fa-users"></i></th>
                                <th>Type</th>
                                <th class="col-md-6">Pool Name</th>
                                <th class="col-md-3">Date</th>
                                <th class="col-md-2">Join</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invitedPools as $pool)
                                @if($pool->status == 'active')
                                    @include('pool.partials.index-pool-row', ['pool' => $pool, 'invite' => true])
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/pool/create" class="btn btn-xs pull-right btn-primary"> CREATE POOL</a>
                    <h3 class="panel-title">My Commissioned Pools</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped  table-condensed" id="myCommissionedPoolsTable">
                                <thead>
                                    <tr>
                                        <th class="col-md-1 text-center">Status</th>
                                        <th class="text-center col-md-1">Time</th>
                                        <th class="col-md-1 text-center" >Type</th>
                                        <th class=" text-left">Pool</th>
                                        <th class="col-md-1 text-center" >Users</th>
                                        <th class="col-md-1 text-left" >Password</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pools as $pool)
                                            @if($userId == $pool->commissioner_id)
                                                @php
                                                    if($squareBoard = $pool->squareboard){
														$game = $squareBoard->game;
													} else {
													$game = null;
												}
                                                @endphp
                                                <tr class="text-center">
                                                    <td>
                                                        @if($squareBoard)
                                                            @if($squareBoard->numbers()->count() == 0)
                                                                <span class="label label-default">OPEN</span>
                                                            @else
                                                                <span class="label label-info">CLOSED</span>
                                                            @endif
                                                        @else
                                                            <span class="label label-success">ACTIVE</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {!! ($game) ? date('D, M-j', strtotime($game->start))."<br><em>".date('H:i A', strtotime($game->start))."</em>" : ""  !!}
                                                    </td>
                                                    <td>
                                                        {{($pool->pool_type) ? $pool->pool_type->name : "-"}}
                                                    </td>
                                                    <td class="text-left">
                                                        <strong><a href="/pool/{{$pool->id}}/">{{$pool->pool_name}}</a></strong><br>{!!  ($game) ? " <em>".$game->tournament_name."</em>" : '' !!}
                                                    </td>
                                                    <td>{{$pool->users->count()}}</td>
                                                    <td class="text-left">
                                                        <strong>{{$pool->pool_password}}</strong>
                                                    </td>
                                                </tr>
                                            @endif
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" class="text-right">
                                            <a class="btn btn-xs btn-default" href="#">Looking for Archives?</a>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">My Pools</h3>
                </div>
                <div class="panel-body">
                        <table class="table table-striped  table-condensed" id="myPoolsTable">
                            <thead>
                            <tr>
                                <th class="col-md-1 text-center">Status</th>
                                <th class="text-center col-md-1">Time</th>
                                <th class="col-md-1 text-center" >Type</th>
                                <th class=" text-left">Pool</th>
                                <th class="col-md-1 text-center" >Users</th>
                                <th class="col-md-1 text-center" >Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($pools as $pool)
                                    @if($userId != $pool->commissioner_id)
                                        @php
                                            if($squareBoard = $pool->squareboard){
                                                $game = $squareBoard->game;
                                            } elseif($schwindy = $pool->schwindy) {
                                                $game = null;
                                            } else {
                                                $game = null;
                                        }
                                        @endphp
                                        <tr class="text-center">
                                            <td>
                                                @if($squareBoard)
                                                    @if($squareBoard->numbers()->count() == 0)
                                                        <span class="label label-default">OPEN</span>
                                                    @else
                                                        <span class="label label-info">CLOSED</span>
                                                    @endif
                                                @else
                                                    <span class="label label-success">ACTIVE</span>
                                                @endif
                                            </td>
                                            <td>
                                                {!! ($game) ? date('D, M-j', strtotime($game->start))."<br><em>".date('H:i A', strtotime($game->start))."</em>" : ""  !!}
                                            </td>
                                            <td>
                                                {{($pool->pool_type) ? $pool->pool_type->name : "-"}}
                                            </td>
                                            <td class="text-left">
                                                <strong><a href="/pool/{{$pool->id}}/">{{$pool->pool_name}}</a></strong><br>{!!  ($game) ? "<em>".$game->tournament_name."</em>" : '' !!}
                                            </td>
        
                                            <td>{{$pool->users->count()}}</td>
                                            <td class="text-center">
                                                @if($pool->already_member($userId))
                                                    <a href='/pool/{$pool->id}/leave' class='btn btn-danger btn-xs'><i class="fa fa-sign-out"></i> Leave</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
    @push('custom-js')
        <script>
            $('#myCommissionedPoolsTable').dataTable({
                "paging": false,
                "searching": false
            });
            $('#myPoolsTable').dataTable({
                "paging": false,
                "searching": false
            });
        </script>
    @endpush
@stop
