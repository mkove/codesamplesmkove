@php
    $userId = (auth()->user()) ? auth()->user()->id : null;
@endphp
@extends('layout.main')
@section('content')
    <h2 class="page-title"><strong>Archived Pools</strong><br> <small> Dashboard </small></h2>
    <section class="pool-nav">
        @include('user.partials.nav',['active' => 'archives'])
    </section>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">My Commissioned Pools</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="/pool/create" class="btn btn-primary"> CREATE POOL</a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-md-1"><i class="fa fa-users"></i></th>
                                    <th>Type</th>
                                    <th class="col-md-6">Pool Name</th>
                                    <th class="col-md-3">Date</th>
                                    <th class="col-md-2">Join</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pools as $pool)
                                    @if($userId == $pool->commissioner_id)
                                        @include('user.partials.pool-row', ['pool' => $pool, 'invite' => false])
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">My Pools</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="col-md-1"><i class="fa fa-users"></i></th>
                            <th>Type</th>
                            <th class="col-md-6">Pool Name</th>
                            <th class="col-md-3">Date</th>
                            <th class="col-md-2">Join</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pools as $pool)
                            @if($userId != $pool->commissioner_id)
                                @include('pool.partials.index-pool-row', ['pool' => $pool, 'invite' => false])
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop
