@extends('layout.main')
@section('content')
    <h2 class="page-title"><strong>My Active Pools</strong><br> <small> Dashboard </small></h2>
    <section class="pool-nav">
        @include('user.partials.nav',['active' => 'pools'])
    </section>
    <hr>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/user/edit" class="btn btn-primary btn-xs pull-right"><i class="fa fa-edit"></i> Edit Your Information</a>
                    <h3 class="panel-title">Profile</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <legend>Public Information</legend>
                                </div>
                            </div>
                            <div class="row">
                                <div for="" class="col-md-12">
                                    <p>Short Name: <strong>{{$user->name}}</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div for="" class="col-md-12">
                                    <p>Global Display Name: <strong>{{$user->global_display_name}}</strong></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <legend>Private Information</legend>
                                </div>
                            </div>
                            <div class="row">
                                <div for="" class="col-md-12">
                                    <p>Full Name: <strong>{{$user->first}} {{$user->last}}</strong> </p>
                                </div>
                            </div>
                            <div class="row">
                                <div for="" class="col-md-12">
                                    <p>Email: <strong>{{$user->email}}</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div for="" class="col-md-4">Phone: <strong>{{$user->phone}}</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
@stop
