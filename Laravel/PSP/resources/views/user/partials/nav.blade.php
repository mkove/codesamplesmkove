@php
    $userId = (auth()->user()) ? auth()->user()->id : null;
@endphp
<hr>


<div class="btn-group" role="group" aria-label="...">
  
    <a href="/user/pools" class="btn btn-link {{($active == 'pools') ? 'active' : ''}}">Active Pools</a>
    <a href="/user/friends" class="btn btn-link {{($active == 'friends') ? 'active' : ''}}">Friends</a>
    <a href="/user/archives" class="btn btn-link {{($active == 'archives') ? 'active' : ''}}">Archives</a>
    <a href="/user" class="btn btn-link {{($active == 'profile') ? 'active' : ''}}">Profile</a>
</div>

