@extends('pspadmin.layout.pspadmin')
@section('content')
    <div class="content-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="block block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">Search Results</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form class="form-horizontal" action="{{route('dashboard.search')}}" method="get">
                                    <div class="form-horizontal  input-group ">
                                        <input class="form-control" type="text" id="searchInput" name="term" placeholder="Search..." value="{{request()->get('term')}}">
                                        <span class="input-group-addon"><i class="si si-magnifier"></i></span>
                                    </div>
                                    <div class="help-block">Searh for users, games, pools</div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block">
                                    <ul class="nav nav-tabs" data-toggle="tabs">
                                        <li class="active">
                                            <a href="#games">Games <span class="badge">{{$games->count()}}</span></a>
                                        </li>
                                        <li>
                                            <a href="#users">Profile <span class="badge">{{$users->count()}}</span></a>
                                        </li>
                                        <li>
                                            <a href="#pools">Pools <span class="badge">{{$pools->count()}}</span></a>
                                        </li>
                                    </ul>
                                    <div class="block-content tab-content">
                                        <div class="tab-pane active" id="games">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    sdfjilsdfklj
                                                    @include('pspadmin.game.partials.index')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="users">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('pspadmin.user.partials.index')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="pools">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('pspadmin.pool.partials.index')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
