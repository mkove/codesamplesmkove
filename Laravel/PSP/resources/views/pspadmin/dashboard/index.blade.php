@extends('pspadmin.layout.pspadmin')
@section('content')
    <div class="content bg-white border-b">
        <div class="row items-push text-uppercase">
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Pools</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-check"></i> Active</small></div>
                <a class="h2 font-w300 text-primary animated flipInX" href="{{route('pool.index')}}">{{number_format($counts['pools'])}}</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Users</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-user"></i> Active</small></div>
                <a class="h2 font-w300 text-primary animated flipInX" href="{{route('user.index')}}">{{number_format($counts['users'])}}</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Invoiced</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-envelope"></i> Invoiced</small></div>
                <a class="h2 font-w300 text-primary animated flipInX" href="{{route('invoice.index')}}"><i class="fa fa-dollar"></i>{{number_format($counts['invoiced'])}}</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Sales</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Paid</small></div>
                <a class="h2 font-w300 text-primary animated flipInX text-success" href="{{route('payment.index')}}"><i class="fa fa-dollar"></i>{{number_format($counts['sales'])}}</a>
            </div>
        </div>
    </div>
    <div class="content bg-white border-b">
        <div class="row">
            <div class="col-md-12 text-center pb-4"><h2>LAST 30 DAYS</h2></div>
        </div>
    </div>



    <div class="content bg-white border-b">
        <div class="row items-push text-uppercase">
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Pools</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-check"></i> Active</small></div>
                <a class="h2 font-w300 text-primary animated flipInX" href="{{route('pool.index')}}">{{number_format($count30['pools'])}}</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Users</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-user"></i> Active</small></div>
                <a class="h2 font-w300 text-primary animated flipInX" href="{{route('user.index')}}">{{number_format($count30['users'])}}</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Invoiced</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-envelope"></i> Invoiced</small></div>
                <a class="h2 font-w300 text-primary animated flipInX" href="{{route('invoice.index')}}"><i class="fa fa-dollar"></i>{{number_format($count30['invoiced'])}}</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="font-w700 text-gray-darker animated fadeIn">Sales</div>
                <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Paid</small></div>
                <a class="h2 font-w300 text-primary animated flipInX text-success" href="{{route('payment.index')}}"><i class="fa fa-dollar"></i>{{number_format($count30['sales'])}}</a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">

            <div class="col-lg-6">
                <!-- Latest Sales Widget -->
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">Active Games</h3>
                    </div>

                    <div class="block-content">
                        <div class="pull-t pull-r-l">
                            <div class="remove-margin-b">
                                <table class="table remove-margin-b font-s13">
                                    <tbody>
                                        <tr class="text-center">
                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Pools</th>

                                        </tr>
                                    </tbody>
                                    <tbody>
                                    @foreach($tourneys as $t)
                                        <tr>
                                            <td>GOLF</td>
                                            <td class="font-w600">
                                                <a href="{{route('golf-tournament.edit',['golf_tournament' => $t->id])}}" tabindex="-1">{{$t->name}}</a>
                                                <br>
                                                <span class="text-small text-muted">{{date('F j, Y G:i A', strtotime($t->start_date))}}</span> <strong>{{$t->status}}</strong>
                                            </td>
                                            <td  class="text-center hidden-xs text-muted text-right" style="width: 50px;">{{$t->golfs->count()}}</td>
                                        </tr>
                                    @endforeach
                                    @foreach($games as $game)
                                        <tr>
                                            <td>{{$game->league}}-{{$game->sport}}</td>
                                            <td class="font-w600">
                                                <a href="{{route('game.edit',['game' => $game->id])}}" tabindex="-1">{{$game->tournament_name}}</a>
                                                <br>
                                                <span class="text-small text-muted">{{date('F j, Y G:i A', strtotime($game->time))}}</span> <strong>{{$game->status}}</strong>
                                            </td>
                                            <td  class="text-center hidden-xs text-muted text-right" style="width: 50px;">
                                                {{$game->squareboards->count() + $game->squareboards->count()}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
