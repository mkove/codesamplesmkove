<div class="js-chat-form block-content block-content-full block-content-mini">
    <form action="{{route('chat-message.store')}}" method="post" id="sendChatMessage" onsubmit="return false;">
        @csrf
        <input type="hidden" name="reloadable" value="pspadmin.chat.partials.index">
        <input type="hidden" name="chat_id" value="{{$chat->id}}">
        <input type="hidden" name="is_commissioner" value="0">
        <input type="hidden" name="visible" value="1">
        <input type="hidden" name="reported" value="0">
        <input type="hidden" name="system" value="1">
        <input type="hidden" name="link" value="0">
        <input type="hidden" name="sent_at" value="{{date('Y-m-d H:i:s')}}">
        <input type="hidden" name="chat_message_type_id" value="">
        <input class="js-chat-input form-control" type="text" name="message" data-target-chat-id="{{$chat->id}}" placeholder="Type a message and hit enter..">
    </form>
</div>
