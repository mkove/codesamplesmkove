<div class="row">
    <div class="col-md-12">
        <div class="block block-rounded block-bordered">
            <div class="block-header bg-gray-lighter">
                <h3 class="block-title">Chat</h3>
            </div>
            <div class="block-content pb-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="js-chat-talk overflow-y-auto block-content block-content-full" data-chat-id="{{$chat->id}}" style="overflow-wrap: break-word; height: 404px;">
                            @php $i=0; @endphp
                            @foreach($chat->messages as $message)
                                @if(!$i%5)
                                    <div class="font-s12 text-muted text-center push-20-t push-10"><em>{{$message->created_at}}</em></div>
                                @endif
                                <div class="block block-rounded block-transparent push-15 push-50-r">
                                    <div class="block-content block-content-full block-content-mini bg-gray-light">
                                        <div class="pull-right text-muted"><small><em>{{$message->created_at}}</em></small></div>
                                        <small><strong>{{($chat->user) ? $message->user->global_display_name : 'SYSTEM'}}</strong></small>
                                        <br>
                                        {!! $message->message !!}
                                    </div>
                                    
                                </div>
                                @php $i++; @endphp
                            @endforeach
                        </div>
                        @include('pspadmin.chat.partials.send')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
