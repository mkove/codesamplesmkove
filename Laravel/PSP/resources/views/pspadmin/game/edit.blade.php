@extends('pspadmin.layout.pspadmin')
@section('content')
    <div class="content-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <div class="block-header bg-warning text-white">
                        <div class="block-options-simple">
                            <form action="{{route('game.destroy', ['game' => $game->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Delete This Game</button>
                            </form>
                        </div>
                        <h3 class="block-title">Editing {{$game->tournament_name}}</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>{!! ucwords($game->league." ".$game->sport." ".$game->season)." (".$game->season_year.")" !!}</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <br>
                                <a href="/admin/game"><i class="fa fa-chevron-left"></i> GO BACK</a>
                                <br>
                                <div class="alert alert-info">
                                    <strong>Note:</strong> Editing game will be overwriten by feed (Broadage, Goalserve, etc.) on next scheduled update. Use this only if <a href="#">Re-Fetching Game</a> didn't work.
                                </div>
                                <div class="block">
                                    <ul class="nav nav-tabs" data-toggle="tabs">
                                        <li class=" active ">
                                            <a href="#game-settings">General</a>
                                        </li>
                                        <li>
                                            <a href="#game-events">Game Events</a>
                                        </li>
                                    </ul>
                                    <div class="block-content tab-content">
                                        <div class="tab-pane active" id="game-settings">
                                            <div class="row">
                                                <div class="col-md-12" id="gameEditScoreContainer">
                                                    @include('pspadmin.game.partials.score')
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" id="gameEditContainer">
                                                    @include('pspadmin.game.partials.edit')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="game-events">
                                            <div class="row">
                                                <div class="col-md-12" id="gameEventsEditContainer">
                                                    @include('pspadmin.game.event.partials.index')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('custom-js')
        <script>
            jQuery(function () {
                $('#gameEditScoreContainer').on('submit', '.update-game', function(ev){
                    var form = $(this);
                    ev.preventDefault();
                    run_update(form);
                });
                
                $('#gameEditContainer').on('submit', '.update-game', function(ev){
                    var form = $(this);
                    ev.preventDefault();
                    run_update(form);
                });
                
                function run_update(form){
                    console.log(form);
                    var containerId = form.data('reload');
                    var formData = form.serialize();
                    var url = form.attr('action');
                    var method = form.attr('method');
                    runAjax(url, method, formData, false, $('#'+containerId));
                }
                
            });
        </script>
    @endpush
@endsection

