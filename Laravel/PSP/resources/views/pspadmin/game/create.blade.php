@extends('pspadmin.layout.pspadmin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Create Game</h3>
                </div>
                <div class="block-content">
                    <form action="{{route('game.store')}}" method="post" class="form-horizontal">
                        @csrf

                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <div class="form-group">
                                    <label for="" class="col-md-12">Pick League</label>
                                    <div class="col-md-12">
                                        <select name="" id="gameLeague" class="form-control">
                                            <option value="">-pick league-</option>
                                            <option value="professional|football">NFL</option>
                                            <option value="professional|basketball">NBA</option>
                                            <option value="college|football">NCAA Football</option>
                                            <option value="college|basketball">NCAA Ball</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="col-md-12">Pick Team</label>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <select name="" id="gameTeam" class="form-control"></select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" id="gameTeamAddHomeButton" type="button"><i class="fa fa-plus"></i> Home</button>
                                                <button class="btn btn-default" id="gameTeamAddAwayButton" type="button"><i class="fa fa-plus"></i> Away</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-1">
                                <input type="hidden" name="home_team_id" id="homeTeamId">
                                <input type="hidden" name="away_team_id" id="awayTeamId">
                                <input type="hidden" name="sport" id="sport">
                                <input type="hidden" name="league" id="league">
                                <div class="form-group">
                                    <label for="" class="col-md-12">Game</label>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" readonly id="awayTeamName" class="form-control">
                                            <span class="input-group-addon">
                                                <i class="fa fa-at"></i>
                                            </span>
                                            <input type="text" readonly id="awayTeamName" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="" class="col-md-12">Status</label>
                                    <div class="col-md-12">
                                        <select name="status" id="" class="form-control">
                                            <option value="Not Started">Not Started</option>
                                            <option value="1st Quarter">1st Quarter</option>
                                            <option value="2nd Quarter">2nd Quarter</option>
                                            <option value="3rd Quarter">3rd Quarter</option>
                                            <option value="4th Quarter">4th Quarter</option>
                                            <option value="Over Time">Over Time</option>
                                            <option value="Final">Final</option>
                                            <option value="Canceled">Canceled</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="" class="col-md-12">Start</label>
                                    <div class="col-md-12">
                                        <input type="datetime-local" name="start" class="form-control">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-md-12">Hometeam</label>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    Q1
                                                </span>
                                                <input type="number" name="hometeam_q1" min="0" max="999" id="homeTeamQ1" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                Q2
                                            </span>
                                                <input type="number" name="hometeam_q2" min="0" max="999" id="homeTeamQ2" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                Q3
                                            </span>
                                                <input type="number" name="hometeam_q3" min="0" max="999" id="homeTeamQ3" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                Q4
                                            </span>
                                                <input type="number" name="hometeam_q4" min="0" max="999" id="homeTeamQ4" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                OT
                                            </span>
                                                <input type="number" name="hometeam_ot" min="0" max="999" id="homeTeamOT" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                FNL
                                            </span>
                                                <input type="number" name="hometeam_totalscore" min="0" max="999" id="homeTeamFNL" class="form-control">
                                            </div>
                                        </div>
        
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-md-12">Awayteam</label>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                Q1
                                            </span>
                                                <input type="number" name="awayteam_q1" min="0" max="999" id="awayTeamQ1" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                Q2
                                            </span>
                                                <input type="number" name="awayteam_q2" min="0" max="999" id="awayTeamQ2" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                Q3
                                            </span>
                                                <input type="number" name="awayteam_q3" min="0" max="999" id="awayTeamQ3" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                Q4
                                            </span>
                                                <input type="number" name="awayteam_q4" min="0" max="999" id="awayTeamQ4" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                OT
                                            </span>
                                                <input type="number" name="awayteam_ot" min="0" max="999" id="awayTeamOT" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                            <span class="input-group-addon">
                                                FNL
                                            </span>
                                                <input type="number" name="awayteam_totalscore" min="0" max="999" id="awayTeamFNL" class="form-control">
                                            </div>
                                        </div>
        
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="" class="col-md-12">Season / Year</label>
                                    <div class="col-md-6">
                                        <select name="season_year" id="seasonYear" class="form-control">
                                            <option value="2019/2020">2019/2020</option>
                                            <option value="2020/2021">2020/2021</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="season" id="season" class="form-control">
                                            <option value="Regular">Regular</option>
                                            <option value="Championship">Championship</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-12">Week</label>
                                    <div class="col-md-12">
                                        <select name="week" id="week" class="form-control">
                                            @for($i=1;$i<20;$i++)
                                                <option value="{{$i}}">Week {{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Games</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-12">
    
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
@endsection
