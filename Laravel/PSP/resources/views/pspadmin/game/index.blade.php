@extends('pspadmin.layout.pspadmin')
@section('content')
<div class="content-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="block block-rounded block-bordered">
                <div class="block-header bg-gray-lighter">
                    <h3 class="block-title">Search</h3>
                </div>
                <div class="block-content pb-5">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route('game.index')}}" method="get">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">Search</label>
                                            <div class="col-xs-12">
                                                <input id="ajaxSearchByGameName" value="{{request()->get('search_query')}}" type="text" name="search_query" class="form-control" placeholder="Search by tournament name">
                                                <input type="hidden" name="id" id="ajaxSearchByGameId">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">Status</label>
                                            <div class="col-xs-12">
                                                <select name="status" id="poolStatus" class="form-control">
                                                    <option value="">ALL</option>
                                                    <option value="Final" {{(request()->get('status') == 'Final') ? 'selected' : ''}}>Final</option>
                                                    <option value="Not Started" {{(request()->get('status') == 'Not Started') ? 'selected' : ''}}>Not Started</option>
                                                    <option value="1st Quarter" {{(request()->get('status') == '1st Quarter') ? 'selected' : ''}}>1st Quarter</option>
                                                    <option value="2nd Quarter" {{(request()->get('status') == '2nd Quarter') ? 'selected' : ''}}>2nd Quarter</option>
                                                    <option value="Half Time" {{(request()->get('status') == 'Half Time') ? 'selected' : ''}}>Half Time</option>
                                                    <option value="3rd Quarter" {{(request()->get('status') == '3rd Quarter') ? 'selected' : ''}}>3rd Quarter</option>
                                                    <option value="4th Quarter" {{(request()->get('status') == '4th Quarter') ? 'selected' : ''}}>4th Quarter</option>
                                                    <option value="Over Time" {{(request()->get('status') == 'Over Time') ? 'selected' : ''}}>Over Time</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">Sport</label>
                                            <div class="col-xs-12">
                                                <select name="sport" id="searchSport" class="form-control">
                                                    <option value="">ALL</option>
                                                    <option {{(request()->get('sport') == 'football') ? "selected" : ""}} value="football">Football</option>
                                                    <option {{(request()->get('sport') == 'basketball') ? "selected" : ""}} value="football">Basketball</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">League</label>
                                            <div class="col-xs-12">
                                                <select name="league" id="searchSport" class="form-control">
                                                    <option value="">ALL</option>
                                                    <option {{(request()->get('league') == 'professional') ? "selected" : ""}} value="professional">Pro</option>
                                                    <option {{(request()->get('league') == 'college') ? "selected" : ""}} value="college">NCAA</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">By Team</label>
                                            <div class="col-xs-12">
                                                <input id="ajaxSearchByTeamName" value="{{request()->get('team_search_query')}}" type="text" name="team_search_query" class="form-control" placeholder="Search by team name">
                                                <input type="hidden" name="id" id="ajaxSearchByTeamId">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">Week</label>
                                            <div class="col-xs-12">
                                                <input id="ajaxSearchByWeek" value="{{request()->get('week')}}" type="number" min="0" max="1000" name="week" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">Season Year</label>
                                            <div class="col-xs-12">
                                                <select name="season_year" id="searchSport" class="form-control">
                                                    <option value="">ALL</option>
                                                    <option {{(request()->get('season_year') == '2019/2020') ? "selected" : ""}} value="2019/2020">2019/2020</option>
                                                    <option {{(request()->get('season_year') == '2020/2021') ? "selected" : ""}} value="2020/2021">2020/2021</option>
                                                    <option {{(request()->get('season_year') == '2021/2022') ? "selected" : ""}} value="2021/2022">2021/2022</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-3">--}}
                                    {{-- <div class="form-group">--}}
                                    {{-- <label class="col-xs-12" for="status">Season</label>--}}
                                    {{-- <div class="col-xs-12">--}}
                                    {{-- <select name="season" id="searchSport" class="form-control">--}}
                                    {{-- <option value="">ALL</option>--}}
                                    {{-- @foreach($seasons as $season)--}}
                                    {{-- <option {{(request()->get('season') == $season->season) ? "selected" : ""}} value="football">{{$season->season}}</option>--}}
                                    {{-- @endforeach--}}
                                    {{-- </select>--}}
                                    {{-- </div>--}}
                                    {{-- </div>--}}
                                    {{-- </div>--}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">Played Between</label>
                                            <div class="col-xs-12">
                                                <div class="input-daterange input-group date" data-date-format="mm/dd/yyyy">
                                                    <input class="form-control datetimepicker-input" type="text" id="start_start" name="start_start" placeholder="From" value="{{request()->get('start_start')}}">
                                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                                    <input class="form-control" type="text" id="start_end" name="start_end" placeholder="To" value="{{request()->get('start_end')}}">
                                                </div> 

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4  text-right">
                                        <div class="form-group">
                                            <label class="col-xs-12" for="status">&nbsp;</label>
                                            <div class="col-xs-12">
                                                <div class="btn-group btn-block">
                                                    <button class="btn   btn-primary"><i class="fa fa-search"></i> Search</button>
                                                    <a class="btn btn-default" href="{{route('game.index')}}"><i class="fa fa-refresh"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('pspadmin.game.partials.index')
        </div>
    </div>
</div>


@push('custom-css') 
<link rel="stylesheet" href="/pspadmin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
@endpush

@push('custom-js')
<script src="/pspadmin/js/plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="/pspadmin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script>
    $(document).ready(function() {

        $('#start_start').datetimepicker();
        $('#start_end').datetimepicker();

        $("#ajaxSearchByTeamId").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/admin/team/search",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 2,
            select: function(event, ui) {
                $('#ajaxSearchByTeamIdField').val(ui.item.id);
            },
            focus: function(event, ui) {
                $('#ajaxSearchByTeamIdField').val(ui.item.id);
            },
        });


        $("#ajaxSearchByGameName").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/admin/game/search",
                    dataType: "json",
                    data: {
                        clean: true,
                        term: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 2,
            select: function(event, ui) {
                $('#ajaxSearchByGameIdField').val(ui.item.id);
            },
            focus: function(event, ui) {
                $('#ajaxSearchByGameIdField').val(ui.item.id);
            },
        });

        $("#ajaxSearchByTeamName").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/admin/team/search",
                    dataType: "json",
                    data: {
                        clean: true,
                        term: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 2,
            select: function(event, ui) {
                $('#ajaxSearchByTeamNameField').val(ui.item.id);
            },
            focus: function(event, ui) {
                $('#ajaxSearchByTeamNameField').val(ui.item.id);
            },
        });

        $('#pool-index').on('click', '.toggle_status', function(ev) {
            var $toggleCheck = $(this);
            var url = $toggleCheck.data('url');
            var data = {
                _token: "{{csrf_token()}}",
                _method: "PATCH",
                status: ($toggleCheck.is(":checked")) ? "active" : "disabled",
            }
            runAjax(url, "POST", data)
        });
    })
</script>
@endpush


@endsection