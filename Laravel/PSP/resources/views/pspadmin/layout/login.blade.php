<html class="no-focus" lang="en"><head>
    <meta charset="utf-8">
    <title>PSP Admin</title>
    
    <meta name="description" content="OneUI - Admin Dashboard Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="Michael Kove">
    <meta name="robots" content="noindex, nofollow">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title')</title>

    <link rel="shortcut icon" href="/pspadmin/img/favicons/favicon.png">
    
    <link rel="icon" type="image/png" href="/pspadmin/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/pspadmin/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/pspadmin/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/pspadmin/img/favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="/pspadmin/img/favicons/favicon-192x192.png" sizes="192x192">
    
    <link rel="apple-touch-icon" sizes="57x57" href="/pspadmin/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/pspadmin/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/pspadmin/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/pspadmin/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/pspadmin/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/pspadmin/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/pspadmin/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/pspadmin/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/pspadmin/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->
    
    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="/pspadmin/js/plugins/slick/slick.min.css">
    <link rel="stylesheet" href="/pspadmin/js/plugins/slick/slick-theme.min.css">
    
    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="/pspadmin/css/bootstrap.min.css">
    <link rel="stylesheet" id="css-main" href="/pspadmin/css/oneui.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
</head>
<body><div class="bg-white pulldown">
    @yield('content')
</div>
<div class="pulldown push-30-t text-center animated fadeInUp">
    <small class="text-muted"><span class="js-year-copy">2019</span> © Premier Sports</small>
</div>
<script src="assets/js/oneui.min-3.5.js"></script><script src="assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="assets/js/pages/base_pages_login.js"></script>

<script type="text/javascript">( function(){ window.SIG_EXT = {}; } )()</script><div id="ritekit-alerts" style="opacity: 0;"></div></body></html>
