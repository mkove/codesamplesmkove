<header id="header-navbar" class="content-mini content-mini-full" style="{{((env('APP_ENV') == 'development')) ? "background:#ffe1e1 !important;" : ''}}">
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
        <li>
            <div class="btn-group">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                    <i class="fa fa-envelope"></i> <span class="hidden-xs">Send Emails</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header"> User Emails</li>
                    <li>
                        <a class="" href="{{route('communication.email.index')}}"><i class="fa fa-user"></i> Users</a>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Internal Messages</li>
                    <li>
                        <a class="" href="#"><i class="fa fa-file-text"></i>[TBD] Users</a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="btn-group">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                    <i class="fa fa-gears"></i> <span class="hidden-xs">Site Settings</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header">Site Content</li>
                    <li>
                        <a class="" href="{{route('content.index')}}"><i class="fa fa-file-text"></i> Content</a>
                    </li>
                    <li>
                        <a class="" href="{{route('content.help')}}"><i class="fa fa-question-circle"></i> Help Content</a>
                    </li>
                    <li>
                        <a class="','static-page.edit','static-page.create'])}}" href="{{route('static-page.index')}}"><i class="fa fa-file-code"></i> Static Pages</a>
                    </li>
                    <li>
                        <a class="" href="{{route('email-content.index')
                            }}"><i class="fa fa-envelope-open"></i> Email</a>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Site Content</li>
                    <li>
                        <a class="" href="{{route('config.index')}}"><i class="fa fa-gears"></i> Config</a>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Cache & Queues</li>

                    <li class="js-header-search header-search">
                        <form action="{{route('system.queue.restart')}}" method="post" class="form-inline">
                            @csrf
                            <button class="btn btn-warning btn-xs" type="submit">
                                <i class="fa fa-refresh"></i> Restart Queues
                            </button>
                        </form>
                        <form action="{{route('system.jobs.process')}}" method="post" class="form-inline ml-3">
                            @csrf
                            <input type="hidden" name="command" value="session:flush">
                            <button class=" btn btn-danger btn-xs" type="submit" tooltip="Will log everyone off including admin"><i class="fa fa-refresh"></i> Flush Session</button>
                        </form>
                    </li>
                </ul>
            </div>
        </li>

        <li>
            <div class="btn-group">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                    <i class="fa fa-terminal"></i> <span class="hidden-xs">CRONs</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header">DATAGOLF</li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('datagolf.get_scores_golf'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> Golf</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> Golf</span></a>
                        @endif
                    </li>
                    <li class="dropdown-header">GOAL SERVE</li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('goalserve.get_scores_golf'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> Golf</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> Golf</span></a>
                        @endif
                    </li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('goalserve.get_scores_nfl'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> NFL</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> NFL</span></a>
                        @endif
                    </li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('goalserve.get_scores_ncaa'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> NCAA</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> NCAA</span></a>
                        @endif
                    </li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('goalserve.get_scores'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> Get Scores</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> Get Scores</span></a>
                        @endif
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">PLAYING</li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('golf.play_entry'))
                            <a href="{{route('config.index')}}"  tooltip="{{(__conf('golf.play_force_replay_entries', 'boolean',1)) ? 'Force replay' : ''}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> Golf @if(__conf('golf.play_force_replay_entries', 'boolean',1))
                                        <i class="fa fa-refresh text-danger"></i>
                                    @endif</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> Golf

                                </span></a>
                        @endif

                    </li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('golf.play_lock_entry'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> LOCK Golf</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> LOCK Golf</span></a>
                        @endif
                    </li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('ncaa.play'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> NCAA</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> NCAA</span></a>
                        @endif
                    </li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('squareboard.play'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> Squares</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> Squares</span></a>
                        @endif
                    </li>
                    <li style="">
                        @if(\App\Helpers\SiteHelper::conf('schwindy.play'))
                            <a href="{{route('config.index')}}"><span class="text-success"><i class="fa fa-clock fa-spin"></i> Schwindy</span></a>
                        @else
                            <a href="{{route('config.index')}}"><span class="text-danger"><i class="fa fa-pause"></i> Schwindy</span></a>
                        @endif
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">JOBS</li>
                    <li>
                        <a href="{{route('system.jobs')}}"><span class=""><i class="fa fa-play"></i> Jobs</span></a>
                    </li>

                </ul>
            </div>
        </li>


{{--        <li>--}}
{{--            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->--}}
{{--            <button class="btn btn-default" data-toggle="layout" data-action="side_overlay_toggle" type="button">--}}
{{--                <i class="fa fa-tasks"></i>--}}
{{--            </button>--}}
{{--        </li>--}}
    </ul>
    <!-- END Header Navigation Right -->
{{--    --}}
{{--    <!-- Header Navigation Left -->--}}
    <ul class="nav-header pull-left">
        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>
        <li class="hidden-xs hidden-sm">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                <i class="fa fa-ellipsis-v"></i>
            </button>
        </li>




{{--        <li class="visible-xs">--}}
{{--            <!-- Toggle class helper (for .js-header-search below), functionality initialized in App() -> uiToggleClass() -->--}}
{{--            <button class="btn btn-default" data-toggle="class-toggle" data-target=".js-header-search" data-class="header-search-xs-visible" type="button">--}}
{{--                <i class="fa fa-search"></i>--}}
{{--            </button>--}}
{{--        </li>--}}
{{--        <li class="js-header-search header-search">--}}
{{--            <form class="form-horizontal" action="{{route('dashboard.search')}}" method="get">--}}
{{--                <div class="form-material form-material-primary input-group remove-margin-t remove-margin-b">--}}
{{--                    <input class="form-control" type="text" id="globalSearch" name="term" placeholder="Search...">sss--}}
{{--                    <span class="input-group-addon"><i class="si si-magnifier"></i></span>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </li>--}}


    </ul>
    <!-- END Header Navigation Left -->
</header>
