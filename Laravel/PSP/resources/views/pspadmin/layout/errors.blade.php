@if($errors->count())
    <div class="container">
        @foreach ($errors->all() as $message)
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <h6>{{$message}}</h6>
                    </div>
                </div>
            </div>
    
        @endforeach
    </div>
@endif
@php
    $messages = session('messages');
@endphp
@if($messages && count($messages))
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <ul style="list-style: none;margin-left:0;padding:0;">
                    @foreach($messages as $message)
                        <li class="alert alert-{{$message['type']}}" style="list-style: none">
                            {!! $message['message'] !!}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif

