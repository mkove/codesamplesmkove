@php
$route = \Route::currentRouteName();
@endphp
<nav id="sidebar">
    <div id="sidebar-scroll">
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op" style="{{((env('APP_ENV') == 'development')) ? "background:red !important;" : ''}}">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                @if(env('APP_ENV') == 'development')
                <a class="h5 text-white">TEST SERVER</a>
                </button>
                @endif
            </div>
            <div class="side-content">

                <ul class="nav-main">
                    <li>
                        <a class="{{__adm_ma($route, ['dashboard'])}}" href="/admin/dashboard"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Pools</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['pool.index','pool.edit'])}}" href="{{route('pool.index')}}"><i class="fa fa-table"></i> Pools</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['pool.create'])}}" href="{{route('pool.create')}}"><i class="fa fa-plus"></i> Add Pool</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['pool.reports'])}}" href="{{route('pool.reports')}}"><i class="fa fa-desktop"></i> Reports</a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide text-warning">Testing</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['test.create-squares'])}}" href="{{route('test.create-squares')}}"><i class="fa fa-bug"></i> Create Squares</a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Schwindy</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['schwindy.lock-admin'])}}" href="{{ route('schwindy.lock-admin') }}"><i class="fa fa-lock"></i> Schwindy Lock Admin</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['schwindy.games'])}}" href="{{route('schwindy.games')}}"><i class="fa fa-football-ball"></i> Schwindy Season</a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Members</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['user.index','user.edit'])}}" href="{{route('user.index')}}"><i class="fa fa-users"></i> Users</a>
                    </li> 
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">MailChimp</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['user.sync'])}}" href="{{route('user.sync')}}"><i class="fa fa-users"></i> Sync Users</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['user.mailchimp_users'])}}" href="{{route('user.mailchimp_users')}}"><i class="fa fa-users"></i> PSPImport</a>
                    </li>
                    {{-- <li>--}}
                    {{-- <a class="{{__adm_ma($route, ['user.create'])}}" href="{{route('user.create')}}"><i class="fa fa-user-plus"></i>Add User</a>--}}
                    {{-- </li>--}}
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Game Management</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['game.index','game.edit'])}}" href="{{route('game.index')}}"><i class="fa fa-football-ball"></i> Games</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['team.index','team.edit'])}}" href="{{route('team.index')}}"><i class="fa fa-users"></i> Teams</a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">NCAA</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['game.create-ncaa'])}}" href="{{route('game.create-ncaa')}}"><i class="fa fa-basketball-ball"></i> NCAA Games</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['game.score-ncaa'])}}" href="{{route('game.score-ncaa')}}"><i class="fa fa-basketball-ball"></i> NCAA Scores</a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Golf</span></li>
                    <!--
                        <li>
                            <a class="{{__adm_ma($route, ['golf.pga.match'])}}" href="{{route('golf.pga.players')}}"><i class="fa fa-users"></i> PGA Players</a>
                        </li>
                        <li>
                            <a class="{{__adm_ma($route, ['golf.pga.tournaments','golf.pga.tournament-fetch'])}}" href="{{route('golf.pga.tournaments')}}"><i class="fa fa-trophy"></i> PGA Tournaments</a>
                        </li>
                        -->
                    <li>
                        <a class="{{__adm_ma($route, ['golf-config.autogrouping'])}}" href="{{route('golf-config.autogrouping')}}"><i class="fa fa-sort-amount-down"></i> Autogrouping </a>
                    </li>

                    <li>
                        <a class="{{__adm_ma($route, ['golf-tournament.index','golf-tournament.edit','golf-tournament.show'])}}" href="{{route('golf-tournament.index')}}"><i class="fa fa-golf-ball"></i> Tournaments</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['golf.player.index','golf.player.edit','golf.player.show'])}}" href="{{route('golf.player.index')}}"><i class="fa fa-users"></i> Golfers</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['golf.player.variations'])}}" href="{{route('golf.player.variations')}}"><i class="fa fa-random"></i> Map Golfers Names</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['golf-prize-config.index','golf-prize-config.create', 'golf-prize-config.edit'])}}" href="{{route('golf-prize-config.index')}}"><i class="fa fa-money"></i> Prize Money </a>
                    </li>
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">Payments</span></li>
                    <li>
                        <a class="{{__adm_ma($route, ['rate.index','rate.edit','rate.create'])}}" href="{{route('rate.index')}}"><i class="fa fa-th"></i> Pool Rates</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['discount.index','discount.edit','discount.create'])}}" href="{{route('discount.index')}}"><i class="fa fa-percent"></i> Discounts</a>
                    </li>
                    <li>
                        <a class="{{__adm_ma($route, ['invoice.index','invoice.edit','invoice.create'])}}" href="{{route('invoice.index')}}"><i class="fa fa-file-invoice-dollar"></i> Invoices</a>
                    </li>
                    {{-- <li class="nav-main-heading"><span class="sidebar-mini-hide">Content</span></li>--}}
                    {{-- <li>--}}
                    {{-- <a class="{{__adm_ma($route, ['content.index'])}}" href="{{route('content.index')}}"><i class="fa fa-file-text"></i> Content</a>--}}
                    {{-- </li>--}}
                    {{-- <li>--}}
                    {{-- <a class="{{__adm_ma($route, ['content.help'])}}" href="{{route('content.help')}}"><i class="fa fa-question-circle"></i> Help Content</a>--}}
                    {{-- </li>--}}
                    {{-- <li>--}}
                    {{-- <a class="{{__adm_ma($route, ['static-page.index','static-page.edit','static-page.create'])}}" href="{{route('static-page.index')}}"><i class="fa fa-file-code"></i> Static Pages</a>--}}
                    {{-- </li>--}}
                    {{-- <li>--}}
                    {{-- <a class="{{__adm_ma($route, ['email-content.index','email-content.edit','email-content.create'])}}" href="{{route('email-content.index')--}}
                    {{-- }}"><i class="fa fa-envelope-open"></i> Email</a>--}}
                    {{-- </li>--}}
                    {{-- <li class="nav-main-heading"><span class="sidebar-mini-hide">Site Admin</span></li>--}}
                    {{-- <li>--}}
                    {{-- <a class="{{__adm_ma($route, ['config.index'])}}" href="{{route('config.index')--}}
                    {{-- }}"><i class="fa fa-gears"></i> Config</a>--}}
                    {{-- </li>--}}

                </ul>
            </div>
            <!-- END Side Content -->
        </div>
    </div>
</nav>