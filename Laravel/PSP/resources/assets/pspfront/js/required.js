window.Vue = require('vue');

import Vue from 'vue'
// import BootstrapVue from "bootstrap-vue"
// Vue.use(BootstrapVue)

// import Multiselect from 'vue-multiselect'
// Vue.component('multiselect', Multiselect);

import ContentMixin from './components/mixins/ContentMixin';
Vue.mixin(ContentMixin);

import Notifications from 'vue-notification'
Vue.use(Notifications);
window.Notification = require('vue-notification');

// import VueSweetalert2 from 'vue-sweetalert2';
// // If you don't need the styles, do not connect
// import 'sweetalert2/dist/sweetalert2.min.css';
// Vue.use(VueSweetalert2);

// import Vue2TouchEvents from 'vue2-touch-events'
// Vue.use(Vue2TouchEvents)

// import axios from 'axios';