import Echo from "laravel-echo";  

const echo_server_port = process.env.MIX_ECHO_SERVER_PORT;
window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':' + echo_server_port,
    path: '/socket.io',
    auth: {
        headers: {

            'Authorization': 'Bearer ${token}'
        },
    },
})