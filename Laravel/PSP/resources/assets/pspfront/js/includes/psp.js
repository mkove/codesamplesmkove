window.copyLink = function (id) {
    var el = document.getElementById("link-" + id);
    el.select();
    document.execCommand('copy');
    notyf.success({
        message: 'Invite Link was copied to clipboard!',
        ripple : true

    });
};

window.showMessage = function(messArr)
{
    $.each(messArr, function( index, value ) {
        $.notify(value.message, {
            arrowSize: 5,
            globalPosition: 'top center',
            className: value.type,
        });
    });
}

window.blink = function(selector) {
    $(selector).fadeTo( 'slow', 0.5, function(){
        $(this).fadeTo( 'slow', 1.0, function(){
            blink(this);
        } )
    } );
    $(selector).fadeOut('slow', function(){
        $(this).fadeIn('slow', function(){
            blink(this);
        });
    });
}

// blink('.blink');
