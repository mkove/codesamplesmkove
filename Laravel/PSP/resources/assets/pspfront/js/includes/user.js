// validate username
window.username_taken = function(username){

    $.ajax({
        url: '/validate-username',
        type: 'GET',
        dataType: 'json',
        data: {username: username},
    })
        .done(function(returnData) {
            if(returnData.exist){
                $('.validateUsernameOutput').html("Username <stong>"+username+"</stong> is taken!");
            } else {
                $('.validateUsernameOutput').html("");
            }

        })
        .fail( function(xhr, textStatus, errorThrown) {
            alert(errorThrown);
        })
        .always(function() {

        });
}

$(function() {

    $(document).on('keyup', '.validateUsername', function () {
        var username = $(this).val();
        username_taken(username);
    });
});
