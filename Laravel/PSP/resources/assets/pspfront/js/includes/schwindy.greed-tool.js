$(function() {

    $(document).on('mouseover', '.hoverable', function (e) {
        var id = $(this).data('team_id');
        $('.hoverable').removeClass('gridTeamHovered');
        $('.hoverable').removeClass('teamSelected');
        $('.team-row-name').removeClass('gridTeamHovered');

        $(this).addClass('teamSelected');
        $(".id-" + id).addClass('gridTeamHovered');
    });

    $(document).on('click', '.teamPickable', function (e) {
        var id = $(this).data('team_id');
        if ($(this).hasClass("keep")) {
            //remove classes from same team
            $(this).removeClass('keep');
            $(this).removeClass('teamSelectedKeep');
            $(".id-" + id).removeClass('gridTeamHoveredKeep');
            $(this).removeClass('hoverable');
            $(".id-" + id).removeClass('hoverable');
        } else {
            $(this).addClass('keep');
            $(this).addClass('teamSelectedKeep');
            $(".id-" + id).addClass('gridTeamHoveredKeep');
            $(this).addClass('hoverable');
            $(".id-" + id).addClass('hoverable');
        }

    });
});
