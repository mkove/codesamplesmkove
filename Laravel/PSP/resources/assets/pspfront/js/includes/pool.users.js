$(function() {
    $(document).on('click', '.remove-user-from-pool', function (event) {
        var $self = $(this);
        $self.closest('tr').addClass('danger');
        swal({
            title: 'REMOVE USER: Are you sure?',
            text: "All user data will be lost (including winnings, standings and picks)!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete user!'
        }).then((result) => {
            if (result.value) {
                var user_id = $self.data('user_id');
                var pool_id = $self.data('pool_id');
                $.ajax({
                    url: '/pool/' + pool_id + '/remove-user',
                    type: 'POST',
                    dataType: 'json',
                    data: {user_id: user_id},
                })
                    .done(function (jsonData) {
                        if (!jsonData.error) {
                            $('#user-roster-row-' + user_id).remove();
                            showMessage(jsonData.message);
                        } else {
                            showMessage(jsonData.message);
                        }
                    })
                    .fail(function () {

                    })
                    .always(function () {

                    });

            }
        })


    });
});
