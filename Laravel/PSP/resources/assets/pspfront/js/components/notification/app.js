import NotificationMenu from './NotificationMenu';
import DropDownMenu from "./DropDownMenu";

if(document.getElementById('siteNotificationApp')) {

    const notificationApp = new Vue(
        {
            el :"#siteNotificationApp",
            components: {
                'notification-menu' : NotificationMenu,
                'dropdown-menu' : DropDownMenu
            },
            data : function(){
                return {
                    message : '',
                    type : null,
                    title : '',
                    unread : '',
                    notices : [],
                }
            },
            mounted() {


            },
            created() {
                axios.get('/notification/count',{
                    headers: {
                        'Content-Type':'application/json',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then((jsonData) => {

                    this.unread = jsonData.data.unread;
                    this.notices = jsonData.data.notifications;
                }).catch((err)=> {

                });
                let user_id = document.getElementById('siteNotificationApp').dataset.user_id;
                if(user_id){
                    let that = this;
                    let ch = Echo.channel('notifs.'+user_id)
                        .listen('.newNotif', (data) => {
                            that.unread = data.unread;
                            var position = "top left";
                            var duration = 5000;
                            if(data.userNotice.level == 'danger'){
                                duration = -1;
                            }
                            that.$notify({
                                group: 'usernotices',
                                classes : 'usernotices',
                                type : data.userNotice.level,
                                title: data.title,
                                duration : duration,
                                text: data.userNotice.notification,
                                position:  position,
                                max : 10,
                                closeOnClick : true,

                            });
                        });
                    this.unread = that.unread;
                }
            },
            methods : {
                showLastTen : function(){

                },
                readAll : function(){
                    axios.get('/notification/read',{
                        headers: {
                            'Content-Type':'application/json',
                            'Access-Control-Allow-Origin': '*'
                        }
                    }).then((jsonData) => {

                        this.unread = jsonData.data.unread;
                        this.notices = jsonData.data.notifications;
                    }).catch((err)=> {

                    });
                }
            }
        });
}
