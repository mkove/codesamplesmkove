import Friends from "./Friends";
window.axios = require('axios');

if(document.getElementById('friendGroup'))
{
    Vue.config.silent = true; 
    const friendGroup = new Vue({
        el: '#friendGroup',
        components: {
            Friends,
        }, 
        created() { 

        },

        ready: function () {

        },
        methods : { 
            saveGroup: function(type, groups) {
                let users = document.getElementById('selected_friend_ids').value;
                console.log('saveGroup=>', type, groups, users);

                if(users == '') {
                    alert('Please select users');
                    return;
                }

                let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                let url ='/user/friends/update-friend-group';
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

                axios.post(url, {
                    type : type,
                    groups : groups,
                    users : users,
                    _token : _token,
                }).then((response) => {
                    if(response.data.error){
                        let messageText="";
                        response.data.messages.forEach(function(err) {
                            messageText += "<div class=' p-3 m-0 alert-"+err.type+"'>"+err.message+"</div>";
                        }); 
                        this.message = messageText;
                    } else {
                        // let newGroup = response.data.group; 
                        // this.user_groups.push(newGroup);
                        this.message = '<div class="alert alert-info p3">'+response.data.message.message+'</div>';
                        window.location.reload();
                    }
                }, (error) => { 
                    // console.log("Entre 4",this.create_entry_error);
                    alert(error);
                });
            }
        },
        computed : { 
        }
    });
}
