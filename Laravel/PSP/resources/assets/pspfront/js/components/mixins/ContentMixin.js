export default {
    name : 'ContentMixin',
    data: function () {
        return {
            contentStorage: window.site_content,
        }
    },
    methods : {
        __c : function(key = null, def = "Custom Text", vars = {}, type = 'string'){
                let content =  this.getContent(key, def, vars, type);
                console.log("Getting content for ", key, def);
                if(content && window.liveEdit){
                    let contentString = "<span class='reload-"+content.id+"'>"+content.content+"</span>";
                    let popupString = "<a " +
                        "href='#' " +
                        "data-toggle='modal' " +
                        "data-target='#liveEditPopup' " +
                        "data-title='Live Edit' " +
                        "data-section='golf' " +
                        "data-note='' " +
                        "data-id='"+content.id+"' " +
                        "data-content='"+content.content+"' " +
                        "data-identifier='("+content.identifier+")' class='edit-content'>" +
                        "<i class='fa fa-edit text-brand-red' style='font-size: 10px !important;'></i>" +
                        "</a>"
                    return contentString+popupString;
                }
                return (content) ? content.content : '';
            },
        getContent : function(key, def, vars, type){
            let localContent = this.contentStorage.find(function(item){
                return (item.identifier === key);
            })
            if(localContent){
                return localContent;
            } else {
                let data = {
                    key : key,
                    default : def,
                    vars : vars,
                    type : type
                };

                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                let savedContent  = null;
                axios.post('/api/content', data).then((jsonData) => {
                    savedContent = jsonData.data.content;
                    window.site_content.push(savedContent);
                }).catch((err)=> {
                    // alert(err);
                });
                return savedContent;
            }


        }
    },
}
