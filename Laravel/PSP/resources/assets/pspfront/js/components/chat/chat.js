import ChatContainer from './ChatContainer.vue';

if(document.getElementById('poolChat')) {
    const poolChat = new Vue({
            el: '#poolChat',
            components : {
                'chat-container' : ChatContainer
            },
            data(){
                return {
                    chat : null,
                }
            },
            methods: {
                onType: function (event){
                    //here you can set any behavior
                },
                sendMessage: function(message){
                    axios.post("/chat/" + this.chat.id + "/send", {
                        message: message,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    })
                    .then((response) => {
                        // let newM = response.data.message;
                        // console.log(newM);
                        // this.chat.messages.push(newM);
                    }, (error) => {
                        // alert(error);
                    });
                },
                getChat: function(event){
                    let chat_id = document.getElementById('poolChat').dataset.id;
                    axios.get('/chat/'+chat_id).then((jsonData) => {
                        this.chat = jsonData.data.chat;
                        // console.log(this.chat);
                    }).catch((err)=> {
                        // alert(err);
                    });
                },
                toggleChat: function(){
                    if(this.chat){
                        this.chat.collapse = !this.chat.collapse;
                        this.scrollBottom();
                        this.markRead();
                    }

                },
                scrollBottom: function(){
                    if(this.chat.collapse === false){
                        var elem = document.getElementById('chatMessageContainer');
                        if(elem){
                            var scrollHeight = Math.max(elem.scrollHeight, elem.clientHeight);
                            elem.scrollTop = scrollHeight - elem.clientHeight;
                        }
                    }
                },
                markRead: function(){
                    if(this.chat.collapse === false) {
                        let chat_id = document.getElementById('poolChat').dataset.id;
                        axios.get('/chat/' + chat_id +'/read').then((jsonData) => {
                            this.chat.unread = 0;
                        }).catch((err) => {
                            // alert(err);
                        });
                        this.chat.messages.forEach(function(message){
                            message.unread = false;
                        })
                    }
                }
            },
            created() {
                this.getChat();

            },
            mounted() {
                // if(this.chat){
                let that = this;
                let chat_id = document.getElementById('poolChat').dataset.id;
                    Echo.channel('chat.'+chat_id)
                        .listen('.new.message', (data) => {

                            that.chat.messages.push(data.message);
                            if(that.chat.me.id !== data.message.user_id && that.chat.collapse){
                                that.chat.unread++;
                            }
                        });
                // }

            },
            updated() {

            }
        });
}
