import StandingsTable from './StandingsTable';
window._ = require('lodash');

if(document.getElementById('golfInfoStandingsApp'))
{
    const golfInfoStandingsApp = new Vue({
        el: '#golfInfoStandingsApp',
        components: {
            StandingsTable,
        },
        data: function () {
            return {
                status : null,
                type : 'score',
                standings : [],
                config : null,
                user : null,
                started : false,
                allLoaded : false,
            }
        },
        created() {
            this.getEntries();
        },
        methods : {
            getEntries :  function(e){
                let lastRecord = 0;
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                let url = document.getElementById('golfInfoStandingsApp').dataset.url;
                let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                axios.get(url).then((response) => {
                    if(response.data.error){
                        // alert.data.messages;
                    } else {
                        this.type = response.data.type;
                        let entries = response.data.entries;
                        // console.log(entries.length);
                        if(entries.length > 0){
                            entries.forEach((entry) => {
                                this.standings.push(entry);
                            });
                        } else {

                        }

                    }
                }, (error) => {

                });

            },


        },
        computed : {

        },
        mounted(){
            this.started = tourneyStarted;
            this.status = status;
             this.config = golfConfig;
            this.user = document.getElementById('golfInfoStandingsApp').dataset.user_id;
       }
    });
}
