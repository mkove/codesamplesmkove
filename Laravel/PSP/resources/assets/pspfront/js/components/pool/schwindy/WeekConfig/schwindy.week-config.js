import SchwindyWeeks from './SchwindyWeeks';

if(document.getElementById('schwindyWeekConfig'))
{
    const schwindyWeekConfig = new Vue(
        {
            el : '#schwindyWeekConfig',
            components: {
                'schwindyweeks' : SchwindyWeeks,
            },

            data: function () {
                return {
                    conf : {
                        weeks : [],
                        total_picks : 32,
                        season_year: "",
                        start_week: 1,
                        end_week : 18,
                        current_week : 1,
                        week_at_start: 1,
                        number_of_team_used : 1
                    },
                    new_created: 0
                }
            },
            created() {
                this.getConfig();
            },
            watch : {
                conf : function(ev){

                }
            },
            methods : {
                getConfig(e){
                    this.new_created = document.getElementById('schwindyWeekConfig').dataset.new_created;
                    let schwindy_id = document.getElementById('schwindyWeekConfig').dataset.id;
                    var self = this;
                    $.ajax({
                        url: '/schwindy/'+schwindy_id,
                        type: 'GET',
                        dataType: 'json',
                        data: {},
                    })
                        .done(function(returnData) {
                            self.conf = returnData.data.config;
                            self.conf.current_week = returnData.data.current_week;
                        })
                        .fail( function(xhr, textStatus, errorThrown) {
                            alert(errorThrown);
                        })
                        .always(function() {

                        });
                    // this.config = vueConfig;

                },

                // update(e){
                //     this.$forceUpdate();
                //     return ;
                // }
            },
        });
}
