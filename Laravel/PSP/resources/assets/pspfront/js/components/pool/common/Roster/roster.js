import RosterApp from "./RosterApp";
if (document.getElementById('squarePoolRoster')) {
    Vue.config.silent = true; // Set individual fields instead.
    const golfLive = new Vue({
        el: '#squarePoolRoster',
        components: {
            'square-roster': RosterApp,
        },
        data: function () {
            return {

            }
        },
        created() {

        },

        ready: function () {

        },

    });

}
