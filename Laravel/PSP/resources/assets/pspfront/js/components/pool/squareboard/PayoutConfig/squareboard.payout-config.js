    if (document.getElementById('payoutNCAA')) {
    const app = new Vue({
        el: '#payoutNCAA',
        data: function () {
            return {
                payoutData: {
                    payouts: {
                        pre: {
                            gameCount: 4,
                            enabled: true,
                            sum: 0,
                            total: 0,
                            name: "Prequalifying Games",
                            short: "pre",
                            round_id : 0,
                        },
                        first: {
                            gameCount: 32,
                            enabled: true,
                            sum: 0,
                            total: 0,
                            name: "1st Round",
                            short: "1",
                            round_id : 1,
                        },
                        second: {
                            gameCount: 16,
                            enabled: true,
                            sum: 0,
                            total: 0,
                            name: "2nd Round",
                            short: "2",
                            round_id : 2
                        },
                        third: {
                            gameCount: 8,
                            enabled: true,
                            sum: 0,
                            total: 0,
                            name: "3rd Round",
                            short: "3",
                            round_id : 3

                        },
                        fourth: {
                            gameCount: 4,
                            enabled: true,
                            sum: 0,
                            total: 0,
                            name: "4th Round",
                            short: "4",
                            round_id : 4,
                        },
                        semi: {
                            gameCount: 2,
                            enabled: true,
                            sum: 0,
                            total : 0,
                            name: "Semi Finals",
                            short: "semi",
                            round_id : 5
                        },
                        championship: {
                            gameCount: 1,
                            enabled: true,
                            sum: 0,
                            total: 0,
                            name: "Championship",
                            short: "Final",
                            round_id : 6
                        }
                    },
                    total: 0,
                    locked: false
                },
                loaded : false
            }
        },
        created() {
            this.loaded = false;
            var self = this;
            axios.get("/pool/" + poolId + "/squareboard/" + squareBoardId + "/payout")
                .then((response) => {
                    self.payoutData.payouts = response.data.payouts;
                    self.loaded = true;
                }, (error) => {
                    alert(error);
                })
        },
        methods: {
            getPayout: function () {


            },
            savePayout: function () {
                axios.post("/pool/" + poolId + "/squareboard/" + squareBoardId + "/payout", {
                    payouts: this.payoutData,
                    _token: $('meta[name="csrf-token"]').attr('content')
                })
                    .then((response) => {
                        if (!response.data.error) {
                            alert("Updated");
                        } else {
                            Alert("Error");
                        }
                        // this.payoutData = response.data.payouts;
                    }, (error) => {
                        alert(error);
                    })
            },
            payoutSum: function (payout) {
                var tmp = Number(payout.sum) * parseInt(payout.gameCount);
                // var sum = Number(payout.sum);
                payout.total = (isNaN(payout.total)) ? 0 : tmp;
                return payout.total;
            },
            updatePayout: function (payout) {
                if (!payout.enabled) {
                    payout.sum = 0;
                }
            }
        },
        computed: {
            finalTotal: function () {
                var t = 0;
                var payouts = this.payoutData.payouts;
                Object.keys(payouts).forEach(function (round) {
                    item = payouts[round];
                    if(item.enabled){
                        t = t + item.total;
                    }
                });
                this.total = t;
                return t;

            }
        }
    });
}
