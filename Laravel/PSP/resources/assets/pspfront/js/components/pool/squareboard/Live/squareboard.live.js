import SquareBoardLiveApp from './SquareBoardLiveApp';
window.moment = require('moment');

if (document.getElementById('squareBoardLiveApp')) {

	const app = new Vue({
		el: '#squareBoardLiveApp',
		components : {
			'square-board-live-app' : SquareBoardLiveApp
		},
		data : function () {
			return { squareboard : {}, squares : [], game : {}, numbers : {}, activequarter: '', updated : null }
		},
		created(){
			this.squares = squares;
			this.numbers = numbers;
			this.game = game; 
		},
		mounted() {
			this.updated = "Recently";
			let squareboard_id = document.getElementById('squareBoardLiveApp').dataset.id;

			Echo.channel('live.squareboard.'+squareboard_id)
				.listen('.squareboard.live.data', (data) => {
					console.log('squareboard.live.data listen ============>', data)
					this.updated = moment().format('h:mm:ss a');
					this._updateGame(data.data.game);
					this._updateSquares(data.data.squares);
				});
		},
		methods : {
			_updateGame : function(game){
				this.game = game;

				this.$set(this.game.hometeam,'q1',game.hometeam.q1);
				this.$set(this.game.hometeam,'q2',game.hometeam.q2);
				this.$set(this.game.hometeam,'q3',game.hometeam.q3);
				this.$set(this.game.hometeam,'q4',game.hometeam.q4);
				this.$set(this.game.hometeam,'ot',game.hometeam.ot);
				this.$set(this.game.hometeam,'total',game.hometeam.total);
				this.$set(this.game.awayteam,'q1',game.awayteam.q1);
				this.$set(this.game.awayteam,'q2',game.awayteam.q2);
				this.$set(this.game.awayteam,'q3',game.awayteam.q3);
				this.$set(this.game.awayteam,'q4',game.awayteam.q4);
				this.$set(this.game.awayteam,'ot',game.awayteam.ot);
				this.$set(this.game.awayteam,'total',game.awayteam.total);
				this.$set(this.game,'timer',game.timer);
				this.$set(this.game,'status',game.status);
			},
			_updateSquares : function(squares){

				this.squares.forEach(function(square){
					let sqId = square.id;
					let newSquare = squares.find(function(sq){
						return sq.id == sqId;
					});
					console.log("Fundnew", newSquare, square);
					square.class = newSquare.class;
					square.wins = newSquare.wins;
				})
			}
		}

	})
}
