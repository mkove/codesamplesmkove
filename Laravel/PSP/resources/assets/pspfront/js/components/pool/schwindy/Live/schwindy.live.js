import SchwindyLive from './SchwindyLive.vue';
import SchwindyLiveScoreTable from './SchwindyLiveScoreTable';
// import Pusher from 'pusher-js';
window.moment = require('moment');
window._ = require('lodash');

if(document.getElementById('schwindyLiveApp'))
{
    const schwindyLiveApp = new Vue({
        el: '#schwindyLiveApp',
        components: {
            'schwindy-live' : SchwindyLive,
            'schwindy-live-score-table' : SchwindyLiveScoreTable
        },
        data: function () {
            return {
                userId : null,
                games : {
                    activeGames : []
                },
                picks : {},
                userTeams : {},
                schwindy_id : null,
                error : false,
                message : "",
            }
        },
        created() {
            let schwindy_id = document.getElementById('schwindyLiveApp').dataset.id;
            let user_id = document.getElementById('schwindyLiveApp').dataset.user_id;
            this.userId = user_id;
            var that = this;
            this.getSchwindyLive();


            Echo.channel('schiwndy.'+schwindy_id)
                .listen('.schwindy.live.update', (data) => {
                    that.games.activeGames = data.games;
                    that.picks = data.picks;
                    that.schwindy_id = data.schwindy_id;
                    that.userTeams = data.userTeams;
                    that.error = data.error;
                    that.message = moment().format('h:mm:ss a');
                });
                this.games = that.games;
                this.picks = that.picks;
                this.schwindy_id = that.schwindy_id;
                this.userTeams = that.userTeams;
                this.error = that.error;
                this.message = that.message;

        },
        methods : {

            getSchwindyLive : async function(e){
                let schwindy_id = document.getElementById('schwindyLiveApp').dataset.id;
                axios.get('/schwindy/'+schwindy_id+'/live/get').then((jsonData) => {
                    this.games.activeGames = jsonData.data.games;
                    this.picks = jsonData.data.picks;
                    this.schwindy_id = jsonData.data.schwindy_id;
                    this.userTeams = jsonData.data.userTeams;
                    this.error = jsonData.data.error;
                    this.message = moment().format('h:mm:ss a');

                }).catch((err)=> {
                    // alert(err);
                });
            },
        },
    });
    // import SchwindyLive from './resources/assets/pspfront/js/components/pool/schwindy/SchwindyLive';


}
