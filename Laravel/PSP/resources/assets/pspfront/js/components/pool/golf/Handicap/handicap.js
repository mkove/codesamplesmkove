import PlayersContainer from "./PlayersContainer";

if(document.getElementById('golfPlayersHandicap'))
{
    const golfGroupApp = new Vue({
        el: '#golfPlayersHandicap',
        components: {
            PlayersContainer,
        },
        data: function () {
            return {
                players : [],
                groupcount : 0,
                groupedPlayers : [],
                golfId : null,
            }
        },
        created() {
            this.golfId = document.getElementById('golfPlayersHandicap').dataset.golf_id;
            this.getGolfPlayers(this.golfId);
        },
        methods : {
            getGolfPlayers : function(id){
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.get('/golf/'+id+'/golf-tournament-player/get-players').then((jsonData) => {
                    this.players =jsonData.data.data.players;
                    let groupedPlayers = {};
                    this.players.forEach(function(player){
                        let grpNum = player.pivot.group_number;
                        let groupStr = "group_"+grpNum;

                        if(!groupedPlayers[groupStr]){
                            groupedPlayers[groupStr] = [];
                        }

                        groupedPlayers[groupStr].push(player);
                    });
                    this.groupedPlayers = groupedPlayers;
                    this.groupcount = jsonData.data.data.groupcount;

                }).catch((err)=> {
                    // alert(err);
                });
            },
            updateHandicap : function(pId, handicap){
                let player = this.players.find(function(p){
                    return (p.id === pId);
                });
                if(player){
                    Vue.set(player.pivot, 'handicap', handicap);
                    let url = "/golf/"+this.golfId+"/golf-tournament-player/update-handicap";

                    let data = {
                        gtp_id : pId,
                        handicap : handicap,
                        _token : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                    };
                    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

                    axios.post(url, data).then((jsonData) => {
                        Vue.$notify({
                            group: 'usernotices',
                            title: 'Handicap Updated',
                            text: 'Updated!'
                        });
                    }).catch((err)=> {
                        // alert(err);
                    });

                }
                //TOD find player update HTP
            },
        },

        computed : {
            playersLoaded :  function(){
                return (this.players.length > 0);
            }
        }
    });
}
