import LiveContainer from "./LiveContainer";
window.moment = require('moment');
window._ = require('lodash');

if(document.getElementById('golfLive'))
{
    Vue.config.silent = true; // Set individual fields instead.
    const golfLive = new Vue({
        el: '#golfLive',
        components: {
            LiveContainer,
        },
        data: function () {
            return {
                updatedd : moment().format('MMMM Do, h:mm:ss a'),
                entries : [],
                entries2 : [],
                entryCount : 0,
                sort : 'total',
                user : null,
                players : [],
                // players2 : [],
                tournament : null,
                sinceupdate : 0,
                toscore : 0,
                round : null,
                type : null,
                count_cut : false,
                handicaps : false,
                prize_final : false,
            }
        },
        created() {
            let golf_id = document.getElementById('golfLive').dataset.id;
            let user_id = document.getElementById('golfLive').dataset.user_id;
            this.toscore = document.getElementById('golfLive').dataset.to_score;
            this.count_cut = document.getElementById('golfLive').dataset.count_cut;
            this.round = document.getElementById('golfLive').dataset.round;
            this.type = document.getElementById('golfLive').dataset.type;
            this.handicaps = document.getElementById('golfLive').dataset.handicaps;
            this.prize_final = document.getElementById('golfLive').dataset.prize_final;

            this.getGolfLivePlayers();
            this.getGolfLiveEntries();
            this.user = liveData.user;
            this.entryCount = liveData.entryCount;
            this.players = liveData.players;
            this.tournament = liveData.tournament;
            setInterval(this.incrementTime, 1000);

            let sub = Echo.channel('live.golf.'+golf_id)
                .listen('.golf.live.update', (data) => {
                    // Vue.set(vm.items, indexOfItem, newValue)
                    data.players.forEach((player) => {
                        let indexOfItem = this.players.findIndex(p => p.id === player.id);
                        Vue.set(this.players, indexOfItem, player)
                    });

                    data.entries.forEach((entry) => {
                        let indexOfItem = this.entries2.findIndex(e => e.id === entry.id);
                        Vue.set(this.entries2, indexOfItem, entry)
                    });
                    this.prize_final = data.prize_final;
                    // this.entries2 = data.entries;
                    this.updatedd = moment().format('MMMM Do, h:mm:ss a');
                    this.sinceupdate = 0;
                });

        },

        mounted() {
            setTimeout(() => {
                window.scrollTo(0,0);
            }, 1000);
        },

        ready: function () {

        },
        methods : {
            incrementTime : function() {
                this.sinceupdate = parseInt(this.sinceupdate) + 1;
            },
            getGolfLivePlayers : async function(e){
                return true;
                // axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                // let url = document.getElementById('golfLive').dataset.players_url;
                // let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                // axios.get(url).then((response) => {
                //     if(response.data.error){
                //         // alert.data.messages;
                //     } else {
                //         let liveData = response.data.liveData;
                //         this.tournament = liveData.tournament;
                //         if(liveData.players){
                //             liveData.players.forEach((player) => {
                //                 this.players.push(player);
                //             });
                //         } else {
                //             // console.log("Live data never came ", liveData);
                //         }
                //
                //     }
                // }, (error) => {
                //
                // });
            },
            getGolfLiveEntries : async function(e){
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                let url = document.getElementById('golfLive').dataset.url;
                if(this.round){
                    url = url+"?round="+this.round;
                }
                let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                axios.get(url).then((response) => {
                    if(response.data.error){
                        // alert.data.messages;
                    } else {
                        let liveData = response.data.liveData;
                        liveData.entries2.forEach((entry) => {
                            this.entries2.push(entry);
                        });

                    }
                }, (error) => {

                });

            },

        },
        computed : {
            hasHandicaps : function(){
                return (this.handicaps > 0);
            },
            getUpdatedAt : function(){
                return this.updatedd;

                let secs = (moment() - this.updatedd)/60;
                if(secs > 60){
                    let mins = secs/60;
                    return Math.round(mins) + " <small>mins. ago</small>";
                }
                return  Math.round(secs) + " <small>sec. ago</small>";
            }
        }
    });
}
