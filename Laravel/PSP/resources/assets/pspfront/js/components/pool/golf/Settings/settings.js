//npm install --save luxon vue-datetime weekstart
//npm install vue2-editor
const contextVars = [];

import SettingsContainer from "./SettingsContainer";

if(document.getElementById('golfPoolSettingsApp'))
{
    const golfPoolSettingsApp = new Vue({
        el: '#golfPoolSettingsApp',
        components: {
            SettingsContainer
        },
        data: function () {
            return {
                redirect : null,
            }
        },
        created() {
            this.redirect = document.getElementById('golfPoolSettingsApp').dataset.redirect;
        },
        methods : {

        },
        computed : {

        },
        mounted(){

        }
    });

    // Vue.mixin({
    //     methods: {
    //         __c : function(label, def){
    //             return this.getContext(label, def);
    //         },
    //         getContext: function (label, def) {
    //             return (contextVars.length && contextVars[label].length) ? contextVars[label] : def;
    //         },
    //     },
    // })
}
