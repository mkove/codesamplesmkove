require('jquery-ui');
require('jquery-ui/ui/widgets/sortable');
require('jquery-ui/ui/widgets/droppable');
require('jquery-ui/ui/widgets/draggable');
require('jquery-ui/ui/disable-selection');

import 'jquery-ui/ui/widgets/draggable.js';
import 'jquery-ui/ui/widgets/draggable.js';
import 'jquery-ui/ui/disable-selection.js';

$(document).ready(function(){
    $('.sortablePlayers').sortable({
        start: function(event, ui) {
            // item = ui.item;
            // newList = oldList = ui.item.parent().parent();
        },
        stop: function(event, ui) {
            // alert("Moved " + item.text() + " from " + oldList.attr('id') + " to " + newList.attr('id'));
        },
        change: function(event, ui) {
            // if(ui.sender) newList = ui.placeholder.parent().parent();
        },
        connectWith: ".sortablePlayers"
    }).disableSelection();

    $('#saveList').on('click', function(ev){
        var url = $(this).data('url');
        var groupData = {};
        $.each($('.sortablePlayers'), function(i,itm){
            var groupNum = $(itm).data('group_number');
            var listItems = $("#group-"+groupNum+" li");
            listItems.each(function(idx, li) {
                let id = $(li).data('golf_player_id');
                groupData[id] = {
                    'group_number' : groupNum,
                };
            });
        });
        var data = {
            group_data : groupData,
            op : 'update'
        };
        runPSPAjax(url,"post", data);
    });

});
