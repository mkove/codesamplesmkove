import GolfEntryPicker from "./GolfEntryPicker";
import ModalPopup from "./ModalPopup";
import VueCookies from 'vue-cookies';
import VuejsDialog from 'vuejs-dialog';
// import VuejsDialogMixin from 'vuejs-dialog/dist/vuejs-dialog-mixin.min.js'; // only needed in custom components
// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

window._ = require('lodash');

// Tell Vue to install the plugin.
Vue.use(VuejsDialog);

Vue.use(VueCookies);

if(document.getElementById('golfPickApp'))
{
    const golfPickApp = new Vue({
        el: '#golfPickApp',
        components: {
            GolfEntryPicker,
            ModalPopup
        },
        data: function () {
            return {
                title : null,
                showModal : false,
                message : false,
                userpicks : false,
                userId : null,
                current_user_id : null,
                golf : null,
                entry : null,
                user : null,
                pending : false,
                pendingSave : false,
                loading : true,
                locked : false,
                create_entry_error : false,
                rename_entry_error : false,
            }
        },
        beforeMount() {
            window.addEventListener("beforeunload", this.preventNav);
        },

        beforeDestroy() {
            window.removeEventListener("beforeunload", this.preventNav);
        },

        created() {
            this.getGolfPool();

        },
        methods : {
            preventNav : function(event){

                if (!this.pending) return;

                event.preventDefault();
                event.returnValue = "";
            },
            getGolfPool :  function(e){
                let golf_id = document.getElementById('golfPickApp').dataset.id;
                let entry_id = document.getElementById('golfPickApp').dataset.entry_id;

                this.userpicks = document.getElementById('golfPickApp').dataset.userpicks;

                if(!this.userpicks){
                    this.current_user_id = document.getElementById('golfPickApp').dataset.user_id;
                }
                axios.get('/golf/'+golf_id+'/show-vue').then((jsonData) => {
                    this.golf = jsonData.data.golf;
                    this.overwrite = jsonData.data.overwrite_lock;
                    this.locked = jsonData.data.locked;
                    // if(!this.locked){
                        this.golf.users = jsonData.data.golf.users.filter(function(user){
                            return (user.id != null);
                        });
                        if(!this.userpicks){
                            //lets find a user and assign it to user
                            let idToFind = parseInt(this.current_user_id);

                            let userToSet = this.golf.users.find(function (user) {
                                return user.id === idToFind;
                            });
                            // cl("test","test1","testg2");
                            this.user = userToSet;
                            if(entry_id){
                                this.selectEntryById(parseInt(entry_id));
                            } else {
                                let lastEntry = this.findLastEntry();
                                // let lastEntry = this.$cookies.get("lastEntry");
                                if(lastEntry){
                                    this.selectEntryById(lastEntry.id);
                                } else {
                                    if (this.user.entries[0]!==undefined){
                                        let firstEntry =  this.user.entries[0]
                                        this.selectEntryById(parseInt(firstEntry.id));
                                    }
                                }
                            }


                        }
                    this.loading = false;
                    // }


                }).catch((err)=> {
                    // alert(err);
                });
            },
            findLastEntry : function(){
                if(this.user){

                    let lastDate = this.user.entries.map(function(e) { return e.updated_at; }).sort().reverse()[0]
                    let lastEntry = this.user.entries.find(function(entry){
                        return (entry.updated_at == lastDate);
                    });
                    return (lastEntry) ? lastEntry : this.user.entries.find(e => true);
                }
                return null;
            },
            selectUser : function(user){
                this.user = user;
                this.sendSpyPost('view');

                //SEND PSY
                // $targetUserId = $request->get('user_id');
                // $entryId = $request->get('entry_id');
                // $action = $request->get('action');
            },
            cancelCreateEntry : function(){
                this.create_entry_error = false;
            },
            cancelRenameEntry : function(){
                this.rename_entry_error = false;
            },
            createEntry : function(user, entryName){
                if(this.picksAllowed){
                    let maxEntries = parseInt(this.golf.max_pool_per_user);
                    let userCount = this.user.entries.length;
                    //UNDER max entries OR commissioner and adding to self
                    let comishAllow = false;
                    if(this.user.commissioner && maxEntries <= userCount && (this.user.commissioner && this.user.id === user.id)){

                        comishAllow = confirm("You are exceeding maximum Entry count of "+maxEntries);
                    }
                    if(maxEntries > userCount || (this.user.commissioner && this.user.id === user.id && comishAllow)){
                        if(entryName.length > 25){
                            this.title = "Error: Name is too long";
                            this.message = "Please limit entry name to 25 characters";
                            this.showModal = true;
                            this.create_entry_error = "Please limit entry name to 25 characters";
                            // console.log("Entre 1",this.create_entry_error);
                        } else {
                            // console.log("Lengths is n", entryName.length);
                            if(entryName.length){
                                this.loading = true;
                                var url ='/golf/'+this.golf.id+"/entry";
                                let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                                let golfId = this.golf.id;
                                let userId = user.id;
                                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

                                axios.post(url, {
                                    user_id : user.id,
                                    name : entryName,
                                    _token : _token,
                                }).then((response) => {
                                    if(response.data.error){
                                        // alert.data.messages;
                                        let messageText="";
                                        response.data.messages.forEach(function(err){
                                            messageText += "<div class=' p-3 m-0 alert-"+err.type+"'>"+err.message+"</div>";
                                        });

                                        this.title = "Error:";
                                        this.message = messageText;
                                        this.showModal = true;
                                        this.create_entry_error = "Error. Please try again.";
                                        this.loading = false;
                                        // console.log("Entre 2",this.create_entry_error);
                                    } else {
                                        let newEntry = response.data.data.entry;
                                        newEntry.unsaved = true;
                                        this.user.entries.push(newEntry);
                                        this.entry = newEntry;
                                        this.$set(this.entry, "unsaved", true);
                                        this.sendSpyPost('create');
                                        this.loading = false;
                                        this.create_entry_error = false;
                                        // console.log("Entre 3",this.create_entry_error);
                                    }

                                }, (error) => {
                                    this.title = "ERROR:";
                                    this.message = "Please try again";
                                    this.showModal = true;
                                    this.create_entry_error = "Error. Please try again.";
                                    this.loading = false;
                                    // console.log("Entre 4",this.create_entry_error);
                                    // alert(error);
                                });
                            } else {
                                // console.log("we a re here ",this.create_entry_error);
                                this.title = "Error!";
                                this.message = "Entry name cannot be empty";
                                this.showModal = true;
                                this.create_entry_error = "Entry name cannot be empty";
                                this.loading = false;
                                // console.log("Entre 5",this.create_entry_error);
                            }
                        }

                    } else{

                        this.title = "Cannot add entry!";
                        this.message = "Max entries per user reached!";
                        this.showModal = true;
                        this.create_entry_error = "Max Entry Reached";
                        this.loading = false;
                        // console.log("Entre 6",this.create_entry_error);

                        // this.create_entry_error = false;
                    }
                } else {
                    this.showLockedMessage();
                }


            },
            deleteEntry : function(entry){
                let options = {
                    reverse: false, // switch the button positions (left to right, and vise versa)
                    okText: 'DELETE',
                    cancelText: 'Cancel',
                    customClass: 'confirm-dialog' // Custom class to be injected into the parent node for the current dialog instance
                };
                let ret = this.$dialog
                    .confirm("Are you sure you want to permanently delete this entry?", options)
                    .then((dialog) =>  {
                        this.loading = true;
                        let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                        let golfId = this.golf.id;
                        let entryId = entry.id;
                        this.sendSpyPost('delete');
                        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

                        axios.post('/golf/'+golfId+"/entry/"+entryId, {
                            user_id : this.user.id,
                            _method : 'DELETE',
                            _token : _token,
                        })
                        .then((response) => {
                            if(response.data.error){

                            } else {
                                let entryId = this.entry.id;
                                let deletedEntryIndex = this.user.entries.findIndex(function(entry){
                                    return (entry.id === entryId);
                                });
                                this.user.entries.splice(deletedEntryIndex, 1);
                                this.entry = null;
                                this.loading = false;
                            }
                        }, (error) => {
                            this.title = "ERROR:";
                            this.message = "Please try again";
                            this.showModal = true;
                            // alert(error);
                        });
                    })
                    .catch(function() {
                        return false;
                    });

                return true;

            },
            selectEntryById : function(entryId){
                if(entryId){
                    let localEntry = this.user.entries.find(function(r){
                        return (r.id === entryId);
                    });
                    if(localEntry){
                        this.entry = localEntry;
                    }
                }
            },
            pickEntry : function(entry){
              this.entry = entry;
                this.sendSpyPost('view');
                this.$cookies.set("lastEntry", this.entry.id);
            },
            saveEntry : function(entry){

                if(this.picksAllowed){
                    let totalPicks = 0;
                    let max = 0;
                    let grps = Object.values(this.golf.config.groups);
                    grps.forEach(function(g){
                        if(g.enabled){
                            max = max + parseInt(g.max);
                        }
                    });
                    entry.groups.forEach(function(group){
                        totalPicks = totalPicks + group.tournament_players.length;
                    });

                    let entryFull = (max === totalPicks);
                    let proceed = entryFull;
                    if(!proceed) {
                        let options = {
                            reverse: false, // switch the button positions (left to right, and vise versa)
                            okText: 'Save',
                            cancelText: 'Cancel',
                            customClass: 'confirm-dialog' // Custom class to be injected into the parent node for the current dialog instance
                        };
                        let ret = this.$dialog
                            .confirm("Your entry is incomplete, continue?", options)
                            .then((dialog) =>  {
                                this._saveEntry(entry);
                            })
                            .catch(function() {
                                return false;
                            });
                        return true;
                    }
                    this._saveEntry(entry);
                    this.sendSpyPost('update');
                } else {
                    this.showLockedMessage();
                }

            },
            _saveEntry : function(entry){
                this.loading = true;
                let url = "/golf/"+this.golf.id+"/entry/"+entry.id;
                let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(url, {
                    entry : entry,
                    _token : _token,
                    _method : "PATCH",
                    user_id : entry.user_id
                })
                    .then((response) => {
                        if(response.data.error){

                        } else {
                            let newEntry = response.data.data;
                            let user = this.golf.users.find(function(user) {
                                return (user.id === newEntry.user_id);
                            });
                            let oldEntryIndex = user.entries.findIndex(function(entry){
                                return (entry.id === newEntry.id);
                            });
                            this.pending = false;
                            entry.unsaved = false;
                            newEntry.unsaved = false;
                            user.entries.splice(oldEntryIndex, 1, newEntry);

                            this.title = "Updated!";
                            this.message = "Your entry was saved!";
                            this.showModal = true;
                            this.loading = false;
                            this.setUnsavedFlag(false);
                        }
                    }, (error) => {
                        alert(error);
                    });

            },
            renameEntry : function(entry, newName){
                if(newName.length > 25){
                    this.title = "Error: Name is too long";
                    this.message = "Please limit entry name to 25 characters";
                    this.showModal = true;
                    this.rename_entry_error = "Please limit entry name to 25 characters";
                } else {
                    if(newName.length){
                        Vue.set(this.entry,'name',newName);
                        this.setUnsavedFlag(true);
                        this.rename_entry_error = false;
                        // entry.name = newName;
                        // entry.unsaved = true;
                        // this.$set(this.members, newMember.name, newMember);
                        this.pending = true;
                        this.sendSpyPost('update');
                    } else {
                        this.title = "Error!";
                        this.message = "Name cannot be empty";
                        this.showModal = true;
                        this.rename_entry_error = "Name cannot be empty";
                    }
                }

            },
            clearEntry : function(entry){
                if(this.picksAllowed){
                    let options = {
                        reverse: false, // switch the button positions (left to right, and vise versa)
                        okText: 'Clear',
                        cancelText: 'Cancel',
                        customClass: 'confirm-dialog' // Custom class to be injected into the parent node for the current dialog instance
                    };
                    let ret = this.$dialog
                        .confirm("Are you sure you want to clear your entry?", options)
                        .then((dialog) =>  {
                            entry.groups.forEach(function(group){
                                group.tournament_players.splice(0,group.tournament_players.length);
                            });
                            this.pending = true;
                            this.setUnsavedFlag(true);
                        })
                        .catch(function() {
                            return false;
                        });

                    return true;
                } else {
                    this.showLockedMessage();
                }
            },
            pickPlayer : function(golf_tournament_player, group) {
                if(this.picksAllowed){
                    if(this.underMaxPicks(group)){
                        //They are trying to add golf_tournament_player that's not in tournament.
                        if(this.isTournamentPlayer(golf_tournament_player)){
                            //they are trying to add tournament golf_tournament_player BUT he's not been grouped by comish
                            if(this.isPlayerGrouped(golf_tournament_player, group.number) || this.isPlayerUpgrade(golf_tournament_player, group.number)){
                                group.tournament_players.push(golf_tournament_player);
                                this.setUnsavedFlag(true);
                                // this.entry.unsaved = true;
                                this.pending = true;
                            }
                        }
                    } else {
                        this.title = "Unable to pick";
                        this.message = "Max players picked";
                        this.showModal = true;
                    }
                } else {
                   this.showLockedMessage();
                }


            },
            saveAll : function(user){
                let self = this;
                user.entries.forEach(function(entry){
                    self.saveEntry(entry);
                });
                this.pending = false;
            },
            unpickPlayer : function(golf_tournament_player, group){
                if(this.picksAllowed){
                    this.pending = true;
                    // this.entry.unsaved = true;
                    this.setUnsavedFlag(true);
                    let indexOfGroupedPlayer = group.tournament_players.findIndex(function(gtp){
                        return (gtp.id === golf_tournament_player.id);
                    });
                    let res = group.tournament_players.splice(indexOfGroupedPlayer, 1);

                } else {
                    this.showLockedMessage();
                }


            },
            movePlayer : function(playerId, targetGroupId){

            },
            isTournamentPlayer : function(golf_tournament_player){

                return this.golf.players.find(function(gtp){
                    return (gtp.golf_tournament_player_id === golf_tournament_player.id);
                });
            },
            isPlayerGrouped : function(golf_tournament_player, groupNumber){
                return this.golf.players.find(function(gtp){
                    return (gtp.golf_tournament_player_id === golf_tournament_player.id && gtp.group_number === groupNumber);
                });
            },
            isPlayerUpgrade : function(golf_tournament_player, groupNumber){
                if(this.golf.lower_to_higher > 0){
                    return this.golf.players.find(function(gtp){
                        //is grouped and group is higher than wanted group
                        return (gtp.golf_tournament_player_id === golf_tournament_player.id && gtp.group_number > groupNumber);
                    });
                }
                //no jumping
                return false;
            },
            underMaxPicks : function(group){
                let curPicks = group.tournament_players.length;
                let groupNumber = group.number;
                let groupConfig = this.golf.config.groups[groupNumber];
                if(groupConfig){
                    return (parseInt(groupConfig.max) > curPicks && groupConfig.enabled);
                }
                return false;
            },
            setUnsavedFlag : function(flag){
                Vue.set(this.entry, 'unsaved', flag);

            },
            sendSpyPost : function(action){
                var url ='/golf/'+this.golf.id+"/ispy";
                let _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(url, {
                    user_id : this.user.id,
                    entry_id : (this.entry) ? this.entry.id : null,
                    action : action,
                    _token : _token
                }).then((response) => {

                }, (error) => {

                });
            },
            showLockedMessage : function(){
                this.title = "Picks are locked";
                this.message = "You cannot update your entry at this time!";
                this.showModal = true;
            }
        },
        computed : {
            isPendingChanges : function(){
                return this.pending;
            },
            createEntryErrors : function(){
                return this.create_entry_error;
            },
            renameEntryErrors : function(){
                return this.rename_entry_error;
            },
            picksAllowed : function(){
                if(this.golf && this.golf.locked) return false;
                if(this.golf){
                    return (this.userpicks || this.golf.pickable || (this.golf.pickable && !this.golf.entry_lock));
                }
                return false;
            }
        }
    });
}
