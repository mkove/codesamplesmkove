$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// window.Pusher = require('pusher-js');
// window.Popper = require('popper.js');
// window.moment = require('moment');

// window.axios = require('axios');
// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

// window.io = require('socket.io-client');

$(document).on('click', '.copyLink', function(){
    var id = $(this).data('container_id');
    var copyText = document.getElementById(id);
    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
    $(this).html("<i class='fa fa-check'></i> Copied!")
}); 

$(document).on('click', '#contentEditButton', function(){
    var content = $('#contentEditText').val();
    var id = $('#contentEditText').data('id');
    var reloadable = '.reload-'+id;
    $.ajax({
        url: '/admin/content/'+id,
        type: 'POST',
        dataType: 'json',
        data: {
            content : content,
            _method: 'PUT',
            _token : $('input[name="_token"]').val()
        },
    })
        .done(function(jsonData) {
            if(jsonData.error){
                alert("Error Update");
            } else {

            }
        })
        .fail(function() {

        })
        .always(function() {

        });

    $(reloadable).each(function() {
        $( this ).addClass( "bg-brand-green" );
        $(this).html(content);
    });
})

$(document).on('click', '#contentEditButtonParagraph', function(){
    var content = $('#contentEditTextParagraph').val();
    var id = $('#contentEditTextParagraph').data('id');
    var reloadable = '.reload-'+id;
    $.ajax({
        url: '/admin/content/'+id,
        type: 'POST',
        dataType: 'json',
        data: {
            content : content,
            _method: 'PUT',
            _token : $('input[name="_token"]').val()
        },
    })
        .done(function(jsonData) {
            if(jsonData.error){
                alert("Error Update");
            } else {

            }
        })
        .fail(function() {

        })
        .always(function() {

        });

    $(reloadable).each(function() {
        $( this ).addClass( "bg-brand-green" );
        $(this).html(content);
    });
})

$('#contentEditTypeTextPopup').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var content = button.data('content') // Extract info from data-* attributes
    var varpath = button.data('identifier');
    console.log(button,id, content, varpath);
    $('#contentEditTypeTextVariablePath').html(varpath);
    $('#contentEditTypeTextTitle').html("Simple Text");
    $('#contentEditTypeTextInput').val(content);
    $('#contentEditTypeTextButton').data('id', id);
})

$('#contentEditTypeParagraphPopup').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var content = button.data('content') // Extract info from data-* attributes
    var varpath = button.data('identifier');
    $('#contentEditTypeParagraphVariablePath').html(varpath);
    $('#contentEditTypeParagraphTitle').html("Paragraph");
    $('#contentEditTypeParagraphInput').val(content);
    $('#contentEditTypeParagraphButton').data('id', id);
})

$(document).on('click', '.toggle-nav', function(ev){
    var menu = $(this).data('menu');

    if($('#'+menu).hasClass('collapsable-nav-open')){
        $('#'+menu).removeClass('collapsable-nav-open');
    } else {
        //first close any open ones
        $('.collapsable-nav').removeClass('collapsable-nav-open');
        $('#'+menu).addClass('collapsable-nav-open');
    }
});

$(document).on('click', '.dd-menu-toggler', function(){
    var toggleId = $(this).data('dd');

    if($('#'+toggleId).hasClass('dd-menu-open')){
        $('.dd-menu').removeClass('dd-menu-open');
        $('#'+toggleId).removeClass('dd-menu-open');
    } else {
        $('.dd-menu').removeClass('dd-menu-open');
        $('#'+toggleId).addClass('dd-menu-open');
    }
})

$(document).on('click', '.verify-user', function(e){
    var $self = $(this);
    var verified = ($self.data('verified') === 1) ? 0 : 1;
    var userId = $self.data('user_id');
    var url = $self.data('url');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {
            user_id : userId,
            verified : verified,
            _token : $('input[name="_token"]').val()
        },
    })
        .done(function(jsonData) {
            if(jsonData.error){
                alert("Error verifying user");
            } else {
                $self.data('verified', verified);
                if(jsonData.data.verified == 1){
                    $('#userVerified-'+userId).html("<i class='fa fa-check text-brand-blue'></i>");
                    // alert('Confirmed');

                } else {
                    $('#userVerified-'+userId).html("");
                    // alert("Unconfirmed");
                }
            }
        })
        .fail(function() {

        })
        .always(function() {

        });
})

$('#myModal').on('shown.bs.modal', function() {
    $(document).off('focusin.modal');
});

window.cl = function(){
}

window.get_psp_config = function(key){
    if(window[key].length){
        return window[key].value;
    }
    return "";
}

window.runPSPAjax = function(url, method, data, silent = false, reloadable = null, autohide = true, responseData = { data : null}, async = true){
    var ok = true;
    if(method.toLowerCase() === "post"){
        data._token = $('meta[name="csrf-token"]').attr('content');
    }
    if(ok){
        if(reloadable){
            $(reloadable).html("<div class='alert alert-info'><p><i class='fa fa-spin fa-gears'></i> Working.... </p></div>");
        }
        $.ajax({
            url: url,
            method : method,
            async : async,
            data : data,
            dataType : 'json',
        }).done(function(jsonData, headerResponse) {
            if(headerResponse.status == 422){
                // responseData.data = jsonData;
            }
            if(!silent){

                var messages = jsonData.messages;
                $.each(messages, function(i, m){
                    $.notify({
                        message: m.message,
                        autoHide: autohide
                    },{
                        type: m.type
                    });
                })
            }
            if(reloadable){
                $(reloadable).html(jsonData.html);
            }
            responseData.data = jsonData;
        }).fail(function( jqXHR, textStatus ) {
            if(jqXHR.status == 422) {
                //validation failed
                var messages = jqXHR.responseJSON.errors;
                $.each(messages, function(i, m){
                    $('[name*="+i+"]').closest('.form-group').addClass('has-error');
                    $.notify({
                        message: "<h3>"+m+"</h3>"
                    },{
                        type: 'danger'
                    });
                })
                responseData.data = null;
            }
            if(reloadable){
                $(reloadable).html("<div class='alert alert-danger'><p><i class='fa fa-exclamation'></i> Error! Refresh this page and try again!</p></div>");
            }
        }).always(function() {
            // alert( "complete" );
        });
    } else {
        return false;
    }
    return true;
};


$('#signupForm').submit(function(){
    var agreed = $('#agree').prop("checked");
    var agreed_age = $('#agree_age').prop("checked");
    if(!agreed){
        alert("Please accept Terms & Conditions");
        return false; // will halt submission
    }

    if(!agreed_age){
        alert("Please confirm you're over age of 18");
        return false; // will halt submission
    }

});