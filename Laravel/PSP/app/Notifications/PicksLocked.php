<?php

    namespace App\Notifications;

    use Illuminate\Bus\Queueable;
    use Illuminate\Notifications\Notification;
    use Illuminate\Contracts\Queue\ShouldQueue;

    class PicksLocked extends Notification implements ShouldQueue
    {
        use Queueable;

        // ...
    }
