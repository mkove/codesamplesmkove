<?php
	namespace App\Models;
	use App\Helpers\SiteHelper;
    use Illuminate\Database\Eloquent\Model as Eloquent;


	class Game extends Eloquent
	{

		protected $table = 'games';
        public function formatted_start($format)
		{
			return date($format, strtotime($this->start));
		}

//		public $with = ['home_team', 'away_team','events'];

//    ALTER TABLE games ADD FULLTEXT fulltext_index (tournament_name)
        protected  $fillable = [
            'contestID',
            'tournament_id',
            'tournament_name',
            'season',
            'sport',
            'sport_id',
            'league',
            'week',
            'timezone',
            'time',
            'contestID',
            'start',
            'gmt',
            'status',
            'round_id',
            'round',
            'round_name',
            'round_shortname',
            'hometeam_shortname',
            'awayteam_shortname',
            'awayteam_halftime',
            'hometeam_halftime',
            'stadium',
            'stadium_id',
            'status',
            'season_id',
            'hometeam_name',
            'hometeam_id',
            'hometeam_q1',
            'hometeam_q2',
            'hometeam_q3',
            'hometeam_q4',
            'hometeam_ot',
            'hometeam_totalscore',
            'season_year',
            'awayteam_name',
            'awayteam_id',
            'awayteam_q1',
            'awayteam_q2',
            'awayteam_q3',
            'awayteam_q4',
            'awayteam_ot',
            'awayteam_totalscore',
            'over_under',
            'hometeam_handicap',
            'awayteam_handicap',
            'timer',
            'ball',
            'current_quarter',
            'completed_quarter',
            'home_team_id',
            'away_team_id',
            'locked'
        ];


//        public function  ncaa_square_wins(){
//            return $this->hasMany('App\Models\NCAASquareWin','game_id','id');
//        }

        public function home_team()
        {
            return $this->hasOne('App\Models\Team', 'id','home_team_id');
        }

        public function away_team()
        {
            return $this->hasOne('App\Models\Team', 'id','away_team_id');
        }

		public function events()
		{
			return $this->hasMany('\App\Models\GameEvent');
		}

		public function get_score($quarter = 'finalscore', $cumulative = true){

            $varHome = "hometeam_{$quarter}";
            $varAway = "awayteam_{$quarter}";
            if($cumulative) {
                switch($quarter){
                    case 'q1':
                        return [
                            'hometeam_score' => isset($this->hometeam_q1) ? $this->hometeam_q1 : null,
                            'awayteam_score' => isset($this->awayteam_q1) ? $this->awayteam_q1 : null,
                        ];
                        break;
                    case 'q2':
                        return [
                            'hometeam_score' => isset($this->hometeam_q2) ? ($this->hometeam_q2 + $this->hometeam_q1) : null,
                            'awayteam_score' => isset($this->awayteam_q2) ? ($this->awayteam_q2 + $this->awayteam_q1) : null,
                        ];
                        break;
                    case 'q3':
                        return [
                            'hometeam_score' => isset($this->hometeam_q3) ? ($this->hometeam_q3 + $this->hometeam_q2  + $this->hometeam_q1) : null,
                            'awayteam_score' => isset($this->awayteam_q3) ? ($this->awayteam_q3 + $this->awayteam_q2 + $this->awayteam_q1) : null,
                        ];
                        break;
                    case 'q4':
                        return [
                            'hometeam_score' => isset($this->hometeam_q4) ? ($this->hometeam_q4 + $this->hometeam_q3 + $this->hometeam_q2  + $this->hometeam_q1) : null,
                            'awayteam_score' => isset($this->awayteam_q4) ? ($this->awayteam_q4 + $this->awayteam_q4 + $this->awayteam_q2 + $this->awayteam_q1) : null,
                        ];
                        break;
                    case 'ot':
                        return [
                            'hometeam_score' => isset($this->hometeam_ot) ? ($this->hometeam_ot + $this->hometeam_q4 + $this->hometeam_q3 + $this->hometeam_q2  + $this->hometeam_q1) : null,
                            'awayteam_score' => isset($this->awayteam_ot) ? ($this->awayteam_ot + $this->awayteam_q4 + $this->awayteam_q4 + $this->awayteam_q2 + $this->awayteam_q1) : null,
                        ];
                        break;
                    default:
                        return [
                            'hometeam_score' => isset($this->$varHome) ? $this->$varHome : null,
                            'awayteam_score' => isset($this->$varAway) ? $this->$varAway : null,
                        ];
                        break;
                }
            } else {
                return [
                    'hometeam_score' => isset($this->$varHome) ? $this->$varHome : null,
                    'awayteam_score' => isset($this->$varAway) ? $this->$varAway : null,
                ];
            }
        }

		public function get_scores($cumulative = true)
        {
            $quarters = ['q1','q2','q3','q4','ot','totalscore'];
            $scores = [];
            foreach($quarters as $quarter){
                $scores[$quarter] = $this->get_score($quarter, $cumulative);
            }
            return $scores;
        }

        public function ncaa_winners()
        {
            return $this->hasMany(NCAASquareWin::class);
        }

        public function clock(){
            return SiteHelper::time_to_seconds($this->timer);
        }

        public function schwindy_picks_visible(){
            return $this->schwindy_picks()->where('visible',true);
        }

        public function schwindy_picks_hidden(){
            return $this->schwindy_picks()->where('visible',false);
        }

        public function schwindy_picks(){
            return $this->hasMany(SchwindyPick::class, 'game_id', 'id');
        }

        public function squareboards(){
            return $this->hasMany(SquareBoard::class, 'game_id', 'id');
        }

        public function getIsNotStartedAttribute()
        {
            return ($this->status == "Not Started");
        }

        public function getHasOvertimeAttribute()
        {
            return ($this->status == "After Over Time");
        }

        public function getIsFinalAttribute()
        {
            return ($this->status == "Final" || $this->status == "After Over Time");
        }

        public function tz_start($tz = 'America/New_York', $format = "M-j, Y, g:i:A"){
            if(!trim($tz)){
                $tz = 'America/New_York';
            }
            $timestamp = strtotime($this->start);
            $dt = new \DateTime("now", new \DateTimeZone($tz)); //first argument "must" be a string
            $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
            return $dt->format($format);
        }
//		Game has many Pools
	}

