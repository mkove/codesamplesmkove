<?php
	namespace App\Models;
	use App\Events\Pool\Squareboard\CreatedEvent;
    use App\Events\Pool\Squareboard\SavedEvent;
    use App\Events\Pool\Squareboard\UpdatedEvent;
    use Illuminate\Database\Eloquent\Model as Eloquent;


	class SquareBoard extends Eloquent
	{

		public $table = 'squareboards';
		protected  $fillable = [
            'x_team_id',
            'y_team_id',
            'sets',
            'hometeam_name',
            'awayteam_name',
            'label',
            'motd',
            'locked',
            'type',
            'max_per_user',
            'config',
            'payout_config',
            'allow_picks',
            'active',
            'game_id',
			'pool_id',
            'win_type',
            'play_status',
            'allow_manual'

		];

        public $with = [
            'game',
            'games',
            'squares',
            'numbers'
        ];
        protected $casts = [
            'payout_config' => 'json',
        ];
        protected $dispatchesEvents = [
            'created' => CreatedEvent::class,
            'updated' => UpdatedEvent::class,
            'saved' => SavedEvent::class,
        ];

		public function pool()
		{
			return $this->belongsTo('App\Models\Pool', 'pool_id');
		}

		public function game()
		{
			return $this->belongsTo('App\Models\Game', 'game_id');
		}

        public function games()
        {
            return $this->belongsToMany('App\Models\Game', 'games_squareboards', 'squareboard_id', 'game_id');
        }

		public function basketball_games()
        {
            return $this->belongsToMany('App\Models\BasketballGame', 'basketball_games_squareboards','squareboard_id','game_id');
        }


        public function squares()
        {
            return $this->hasMany('App\Models\Square', 'squareboard_id');
        }

        public function user_squares($userId){
            return $this->hasMany('App\Models\Square', 'squareboard_id')->where('squares.user_id', $userId);
        }


		public function numbers()
		{
			return $this->hasMany('App\Models\SquareBoardNumber', 'squareboard_id');
		}

        public function getPayoutConfigAttribute($value)
        {
            return json_decode($value, false);
        }

        public function setPayoutConfigAttribute($data){
            return $this->attributes['payout_config'] = json_encode($data);
        }

//		public function getPayoutConfigAttribute($value){
//		    dd("HERE", $value);
//            return json_decode($value);
//        }
//
//
//        public function setPayoutConfigAttribute($data = []){
//		    dd("THERE", $data);
//            return $this->attributes['payout_config'] = json_encode($data);
//        }

//		public function payout(){
//		    return json_decode($this->payout_config);
//        }
	}
