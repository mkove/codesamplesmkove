<?php
	namespace App\Models;
	use Illuminate\Database\Eloquent\Model as Eloquent;


	class Schwindy extends Eloquent
	{

		public $table = 'schwindy';
		
		protected  $fillable = [
			'pool_id',
			'label',
			'weekly_bonus',
			'deadline',
			'config',
			'locked',
            'current_week',
            'autopick_pending',
            'week_played',
            'completed',
		];

		public $timestamps = [

		];

		public function users(){
            return $this->hasManyThrough(
                'App\Models\User',
                'App\Models\SchwindyPick',
                'schwindy_id',
                'user_id',
                'id',
                'id');
        }

        public $with = ['picks', 'game'];

		public function pool()
		{
			return $this->belongsTo('\App\Models\Pool', 'pool_id');
		}

		public function picks()
		{
			return $this->hasMany('\App\Models\SchwindyPick', 'schwindy_id');
		}

		public function game()
		{
			return $this->belongsTo('\App\Models\Game', 'game_id');
		}
		
		public function schwindy_users()
		{
			return $this->belongsToMany(User::class, 'schwindy_users', 'schwindy_id', 'user_id')->withPivot(['display_name', 'notify_at']);
		}

        public function getConfigAttribute($value)
        {
            return json_decode($value, false);
        }

        public function setConfigAttribute($data){
		    return $this->attributes['config'] = json_encode($data);
        }

        public function getConfig($key = null){
		    if($key){
		        if(isset($this->config) && $this->config->{$key}){
                    return $this->config->{$key};
                }
            }
            return null;
        }

	}
