<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    /**
     * Class GolfPlayer
     * @package App\Models
     */
    class GolfPlayer extends Model
    {
        protected $table = 'golf_players';



        protected $fillable = [
            'name',
            'external_id',
            'country',
            'pos',
            'avg_points',
            'points_total',
            'events',
            "internal_note",
            'order',
            'photo',
            'first_name',
            'last_name',
            'middle_name',
            'display_name_mobile',
            'display_name',
            'config',
            'pga_id',
	        'dg_id',
			'country_code',
			'datagolf_rank',
			'owgr_rank',
        ];

        public function golf_plays(){
            return $this->hasMany(GolfTournamentPlayer::class,'player_id','id');
        }

        protected $short_name;


        public function golfs(){

//            return $this->hasManyThrough()
        }

        public function getShortNameAttribute() {
            $this->short_name = substr($this->first_mame, 0, 1).". ". $this->last_name;
            return $this->short_name;
        }

        public function getConfigAttribute($value)
        {
            return json_decode($value, false);
        }

        public function setConfigAttribute($data){
            return $this->attributes['config'] = json_encode($data);
        }

        public function name_variations(){
            return $this->hasMany(GolfPlayerNameVariation::class,'golf_player_id', 'id');
        }


    }


