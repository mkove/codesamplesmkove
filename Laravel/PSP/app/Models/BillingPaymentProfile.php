<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model as Eloquent;


    class BillingPaymentProfile extends Eloquent
    {

        protected $table = "billing_payment_profiles";

        protected $fillable = [
            'user_billing_id',
            'provider_profile_id',
            'type',
            'first_name',
            'last_name',
            'company',
            'street_1',
            'street_2',
            'city',
            'state',
            'zip',
            'country',
            'phone',
            'card_type',
            'card_number',
            'card_expiration',
            'is_payment_token',
            'card_art',
            'card_brand',
            'bank_account',
            'account_type',
            'routing_number',
            'e_check_type',
            'bank_name',
            'active',
            'flagged',
            'flag',
            'default',
        ];


        public function user_billing()
        {
            return $this->belongsTo(UserBilling::class, 'user_billing_id', 'id');
        }

        public function pools()
        {
            return $this->belongsToMany(Pool::class, 'pool_billing_payment_profile', 'billing_payment_profile_id', 'pool_id');
        }
    }

