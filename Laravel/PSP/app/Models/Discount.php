<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    //

    protected $table = 'discounts';
    protected $fillable = [
        'id',
        'name',
        'code',
        'used',
        'type',
        'amount',
        'max_use',
        'start_date',
        'end_date',
        'active',
        'description',
        'notes',
        'created_by_user_id',
        'admin_only'
    ];


//    ADD ADMIN ONLY admin_only


//    public function invoices(){
//        return $this->belongsToMany(Invoice::class, 'invoice_discounts','discount_id', 'invoice_id');
//    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by_user_id', 'id');
    }
}
