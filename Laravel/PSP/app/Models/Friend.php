<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Friend extends Model
    {
        protected $table = 'user_friends';

        protected $fillable = [
            'user_id',
            'friend_user_id',
            'name',
            'email',
            'note',
            'auto',
            'blocked',
            'lists',
            'removed',
        ];


        public function user(){
            return $this->belongsTo(User::class, 'user_id', 'id');
        }

        public function friend(){
            return $this->belongsTo(Rate::class, 'friend_user_id', 'id');
        }

    }


