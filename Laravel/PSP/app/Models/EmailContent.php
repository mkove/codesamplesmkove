<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailContent extends Model
{
    protected $table = 'email_contents';
    protected $fillable = [
            'id',
            'trigger_label',
            'subject',
            'content',
            'config',
            'type',
            'section',
            'scheduled',
            'schedule',
            'note',
    ];

    public function getNameAttribute(){
        return ucwords(str_replace('_', ' ', $this->trigger_label));
    }

    public function getConfigAttribute($value)
    {
        return json_decode($value, false);
    }

    public function setConfigAttribute($data){
        return $this->attributes['config'] = json_encode($data);
    }

}
