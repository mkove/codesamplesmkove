<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model as Eloquent;


    class Square extends Eloquent
    {

        protected $table = 'squares';
        protected  $fillable = [
                'id',
                'squareboard_id',
                'x',
                'y',
                'label',
                'short_label',
                'user_id',
                'populated',
                'note',
                'populated_by_user_id',
                'square_number'
        ];


        /*
 *   $table->integer('squareboard_id')->unsigned();
            $table->integer('x');
            $table->integer('y');
            $table->string('label', 45)->nullable();
            $table->string('short_label', 5)->nullable();
            $table->enum('populated', array('user','system','commissioner'))->nullable();
            $table->string('note', 45)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('populated_by_user_id')->unsigned()->nullable();*/

        public $timestamps = [

        ];

        //TODO: this might cause circular relatonships loads....
        public $with = ['square_wins'];

        public function squareboard()
        {
            return $this->belongsTo('App\Models\SquareBoard', 'squareboard_id');
        }

        public function user()
        {
            return $this->belongsTo('App\Models\User','user_id','id');
        }

        public function square_wins(){
            return $this->hasMany('App\Models\SquareWin','square_id','id');
        }

        public function  ncaa_square_wins(){
            return $this->hasMany('App\Models\NCAASquareWin','square_id','id');
        }



    }
