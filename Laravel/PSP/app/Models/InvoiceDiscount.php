<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model as Eloquent;

    class InvoiceDiscount extends Eloquent {

        protected $table = "invoice_discounts";
        //mini hack for existing dates
        public $fillable = [
            'invoice_id',
            'discount_id',
            'name',
            'applied_at',
            'code',
            'type',
            'amount',
            'admin_override',
        ];

        protected $with = ['discount'];

        public function invoice(){
            return $this->belongsTo(Invoice::class, 'invoice_id','id');
        }

        public function discount(){
            return $this->belongsTo(Discount::class, 'discount_id','id');
        }
    }
