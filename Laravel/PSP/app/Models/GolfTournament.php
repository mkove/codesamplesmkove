<?php

namespace App\Models;

use App\Helpers\GolfHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GolfTournament
 * @package App\Models
 */
class GolfTournament extends Model
{
    protected $table = 'golf_tournaments';

    protected $fillable = [
        'name',
        'external_id',
        'start_date',
        'end_date',
        'status',
        'country',
        'venue',
        'type',
        'par',
        'gender',
        'purse',
        'active',
        'first_tee_off_at',
        'config',
        'prize_final',
        'archived',
	    'current_season',
		'course',
		'event_id',
		'event_name',
		'location',
		'r1_teetime',
		'r2_teetime',
		'r3_teetime',
		'r4_teetime',
		'start_hole',
		'current_round',
    ];




    protected $with = [
        //            'tournament_players',
    ];

    public function tournament_players()
    {
        return $this->hasMany(GolfTournamentPlayer::class, 'tournament_id', 'id');
    }

    public function golfs()
    {
        return $this->hasMany(Golf::class, 'golf_tournament_id', 'id');
    }


    public function getConfigAttribute($value)
    {
        return json_decode($value, false);
    }

    public function setConfigAttribute($data)
    {
        return $this->attributes['config'] = json_encode($data);
    }

    public function getNotStartedAttribute()
    {
        return ($this->status == 'Not Started');
        return GolfHelper::get_not_started($this->status);
    }

    public function getTeedOffAttribute()
    {
        $lock_time = $this->first_tee_off_at ? $this->first_tee_off_at : $this->start_date;
        return (date('U', strtotime($lock_time)) < date('U'));
    }

    public function getStartedAttribute()
    {
        return (GolfHelper::get_current_round($this->status) !== null);
    }

    public function getFinishedAttribute()
    {
        return GolfHelper::is_finished($this->status);
        //            return (date('U', strtotime($this->start_date)) < date('U'));
    }

    public function getFirstTeeOff()
    {
        $teeOff = $this->tournament_players()->orderBy('tee_time', 'asc')->first();
        if ($teeOff) {
            return $teeOff->tee_time;
        }
        return date('Y-m-d H:i:s', strtotime($this->start_date));
    }

    public function getCompletedRoundAttribute()
    {
        return GolfHelper::get_completed_round($this->status);
    }

    public function getTournamentCurrentRoundAttribute()
    {
        return GolfHelper::get_current_round($this->status);
    }
	
	public function getInProgressAttribute(){
		
        return GolfHelper::in_progress($this->status);
	}

    public function getPrizePayoutAttribute()
    {
        return $this->prizes()->sum('payout');
    }

    public function getPrizePercentAttribute()
    {
        return $this->prizes()->sum('percent');
    }

    public function prizes()
    {
        return $this->hasMany(GolfTournamentPrize::class, 'golf_tournament_id', 'id');
    }
}
