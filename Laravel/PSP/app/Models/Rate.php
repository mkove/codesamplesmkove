<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rates';
    protected  $fillable = [
        'name',
        'taxable',
        'tax',
        'amount',
        'type',
        'pool_type_id',
        'has_cap',
        'max',
        'notes',
        'description',
        'active',
    ];




    public function pool_type(){
        return $this->belongsTo(PoolType::class, 'pool_type_id','id');
    }

    public function invoices(){
        return $this->hasMany(Invoice::class, 'rate_id','id');
    }

    public function dynamic_rates(){
        return $this->hasMany(DynamicRate::class)->orderBy('min_items','asc')->orderBy('max_items','asc');
    }
}
