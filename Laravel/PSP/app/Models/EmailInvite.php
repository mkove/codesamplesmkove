<?php
	namespace App\Models;
	use Illuminate\Database\Eloquent\Model as Eloquent;

	class EmailInvite extends Eloquent {

		protected  $table = "email_invites";
		//mini hack for existing dates
		const CREATED_AT = 'created';
		const UPDATED_AT = 'updated';

		public $fillable = [
			'pool_id',
			'accepted',
			'pool_users_id',
			'status',
			'email',
			'invite_code',
            'invited_by_user_id',
            'approved',
            'email_sent',
            'message',
            'name',
            'generic_email'
			];

		public $with = [
		    'user',
            'invited_by_user'
        ];

        public function pool()
		{
			return $this->belongsTo('App\Models\Pool', 'pool_id');
		}

		public function user()
		{
			return $this->belongsTo('App\Models\User', 'pool_users_id');
		}

		public function invited_by_user(){
            return $this->belongsTo('App\Models\User', 'invited_by_user_id');
        }

        public function getInvitedByDisplayNameAttribute(){
            return ($this->invited_by_user()) ? $this->invited_by_user->global_display_name : null;
        }

        public function getInvitedByFullNameAttribute(){
            return ($this->invited_by_user()) ? $this->invited_by_user->full_name : null;
        }

        public function getInvitedByEmailAttribute(){
            return ($this->invited_by_user()) ? $this->invited_by_user->email : null;
        }

        public function getAcceptUrlAttribute(){
            return url("/pool/".$this->pool_id."/join?inviteCode=".$this->invite_code);
        }
	}
