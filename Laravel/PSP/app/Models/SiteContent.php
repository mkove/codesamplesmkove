<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model as Eloquent;


    class SiteContent extends Eloquent {

        protected $table = 'site_contents';
        protected $fillable = [
            'id',
            'identifier',
            'section',
            'type',
            'note',
            'label',
            'content',
            'vars',
        ];




        public function getVarsAttribute($value)
        {
            return json_decode($value, false);
        }

        public function setVarsAttribute($data){
            return $this->attributes['vars'] = json_encode($data);
        }
    }
