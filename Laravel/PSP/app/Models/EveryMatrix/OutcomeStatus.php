<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class OutcomeStatus extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'outcome_status';
		protected  $fillable = ['id','version','name','description'];
}
