<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantInfoDetailTypeUsage extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_participant_info_detail_type_usage';
		protected  $fillable = ['id','version','eventParticipantInfoDetailTypeId','eventParticipantInfoTypeId','sportId'];
}
