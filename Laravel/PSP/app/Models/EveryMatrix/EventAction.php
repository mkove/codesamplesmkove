<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventAction extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_action';
		protected  $fillable = [
		    'id',
            'version',
            'typeId',
            'eventId',
            'providerId',
            'statusId',
            'eventPartId',
            'paramFloat1'
        ];
		protected $with = [
            'status',
            'type',
            'part',
            'details',
        ];

		public function type(){
		    return $this->hasOne(EventActionType::class, 'id', 'typeId');
        }

        public function status(){
		    return $this->hasOne(EventActionStatus::class, 'id','statusId');
        }

        public function part(){
		    return $this->hasOne(EventPart::class,'id','eventPartId');
//		    return $this->hasOne(EventAction)
        }

        public function details(){
		    return $this->hasMany(EventActionDetail::class,'eventActionId');
        }

        //EventInfo
        public function info(){
		    return $this->hasMany(EventInfo::class,'eventId', 'id');
        }
}
