<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantInfoDetailType extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_participant_info_detail_type';
		protected  $fillable = ['id',
            'version',
            'name',
            'description',
            'hasParamFloat1',
            'hasParamParticipantId1',
            'hasParamBoolean1',
            'description',
            'paramFloat1Description',
            'paramBoolean1Description',
        ];}
