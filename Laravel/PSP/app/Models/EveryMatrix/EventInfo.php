<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventInfo extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_info';

    protected  $fillable = [
		    'id',
            'version',
            'typeId',
            'eventId',
            'providerId',
            'statusId',
            'eventPartId',
            'paramFloat1',
            'paramFloat2',
            'paramParticipantId1',
            'paramParticipantId2',
            'paramEventStatusId1',
            'paramEventPartId1',
        ];
}
