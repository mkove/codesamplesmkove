<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class ParticipantRole extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'participant_role';
		protected  $fillable = ['id','version','name','description','isPrimary'];
}
