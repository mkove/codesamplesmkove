<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantRelation extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_participant_relation';
		protected  $fillable = [
		    'id',
            'version',
            'eventId',
            'eventPartId',
            'participantId',
            'participantRoleId',
            'parentParticipantId',
        ];
}
