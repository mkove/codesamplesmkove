<?php
namespace App\Models\EveryMatrix;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Event extends Eloquent
{
    protected $connection = 'psp_everymatrix';
    protected $dates = ['startTime', 'endTime'];
    protected $table = 'event';
    protected  $fillable = [
        'id',
        'version',
        'typeId',
        'isComplete',
        'sportId',
        'parentId',
        'currentPartId',
        'templateId',
        'name',
        'startTime',
        'endTime',
        'deleteTimeOffset',
        'venueId',
        'statusId',
        'hasLiveStatus',
        'rootPartId',
        'url',
        'parentPartId'
    ];

    public function parent(){
        return $this->hasOne(Event::class, 'id','parentId');
    }

    public function setStartTimeAttribute( $value ) {
        $this->attributes['startTime'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function setEndTimeAttribute( $value ) {
        $this->attributes['endTime'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function type(){
        return $this->hasOne(EventType::class,'id', 'typeId');
    }

    public function status(){
        return $this->hasOne(EventStatus::class,'id', 'statusId');
    }

    public function location(){
        return $this->hasOne(Location::class, 'id', 'venueId');
    }

    public function participants(){
        return $this->belongsToMany(Participant::class, 'event_participant_relation', 'eventId', 'participantId');
    }

    public function actions(){
        return $this->hasMany(EventAction::class, 'eventId','id');
    }

    public function sport(){
        return $this->belongsTo(Sport::class,'sportId','id');
    }

    public function info(){
        return $this->hasOne(EventInfo::class, 'id', 'eventId');
    }

    public function participants_infos(){
        return $this->hasMany(EventParticipantInfo::class, 'eventId', 'id');
    }

    public function event_part_default_usage(){
        return $this->hasMany(EventPartDefaultUsage::class,'parentEventId', 'parentId');
    }

    public function template(){
        return $this->hasOne(EventTemplate::class, 'id', 'templateId');
    }

    public function root_part(){
//        return $this->hasOne(   )
    }
}
