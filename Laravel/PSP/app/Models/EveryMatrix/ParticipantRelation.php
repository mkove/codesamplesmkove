<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class ParticipantRelation extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'participant_relation';
		protected  $fillable = ['id','version','typeId','fromParticipantId','toParticipantId','paramParticipantRoleId'];
}
