<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class ParticipantRelationType extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'participant_relation_type';
		protected  $fillable = ['id','version','name'];
}
