<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class OutcomeTypeBettingTypeRelation extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'outcome_type_betting_type_relation';
		protected  $fillable = ['id','version','outcomeTypeId','bettingTypeId'];
}
