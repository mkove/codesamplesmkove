<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class LocationRelation extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'location_relation';
		protected  $fillable = ['id','version','typeId','fromLocationId','toLocationId'];
}
