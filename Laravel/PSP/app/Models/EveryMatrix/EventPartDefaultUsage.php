<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventPartDefaultUsage extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_part_default_usage';
		protected  $fillable = [
		    'id',
            'version',
            'parentEventId',
            'eventTypeId',
            'sportId',
            'rootPartId'
        ];

		public function event_type(){
		    return $this->hasOne(EventType::class, 'id', 'eventTypeId');
        }

        public function root_part(){
		    return $this->hasOne(EventPart::class, 'id','rootPartId');
        }
}
