<?php
    namespace App\Models\EveryMatrix;
    use Illuminate\Database\Eloquent\Model as Eloquent;
    class EMLog extends Eloquent
    {
        protected $connection = 'psp_everymatrix';
        protected $table = 'e_m_logs';
        protected  $fillable = [
            'id',
            "model",
            'action',
            'outcome',
            'error',
            'affectedId',
        ];


        public function log($type, $id, $model, $outcome = "Ok", $error = false)
        {
            $data = [
                'model' => $model,
                'action' => $type,
                'outcome' => $outcome,
                'error' => $error,
                'affectedId' => $id,
            ];
            return $this->create($data);
        }
    }
