<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventActionStatus extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_action_status';
		protected  $fillable = ['id','version','name','isAvailable','description'];
}
