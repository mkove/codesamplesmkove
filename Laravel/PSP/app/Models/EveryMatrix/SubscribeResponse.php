<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class SubscribeResponse extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'subscribe_response';
		protected  $fillable = ['subscriptionId'];
}
