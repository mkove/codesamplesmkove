<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventActionTypeUsage extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_action_type_usage';
		protected  $fillable = ['id','version','eventActionTypeId','eventTypeId','eventPartId','sportId'];
}
