<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventActionDetail extends Eloquent {
    protected $connection = 'psp_everymatrix';
    protected $fillable = [
        'id',
        'version',
        'typeId',
        'eventActionId',
        'statusId',
        'paramParticipantId1',
        'paramString1PossibleValues',
        'paramParticipantId1Description',
        'paramFloat2Description',
        'paramString1Description',
        'paramBoolean1Description',
    ];

    protected $table = 'event_action_detail';
    protected $with = [
                'type',
                'status',
                'participant',
    ];

//"typeId" => 1
//"eventActionId" => 9795463505672320
//"statusId" => 4
//"paramParticipantId1" => 26100

    public function type(){
        return $this->hasOne(EventActionDetailType::class, 'id', 'typeId');
    }

    public function status(){
        return $this->hasOne(EventActionDetailStatus::class, 'id','statusId');
    }

    public function participant(){
        return $this->hasOne(Participant::class, 'id', 'paramParticipantId1');
    }
}
