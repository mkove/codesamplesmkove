<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class ParticipantType extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'participant_type';
		protected  $fillable = ['id','version','name','isIndividual','hasName','hasFirstName','hasLastName'];
}
