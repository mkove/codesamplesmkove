<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Participant extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'participant';
		protected  $fillable = [
		    'id',
            'version',
            'typeId',
            'name',
            'countryId',
            "isMale",
            "firstName",
            "lastName",
            "url",
            "birthTime",
        ];


		public function usage(){
		    return $this->hasMany(ParticipantUsage::class, 'participantId', 'id');
        }

        public function roles()
        {
            return $this->belongsToMany(ParticipantRole::class,
                'event_participant_relation',
                'participantId',
                'participantRoleId'
            ); //->wherePivot('eventId','=',$eventId);
        }

        public function events(){
		    return $this->belongsToMany(Event::class,
            'event_participant_relation',
            'participantId',
            'eventId'
            );
        }
}
