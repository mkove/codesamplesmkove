<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventActionType extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_action_type';
		protected  $fillable = ['id',
            'version',
            'name',
            'description',
            'hasParamFloat1',
            'paramFloat1Description',
            'hasParamParticipantId1',
            'hasParamParticipantId2',
            'paramParticipantId1Description',
            'paramParticipantId2Description',
        ];
}
