<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Provider extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'provider';
		protected  $fillable = ['id','version','name','locationId','url','isBookmaker','isBettingExchange','bettingCommissionVACs','isLiveOddsApproved','isNewsSource','isEnabled'];
}
