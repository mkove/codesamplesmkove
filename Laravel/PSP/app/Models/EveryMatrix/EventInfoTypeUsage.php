<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventInfoTypeUsage extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_info_type_usage';
		protected  $fillable = ['id','version','eventInfoTypeId','eventTypeId','eventPartId','sportId'];
}
