<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventInfoStatus extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_info_status';
		protected  $fillable = ['id','version','name','isAvailable','description'];
}
