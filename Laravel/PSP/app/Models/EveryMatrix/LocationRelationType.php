<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class LocationRelationType extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'location_relation_type';
		protected  $fillable = ['id','version','name'];
}
