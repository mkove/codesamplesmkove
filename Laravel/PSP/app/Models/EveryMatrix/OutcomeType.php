<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class OutcomeType extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'outcome_type';
		protected  $fillable = [
		    'id',
            'version',
            'name',
            'description',
            'hasParamFloat1',
            'hasParamFloat2',
            'hasParamFloat3',
            'hasParamBoolean1',
            'paramBoolean1Description',
            'hasParamString1',
            'hasParamParticipantId1',
            'paramParticipant1MustBePrimary',
            'paramParticipant1MustBeRoot',
            'hasParamParticipantId2',
            'paramParticipant2MustBePrimary',
            'paramParticipant2MustBeRoot',
            'hasParamParticipantId3',
            'paramParticipant3MustBePrimary',
            'paramParticipant3MustBeRoot',
            'hasParamEventPartId1',
            'paramParticipantId1Description',
            'paramFloat1Description',
            'paramString1Description',
            'paramString1PossibleValues',
            'paramParticipantId2Description',
            'paramFloat2Description',
            'paramFloat3Description',
            'paramParticipantId3Description',
            'paramParticipant1MustHaveRoleId',
            'paramEventPartId1Description',

            ];
}
