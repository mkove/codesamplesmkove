<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantInfoTypeUsage extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_participant_info_type_usage';
		protected  $fillable = ['id','version','eventParticipantInfoTypeId','eventTypeId','eventPartId','sportId'];
}
