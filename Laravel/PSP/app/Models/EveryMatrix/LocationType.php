<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class LocationType extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'location_type';
		protected  $fillable = ['id','version','name','hasCode'];
}
