<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventPart extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_part';
		protected  $fillable = [
		    'id','version',
            'name',
            'orderNum',
            'isDrawPossible',
            'parentId',
            'description'
        ];

		public function parent(){
		    return $this->hasOne(EventPart::class, 'id', 'parentId');
        }
}
