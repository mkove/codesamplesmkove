<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class ParticipantUsage extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'participant_usage';
		protected  $fillable = ['id','version','participantId','sportId'];
}
