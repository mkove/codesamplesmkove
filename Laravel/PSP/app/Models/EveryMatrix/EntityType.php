<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EntityType extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'entity_type';
		protected  $fillable = ['id','version','name'];
}
