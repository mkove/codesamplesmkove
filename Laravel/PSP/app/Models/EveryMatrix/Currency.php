<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Currency extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'currency';
		protected  $fillable = ['id','version','name','code'];
}
