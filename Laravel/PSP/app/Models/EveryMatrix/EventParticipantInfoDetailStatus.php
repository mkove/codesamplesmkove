<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantInfoDetailStatus extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_participant_info_detail_status';
		protected  $fillable = ['id','version','name','isAvailable','description'];
}
