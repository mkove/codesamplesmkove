<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventStatus extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_status';
		protected  $fillable = ['id','version','name','description'];
}
