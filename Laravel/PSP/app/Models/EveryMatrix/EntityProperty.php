<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EntityProperty extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'entity_property';
		protected  $fillable = ['id','version','typeId','entityTypeId','name','description'];
}
