<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventType extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_type';
		protected  $fillable = ['id','version','name'];
}
