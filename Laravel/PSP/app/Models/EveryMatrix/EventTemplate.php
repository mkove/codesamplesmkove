<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventTemplate extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_template';
		protected  $fillable = ['id','version','name','eventTypeId','sportId','venueId','rootPartId', 'url'];

		public function type(){
		    return $this->belongsTo(EventType::class, 'eventTypeId', 'id');
        }

        public function root_part(){
		    return $this->belongsTo(EventPart::class, 'rootPartId', 'id');
        }
}
