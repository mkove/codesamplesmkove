<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class BettingOfferStatus extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'betting_offer_status';
		protected  $fillable = ['id','version','name','isAvailable','description'];
}
