<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Sport extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'sport';
		protected  $fillable = [
		    'id',
            'version',
            'name',
            'parentId',
            'description',
        ];
}
