<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventInfoType extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_info_type';
		protected  $fillable = [
		    'id',
            'version',
            'name',
            'description',
            'hasParamFloat1',
            'paramFloat1Description',
            'hasParamFloat2',
            'paramFloat2Description',
            'hasParamParticipantId1',
            'paramParticipantId1Description',
            'hasParamParticipantId2',
            'paramParticipantId2Description',
            'hasParamEventPartId1',
            'hasParamString1',
            'hasParamBoolean1',
            'hasParamEventStatusId1',
            'hasParamTime1',
            'paramParticipantIdsMustBeOrdered',
            'paramString1Description',
    'paramString1PossibleValues',
    'paramBoolean1Description',
    'paramEventPartId1Description',
    'paramEventStatusId1Description',
    'paramTime1Description',
        ];
}
