<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EntityPropertyType extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'entity_property_type';
		protected  $fillable = ['id','version','name'];
}
