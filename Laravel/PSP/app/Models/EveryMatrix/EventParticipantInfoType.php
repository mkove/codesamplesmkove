<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantInfoType extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_participant_info_type';
		protected  $fillable = ['id','version','name','description'];
}
