<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantInfo extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_participant_info';
		protected  $fillable = ['id','version','typeId','eventId','providerId','statusId','eventPartId','participantId'];
}
