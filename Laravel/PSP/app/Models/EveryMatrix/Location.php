<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Location extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'location';
		protected  $fillable = ['id','version','typeId','name','code','isHistoric'];
}
