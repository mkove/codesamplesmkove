<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventActionDetailType extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_action_detail_type';
		protected  $fillable = ['id',
            'version',
            'name',
            'description',
            'hasParamFloat1',
            'paramFloat1Description',
            'hasParamFloat2',
            'hasParamParticipantId1',
            'hasParamString1',
            'hasParamBoolean1',
            'paramString1PossibleValues',
            'paramParticipantId1Description',
            'paramFloat2Description',
            'paramString1Description',
            'paramBoolean1Description',
        ];
}
