<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventActionDetailStatus extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_action_detail_status';
		protected  $fillable = ['id','version','name','isAvailable','description'];
}
