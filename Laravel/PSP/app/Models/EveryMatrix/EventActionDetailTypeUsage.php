<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventActionDetailTypeUsage extends Eloquent
{
		protected $connection = 'psp_everymatrix';protected $table = 'event_action_detail_type_usage';
		protected  $fillable = ['id','version','eventActionDetailTypeId','eventActionTypeId','sportId'];
}
