<?php
namespace App\Models\EveryMatrix;
use Illuminate\Database\Eloquent\Model as Eloquent;
class EventParticipantInfoDetail extends Eloquent
{
		protected $connection = 'psp_everymatrix';
		protected $table = 'event_participant_info_detail';
		protected  $fillable = [
		    'id',
            'version',
            'typeId',
            'eventParticipantInfoId',
            'statusId',
            'paramFloat1',
            'description',
            'paramFloat1Description',
            'paramBoolean1Description'
        ];
}
