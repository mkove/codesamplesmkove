<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table = 'payments';
    protected $fillable = [
        'invoice_id',
        'user_id',
        'gateway_id',
        'provider_response',
        'gateway_payment_id',
        'provider',
        'paid_at',
        'token',
        'amount',
        'note',
    ];

    public function invoice(){
        return $this->belongsTo(Invoice::class, 'invoice_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id','id');
    }

}

