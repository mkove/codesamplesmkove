<?php

namespace App\Models;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class PageRedirect extends Model
{
    //
    protected $table = 'page_redirects';
    protected $fillable = [
        'target_path',
        'source_path',
        'code',
        'hits',
        'target_path',
    ];
    public function getRouteKeyName() {
        return 'source_path';
    }


    public function getUrlAttribute(){
        return route('redirect.route',['catchAllSlug' => $this->source_path]);
    }


}
