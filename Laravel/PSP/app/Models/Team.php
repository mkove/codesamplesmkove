<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model as Eloquent;
//    https://teamcolors.jim-nielsen.com/

class Team extends Eloquent
{
    protected $table = 'teams';

    protected  $fillable = [
        'name',
        'short_name',
        'alt_name',
        'sport',
        'provider_id',
        'league',
        'active',
        'primary_color',
        'secondary_color',
        'supplement_color_1',
        'supplement_color_2',
        'logo',
    ];

    public $with = [];

    public function home_games(){
        return $this->hasMany('App\Models\Game','home_team_id', 'id');
    }

    public function away_games(){
        return $this->hasMany('App\Models\Game','away_team_id', 'id');
    }



}

