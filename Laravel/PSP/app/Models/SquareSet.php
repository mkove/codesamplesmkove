<?php
	namespace App\Models;
	use Illuminate\Database\Eloquent\Model as Eloquent;

	class SquareSet extends Eloquent
	{

		protected $table = 'square_sets';
		protected  $fillable = [];

		protected $timestamps = [

		];
	}