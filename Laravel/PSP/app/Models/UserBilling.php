<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class UserBilling extends Model
{
    protected $table = 'user_billings';
    protected $fillable = [
        'user_id',
        'customer_profile_id',
        'provider',
        'note',
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function payment_profiles(){
        return $this->hasMany(BillingPaymentProfile::class,'user_billing_id','id');
    }
}
