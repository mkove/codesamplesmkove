<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicRate extends Model
{
    //
    protected $table = 'dynamic_rates';
    protected $fillable = [
        'rate_id',
        'per',
        'min_items',
        'max_items',
        'name',
        'taxable',
        'tax',
        'amount',
        'has_cap',
        'max',
        'notes',
        'description',
        'active',
        'is_flat'
    ];

    public function rate(){
        return $this->belongsTo(Rate::class);
    }

}
