<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model as Eloquent;

    class UserNotification extends Eloquent
    {

        protected $table = 'user_notifications';
        protected  $fillable = [
            'id',
            'user_id',
            'commissioner_id',
            'pool_id',
            'type',
            'level',
            'action',
            'action_name',
            'notification',
            'push',
            'read',
            'dismissed',
            'url',
            'read_at',
            ];


        public function user()
        {
            return $this->belongsTo(User::class,'user_id','id');
        }

        public function commissioner()
        {
            return $this->belongsTo(User::class,'commissioner_id','id');
        }

        public function pool()
        {
            return $this->belongsTo(Pool::class,'pool_id','id');
        }

    }
