<?php
	namespace App\Models;
	use Illuminate\Database\Eloquent\Model as Eloquent;

	class GameEvent extends Eloquent
	{

	    /*
	     * ALTER TABLE `psp`.`game_events`
ADD COLUMN `team_id` INT(10) NULL AFTER `order`;
ALTER TABLE `psp`.`game_events`
CHANGE COLUMN `team_id` `team_id` INT(10) UNSIGNED NULL DEFAULT NULL ;
ALTER TABLE `psp`.`game_events`
CHANGE COLUMN `team` `scoring_team` ENUM('home', 'away') NULL DEFAULT NULL ;

	     * */
		protected $table = 'game_events';
		protected  $fillable = [
			'quarter',
			'action',
			'type',
            'by',
			'player_id',
            'event_id',
            'team_id',
			'home_score',
			'away_score',
			'game_id',
			'time',
            'clock',
            'points',
            'order',
            'scoring_team',
			'created_at',
			'updated_at',
            'pat_pending'
		];
//    php artisan make:migration add_event_id_to_game_events_table --table=game_events





        public $timestamps;

		public function game()
		{
			return $this->belongsTo('App\Models\Game');
		}

		public function team(){
		    return $this->belongsTo(Team::class);
        }
	}
