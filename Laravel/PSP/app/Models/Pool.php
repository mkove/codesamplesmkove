<?php
	namespace App\Models;
	use App\Events\LogUserActivityEvent;
    use Illuminate\Database\Eloquent\Model as Eloquent;

	/**
     * https://softonsofa.com/laravel-querying-any-level-far-relations-with-simple-trick/ <<<<<<<<<<< good shit
	 * ALTER TABLE `rkqwwdpxvz`.`pools`
	ADD COLUMN `description` TEXT NULL AFTER `game_id`,
	ADD COLUMN `commissioner_note` VARCHAR(45) NULL AFTER `description`,
	ADD COLUMN `slug` VARCHAR(45) NULL AFTER `commissioner_note`,
	ADD COLUMN `updated_at` TIMESTAMP NULL AFTER `slug`,
	ADD COLUMN `created_at` TIMESTAMP NULL AFTER `updated_at`;
 no

	 */
	class Pool extends Eloquent{
//    ALTER TABLE pools ADD FULLTEXT fulltext_index (pool_name)

		protected $table = "pools";
		const CREATED_AT = 'created';
		const UPDATED_AT = 'updated';
		protected  $fillable = [
			'commissioner_id',
			'pool_name',
			'pool_password',
			'description',
			'status',
			'config',
            'league',
            'event',
            'type',
            'rules',
            'allow_user_invite',
            'puid',
            'hash'
		];
        public $with = [
            'chat',
            'pool_type',
            'commissioners',
            'invites',
            'users',
            'all_users',
            'invoice'
        ];

        //retrieved, creating, created, updating, updated, saving, saved, deleting, deleted, restoring, restored
        protected $dispatchesEvents = [
            'created' => LogUserActivityEvent::class,
            'updated' => LogUserActivityEvent::class,
            'saved' => LogUserActivityEvent::class,
            'deleted' => LogUserActivityEvent::class,
        ];

		public function squareboard()
		{
	        return $this->hasOne(SquareBoard::class, 'pool_id', 'id');
		}

		public function schwindy()
		{
			return $this->hasOne(Schwindy::class, 'pool_id', 'id');
		}

		public function golf(){
		    return $this->hasOne(Golf::class, 'pool_id', 'id');
        }

		public function chat()
		{
			return $this->hasOne(Chat::class, 'pool_id', 'id');
		}

		public function pool_type()
		{
			return $this->belongsTo(PoolType::class, 'type', 'id');
		}

		public function billing_payment_profile(){
		    return $this->belongsToMany(BillingPaymentProfile::class, 'pool_billing_payment_profile','pool_id','billing_payment_profile_id');
        }

		public function commissioner()
		{
			return $this->belongsTo(User::class, 'commissioner_id');
		}

        public function commissioners()
        {
            return $this->belongsToMany(User::class, 'pool_users', 'pool_id', 'user_id')->wherePivot('status','active')->where('commissioner', true)->withPivot('status','display_name','last_chat_message_id','verified','archived', 'approved', 'commissioner_note', 'user_note');
        }

        public function com_count(){
		    return $this->commissioners()->count();
        }

		public function invites()
		{
			return $this->hasMany(EmailInvite::class, 'pool_id')->where('status','active');
		}

		public function users()
		{
			return $this->belongsToMany(User::class, 'pool_users', 'pool_id', 'user_id')
                ->wherePivot('status','active')->withPivot('status','display_name','last_chat_message_id','verified','archived', 'approved', 'commissioner_note', 'user_note','commissioner');
		}

        public function bad_users()
        {
            return $this->belongsToMany(User::class, 'pool_users', 'pool_id', 'user_id')
                ->wherePivot('status','disabled')->withPivot('status','display_name','last_chat_message_id','verified','archived', 'approved', 'commissioner_note', 'user_note','commissioner');
        }

        public function unapproved_users()
        {
            return $this->belongsToMany(User::class, 'pool_users', 'pool_id', 'user_id')
                ->wherePivot('approved',false)
                ->wherePivot('status', 'active')
                ->withPivot('status','display_name','last_chat_message_id','verified','archived', 'approved', 'commissioner_note', 'user_note','commissioner');
        }

		public function all_users()
		{
			return $this->belongsToMany(User::class, 'pool_users', 'pool_id', 'user_id')->withPivot('status','display_name','last_chat_message_id','verified','archived', 'approved', 'commissioner_note', 'user_note','commissioner');
		}

        public function unexposed_users(){
            $freshUsers = [];
            foreach($this->users as $user){
                $freshUser = [
                    'id' => $user->id,
                    'display_name' => (trim($user->pivot->display_name) != "") ? $user->pivot->display_name : $user->global_display_name,
                ];
                $freshUsers[] = $freshUser;
            }
            return $freshUsers;
        }

		public function i_am_invited($email)
		{
			return $this->invites()->where('email', $email)->count();
		}

		public function is_commissioner($userId)
		{
			return ($this->commissioners()->where('users.id' ,$userId)->count() > 0);
		}

		public function is_creator($userId){
		    return ($this->commissioner_id == $userId);
        }

		public function approved($userId){
		    return ($this->unapproved_users()->where('users.id', $userId)->count() === 0);
        }

		public function can_join($email, $password = null)
		{
			$email = strtolower($email);
			$alreday = $this->users()->where('email', $email)->count();
			if($alreday) return false;
			if($password) {
				if($password == $this->pool_password) {
					return true;
				}
			}
			$invited = $this->invites()->where('email', $email)->count();
			if($invited) {
				return true;
			}
			return false;
		}

		public function already_member($userId)
		{
			return ($this->users()->where('users.id', $userId)->count() > 0);
		}

		public function banned_member($userId){
            return $this->bad_users()->where('users.id', $userId)->count();
        }

        public function invoice(){
            return $this->hasOne(Invoice::class, 'pool_id','id');
        }

        public function getUrlAttribute(){
		    return route('psp.pool.guest_show',['hash' => $this->hash]);
        }

        public function getTypeNameAttribute(){
            return ($this->pool_type()) ? ucwords($this->pool_type->name) : "PSP Pool";
        }

        public function getTypeTemplateAttribute(){
            return ($this->pool_type()) ? ucwords($this->pool_type->name) : "PSP Pool";
        }



	}
