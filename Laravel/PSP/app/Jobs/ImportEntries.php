<?php

namespace App\Jobs;

use App\Mail\AdminNotice;
use App\Models\GolfEntry;
use App\Providers\Pool\Golf\GolfEntryProvider;
use App\Providers\Pool\Golf\GolfGroupProvider;
use App\Providers\Pool\Golf\GolfProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Throwable;

class ImportEntries implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $path;
    protected $replace;
    protected $userId;
    protected $golfId;
    private $_errors;
    private $_groupedPlayers;
    private $_golf;
    private $_golfEntryProvider;
    private $_golfGroupProvider;
    private $_golfProvider;

    public $tries = 1;
    public $backoff = 15;
    public $timeout = 0; //no timeout

    public function __construct($path, $golfId, $userId, $replace = [])
    {
        $this->path = $path;
        $this->replace = $replace;
        $this->userId = $userId;
        $this->golfId = $golfId;
        $this->_errors = [];
    }


    public function handle()
    {
        //LOAD UP

        $this->_load_dep();
        $this->_load_golf();
        $this->_groupedPlayers = $this->_golf->grouped_tournament_players;
        $toEmail = __conf('system.send_system_notices_to_email', 'text', "mkovalch@gmail.com");
        // $emailSender = \Mail::to($toEmail)->cc('michael.kove@gmail.com');
        $fileData = \Storage::get($this->path);
        $importedData = $this->_clean_input($fileData);
        $entryCount = 0;
        if ($importedData && count($importedData) > 0) {
            //TESTING SMALL
            //                $data = array_slice($importedData, 0, 5);
            //REAL
            $data = $importedData;

            $importEntryCount = count($data);
            if (!$this->userId) {
                $user = $this->_golf->pool->commissioners->first();
                $this->userId = $user->id;
            }
            $startNotices = new AdminNotice("Import STARTED for " . $this->_golf->pool->pool_name, "Total Entry Count: " . $importEntryCount);
            // $emailSender->send($startNotices);
            $startNotices->send_admin_email($toEmail);

            $deleteCount = 0;
            foreach ($this->_golf->entries as $entry) {
//                \Log::info("Deleting " . $entry->name);
                $deleteCount++;
                GolfEntry::destroy($entry->id);
            }
            //reload
            $this->_load_golf();
            $this->_errors[] = "<span style='color:#00438b;'>DELETED OLD ENTRIES: <strong>" . $deleteCount . "</strong></span>";

            foreach ($data as $entryData) {
                try {
                    $entryCount++;
                    $playerAddCount = 0;
                    $entryName = $entryData[0];
//                    \Log::info("Importing " . $entryName);
                    $entry = $this->_add_entry($entryName);
                    if ($entry) {
                        $groupCount = count($entryData);
                        for ($g = 1; $g < $groupCount; $g++) {
                            $idsToSync = []; //reset per group
                            $group = $entry->groups->where('number', $g)->first();
                            $importPlayers = (is_array($entryData[$g])) ? $entryData[$g] : [$entryData[$g]];
                            foreach ($importPlayers as $ip) {
                                $gtpId = $this->_get_gtp_id($ip);
                                $this->_process_gtp($gtpId, $idsToSync, $ip, $group->name);
                            }
//                            \Log::info($entry->name . " : " . $group->name . " IDS: ");
//                            \Log::info($idsToSync);
                            if (count($idsToSync)) {
                                $group->tournament_players()->sync($idsToSync, true); //always reset
                                $updateQuery = ['total_players' => $group->tournament_players()->count()];
                                $this->_golfGroupProvider->update($group->id, $updateQuery);
                                $playerAddCount = $playerAddCount + $updateQuery['total_players'];
                            } else {
//                                \Log::info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NO MATCH NO ENTRY" . $entryName . " G " . $g);
                            }
                        }
                        //                            $this->_errors[] = "<span style='color:green;'>Added <strong>".$updateQuery['total_players']."</strong> to group <strong>".$group->name."</strong></span>";
                    } else {
                        $this->_errors[] = "<span style='color:darkred;'>FAILED TO ADD <strong>" . $entryName . "</strong>, possibly bad entry name</span>";
                    }
                } catch (\Exception $e) {
                    $message = $e->getMessage();
                    \Log::error($message);
                    $this->_errors[] = "<span style='color:darkred;'>Error Occurred: <strong>{$message}</strong></span>";
                }
            }
        } else {
            $this->_errors[] = "<span style='color:darkred;'>Could Not read file!</span>";
        }


        $stringMessage = implode('<br>', $this->_errors);
        $endNotices = new AdminNotice("Import FINISHED for " . $this->_golf->pool->pool_name, "Imported <strong style='color:green;'>" . $entryCount . "</strong> items. <br><br>REPORT \n<br>" . $stringMessage);
        // $emailSender->send($endNotices);
        $endNotices->send_admin_email($toEmail);


        return true;
    }

    public function failed(Throwable $exception)
    {
        \Log::error($exception->getMessage());
        \Log::error($exception->getTraceAsString());
        // $emailSender = \Mail::to("michael.kove@gmail.com");
        $endNotices = new AdminNotice("Import FAILED", $exception->getMessage());
        // $emailSender->send($endNotices);
        $endNotices->send_admin_email("michael.kove@gmail.com");

        // Send user notification of failure, etc...
    }

    private function _load_dep()
    {
        $this->_golfEntryProvider = resolve(GolfEntryProvider::class);
        $this->_golfGroupProvider = resolve(GolfGroupProvider::class);
        $this->_golfProvider = resolve(GolfProvider::class);
    }

    private function _load_golf()
    {
        $this->_golf = $this->_golfProvider->get(['id' => $this->golfId], true)->with([
            'pool',
            'pool.commissioners',
            'tournament',
            'tournament.tournament_players',
            'tournament.tournament_players.player',
            'grouped_tournament_players',
            'groups'
        ])->first();
    }

    private function _get_gtp_id($playerString)
    {
        $gtpId = null;
        try {
            $gtpId = isset($this->replace[$playerString]) ? $this->replace[$playerString] : null;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return $gtpId;
    }

    private function _process_gtp($gtpId, &$idsToSync, $stringName = "(NA)", $groupName = "(NA)")
    {
        try {
            if ($gtpId) {
                $groupedTournamentPlayer = $this->_groupedPlayers->where('id', $gtpId)->first();
                if ($groupedTournamentPlayer) {
                    $idsToSync[$gtpId] = ['order' => null, 'by_user_id' => $this->userId];
                } else {
                    $this->_errors[] = "<span style='color:darkred;'>Player <strong>" . $stringName . "</strong> was not grouped. Add him to a group <strong>" . $groupName . "</strong> and rerun</span>";
                }
            } else {
                $this->_errors[] = "<span style='color:#644219;'>Player <strong>" . $stringName . "</strong> was not matched to local properly</span>";
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
    }

    private function _add_entry($name)
    {
        $entryData = [
            'user_id' => $this->userId,
            'name' => $name,
            'golf_id' => $this->_golf->id
        ];
        $existingEntry = $this->_golfEntryProvider->find_by_name($name, $this->_golf);
        if ($existingEntry) {
            $i = 2;
            while ($existingEntry) {
                $newName = $entryData['name'] . " (#{$i})";
                $existingEntry = $this->_golfEntryProvider->find_by_name($newName, $this->_golf);
                $i++;
            }
            $entryData['name'] = $newName;
            $this->_errors[] = "<span style='color:#4a0608;'>Duplicate Entry Name for <strong>" . $name . "</strong> Renamed >> <strong>" . $newName . "</strong></span>";
        }
        $createdRoster = $this->_golfEntryProvider->create($entryData);
        $groupData = [
            'golf_entry_id' => $createdRoster->id,
        ];
        $this->_golfGroupProvider->create_many($this->_golf->number_of_groups, $groupData);
        $createdRoster->load(['groups', 'groups.tournament_players', 'groups.tournament_players.player']);
        return $createdRoster;
    }

    private function _clean_input($raw)
    {
        $cleanData = [];
        try {
            $importRows = explode(PHP_EOL, $raw);
            $cleanData = [];
            foreach ($importRows as $row) {
                $itms = explode(',', $row);
                $cleanItem[0] = $itms[0];
                $itmCount = count($itms);
                for ($i = 1; $i < $itmCount; $i++) {
                    $cleanItem[$i] = str_replace('\'', "", trim($itms[$i]));
                }
                $cleanData[] = $cleanItem;
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            \Log::error($e->getTraceAsString());
            return [];
        }
        return $cleanData;
    }
}
