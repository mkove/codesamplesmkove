<?php

namespace App\Jobs;

use App\Events\GolfLiveUpdateEvent;
use App\Models\GolfTournament;
use App\Providers\Pool\Golf\GolfEntryProvider;
use App\Providers\Pool\Golf\GolfTournamentPlayerProvider;
use App\Providers\Pool\Golf\GolfTournamentProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PushGolfLiveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $golf;
    private $_golfProvider;

    public $tries = 1;

    public function __construct($golf, $golfProvider)
    {
        $this->_golfProvider = $golfProvider;
        $this->golf = $golf;
    }

    public function handle()
    {
        try{
            $this->golf->load([
                'pool',
                'grouped_tournament_players',
                'tournament',
                'entries',
                'entries.groups',
                'entries.groups.tournament_players',
                'tournament.tournament_players',
                'tournament.tournament_players.player',
            ]);
            $tournament = $this->golf->tournament;
            $handicaps = $this->_golfProvider->load_handicaps($this->golf);
            $tData = [
                'par' => $tournament->par,
                'status' => $tournament->status,
            ];
            $players = $this->_golfProvider->load_players_live($this->golf, $tournament, $handicaps);
            $entries = $this->_golfProvider->load_entries_live($this->golf, $handicaps);

            broadcast(new GolfLiveUpdateEvent($this->golf->id, $tData, $players, $entries));
        } catch (\Exception $e){
            __dlog($e);
        }
        return;
    }

    public function failed(\Throwable $exception)
    {
        __dlog($exception);
        // Log failure
    }
}






