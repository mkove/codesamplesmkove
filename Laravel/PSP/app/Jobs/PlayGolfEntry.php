<?php

namespace App\Jobs;

use App\Models\GolfTournament;
use App\Providers\Pool\Golf\GolfEntryProvider;
use App\Providers\Pool\Golf\GolfTournamentPlayerProvider;
use App\Providers\Pool\Golf\GolfTournamentProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


//NOT IMPLEMENTED AT THIS MOMENT
class PlayGolfEntry implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $entry;
    protected $golf;
    protected $towardScore;
    protected $handicaps;
    protected $forceReplayRounds;
    protected $roundHandicap;
    private $_golfEntryProvider;

    public $timeout = 1;
    public $tries = 1;

    public function __construct($entry, $golf, $handicaps, $forceReplayRounds = false)
    {
        $this->_golfEntryProvider = resolve(GolfEntryProvider::class);
        $this->entry = $entry;
        $this->golf = $golf;
        $this->handicaps = $handicaps;
        $this->forceReplayRounds = $forceReplayRounds;

    }

    public function handle()
    {
        try{
            $towardsScore = $this->golf->toward_score;
            $countCut = $this->golf->count_cut;
            $type = $this->golf->type;
            $roundHandicaps  = ($type === 'handicap');
            $currentRound = $this->golf->tournament->tournament_current_round;
            if($this->forceReplayRounds){
                for($r=1; $r<6;$r++){
                    $this->_golfEntryProvider->compute_round_total(
                        $this->entry,
                        $r,
                        $towardsScore,
                        $this->handicaps,
                        $this->forceReplayRounds,
                        $countCut,
                        $roundHandicaps
                    );
                }
            } else {
                $this->_golfEntryProvider->compute_round_total(
                    $this->entry,
                    $currentRound,
                    $towardsScore,
                    $this->handicaps,
                    false,
                    $countCut,
                    $roundHandicaps
                );
                if($currentRound < 5){
                    $this->_golfEntryProvider->compute_round_total(
                        $this->entry,
                        5,
                        $towardsScore,
                        $this->handicaps,
                        false,
                        $countCut,
                        $roundHandicaps);
                }
            }
            $this->entry->refresh();
            if($type === 'prize') {
                $this->_golfEntryProvider->play_prizes($this->entry, $towardsScore);
            }
            if($type === 'trackmeet'){
                $this->_golfEntryProvider->play_trackmeet($this->entry, $towardsScore);
            }
	
	
        }catch (\Exception $e){
            \Log::error($e->getMessage());
            __dlog($e);
        }
        return true;
    }
}
