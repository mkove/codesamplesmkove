<?php
    namespace App\Events;

    use App\Helpers\UserNotificationHelper;
    use App\Models\UserNotification;
    use Illuminate\Broadcasting\Channel;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Broadcasting\PrivateChannel;
    use Illuminate\Broadcasting\PresenceChannel;
    use Illuminate\Broadcasting\InteractsWithSockets;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

    class PushSquarePicks implements ShouldBroadcastNow
    {
        use SerializesModels;

        public $squares;
        public $squareboard_id;

        public function __construct($id, $squares)
        {
            $this->squareboard_id = $id;
            foreach($squares as &$square){
                $square['pending'] = false;
            }
            $this->squares = $squares;
        }

        public function broadcastAs()
        {
            return 'squarepickUpdate';
        }
        /**
         * Get the channels the event should broadcast on.
         *
         * @return \Illuminate\Broadcasting\Channel|array
         */
        public function broadcastOn()
        {
            $channel = new Channel('squarepicks.'.$this->squareboard_id);
            return  $channel;
        }

    }
