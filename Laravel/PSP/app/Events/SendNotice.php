<?php
    namespace App\Events;

    use App\Helpers\UserNotificationHelper;
    use App\Models\UserNotification;
    use Illuminate\Broadcasting\Channel;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Broadcasting\PrivateChannel;
    use Illuminate\Broadcasting\PresenceChannel;
    use Illuminate\Broadcasting\InteractsWithSockets;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

    class SendNotice implements ShouldBroadcastNow
    {
        use SerializesModels;

        public $userNotice;
        public $user_id;
        public $unread;
        public $title;

        public function __construct(UserNotification $userNotice)
        {
            $this->title = $userNotice->pool->pool_name;
            $this->userNotice = $userNotice;
            $this->user_id = $userNotice->user_id;
            $this->unread = UserNotificationHelper::unread_count($this->user_id);
        }

        public function broadcastAs()
        {
            return 'newNotif';
        }
        /**
         * Get the channels the event should broadcast on.
         *
         * @return \Illuminate\Broadcasting\Channel|array
         */
        public function broadcastOn()
        {
            $channel = new Channel('notifs.'.$this->user_id);
            return  $channel;
        }

}
