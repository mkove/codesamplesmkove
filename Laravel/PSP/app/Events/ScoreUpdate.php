<?php

	namespace App\Events;

	use Illuminate\Broadcasting\Channel;
	use Illuminate\Queue\SerializesModels;
	use Illuminate\Broadcasting\PrivateChannel;
	use Illuminate\Broadcasting\PresenceChannel;
	use Illuminate\Broadcasting\InteractsWithSockets;
	use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

	class ScoreUpdate implements ShouldBroadcast
	{
		use SerializesModels;

		public $game;
		public $changes;

		/**
		 * Create a new event instance.
		 *
		 * @return void
		 */
		public function __construct(\App\Models\Game $game, $changes = [])
		{
			$this->game = $game;
			$this->changes = $changes;
		}

		/**
		 * Get the channels the event should broadcast on.
		 *
		 * @return Channel|array
		 */
		public function broadcastOn()
		{
			return new PrivateChannel('game-update.'.$this->game->id);
		}
	}