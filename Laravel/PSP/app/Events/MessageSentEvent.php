<?php
namespace App\Events;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSentEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $message;
    public $chat_id;
    public $unread;
    /**
    * Create a new event instance.
    *
    * @return void
    */
    public function __construct($user, ChatMessage $message, Chat $chat)
    {
        $this->user = null;
        $this->chat_id = $chat->id;
        $messageData = $message->toArray();
        unset($messageData['user']);
        $this->message = $messageData;
    }

    public function broadcastAs()
    {
        return 'chat.message.sent';
    }

    /**
    * Get the channels the event should broadcast on.
    *
    * @return Channel|array
    */
    public function broadcastOn()
    {
        return new Channel('chat.'.$this->chat_id);
    }
}
