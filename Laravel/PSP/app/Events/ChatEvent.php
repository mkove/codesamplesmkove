<?php namespace App\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ChatEvent implements ShouldBroadcastNow
{
    use SerializesModels;
    public $message;
    public $chat_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */

//    public function broadcastAs()
//    {
//        return 'chat.'.$this->chat->id;
//    }

    public function __construct($chatId, $message)
    {
//        \Log::info($message);
	    $this->chat_id = $chatId;
	    $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('chat.'.$this->chat_id);
    }


    public function broadcastAs()
    {
        return 'new.message';
    }


}
