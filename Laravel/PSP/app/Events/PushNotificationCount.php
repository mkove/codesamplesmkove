<?php
    namespace App\Events;

    use App\Models\UserNotification;
    use Illuminate\Broadcasting\Channel;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Broadcasting\PrivateChannel;
    use Illuminate\Broadcasting\PresenceChannel;
    use Illuminate\Broadcasting\InteractsWithSockets;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

    class PushNotificationCount implements ShouldBroadcastNow
    {
        use SerializesModels;

         public $count;
         public $user_id;
         public $unread;


        public function __construct(int $curNoticeCount, $userId)
        {
            $this->user_id = $userId;
            $this->unread = $curNoticeCount;
//            \Log::info("Broadcasting this hsit  to ".$this->user_id);
        }

        public function broadcastAs()
        {
            return 'notifCount';
        }
        /**
         * Get the channels the event should broadcast on.
         *
         * @return \Illuminate\Broadcasting\Channel|array
         */
        public function broadcastOn()
        {
            $channel = new Channel('notifs.'.$this->user_id);
//            \Log::info($channel);
            return  $channel;
        }

    }
