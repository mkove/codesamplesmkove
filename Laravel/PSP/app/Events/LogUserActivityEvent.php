<?php

    namespace App\Events;

    use Illuminate\Queue\SerializesModels;

    class LogUserActivityEvent
    {
        use SerializesModels;

        public $model;

        /**
         * Create a new event instance.
         *
         * @param  \App\Order  $order
         * @return void
         */
        public function __construct($model)
        {
            $this->model = $model;
//            \Log::info("Logging event in event activity ");
//            \Log::info($model);
        }
    }
