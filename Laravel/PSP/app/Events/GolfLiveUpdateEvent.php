<?php
    namespace App\Events;

    use App\Models\User;
    use Illuminate\Broadcasting\Channel;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Broadcasting\PrivateChannel;
    use Illuminate\Broadcasting\PresenceChannel;
    use Illuminate\Foundation\Events\Dispatchable;
    use Illuminate\Broadcasting\InteractsWithSockets;
    use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
    use App\Providers\Pool\Golf\GolfProvider;
    class GolfLiveUpdateEvent implements ShouldBroadcastNow
    {
        use SerializesModels;
//        use Dispatchable, InteractsWithSockets, SerializesModels;

        public $user;

        public $golfId;
        public $entries;
        public $players;
        public $sort;
        public $tournament;
        public $message;
        public $error;

        /**
         * GolfLiveUpdateEvent constructor.
         * @param $golf
         * @param $user
         * @param GolfProvider $golfProvider
         */
        public function __construct($id, &$tournament, &$players, &$entries)
        {
            try {
                $this->golfId = $id;
                $this->tournament = $tournament;
                $this->entries = $entries;
                $this->players = $players;
            } catch (\Exception $e){
                \Log::warning($e->getMessage());
                \Log::warning($e->getTraceAsString());
            }

        }

        public function broadcastAs()
        {
            return 'golf.live.update';
        }

        /**
         * Get the channels the event should broadcast on.
         *
         * @return Channel|array
         */
        public function broadcastOn()
        {
            return new Channel('live.golf.'.$this->golfId);
        }
    }
