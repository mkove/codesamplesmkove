<?php

namespace App\Events;

use App\Helpers\SchwindyHelper;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class SchwindyUpdateEvent implements ShouldBroadcastNow
{
    use SerializesModels;

    public $schwindy_id;
    public $picks;
    public $games;
    public $userTeams;
    public $message;
    public $error;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($schwindy, $games)
    {
        try {
            $responseData = SchwindyHelper::build_live_response_data($schwindy, $games);

            $this->userTeams = isset($responseData['userTeams']) ? $responseData['userTeams'] : [];
            $this->picks = isset($responseData['picks']) ? $responseData['picks'] : [];
            $this->games = isset($responseData['games']) ? $responseData['games'] : [];
            $this->schwindy_id = isset($responseData['schwindy_id']) ? $responseData['schwindy_id'] : null;
            $this->error = isset($responseData['error']) ? $responseData['error'] : null;
            $this->message = isset($responseData['message']) ? $responseData['message'] : null;
        } catch (\Exception $e) {
            \Log::warning($e->getMessage());
            \Log::warning($e->getTraceAsString());
        }
    }

    private function _calc_percent($allPicks, &$games, $schwindy)
    {
        $userCount = $schwindy->pool->users->count();
        foreach ($games as &$game) {
            $homeTeamPercent = SchwindyHelper::team_percent($game->home_team_id, $allPicks, $schwindy->config, $userCount);
            $awayTeamPercent = SchwindyHelper::team_percent($game->away_team_id, $allPicks, $schwindy->config, $userCount);
            $game->home_percent = $homeTeamPercent;
            $game->away_percent = $awayTeamPercent;
        }
    }

    public function broadcastAs()
    {
        return 'schwindy.live.update';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('schiwndy.' . $this->schwindy_id);
    }
}
