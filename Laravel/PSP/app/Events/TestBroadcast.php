<?php
	namespace App\Events;
	
	use App\Models\UserNotification;
	use Illuminate\Broadcasting\Channel;
	use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
	use Illuminate\Queue\SerializesModels;
	use Illuminate\Broadcasting\PrivateChannel;
	use Illuminate\Broadcasting\PresenceChannel;
	use Illuminate\Broadcasting\InteractsWithSockets;
	use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
	
	class TestBroadcast implements ShouldBroadcastNow
	{
		use SerializesModels;
		
		public $data;
		
		
		public function __construct($hash = null)
		{
			$this->data = ['broadcasted' => true, 'hash' => $hash, 'data' => date('U'), 'random' => rand(1,9999)];
		}
		
		public function broadcastAs()
		{
			return 'broadcastTest';
		}
		/**
		 * Get the channels the event should broadcast on.
		 *
		 * @return \Illuminate\Broadcasting\Channel|array
		 */
		public function broadcastOn()
		{
			$channel = new Channel('testbroadcast');
			return  $channel;
		}
		
	}
