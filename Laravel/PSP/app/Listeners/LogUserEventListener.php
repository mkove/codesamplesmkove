<?php

    namespace App\Listeners;

    use App\Events\LogUserActivityEvent;

    class LogUserEventListener
    {
        /**
         * Create the event listener.
         *
         * @return void
         */
        public function __construct()
        {
            //
        }

        /**
         * Handle the event.
         *
         * @param  \App\Events\OrderShipped  $event
         * @return void
         */
        public function handle(LogUserActivityEvent $event)
        {
            // Access the order using $event->order...
        }
    }
