<?php

    namespace App\Console\Commands\GoalServeFeed\NCAAFootball;

    use App\Helpers\SiteHelper;
    use App\Helpers\SiteHelper as SH;
    use App\Models\Game;
    use App\Providers\Game\Team\Team as TeamProvider;
    use App\Providers\Game\Game as GameProvider;
    use App\Providers\GameEvent\GameEvent as GameEventProvider;
    use Illuminate\Console\Command;
    use App\Helpers\Goalserve\Professional\NFL as GS_NFL;
    class GetData extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        private $_season;
        private $_type;
        private $_xmlFile;
        private $_gameProvider;
        private $_gameEventProvider;
        private $_teamProvider;
        protected $signature = 'goalserve:ncaa {type}';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Get NCAA Data';
//        CODES: for status
        /*
         * Postponed
        Cancelled
        Abandoned
        Not Started
        1st Quarter
        2nd Quarter
        3rd Quarter
        4th Quarter
        Overtime
        Final
        After Over Time - game finished after overtime
        Break Time
        Awarded
        Walk Over
        Interrupted
        Delayed
         * */
        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct(
            TeamProvider $teamProvider,
            GameProvider $gameProvider,
            GameEventProvider $gameEventProvider
    ) {
            $this->_season = [
                'ncaa' => [
                    'start' => date('d.m.Y', strtotime("-14 days")),
                    'end' => date('d.m.Y', strtotime("+14 days"))
                ]
            ];
            $this->_teamProvider = $teamProvider;
            $this->_gameProvider = $gameProvider;
            $this->_gameEventProvider = $gameEventProvider;

            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle() {

            $this->_type = $this->argument('type');
            switch ($this->_type) {
                case 'score':

                    $this->_xmlFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/fbs-scores";
                    $this->_get_score();
                    break;
                case 'odds':

                    $this->_xmlFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/fbs-shedule?date1={$this->_season['ncaa']['start']}&date2={$this->_season['ncaa']['end']}&showodds=1";
                    $this->_get_odds();
                    break;
                case 'schedule':

                    $this->_xmlFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/fbs-shedule";
                    $this->_get_schedule();

                    break;
            }
        }

        private function _get_score(){
            $xmlRaw = GS_NFL::get_xml($this->_xmlFile);
            $xml = simplexml_load_string($xmlRaw);

            foreach($xml->category->match as $game) {
                $contestId = (string)$game['contestID'];
                $localGame = $this->_gameProvider->get_game_by_contest_id($contestId, 'football', 'college');
                if($localGame) {
//                    \Log::info("Doing ".$localGame->away_team_id." at ".$localGame->home_team_id." ".$localGame->tournament_name);
                    if($game['status'] != "Not Started") {
                        if($game['status'] == 'Halftime'){
                            $curQ = 2;
                            $compQ = 2;
                        } else {
                            $curQ = SiteHelper::convert_qurater_to_number($game['status']);
                            if($curQ < 6 && $curQ > 0){
                                $compQ = $curQ - 1;
                            } else {
                                $compQ = 0;
                            }
                        }
                        $homeTeam = $this->_get_team(intval($game->hometeam['id']), strval($game->hometeam['name']));
                        $awayTeam = $this->_get_team(intval($game->awayteam['id']), strval($game->awayteam['name']));
                        $updateData = [
                            'status' => strval($game['status']),
                            'hometeam_q1' => intval($game->hometeam['q1']),
                            'hometeam_q2' => intval($game->hometeam['q2']),
                            'hometeam_q3' => intval($game->hometeam['q3']),
                            'hometeam_q4' => intval($game->hometeam['q4']),
                            'hometeam_ot' => intval($game->hometeam['ot']),
                            'hometeam_totalscore' => intval($game->hometeam['totalscore']),
                            'awayteam_q1' => intval($game->awayteam['q1']),
                            'awayteam_q2' => intval($game->awayteam['q2']),
                            'awayteam_q3' => intval($game->awayteam['q3']),
                            'awayteam_q4' => intval($game->awayteam['q4']),
                            'awayteam_ot' => intval($game->awayteam['ot']),
                            'tournament_name' => $awayTeam->name." at ".$homeTeam->name,
                            'hometeam_id' => $homeTeam->provider_id,
                            'hometeam_name' => $homeTeam->name,
                            'home_team_id' => $homeTeam->id,
                            'awayteam_id' => $awayTeam->provider_id,
                            'awayteam_name' => $awayTeam->name,
                            'away_team_id' => $awayTeam->id,
                            'ball' => (strval($game->awayteam['drive']) != '') ?  'awayteam' : 'hometeam',
                            'current_quarter' => $curQ,
                            'completed_quarter' => $compQ,
                            'timer' => strval($game['timer']),
                            'awayteam_totalscore' => intval($game->awayteam['totalscore']),
                        ];
                        $this->_gameProvider->update($localGame->id, $updateData);
                        $event = $this->_update_game_events($localGame->id, $game);
                    } else {
                        $homeTeam = $this->_get_team(intval($game->hometeam['id']), strval($game->hometeam['name']));
                        $awayTeam = $this->_get_team(intval($game->awayteam['id']), strval($game->awayteam['name']));
                        $updateData = [
                            'tournament_name' => $awayTeam->name." at ".$homeTeam->name,
                            'hometeam_id' => $homeTeam->provider_id,
                            'hometeam_name' => $homeTeam->name,
                            'home_team_id' => $homeTeam->id,
                            'awayteam_id' => $awayTeam->provider_id,
                            'awayteam_name' => $awayTeam->name,
                            'away_team_id' => $awayTeam->id,
                        ];
                        $this->_gameProvider->update($localGame->id, $updateData);
                    }
                }
                else {

                }
            }

        }

        private function _get_odds(){
            $gameOdds = GS_NFL::process($this->_xmlFile);
            foreach($gameOdds as $id => $game)
            {
                try{
                    $contestId = $game['contestID'];
                    $localGame = $this->_gameProvider->get_game_by_contest_id($contestId, 'football', 'professional');
                    if($localGame){

                        $updateData = [];
                        $updateData['hometeam_handicap'] = $game['hometeam_handicap'];
                        $updateData['awayteam_handicap'] = $game['awayteam_handicap'];
                        $updateData['over_under'] = $game['over_under'];

                        $return = $this->_gameProvider->update($localGame->id, $updateData);
                    }

                }  catch (Exception $e){
                    \Log::error($e->getMessage());
                    \Log::error($e->getTraceAsString());
                }
            }
        }

//        TODO: REMOvE ##
        private function _get_schedule(){
            $realGames = GS_NFL::process($this->_xmlFile);

            foreach($realGames as $id => $game)
            {
                try{
                    $localGame = null;
                    $homeTeam = $this->_get_team($game['hometeam_id'],$game['hometeam_name']);
                    $awayTeam = $this->_get_team($game['awayteam_id'],$game['awayteam_name']);
                    $contestId = (string)$game['contestID'];

                    $localGame = $this->_gameProvider->get_game_by_contest_id($contestId, 'football', 'college');
                    if(!$localGame){
                        $localGame = $this->_gameProvider->create($game);
//                         $this->_gameProvider->get_game_by_contest_id($contestId, 'football', 'college');
                    }
                    $game['sport'] = 'football';
                    $game['league'] = 'college';
                    $game['tournament_id'] = strval($id);
                    $game['tournament_name'] = $awayTeam->name." at ".$homeTeam->name;
                    $game['hometeam_id'] = $homeTeam->provider_id;
                    $game['hometeam_name'] = $homeTeam->name;
                    $game['home_team_id'] = $homeTeam->id;
                    $game['awayteam_id'] = $awayTeam->provider_id;
                    $game['awayteam_name'] = $awayTeam->name;
                    $game['away_team_id'] = $awayTeam->id;
                    unset($game['hometeam_handicap']);
                    unset($game['awayteam_handicap']);
                    unset($game['over_under']);
                    if(!isset($localGame->id)){
//                        dd($localGame, $game);
                    }
                    $this->_gameProvider->update($localGame->id, $game);
                }  catch (\Illuminate\Database\QueryException $e){
                    \Log::error($e->getMessage());
                    \Log::error($e->getTraceAsString());
                }
            }
        }

        private function _create_team($goalServeTeam){
            $team = $this->_teamProvider->create([
                'name' => $goalServeTeam['name'],
                'short_name' => "##",
                'alt_name' => $goalServeTeam['name'],
                'sport' => 'football',
                'provider_id' => $goalServeTeam['id'],
                'league' => 'college',
                'active' => true
            ]);
            $team->short_name = "##";
            $team->save();
            return $team;

        }

        private function _update_game_events($localGameId, $game){

            $gameEvents = [];
            $events = $game->events;
            $quarters = [
                'q1' => 'firstquarter',
                'q2' => 'secondquarter',
                'q3' => 'thirdquarter',
                'q4' => 'fourthquarter',
                'ot' => 'overtime',
            ];
            foreach($quarters as $q => $qstring) {
                if(isset($events->{$qstring}) && $events->{$qstring}) {
                    foreach($events->{$qstring}->event as $event){
                        try {
                            $time = SH::time_to_seconds(strval($event->attributes()['min']));
                            $eventId = ($event->attributes()['id']) ? (int)$event->attributes()['id'] : null;
                            $gameEvents = [
                                'event_id' => $eventId,
                                'quarter' => $q,
                                'action' => strval($event->attributes()['player']),
                                'type' => strval($event->attributes()['type']),
                                'by' => strval($event->attributes()['team']),
                                'home_score' => ($event->attributes()['home_score']) ? (int)$event->attributes()['home_score'] : "0",
                                'away_score' =>  ($event->attributes()['away_score']) ? (int)$event->attributes()['away_score'] : "0",
                                'game_id' => ($localGameId) ? (int)$localGameId : 0,
                                'clock' => $time,
                                'player_id' => ($event->attributes()['player_id']) ? (int)$event->attributes()['player_id'] : 0,
                                'time' => $time,
                            ];
                            $eve = $this->_gameEventProvider->create_update($eventId, $gameEvents);

                        } catch (\Exception $e) {
                            \Log::error($e->getMessage());
                            \Log::error($e->getTraceAsString());
                        }

                    }
                }
            }

        }

        private function _get_team($providerId, $providerName){
//            \Log::info("Getting team ".$providerId." with ".$providerName);
            $team = $this->_teamProvider->find_by_provider_id($providerId);
            if(!$team) {
//                \Log::info("no team");
                $gsTeam = [
                    'name' => strval($providerName),
                    'id' => intval($providerId)
                ];
                $team = $this->_create_team($gsTeam);
            }
            return $team;
        }

    }
