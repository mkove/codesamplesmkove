<?php

    namespace App\Console\Commands\GoalServeFeed\NFL;

    use App\Helpers\GameHelper;
    use App\Helpers\SiteHelper;
    use App\Helpers\TeamHelper;
    use Illuminate\Console\Command;
    use App\Helpers\Goalserve\Professional\NFL as GS_NFL;
    use App\Helpers\SiteHelper as SH;
    use Illuminate\Support\Facades\Log;
    use App\Providers\Game\Game as GameProvider;
    use App\Providers\GameEvent\GameEvent as GameEventProvider;
    use App\Providers\Game\Team\Team as TeamProvider;

    class GetData extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        private $_season;
        private $_type;
        private $_xmlFile;
        private $_gameProvider;
        private $_gameEventProvider;
        protected $signature = 'goalserve:nfl {type}';

//        CODES: for status
/*
 * Postponed
Cancelled
Abandoned
Not Started
1st Quarter
2nd Quarter
3rd Quarter
4th Quarter
Overtime
Final
After Over Time - game finished after overtime
Break Time
Awarded
Walk Over
Interrupted
Delayed
 * */
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Get NFL Data (Schedule and Scores)';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct(GameProvider $gameProvider, GameEventProvider $gameEventProvider, TeamProvider $teamProvider) {
            $this->_season = [
                'nfl' => [
                    'start' => date('d.m.Y', strtotime("-14 days")),
                    'end' => date('d.m.Y', strtotime("+14 days"))
                ]
            ];
            $this->_gameProvider = $gameProvider;
            $this->_gameEventProvider = $gameEventProvider;
            $this->_teamProvider = $teamProvider;

            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle() {
            $this->_type = $this->argument('type');
            switch ($this->_type) {
                case 'score':
                    $this->_xmlFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/nfl-scores";
                    $this->_get_score();
                    break;
                case 'odds':

                    $this->_xmlFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/nfl-shedule?date1={$this->_season['nfl']['start']}&date2={$this->_season['nfl']['end']}&showodds=1";
                    $this->_get_odds();
                    break;
                case 'schedule':
                    $this->_xmlFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/nfl-shedule";
                    $this->_get_schedule();
                    break;
            }
        }

        private function _get_score(){
            try {
                $xmlRaw = GS_NFL::get_xml($this->_xmlFile);
                $xml = simplexml_load_string($xmlRaw);
                foreach($xml->category->match as $game) {
                    try {
                        $status = strval($game['status']);

                        $contestId = (string)$game['contestID'];
                        $localGame = $this->_gameProvider->get_game_by_contest_id($contestId, 'football', 'professional');
                        if($status != "Not Started") {
                            if($status == 'Halftime'){
                                $curQ = 2;
                                $compQ = 2;
                            } else {
                                $curQ = SiteHelper::convert_qurater_to_number($status);
                                if($curQ < 6 && $curQ > 0){
                                    $compQ = $curQ - 1;
                                } else {
                                    $compQ = 0;
                                }
                            }

                            $updateData = [
                                'status' => strval($status),
                                'hometeam_q1' => intval($game->hometeam['q1']),
                                'hometeam_q2' => intval($game->hometeam['q2']),
                                'hometeam_q3' => intval($game->hometeam['q3']),
                                'hometeam_q4' => intval($game->hometeam['q4']),
                                'hometeam_ot' => intval($game->hometeam['ot']),
                                'hometeam_totalscore' => intval($game->hometeam['totalscore']),
                                'awayteam_q1' => intval($game->awayteam['q1']),
                                'awayteam_q2' => intval($game->awayteam['q2']),
                                'awayteam_q3' => intval($game->awayteam['q3']),
                                'awayteam_q4' => intval($game->awayteam['q4']),
                                'awayteam_ot' => intval($game->awayteam['ot']),
                                'awayteam_totalscore' => intval($game->awayteam['totalscore']),
                                'ball' => (strval($game->awayteam['drive']) != '') ?  'awayteam' : 'hometeam',
                                'current_quarter' => $curQ,
                                'completed_quarter' => $compQ,
                                'timer' => strval($game['timer']),
                                'start' => date('Y-m-d H:i:s', strtotime($game['date']." ".$game['time'])),
                                'gmt' => date('Y-m-d H:i:s', strtotime($game['date']." ".$game['time'])),
                                'locked' => ($status == 'Final' || $status == 'After Over Time')
                            ];

                            $this->_gameProvider->update($localGame->id, $updateData);
                            $this->_update_game_events($localGame->id, $game);
                        }
                    } catch(\Exception $e){
                        \Log::error($e->getMessage());
                        \Log::error($e->getTraceAsString());
                    }

                }
            } catch (\Exception $e){
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

        }



        private function _get_odds(){
            $gameOdds = GS_NFL::process($this->_xmlFile);
            foreach($gameOdds as $id => $game)
            {
                try{
                    $contestId = $game['contestID'];
                    $localGame = $this->_gameProvider->get_game_by_contest_id($contestId, 'football', 'professional');
                    if($localGame){

                        $updateData = [];
                        $updateData['hometeam_handicap'] = $game['hometeam_handicap'];
                        $updateData['awayteam_handicap'] = $game['awayteam_handicap'];
                        $updateData['over_under'] = $game['over_under'];

                        $return = $this->_gameProvider->update($localGame->id, $updateData);
                    }

                }  catch (Exception $e){
                    \Log::error($e->getMessage());
                    \Log::error($e->getTraceAsString());
                }
            }
        }

        private function _get_schedule(){
            $realGames = GS_NFL::process($this->_xmlFile);
            foreach($realGames as $id => $game)
            {
                try{
                    $hometeam = $this->_teamProvider->find_by_provider_id($game['hometeam_id']);
                    if(!$hometeam){
                        $hometeam = $this->_teamProvider->create([
                            'name' => $game['hometeam_name'],
                            'short_name' => "XX",
                            'alt_name' => $game['hometeam_name'],
                            'sport' => 'football',
                            'provider_id' => $game['hometeam_id'],
                            'league' => 'professional',
                            'active' => true
                        ]);
                    }
                    $awayteam = $this->_teamProvider->find_by_provider_id($game['awayteam_id']);
                    if(!$awayteam){
                        $awayteam = $this->_teamProvider->create([
                            'name' => $game['awayteam_name'],
                            'short_name' => "XX",
                            'alt_name' => $game['awayteam_name'],
                            'sport' => 'football',
                            'provider_id' => $game['awayteam_id'],
                            'league' => 'professional',
                            'active' => true
                        ]);
                    }
                    $curQ = SiteHelper::convert_qurater_to_number($game['status']);
                    if($curQ < 6 && $curQ > 0){
                        $compQ = $curQ - 1;
                    } else {
                        $compQ = 0;
                    }
                    $game['sport'] = 'football';
                    $game['league'] = 'professional';
                    $game['tournament_id'] = strval($id);
                    $game['over_under'] = 0;
                    $game['home_team_id'] = $hometeam->id;
                    $game['away_team_id'] = $awayteam->id;
                    $game['ball'] = " ";
                    $game['round_id'] = 0;
                    $game['round'] = " ";
                    $game['round_name'] = " ";
                    $game['round_shortname'] = " ";
                    $game['hometeam_shortname'] = " ";
                    $game['awayteam_shortname'] = " ";
                    $game['awayteam_halftime'] = 0;
                    $game['hometeam_halftime'] = 0;
                    $game['stadium'] = " ";
                    $game['stadium_id'] = 0;
                    $game['season_id'] = 0;
                    $game['current_quarter'] = 0;
                    $game['completed_quarter'] = 0;
                    $game['season_year'] = __conf('schwindy.season');
                    $contestId = $game['contestID'];
                    $localGame = $this->_gameProvider->get_game_by_contest_id($contestId, 'football', 'professional');
                    if(!$localGame){
                        $this->_gameProvider->create($game);
                    }
                    else {
                        unset($game['hometeam_handicap']);
                        unset($game['awayteam_handicap']);
                        unset($game['over_under']);
                        $this->_gameProvider->update($localGame->id, $game);
                    }
                }  catch (\Illuminate\Database\QueryException $e){
                    \Log::error($e->getMessage());
                    \Log::error($e->getTraceAsString());
                }
            }
        }

        private function _update_game_events($localGameId, $game){
            $gameEvents = [];
            $events = $game->events;
            $quarters = [
                'q1' => 'firstquarter',
                'q2' => 'secondquarter',
                'q3' => 'thirdquarter',
                'q4' => 'fourthquarter',
                'ot' => 'overtime',
            ];
            foreach($quarters as $q => $qstring) {
                try{
                    if(isset($events->{$qstring}) && $events->{$qstring}) {
                        foreach($events->{$qstring}->event as $event){
                            try {
                                $clock = SH::time_to_seconds(strval($event->attributes()['min']));
                                $eventId = ($event->attributes()['id']) ? (int)$event->attributes()['id'] : null;
                                $gameEvents = [
                                    'event_id' => $eventId,
                                    'quarter' => $q,
                                    'action' => strval($event->attributes()['player']),
                                    'type' => strval($event->attributes()['type']),
                                    'by' => strval($event->attributes()['team']),
                                    'home_score' => ($event->attributes()['home_score']) ? (int)$event->attributes()['home_score'] : "0",
                                    'away_score' =>  ($event->attributes()['away_score']) ? (int)$event->attributes()['away_score'] : "0",
                                    'game_id' => ($localGameId) ? (int)$localGameId : 0,
                                    'time' => $clock,
                                    'clock' => $clock,
                                    'player_id' => ($event->attributes()['player_id']) ? (int)$event->attributes()['player_id'] : 0,
                                ];

//                            var_dump($gameEvents);
                                $eve = $this->_gameEventProvider->create_update($eventId, $gameEvents);
                            } catch (\Exception $e) {
                                \Log::error($e->getMessage());
                                \Log::error($e->getTraceAsString());
                            }

                        }
                    }
                } catch (\Exception $e){
                    \Log::error($e->getMessage());
                    \Log::error($e->getTraceAsString());
                }

            }

        }
    }
