<?php

    namespace App\Console\Commands\Billing;

    use App\Helpers\SiteHelper;
    use App\Providers\Pool\Golf\GolfProvider;
    use App\Providers\Pool\Golf\GolfEntryProvider;
    use App\Providers\Pool\Golf\GolfTournamentPlayerProvider;
    use App\Providers\Pool\Golf\GolfTournamentProvider;
    use Illuminate\Console\Command;


    class ChargeGolfCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */

        private $_golfEntryProvider;
        private $_golfProvider;
        private $_golfTournamentProvider;
        private $_golfTournamentPlayerProvider;
        private $_force;

        protected $signature = 'billing:charge-golf';
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Charge Golf Invoices (Runs on Thur)';


        public function __construct(
            GolfEntryProvider $grp,
            GolfProvider $g,
            GolfTournamentProvider $gtp,
            GolfTournamentPlayerProvider $gtpp
        ) {
            $this->_golfTournamentProvider = $gtp;
            $this->_golfProvider = $g;
            $this->_golfEntryProvider = $grp;
            $this->_golfTournamentPlayerProvider = $gtpp;
            $this->_force = SiteHelper::conf('golf.play_force_replay_entries', 'boolean', 1);
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle() {

            //TODO
//            1. Find all pools this week
//            2.
        }
    }
