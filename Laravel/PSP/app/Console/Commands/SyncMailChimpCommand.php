<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class SyncMailChimpCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mailchimp:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync subscription status in mailchimp to psp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // server api 
        $mailchimp_api_key = __conf('system.mailchimp_api_key', 'text', "826161a71aedec0922ab9f6b05bd7d0e-us5");
        $mailchimp_server_prefix = __conf('system.mailchimp_server_prefix', 'text', "us5");
        $mailchimp_listId = __conf('system.mailchimp_list_id', 'text', "4b935ed2a4");

        $client = new \MailchimpMarketing\ApiClient();
        $client->setConfig([
            'apiKey' => $mailchimp_api_key,
            'server' => $mailchimp_server_prefix,
        ]);

        $response = $client->lists->getListMembersInfo($mailchimp_listId);
        $total_items = $response->total_items;
        $offsets = intdiv($total_items, 1000);
        $last_offset = $total_items % 1000;

        for ($offset = 0; $offset < $offsets; $offset++) {
            $response = $client->lists->getListMembersInfo($mailchimp_listId, $fields = null, $exclude_fields = null, $count = '1000', $offset = $offset);
            $members = $response->members;

            foreach ($members as $member) {
                $this->update_user_status($member->email_address, $member->status);
            }
        }
        $response = $client->lists->getListMembersInfo($mailchimp_listId, $fields = null, $exclude_fields = null, $count = $last_offset, $offset = $offset);
        $members = $response->members;

        foreach ($members as $member) {
            $this->update_user_status($member->email_address, $member->status);
        }
        return true;
    }

    public function update_user_status($email, $status)
    {
        $user = User::where('email', '=', $email)->first();
        $status = $status == 'subscribed' ? 0 : 1;
        if ($user && $status != $user->transactional_only) {

            $userData = [
                'transactional_only' => $status
            ];
            $user->update($userData);
        }
        return;
    }
}
