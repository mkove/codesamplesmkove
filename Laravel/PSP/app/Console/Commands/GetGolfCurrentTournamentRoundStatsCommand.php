<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\GolfPlayerStatsRound;
use App\Helpers\GolfHelper;
use App\GolfPlayerRoundStat;
use App\Models\php;
class GetGolfCurrentTournamentRoundStatsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'golf:tournament_rounds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Tournament rounds and stats';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


       $golfHelper = new GolfHelper();
       $tournamentInfo = $golfHelper->getCurrentTournamentInfo();

        $tournament = php::where('tournament_id', $tournamentInfo['tournament_id'])->first();

		$tournament_id = $tournamentInfo['tournament_id'];
		$rounds_data = $golfHelper->getRoundsAsCollection();

        // if ($tournament_id) {


        foreach ($rounds_data  as $dd) {
			$rn = array();
			$data['tournament_id'] = $tournament_id;
			$data['external_id'] = $dd['player_id'];
			foreach($dd['rounds']->round as $round){
				$rn[] = $round['result'];
			}

			$data['round_1'] = intval($rn[0]);
			$data['round_2'] = intval($rn[1]);
			$data['round_3'] = intval($rn[2]);
			$data['round_4'] = intval($rn[3]);

			$player = GolfPlayerStatsRound::where('tournament_id', $tournament_id)->where('external_id',  $data['external_id'])->first();
			if(!$player){

				GolfPlayerStatsRound::create($data);
			}
			else{
				GolfPlayerStatsRound::where('tournament_id', $tournament_id)->where('external_id',  $data['external_id'])->update($data);
			}
        }

			// foreach($rounds_data as $rounds_d) {
				// foreach($tournamentinfo->player as $tournament_player) {
					// foreach($tournament_player->rounds as $tournament_player_round) {
						// foreach ($rounds_data->rounds as $round) {

						  // $tournament_round['player_id'] = $tournament_player['id'];
						  // $tournament_round['round_number'] = $round['number'];
						  // $tournament_round['round_result'] = $round['result'];

						  // GolfPlayerRound::create($tournament_round);
						// }
					// }
				// }
			// }
		// }
        #dd($tournament_round);

        // 2nd Table... Golf Player Round Stats
        /*if($tournamentInfo) {
            foreach ($tournamentInfo->tournament->player as $tournament_player) {
                $players_round_stats_count = GolfPlayerRoundStat::where('player_id', $tournament_player['id'])->whereDate('created_at',date("Y-m-d"))->get();//->count();
                foreach ($tournament_player->stats->rounds as $tournament_player_stat_rounds) {
                    foreach ($tournament_player_stat_rounds->round as $tournament_player_stat_round) {

                        $round_stats ["hole_number"] = $tournament_player_stat_round->hole['number'];
                        $round_stats ["par"] = $tournament_player_stat_round->hole['par'];
                        $round_stats ["score"] = $tournament_player_stat_round->hole['score'];
                        $round_stats ["player_id"] = $tournament_player['id'];
                        $round_stats ["round_id"] = $tournament_player_stat_round["number"];
                        //print_r($players_round_stats_count);
                        if(count($players_round_stats_count) < 1){
                            GolfPlayerRoundStat::create($round_stats);
                        }else{
//                            GolfPlayerRoundStat::where('player_id',$tournament_player['id'])->where('round_id',$tournament_player_stat_round["number"])->update(
//                                [
//                                    'hole_number' => $tournament_player_stat_round->hole['number'],
//                                    'par' => $tournament_player_stat_round->hole['par'],
//                                    'score' => $tournament_player_stat_round->hole['score'],
//                                    'round_id' => $tournament_player_stat_round["number"]
//                                ]
//                            );
                        }
                    }
                }
            }
        }else{
            dd("Tournament not found");
        }
        dd("exit");*/
    }
}
