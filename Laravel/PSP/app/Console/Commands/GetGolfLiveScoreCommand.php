<?php

namespace App\Console\Commands;
use App\Helpers\GolfHelper;
use Illuminate\Console\Command;
use App\Models\php;
use Carbon\Carbon;
use App\GolfPlayerStat;
use App\Models\GolfPlayersInTournament;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class GetGolfLiveScoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'golf:score';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get score and save to DB

		 $golfHelper = new GolfHelper();
        $tournamentInfo = $golfHelper->getCurrentTournamentInfo();
	#	dd($tournamentInfo);
        $current_score = $golfHelper->getCurrentScoreAsCollection();
//dd($current_score);
//        DB::beginTransaction();
//        try {

        $tournament = php::where('tournament_id', $tournamentInfo['tournament_id'])->first();


		$player_data_id =array();

		$tournament_id = $tournamentInfo['tournament_id'];
        if ($tournament_id) {

			//$tournament = GolfTournament::where('tournament_id', $tournamentInfo['tournament_id'])->update(array('tournament_current_round'=>$tournamentInfo['status'],'par'=>$tournamentInfo['par']));

			$tournament = php::where('tournament_id', $tournamentInfo['tournament_id'])->update(array('tournament_current_round'=>$tournamentInfo['status']));

            $data = [];
            foreach ($current_score as $score) {
					 $p = $score['player_id'];

					$player_data_id[] = intval($p);
					$rn = array();

					foreach($score['rounds']->round as $round){
						$rn[] = $round['result'];
					}


					if($rn[0]!='--'){
						$score['round_1'] = intval($rn[0]) - $tournamentInfo['par'];
					}
					else{
						$score['round_1'] = 0;
					}

					if($rn[1]!='--'){
						$score['round_2'] = intval($rn[1]) - $tournamentInfo['par'];
					}
					else{
						$score['round_2'] = 0;
					}

					if($rn[2]!='--'){
						$score['round_3'] = intval($rn[2]) - $tournamentInfo['par'];
					}
					else{
						$score['round_3']=0;
					}

					if($rn[3]!='--'){
						$score['round_4'] = intval($rn[3]) - $tournamentInfo['par'];
					}
					else{
						$score['round_4'] = 0;
					}

                    $data  = [
                        'player_id' => $score['player_id'],
                        'player_name' => $score['player_name'],
                        'tournament_id' => $tournament_id,
                        'scoreToPar' => intval($score['scoreToPar']),
                        'pos' => intval($score['pos']),
                        'par' => $score['par'],
                        'hole' => $score['hole'],
                        'total' =>  intval($score['total']),
                        'round_1' =>  intval($score['round_1']),
                        'round_2' =>  intval($score['round_2']),
                        'round_3' =>  intval($score['round_3']),
                        'round_4' =>  intval($score['round_4']),
                        'created_at' => Carbon::now()
                    ];
					if($score['today']){
                        $data['today'] = intval($score['today']);
                    }

				$player = GolfPlayerStat::where('tournament_id', $tournament_id)->where('player_id',  $score['player_id'])->first();
				if(!$player){
					GolfPlayerStat::create($data);
				}
				else{
					GolfPlayerStat::where('tournament_id', $tournament_id)->where('player_id',  $score['player_id'])->update($data);
				}



            }

 		//dd($player_data_id);
			// $remaing_players = GolfPlayersInTournament::where('tournament_external_id', $tournament_id)->whereNotIn('player_id', $player_data_id);
				// dd($remaing_players);
				// foreach($remaing_players as $player){
					// dd($player);
					// $data  = [
                        // 'player_id' => $score['player_id'],
                        // 'player_name' => $score['player_name'],
                        // 'tournament_id' => $tournament_id,
                        // 'scoreToPar' => intval($score['scoreToPar']),
                        // 'pos' => "",
                        // 'par' => "CUT",
                        // 'hole' => $score['hole'],
                        // 'today' =>  "0",
                        // 'total' => "0",
                        // 'round_1' =>  "0",
                        // 'round_2' =>  "0",
                        // 'round_3' =>  "0",
                        // 'round_4' => "0",
                        // 'created_at' => Carbon::now()
                // ];
                    // GolfPlayerStat::updateOrCreate(['player_id'=> $player->id],$data);


                // }
        }
		/****************************************************/
    }
}
