<?php

namespace App\Console\Commands\Play;

use App\Events\Pool\Squareboard\SquareBoardLivePushEvent;
use App\Events\Pool\Squareboard\SquareBoardLiveUpdateEvent;
use App\Helpers\GameHelper;
use App\Mail\LockToggle;
use App\Models\GameEvent;
use App\Models\Square;
use App\Models\SquareBoard;
use App\Models\SquareWin;
use Illuminate\Console\Command;
use App\Helpers\GoalServeHelper;
use App\Helpers\SiteHelper;
use App\Helpers\SquareHelper;
use Illuminate\Support\Facades\Log;

class PlaySquare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    private $_season;
    private $_lock;
    private $_visible;
    private $_game_id;
    protected $chatProvider;
    protected $signature = 'play:square {id?} {--force=0} {--gameId=}';
    protected $squareBoardService;
    protected $squareBoardNumberService;
    protected $gameEventService;
    protected $gameService;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Play squares command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        \App\Providers\Pool\SquareBoard\SquareBoard $squareBoardService,
        \App\Providers\Pool\SquareBoard\SquareBoardNumber\SquareBoardNumber $squareBoardNumberService,
        \App\Providers\GameEvent\GameEvent $gameEventService,
        \App\Providers\Game\Game $gameService,
        \App\Providers\Pool\SquareBoard\Square\Square $squareService,
        \App\Providers\Chat\ChatService $chat
    ) {
        $this->squareBoardService = $squareBoardService;
        $this->squareBoardNumberService = $squareBoardNumberService;
        $this->gameEventService = $gameEventService;
        $this->squareService = $squareService;
        $this->gameService = $gameService;
        $this->chatProvider = $chat;
        $this->squareboards = null;
        $this->force = false;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $force = $this->option('force') ?? __conf('Illuminate\Support\Str::slug', 'boolean', '1'); // $this->argument('force');
        $this->force = $force;

        $this->_game_id = $this->option('gameId');
        //get teh boards
        $this->_get_boards($id, $force);
        //    dump("Got ", $id, $force);
        if ($this->force) {

            //clean up bad plays
            foreach ($this->squareboards as $squareBoard) {
                $this->_delete_bad_plays($squareBoard);
                $this->squareBoardService->update($squareBoard, ['play_status' => 'new']); // update and ready for replay
            }
            //reset the boards
            $this->squareboards = null; // reset
            $this->_get_boards($id, $this->force);
        }
        $onof = ($this->force) ? "ON" : "OFF";

        foreach ($this->squareboards as $squareBoard) {
            try {
                $game = $squareBoard->game;
                $gameSt = ($game) ? $game->status : "";
                if (($game && GameHelper::game_in_play($game, $force)) || ($game && $squareBoard->play_status != 'final')) {
                    $this->squareBoardService->update($squareBoard, ['play_status' => 'playing']);
                    $sbWins[$squareBoard->id] = $this->_process_type($squareBoard);
                    $squareBoard->load([
                        'squares',
                        'game',
                        'squares.user',
                        'squares.square_wins',
                    ]);
                    $this->_push_live($squareBoard);
                    if (GameHelper::game_finished($game)) {
                        //update last one time having final run
                        $this->squareBoardService->update($squareBoard, ['play_status' => 'final']);
                    }
                    event(new \App\Events\SquareWon($squareBoard));
                }
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }
        }
    }


    private function _get_boards($id = null, $force = false)
    {
        if ($id) {
            $this->squareboards = SquareBoard::where('id', $id)->where('config', '<>', '');
        } else {
            $this->squareboards = SquareBoard::where('config', '<>', '');
        }
        if ($this->_game_id) {
            $this->squareboards = $this->squareboards->where('game_id', $this->_game_id);
        } else {
            $this->squareboards = $this->squareboards->where('game_id', '!=', null);
        }
        if (!$force) {
            $this->squareboards = $this->squareboards->whereIn('play_status', ['playing', 'new']);
        }
        $this->squareboards = $this->squareboards->whereHas('numbers')->with([
            'pool',
            'squares',
            'game',
            'squares.user',
            'squares.square_wins',
        ])->get(); // we are not playing blank squares
    }

    /**
     * @param $squareBoard
     */
    private function _delete_bad_plays(&$squareBoard)
    {
        foreach ($squareBoard->squares as $square) {
            try {
                foreach ($square->square_wins as $w) {
                    $w->delete();
                }
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }
        }
    }

    private function _clean_bad_wins(&$squareBoard)
    {
    }

    private function _process_type(&$squareBoard)
    {

        //          $this->_delete_bad_plays($squareBoard);
        $winTypeData = SquareHelper::get_quarters_by_win_type($squareBoard);
        $events = $this->gameEventService->get_events_by_game_id($squareBoard->game_id);
        $playScoreChange = SiteHelper::conf('squareboard.allow_every_score');

        $game = $squareBoard->game;
        $gameSt = ($game) ? $game->status : "";

        if ($winTypeData['scoreChange'] && $playScoreChange) {
            // ***tempcode. have to set the exact status 
            if ($gameSt != "Overtime")
                $this->_process_score_change($events, $squareBoard);
            //do we have double dipping trouble?
            if (count($winTypeData['ddQuarters'])) {
                $playFinal = false;
                $this->_process_quarter_wins($winTypeData['ddQuarters'], $squareBoard);
            }
        } else if (count($winTypeData['quarters'])) {
            if ($winTypeData['half']) {
                $this->_process_half_quarter_wins($winTypeData['quarters'], $squareBoard);
                $this->_process_quarter_wins($winTypeData['quarters'], $squareBoard);
            } else {
                $this->_process_quarter_wins($winTypeData['quarters'], $squareBoard);
            }
        }
        //            if($winTypeData['finalScore']){ //Don't double score 4th and Final
        $this->_process_final_win($squareBoard);
        //            }

        return true;
    }

    /**
     * @param $events
     * @param $squareBoard
     */
    private function _process_score_change($events, $squareBoard)
    {
        try {
            $curQuarter = $this->gameService->get_current_quarter($squareBoard->game);
            //                TODO:: fix the finding square ... no fucking idea how to find sqaure with off point wins
            foreach ($events as $event) {
                try {
                    $q = $event->quarter;
                    $numericQuarter  = SiteHelper::convert_qurater_to_number($q, true);
                    //we are playing to win quarters that are double dips
                    if ($numericQuarter <= $curQuarter) { //quarter is over or still playing
                        $scores = ['hometeam' => $event->home_score, 'awayteam' => $event->away_score];
                        $this->_win_($squareBoard, $numericQuarter, null, 'score', $scores, $event); //pass Event ID for sscore change to prevent pre-PAT scoring
                    }
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * @param $quarters
     * @param $squareBoard
     *
     */
    private function _process_half_quarter_wins($quarters, $squareBoard)
    {
        try {
            $curQuarter = $this->gameService->get_current_quarter($squareBoard->game);
            foreach ($quarters as $q) {
                try {
                    $numericQuarter  = SiteHelper::convert_qurater_to_number($q, true);
                    $overHalf = $squareBoard->game->clock() < 450;
                    $sameQuarter = ($numericQuarter == $curQuarter);
                    $quarterPast = ($numericQuarter < $curQuarter);
                    $halfInSameQuarter = ($overHalf && $sameQuarter);
                    //we are playing to win quarters that are double dips
                    if (($halfInSameQuarter || $quarterPast) && $numericQuarter < 5) {
                        $this->_win_($squareBoard, $q, '7:30', 'half_quarter');
                    }
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * @param $squareBoard
     */
    private function _process_final_win($squareBoard)
    {
        try {
            if ($squareBoard->game && $squareBoard->game->is_final) {
                $scores = [
                    'awayteam' => $squareBoard->game->awayteam_totalscore,
                    'hometeam' => $squareBoard->game->hometeam_totalscore,
                ];
                $this->_win_($squareBoard, 4, '0:00', 'final', $scores);
                $this->squareBoardService->update($squareBoard, ['play_status' => 'final']);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * @param $quarters
     * @param $squareBoard
     */
    private function _process_quarter_wins($quarters, $squareBoard)
    {
        try {
            $game = $squareBoard->game;
            $curQuarter = $this->gameService->get_current_quarter($squareBoard->game);
            foreach ($quarters as $q) {
                try {
                    $numericQuarter  = SiteHelper::convert_qurater_to_number($q, true);
                    $quarterPast = (($numericQuarter < $curQuarter) || ($game && $game->is_final));
                    if ($quarterPast && $numericQuarter < 4) { //quarter is over or still playing
                        $winType = ($numericQuarter == 2) ? 'half' : 'quarter';
                        $this->_win_($squareBoard, $numericQuarter, '00:00', $winType);
                    }
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    //enum('quarter','half_quarter','score','half','final')

    /**
     * @param $squareBoard
     * @param $q
     * @param null $time
     * @param null $winType
     * @param null $forceScores
     */
    private function _win_(&$squareBoard, $q, $time = null, $winType = null, $forceScores = null, $event = null)
    {
        try {
            //                dump("Winning", $q, $time, $winType, $forceScores);
            $time = ($time) ? $time : "00:00";
            $seconds = SiteHelper::time_to_seconds($time);
            //used for score changes... to not keep track of time of score change
            $game = $squareBoard->game;
            if ($game->status !== "Not Started") {
                if ($game->events->count()) {
                    $scores = ($forceScores) ? $forceScores : $this->gameEventService->score_at($game->id, $q, $seconds);
                } else {
                    $scores = ($forceScores) ? $forceScores : $this->gameService->quarter_score($game, $q);
                }

                $numbers = $this->squareBoardNumberService->get_numbers($squareBoard, $q);
                if ($scores && $numbers && $scores['hometeam'] !== null && $scores['awayteam'] !== null) {
                    $xy = SquareHelper::get_xy_coordinates($scores, $numbers);
                    $square = $this->squareService->get_by_coordinates($squareBoard, $xy['x'], $xy['y']);

                    if ($square) {

                        //TODO: move this to service provider
                        $squareWinType = ($winType) ? $winType : 'final';
                        $qr = ucwords(str_replace('_', ' ', $squareWinType));
                        $x = SquareHelper::get_last_digit($scores['hometeam']);
                        $y = SquareHelper::get_last_digit($scores['awayteam']);
                        $coords = $y . ", " . $x;
                        $primary = ($squareWinType == 'final');
                        $qn = SiteHelper::convert_qurater_to_number($q);
                        $clock = SquareHelper::get_clock_at_win($winType, $event);
                        $gameOver = GameHelper::game_finished($game);
                        $data =
                            [
                                'square_id' => $square->id,
                                'hometeam_score' => $scores['hometeam'],
                                'awayteam_score' => $scores['awayteam'],
                                'primary' => $primary,
                                'win_type' => $squareWinType,
                                'squareboard_number_id' => $numbers->id,
                                'quarter' => $qn,
                                'event_id' => ($event) ? $event->id : null,
                                'clock' => $clock,
                                'pat_pending' => ($event) ? $event->pat_pending : false,
                            ];
                        if ($event) {

                            $oldSquareWin = SquareWin::where('event_id', $event->id)
                                ->where('win_type', $squareWinType)
                                ->where('squareboard_number_id', $numbers->id)
                                ->where('quarter', $qn)
                                ->first();
                            if (($oldSquareWin && $oldSquareWin->square_id != $square->id) || !$event->pat_pending) {
                                if ($oldSquareWin) {
                                    $oldSquareWin->delete(); // DELETE BAD PLAY on square change
                                }

                                $data['pat_pending'] = false; //SET pat pending aftet del
                            }
                        } else {
                            $data['pat_pending'] = false; //SET pat pending aftet del
                        }
                        if ($this->force) {
                            $data['pat_pending'] = false;
                        }
                        if ($gameOver) {
                            $data['pat_pending'] = false;
                        }
                        $squareWin = \App\Models\SquareWin::firstOrNew($data);
                        if ($squareWin->wasRecentlyCreated) {
                            if (!$data['pat_pending']) {
                                $this->_notify($square, $squareBoard, $coords, $squareWinType, $q);
                            }
                        }
                        $squareWin->save();
                        return $squareWin;
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }

    private function _push_live(&$squareBoard)
    {
        try {
            $game = $squareBoard->game;
            $data = [
                'game' => $this->_build_game($game),
                'squares' => $this->_get_squares($squareBoard, $game)
            ];
            event(new SquareBoardLivePushEvent($squareBoard->id, $data));
        } catch (\Exception $е2) {
            \Log::error($е2->getMessage());
            \Log::error($е2->getTraceAsString());
        }



        //            return response()->json($data);
    }

    private function _notify(&$square, &$squareBoard, $coords, $type = 'score', $quarter = 1)
    {

        $prettyQ = SiteHelper::convert_number_to_quarter($quarter);
        $notices = [
            'score' =>  "Score Change Winner: <span class='text-brand-orange'>" . $square->label . "</span> <span class='text-brand-blue'>(" . $coords . ")</span>",
            'quarter' => $prettyQ . " Quarter Winner: <span class='text-brand-orange'>" . $square->label . "</span> <span class='text-brand-blue'>(" . $coords . ")</span>",
            'half_quarter' => $prettyQ . " Half Quarter Winner: <span class='text-brand-orange'>" . $square->label . "</span> <span class='text-brand-blue'>(" . $coords . ")</span>",
            'half' => "Halftime Score Winner: <span class='text-brand-orange'>" . $square->label . "</span> <span class='text-brand-blue'>(" . $coords . ")</span>",
            'final' => "Final Score Winner: <span class='text-brand-orange'>" . $square->label . "</span> <span class='text-brand-blue'>(" . $coords . ")</span>",
        ];
        $message = isset($notices[$type]) ? $notices[$type] : null;
        if ($message) {
            $this->chatProvider->send($squareBoard->pool->chat, null, $message);
        }
    }

    private function _build_game(&$game)
    {
        return $this->gameService->build_game($game);
    }

    private function _get_squares(&$squareBoard, &$game)
    {
        $q = $this->gameService->get_current_quarter($game);
        $predictionNumbers = $this->squareBoardNumberService->get_numbers($squareBoard, $q);
        $possibleWins = $this->squareBoardService->get_projections($squareBoard, $predictionNumbers);
        return $this->squareBoardService->build_squareboard_squares($squareBoard, $possibleWins, $game);
    }
}
