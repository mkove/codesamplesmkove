<?php
	namespace App\Console\Commands\Play\Schwindy;

	use App\Helpers\SchwindyHelper;
    use App\Helpers\SiteHelper;
    use App\Helpers\UserNotificationHelper;
    use App\Models\Schwindy;
    use App\Models\SchwindyPick;
    use App\Models\Team;
    use Illuminate\Console\Command;
	use App\Helpers\GoalServeHelper as GS;
	use App\Helpers\TeamHelper as TH;
    use App\Providers\Chat\ChatService as Chat;
    use App\Providers\Game\Game as GameProvider;
    use App\Helpers\SchwindyHelper as SH;
	class Autopick extends Command {
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */


		protected $signature = 'schwindy:autopick {week?} {id?}';

        private $_skip = [];
        public $gameProvider;
        public $week;

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Autopick Schwindy';
        private $_weeklyGames = null;
		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct(
		    GameProvider $gameProvider
        ) {
            $this->gameProvider = $gameProvider;
			parent::__construct();
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle() {

            $this->week = $this->argument('week');
            $id = $this->argument('id');


            if($this->week){

            } else {

            }
            $currentWeek = ($this->week) ?? SiteHelper::conf('schwindy.current_week','number','1');
            if($id){
                $schwindys = Schwindy::with(['pool', 'pool.users', 'pool.users.schwindyPicks', 'pool.chat', 'picks'])
                    ->where('id', $id)
                    ->where('current_week',$currentWeek)
                    ->get();
            } else {
                $schwindys = Schwindy::with(['pool', 'pool.users', 'pool.users.schwindyPicks', 'pool.chat', 'picks'])
                    ->where('completed', false)
                    ->where('autopick_pending', true)
                    ->where('current_week',$currentWeek)
                    ->get();
            }



		    foreach($schwindys as $schwindy){
		        $config = $schwindy->config;


		        if($currentWeek >= $config->start_week && $currentWeek <= $config->end_week){
		            $currentWeekPlayed = $this->gameProvider->week_played($currentWeek, "Regular Season", $config->season_year);


                    if($currentWeekPlayed){

                        //Do autopick here for this schwindy
                        $gameFilters = [
                            'sport' =>'football',
                            'league' => 'professional',
                            'season_year' => $config->season_year,
                            'season' => 'Regular Season',
                            'week' => $currentWeek
                        ];
                        $games = $this->gameProvider
                            ->get($gameFilters,true)
                            ->with(['home_team','away_team'])
                            ->orderBy('time','ASC')
                            ->get();
                        $roster = $this->_build_team_roster($games);
                        $this->_pick_shit($schwindy, $roster);
                    }
                }

		        //in any case no need to autopick anymore and week is now done,
		        $schwindy->autopick_pending = false;

                if($schwindy->current_week < $schwindy->config->end_week){
                    if(!$this->week){

                        $schwindy->current_week++;
                    }
                } else {
                    $schwindy->completed = true;
                }

		        $schwindy->save();
            }
            $currentWeek++;
		    if(!$this->week){ //SKIP if was force run


                SiteHelper::set_conf('schwindy.current_week',$currentWeek);
            }
		}

		private function _pick_shit($schwindy, &$roster) {

		    //shit pick logic here
            $poolUsers = $schwindy->pool->users;
            $config = $schwindy->config;
            foreach($poolUsers as $user){

                $this->_skip = array(); //set blank slate for teams that we will skip after shit pick them.

                $allUserPicks = SchwindyPick::where('user_id', $user->id)->where('schwindy_id', $schwindy->id)->get();
                $thisWeekUserPicks = SchwindyPick::where('user_id', $user->id)->where('schwindy_id', $schwindy->id)->where('week',$schwindy->current_week)->get();
                $weeklyMin = SchwindyHelper::get_week_min($schwindy->current_week, $schwindy->config);
                $forcePick = ($weeklyMin - $thisWeekUserPicks->count());


                if($forcePick > 0){

                    for($i=1;$i<=$forcePick;$i++){
                        $this->_pick_team($user, $schwindy, $roster, "*");
                    }
                } else {

                    $canStillPick = SH::sum_max_picks($config->weeks);
                    $alreadyPicked = SH::num_of_teams_picked($allUserPicks);
                    $needsToPick = $config->total_picks;

                    $shitPicksThisWeek = $canStillPick - $alreadyPicked - $needsToPick;

                    //missing N number of picks
                    if($shitPicksThisWeek < 0){
                        //get actual number of missing
                        $shitPicksThisWeek = abs($shitPicksThisWeek);

                        //let's say missing 2
                        $weeklyMax = SchwindyHelper::get_week_max($schwindy->current_week, $schwindy->config);
                        //MAX yo ucan force pick this week
                        $ceil = ($weeklyMax < $shitPicksThisWeek) ? $weeklyMax : $shitPicksThisWeek;
                        for($p=1;$p<=$ceil;$p++){

                            $this->_pick_team($user, $schwindy, $roster, "^");
                        }
                    }
                }



            }
		}


		private function _pick_team(&$user, &$schwindy, $roster, $tag = "") {

		    $chat = $schwindy->pool->chat;
		    $userPicks = SchwindyPick::where('user_id', $user->id)->where('schwindy_id', $schwindy->id)->get(); //reload on every shit pick
            foreach($roster as $teamId => $points){

                $teamPickCount = SH::num_team_picked($teamId, $userPicks);
                //do not re-assign same shit team twice, use _skip var.
                if(!SH::is_team_over_max($teamPickCount,$schwindy->config) && !in_array($teamId, $this->_skip)){
                    //Create pick and call it a day
                    $team = Team::find($teamId);
                    $gameFilters = [
                        'sport' =>'football',
                        'league' => 'professional',
                        'season_year' => $schwindy->config->season_year,
                        'season' => 'Regular Season',
                        'week' => $schwindy->current_week,
                    ];
                    //GET SPECIFIC GAME by TEAM ID AND WEEK
                    $game = $this->gameProvider
                        ->get($gameFilters,true)
                        ->where(function($query) use ($teamId){
                            $query->where('home_team_id', $teamId)->orWhere('away_team_id', $teamId);
                        })
                        ->first();
                    $pickData = [
                        'schwindy_id' => $schwindy->id,
                        'team_id' => $teamId,
                        'provider_team_id' => $team->provider_id,
                        'user_id' => $user->id,
                        'game_id' => $game->id,
                        'week' => $schwindy->current_week,
                        'autopick' => true,
                        'locked' => true,
                        'points' => $points,
                        'final' => true,
                        'visible' => true,
                        'team_name' => $team->name.$tag,
                        'team_shortname' => $team->short_name
                    ];

                    $pick = SchwindyPick::create($pickData);
                    $this->_skip[] = $teamId;
                    UserNotificationHelper::send_schwindy_notice($user->id, 'autopick', $schwindy->pool, null, $pick, true);
                    return true;

                    break; // exit loop,

                }
            }


		}

		private function _build_team_roster($games){
		    $roster = [];
		    foreach($games as $game){
                $roster[$game->home_team_id] = $game->hometeam_totalscore;
                $roster[$game->away_team_id] = $game->awayteam_totalscore;
            }
		    //put first small number on top
            asort($roster);
//            arsort($roster);
		    return $roster;
        }







}














