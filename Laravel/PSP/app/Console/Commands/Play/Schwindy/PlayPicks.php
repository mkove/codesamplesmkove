<?php

    namespace App\Console\Commands\Play\Schwindy;
    use App\Events\SchwindyUpdateEvent;
    use App\Helpers\SiteHelper;
    use App\Models\Schwindy;
    use App\Models\SchwindyPick;
    use App\Providers\Pool\Schwindy\Schwindy as SchwindyProvider;
    use App\Providers\Pool\Schwindy\SchwindyPick as SchwindyPickProvider;
    use App\Providers\Game\Game as GameProvider;
    use App\Providers\Game\Team\Team as TeamProvider;
    use App\Providers\Chat\ChatService as ChatService;
    use Illuminate\Console\Command;
    use App\Helpers\GoalServeHelper as GS;

	class PlayPicks extends Command
	{

		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		private $_schwindyPickProvider;
        private $_schwindyProvider;
        private $_gameProvider;
        private $_teamProvider;
        private $_chatService;


		private $_season;

		protected $signature = 'schwindy:play';


		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Get Game Events And Update Schwindy Points';

		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct(
		    SchwindyPickProvider $schwindyPickProvider,
            SchwindyProvider $schwindyProvider,
            GameProvider $gameProvider,
            TeamProvider $teamProvider,
            ChatService $chatService
        )
		{
            $this->_schwindyProvider = $schwindyProvider;
            $this->_schwindyPickProvider = $schwindyPickProvider;
            $this->_gameProvider = $gameProvider;
            $this->_teamProvider = $teamProvider;
            $this->_chatService = $chatService;
			parent::__construct();
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
            $schwindys = Schwindy::where('completed', false)
                ->with(
                [
                    'pool',
                    'pool.users',
                    'pool.chat',
                    'picks',
                    'picks.game'
                ]
            )
                ->get();

            foreach($schwindys as $schwindy){
                try {
                    $curWeek = $schwindy->current_week;
                    //play this schwindy and update the week once done (if all games played
                    $this->_schwindyProvider->play_schwindy($schwindy, $curWeek, false, true);
                } catch (\Exception $e){
                    \Log::error($e->getMessage());
                    \Log::error($e->getTraceAsString());
                }

            }

            //DO NOT FORGET TO UPGRADE TO NEXT WEEK AT TEH END
		}


	}
