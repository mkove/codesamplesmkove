<?php

namespace App\Console\Commands\Play\Schwindy;

use App\Events\SchwindyUpdateEvent;
use App\Helpers\SiteHelper;
use App\Models\Schwindy;
use App\Models\SchwindyPick;
use App\Providers\Pool\Schwindy\Schwindy as SchwindyProvider;
use App\Providers\Pool\Schwindy\SchwindyPick as SchwindyPickProvider;
use App\Providers\Game\Game as GameProvider;
use App\Providers\Game\Team\Team as TeamProvider;
use App\Providers\Chat\ChatService as ChatService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Helpers\GoalServeHelper as GS;

class LockSchwindyCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    private $_schwindyPickProvider;
    private $_schwindyProvider;
    private $_gameProvider;
    private $_teamProvider;
    private $_chatService;
    private $_type;
    private $_seasonYear;
    private $_currentWeek;

    protected $signature = 'schwindy:lock {type}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lock Schwindy by game or all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        SchwindyPickProvider $schwindyPickProvider,
        SchwindyProvider $schwindyProvider,
        GameProvider $gameProvider,
        TeamProvider $teamProvider,
        ChatService $chatService
    ) {
        $this->_schwindyProvider = $schwindyProvider;
        $this->_schwindyPickProvider = $schwindyPickProvider;
        $this->_gameProvider = $gameProvider;
        $this->_teamProvider = $teamProvider;
        $this->_chatService = $chatService;

        $this->_seasonYear = SiteHelper::conf('schwindy.season');
        $this->_currentWeek = SiteHelper::conf('schwindy.current_week', 'number', '1');
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->_type = $this->argument('type');

        $filters = [
            'season' => 'Regular Season',
            'sport' => 'football',
            'league' => 'professional',
            'season_year' => $this->_seasonYear,
        ];
        switch ($this->_type) {
            case 'game':

                $now = Carbon::now();
                $games = $this->_gameProvider->get($filters, true)
                    ->where('start', '<', $now)
                    ->where('status', '<>', 'Not Started')
                    ->where('locked', false)
                    ->where('week', $this->_currentWeek)
                    ->get();
                $this->_run_games($games);
                //                        $invisibleGames = $this->_gameProvider->get($filters, true)
                //                            ->where('status', 'Not Started')
                ////                            ->where('week',$this->_currentWeek)
                //                             ->get();
                //                        $this->_hide_picks($invisibleGames);
           //     \Log::info("Lock game start........." . $this->_currentWeek);
                break;
            case 'all':
                //cause this only runs on sunday
                $games = $this->_gameProvider->get($filters, true)->where('week', $this->_currentWeek)
                    ->get();
                $this->_run_games($games);
          //      \Log::info("Lock all start........." . $this->_currentWeek);
                break;
            case 'fuck':
                //                    $picks = SchwindyPick::get();
                //                    foreach($picks as $pick){
                //                        $pick->visible = false;
                //                        $pick->save();
                //                    }
                break;
            case 'unlock':
                //                    \Log::info("Running the unlocking force");
                //                    $filters = [
                //                        'season'=>'Regular Season',
                //                        'sport'=>'football',
                //                        'league'=>'professional',
                //                        'season_year' => $this->_seasonYear,
                //                    ];
                //                    $otherGames = $this->_gameProvider->get($filters, true)
                //                        ->where(function($query){
                //                            $query->where('status','<>', 'Final')->orWhere('status','<>', 'After Over Time'); //any final game
                //                        })
                //                        ->get();
                //                    \Log::info("Game Count to unlock ".$otherGames->count());
                //                    foreach($otherGames as $game){
                //                        \Log::info("Unlocking .".$game->tournament_name);
                //                        $game->locked = false;
                //                        $game->save();
                //                    }
                break;
            default:

                break;
        }


        return true;
    }

    private function _hide_picks($games)
    {
        foreach ($games as $game) {
            $gamePicks = $this->_schwindyPickProvider->get_picks_by_game_id($game->id);
            //lock game
            foreach ($gamePicks as $pick) {
                try {
                    if ($pick->visible) {
                        $this->_schwindyPickProvider->update($pick->id, ['visible' => false]);
                    }
                } catch (\Exception $e) {
                    \Log::error($e->getTraceAsString());
                }
            }
        }
    }

    private function _run_games($games)
    {
        $notices = [];
        foreach ($games as $game) {
            $gamePicks = $this->_schwindyPickProvider->get_picks_by_game_id($game->id);
            //lock game
            $this->_gameProvider->update($game->id, ['locked' => true]);
            \Log::info("Game is locked........." . $game->id);

            foreach ($gamePicks as $pick) {
                try {
                    // $closedEntity = ($this->_type == 'all') ? "WEEK " . $this->_currentWeek : $game->tournament_name;
                    // $notices[$pick->schwindy->id] = "Picks for <strong class='text-brand-blue'>" . $closedEntity . "</strong> are now <strong class='text-brand-red'>LOCKED</strong>";
                    // $notifySchwindyIds[] = $pick->schwindy_id;
                    //show picks
                    $this->_schwindyPickProvider->update($pick->id, ['visible' => true, 'locked' => true]);
                } catch (\Exception $e) {
                    \Log::error($e->getTraceAsString());
                }
            }
        }
    }
}
