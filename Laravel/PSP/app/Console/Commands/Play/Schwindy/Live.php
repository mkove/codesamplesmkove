<?php

namespace App\Console\Commands\Play\Schwindy;

use App\Events\SchwindyUpdateEvent;
use App\Models\Schwindy;
use App\Models\SchwindyPick;
use App\Providers\Pool\Schwindy\Schwindy as SchwindyProvider;
use App\Providers\Pool\Schwindy\SchwindyPick as SchwindyPickProvider;
use App\Providers\Game\Game as GameProvider;
use App\Providers\Game\Team\Team as TeamProvider;
use App\Providers\Chat\ChatService as ChatService;
use Illuminate\Console\Command;
use App\Helpers\GoalServeHelper as GS;

class Live extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    private $_schwindyPickProvider;
    private $_schwindyProvider;
    private $_gameProvider;
    private $_teamProvider;
    private $_chatService;


    private $_season;

    protected $signature = 'schwindy:live';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push live data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        SchwindyPickProvider $schwindyPickProvider,
        SchwindyProvider $schwindyProvider,
        GameProvider $gameProvider,
        TeamProvider $teamProvider,
        ChatService $chatService
    ) {
        $this->_schwindyProvider = $schwindyProvider;
        $this->_schwindyPickProvider = $schwindyPickProvider;
        $this->_gameProvider = $gameProvider;
        $this->_teamProvider = $teamProvider;
        $this->_chatService = $chatService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $gameFilters = [
            'sport' => 'football',
            'league' => 'professional',
            'season_year' => __conf('schwindy.season', 'text', '2021/2022'),
            'season' => 'Regular Season',
        ];
        $games = $this->_gameProvider
            ->get($gameFilters, true)
            ->with(['home_team', 'away_team'])
            ->orderBy('time', 'ASC')->get();



        if ($games->count()) {
            $schwindys = Schwindy::get();
            foreach ($schwindys as $schwindy) {
                broadcast(new SchwindyUpdateEvent($schwindy, $games));
            }
        }
        return true;
    }
}
