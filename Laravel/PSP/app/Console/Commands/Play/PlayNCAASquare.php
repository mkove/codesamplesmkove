<?php

    namespace App\Console\Commands\Play;

    use App\Helpers\TeamHelper;
    use App\Mail\LockToggle;
    use App\Models\NCAASquareWin;
    use App\Models\Square;
    use App\Models\SquareBoard;
    use App\Models\SquareWin;
    use Illuminate\Console\Command;
    use App\Helpers\GoalServeHelper;
    use App\Helpers\SiteHelper;
    use App\Helpers\SquareHelper;

    class PlayNCAASquare extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        private $_season;
        private $_lock;
        private $_visible;
        private $_remaps;

        protected $signature = 'play:ncaasquare {id?} {--force=0}';
        protected $squareBoardService;
        protected $squareBoardNumberService;
        protected $gameEventService;
        protected $gameService;
        protected $delete_bad_plays;
        protected $squareService;
        protected $id;
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Play NCAA Tourney squares command. Optional arguments id=squareboard_id, --force=[1/0]';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct(
            \App\Providers\Pool\SquareBoard\SquareBoard $squareBoardService,
            \App\Providers\Pool\SquareBoard\SquareBoardNumber\SquareBoardNumber $squareBoardNumberService,
            \App\Providers\GameEvent\GameEvent $gameEventService,
            \App\Providers\Game\Game $gameService,
            \App\Providers\Pool\SquareBoard\Square\Square $squareService
        )
        {
            $this->squareBoardService = $squareBoardService;
            $this->squareBoardNumberService = $squareBoardNumberService;
            $this->gameEventService = $gameEventService;
            $this->squareService = $squareService;
            $this->gameService = $gameService;
            $this->_remaps = [];
            $this->id = null;
            $this->delete_bad_plays = __conf('ncaa.force_replay_and_rescore_season','boolean','0');
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $this->line('Started');

            try {
                $this->id = $this->argument('id');
                $this->delete_bad_plays = $this->option('force');
                if($this->delete_bad_plays){
                    $this->line('Flag is set to re-score');
                }
                $squareBoards = SquareBoard::with(['games','squares','numbers'])
                    ->whereHas('pool', function($query){
                        $query->where('type','4');
                    })->whereHas('numbers');
                if($this->id){
                    $squareBoards = $squareBoards->where('id', $this->id);
                }



                $ncaaGames = $this->gameService->get([
                    'sport' => 'basketball',
                    'league' => 'college',
                    'season_year' => SiteHelper::conf('ncaa.season', 'text','2019/2020'),
                ], true)->orderBy('round_id', 'asc')->get();
                $squareBoards = $squareBoards->get();
            } catch(\Exception $e){
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
                $squareBoards = [];
            }
            $this->line('Grabbed '.$squareBoards->count()." boards");

            foreach($squareBoards as $squareBoard){
//                \Log::info("Delete bad plays flag is ".$this->delete_bad_plays);
                if($this->delete_bad_plays){
                    $this->line('Reseting '.$squareBoard->pool->pool_name);
                    foreach($squareBoard->squares as $square){
                        foreach($square->ncaa_square_wins as $ncaa){
                            try {
                                $ncaa->delete();
                            } catch(\Exception $e){
                                \Log::error($e->getMessage());
                                \Log::error($e->getTraceAsString());
                            }

                        }
                    }
                    $squareBoard->refresh();
                }

                foreach($ncaaGames as $game){
                    try{
                        $this->_play_game($squareBoard, $game);
                    } catch(\Exception $e){
                        \Log::error($e->getMessage());
                        \Log::error($e->getTraceAsString());
                    }
                }
            }
            return true;
        }

        private function _enabled_rounds($config){

            try {
                $enables = [
                    '0' => (isset($config->payouts->pre)) ? $config->payouts->pre->enabled : false,
                    '1' => (isset($config->payouts->first)) ? $config->payouts->first->enabled : false,
                    '2' => (isset($config->payouts->second)) ? $config->payouts->second->enabled : false,
                    '3' => (isset($config->payouts->third)) ? $config->payouts->third->enabled : false,
                    '4' => (isset($config->payouts->fourth)) ? $config->payouts->fourth->enabled : false,
                    '5' => (isset($config->payouts->semi)) ? $config->payouts->semi->enabled : false,
                    '6' => (isset($config->payouts->championship) )? $config->payouts->championship->enabled : false,
                ];
                return $enables;
            } catch (\Exception $e){
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
//                \Log::info($config);
            }
        }

        private function _enabled_to_play($roundId, $playRounds){


            if(isset($playRounds[$roundId])){
                return ($playRounds[$roundId]);
            }
            return false;
        }

        private function _play_game(&$squareBoard, &$game)
        {

            $playRounds = $this->_enabled_rounds($squareBoard->payout_config);
            $enabled = $this->_enabled_to_play($game->round_id, $playRounds);
            if($game->round_id == 7){
                $game->round_id = 6;
                $game->save();
            }
            if($game->is_final && $enabled){
                $homeTeamScore = $game->hometeam_totalscore;
                $awayTeamScore = $game->awayteam_totalscore;
                $homeTeamHalfScore = $game->hometeam_q2;
                $awayTeamHalfScore = $game->awayteam_q2;

                $winScore = ($homeTeamScore > $awayTeamScore) ? $homeTeamScore : $awayTeamScore;
                $loseScore = ($homeTeamScore < $awayTeamScore) ? $homeTeamScore : $awayTeamScore;

                $winHalfScore = ($homeTeamHalfScore > $awayTeamScore) ? $homeTeamHalfScore : $awayTeamHalfScore;
                $loseHalfScore = ($homeTeamHalfScore < $awayTeamHalfScore) ? $homeTeamHalfScore : $awayTeamHalfScore;
                //because horizontal is always a winning team


                $scores = [
                    'hometeam' => $winScore,
                    'awayteam' => $loseScore
                ];

                $halfScores = [
                    'hometeam' => $winHalfScore,
                    'awayteam' => $loseHalfScore
                ];

                $numbers = $this->squareBoardNumberService->get_numbers($squareBoard, 4);
                $payoutConfig = $squareBoard->payout_config;
//                \Log::info($payoutConfig);
                if($numbers){
                    if($game->round_id == 5){ //semi final half score
//                        \Log::info(" if enabled");
//                        \Log::info($payoutConfig->payouts->semi_half->enabled);
                        if(isset($payoutConfig->payouts->semi_half->enabled) && $payoutConfig->payouts->semi_half->enabled){
//                            \Log::info("Playing half");
                            $this->_play_scores($halfScores, $numbers, $squareBoard, $game, $homeTeamHalfScore, $awayTeamHalfScore, $winHalfScore, $loseHalfScore, 'half');
                        }
                    }
                    if($game->round_id == 6){ //final half score
//                        \Log::info(" if enabled");
//                        \Log::info($payoutConfig->payouts->championship_half->enabled);
                        if(isset($payoutConfig->payouts->championship_half->enabled) && $payoutConfig->payouts->championship_half->enabled){
//                            \Log::info("Playing half");
                            $this->_play_scores($halfScores, $numbers, $squareBoard, $game, $homeTeamHalfScore, $awayTeamHalfScore, $winHalfScore, $loseHalfScore, 'half');
                        }
                    }

                    $this->_play_scores($scores, $numbers, $squareBoard, $game, $homeTeamScore, $awayTeamScore, $winScore, $loseScore, 'final');
                }
            }
        }


        private function _play_scores(&$scores, &$numbers, &$squareBoard, &$game, $homeTeamScore, $awayTeamScore, $winScore, $loseScore, $type = 'final'){
            $xy = SquareHelper::get_xy_coordinates($scores, $numbers);

            $square = $this->squareService-> get_by_coordinates($squareBoard, $xy['x'], $xy['y']);

            if($square){
                if($square->user_id){
                    $squareCounted = NCAASquareWin::where('square_id',$square->id)
                        ->where('game_id', $game->id)
                        ->where('win_type',$type)
                        ->get();

                    if(!$squareCounted->count()){
                        $winTeam = ($homeTeamScore > $awayTeamScore) ? $game->home_team : $game->away_team;
                        $loseTeam = ($homeTeamScore < $awayTeamScore) ? $game->home_team : $game->away_team;
                        $ncaaSquareWinData = [
                            'square_id' => $square->id,
                            'game_id' => $game->id,
                            "win_team" => $winTeam->name,
                            "lose_team" => $loseTeam->name,
                            'win_score' => $winScore,
                            'lose_score' => $loseScore,
                            'x'  => SquareHelper::get_last_digit($winScore),
                            'y' => SquareHelper::get_last_digit($loseScore),
                            'round' => $game->round,
                            'round_id' => $game->round_id,
                            'game_date' => $game->start,
                            'win_type' => $type,
                        ];
                        //check if this needs to be created or updated
                        NCAASquareWin::create($ncaaSquareWinData);
                    }
                }
            }
        }
    }


