<?php

    namespace App\Console\Commands\Play\Squareboard;

    use App\Mail\LockToggle;
    use App\Models\Pool;
    use App\Models\Square;
    use App\Models\SquareBoard;
    use App\Models\User;
    use App\Providers\Email\EmailContentProvider;
    use Illuminate\Console\Command;
    use App\Helpers\GoalServeHelper;
    use App\Helpers\SiteHelper;
    use App\Helpers\SquareHelper;
    use Illuminate\Support\Facades\Log;

    class RemindCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */

        protected $signature = 'remind:squareboard-generate';
        protected $squareBoardService;
        protected $squareBoardNumberService;
        protected $gameEventService;
        protected $emailContentProvider;
        protected $gameService;
        protected $squareService;
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Remind commissioners to generate numbers';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct(
            \App\Providers\Pool\SquareBoard\SquareBoard $squareBoardService,
            \App\Providers\Pool\SquareBoard\SquareBoardNumber\SquareBoardNumber $squareBoardNumberService,
            \App\Providers\GameEvent\GameEvent $gameEventService,
            \App\Providers\Game\Game $gameService,
            \App\Providers\Pool\SquareBoard\Square\Square $squareService,
            \App\Providers\Email\EmailContentProvider $emailContentProvider,
            \App\Providers\Chat\ChatService $chat
        )
        {
            $this->squareBoardService = $squareBoardService;
            $this->squareBoardNumberService = $squareBoardNumberService;
            $this->gameEventService = $gameEventService;
            $this->squareService = $squareService;
            $this->gameService = $gameService;
            $this->emailContentProvider = $emailContentProvider;
            $this->squareboards = null;
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
           $squareBoards = $this->squareBoardService->get(null, [], true)->whereDoesntHave('numbers')->get();
           foreach($squareBoards as $squareBoard){
               try {
                   $takenSquareCount = $this->squareService->get(null, ['squareboard_id' => $squareBoard->id, 'user_id' => null], true)->count();
                   if($takenSquareCount === 0){
                       if($pool =  $squareBoard->pool) {
                           foreach($pool->commissioners as $user){
                               try {
                                   $data = [
                                       'user' => $user,
                                       'pool' => $squareBoard->pool,
                                       'current_user' => $user,
                                       'squareBoard' => $squareBoard,
                                       'numbers_list' => $this->squareBoardNumberService->get_user_numbers($squareBoard, $user->id)
                                   ];
                                   $result = $this->emailContentProvider->process_email('generate_numbers_reminder', $user->email, $data);

                               } catch (\Exception $e){
                                   \Log::error($e->getTraceAsString());
                               }
                           }
                       }
                   }
               } catch (\Exception $e){
                   \Log::error($e->getTraceAsString());
               }

           }

        }




    }




