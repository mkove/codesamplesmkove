<?php

    namespace App\Console\Commands\Play\Golf;

    use App\Events\GolfLiveUpdateEvent;
    use App\Providers\Pool\Golf\GolfProvider;
    use App\Providers\Pool\Golf\GolfTournamentProvider;
    use Illuminate\Console\Command;


    class PushLiveCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */

        private $_golfProvider;
        private $_golfTournamentProvider;

        protected $signature = 'push:live-golf {id?}';
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Push live shit to golf';

        private $_gtp;
        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct(GolfProvider $golfProvider, GolfTournamentProvider  $golfTournamentProvider)
        {
            $this->_golfProvider = $golfProvider;
            $this->_golfTournamentProvider = $golfTournamentProvider;
            parent::__construct();

        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $id = $this->argument('id');
            if($id){
                $localTourney = $this->_golfTournamentProvider->find($id);
                if($localTourney){
                    try{
                        $this->_push_tourney($localTourney);
                    }catch (\Exception $e){
                        __dlog($e);
                    }
                } else {
                   // \Log::info("No local tourney to push");
                }
            } else {
                $localTourneys = $this->_golfTournamentProvider->get_actives();
                if($localTourneys->count()){
                    foreach($localTourneys as $localTourney){
                        $this->_push_tourney($localTourney);
                    }
                } else {
                  //  \Log::info("No local tourney to push");
                }
            }
            return true;
        }

        private function _push_tourney(&$tournament){
            if($tournament){
                foreach($tournament->golfs as $golf){
                    try{

                        $golf->load([
                            'pool',
                            'grouped_tournament_players',
                            'tournament',
                            'entries',
                            'entries.groups',
                            'entries.groups.tournament_players',
                            'tournament.tournament_players',
                            'tournament.tournament_players.player',
                        ]);
                        $handicaps = $this->_golfProvider->load_handicaps($golf);
                        $tData = [
                            'par' => $tournament->par,
                            'status' => $tournament->status,
                        ];
                        $players = $this->_golfProvider->load_players_live($golf, $golf->tournament, $handicaps);
                        $entries = $this->_golfProvider->load_entries_live($golf, $handicaps);
                        broadcast(new GolfLiveUpdateEvent($golf->id, $tData, $players, $entries));
                    }catch (\Exception $e){
                        __dlog($e);
                    }
                }
            }
        }
    }


