<?php

    namespace App\Console\Commands\Play\Golf;

    use App\Events\GolfLiveUpdateEvent;
    use App\Events\GSLogEvent;
    use App\Helpers\SiteHelper;
    use App\Models\Golf;
    use App\Providers\Pool\Golf\GolfProvider;
    use App\Providers\Pool\Golf\GolfEntryProvider;
    use App\Providers\Pool\Golf\GolfTournamentPlayerProvider;
    use App\Providers\Pool\Golf\GolfTournamentProvider;
    use Illuminate\Console\Command;


    class PlayEntryCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */

        private $_golfEntryProvider;
        private $_golfProvider;
        private $_golfTournamentProvider;
        private $_golfTournamentPlayerProvider;
        private $_force;

        protected $signature = 'play:entry-golf {id?} {--force=}';
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Update entry on live play';


        public function __construct(
            GolfEntryProvider $grp,
            GolfProvider $g,
            GolfTournamentProvider $gtp,
            GolfTournamentPlayerProvider $gtpp
        )
        {
            $this->_golfTournamentProvider = $gtp;
            $this->_golfProvider = $g;
            $this->_golfEntryProvider = $grp;
            $this->_golfTournamentPlayerProvider = $gtpp;
            parent::__construct();
        }


        /**
         * Execute the console command.
         *
         * @return mixed
         */


        public function handle()
        {
            $this->_force = ($this->option('force')) ??  SiteHelper::conf('golf.play_force_replay_entries', 'boolean',1);
            $id = $this->argument('id');
            $replayPastRounds = __conf('golf.force_replay_previous_rounds','boolean','0');
            if($id){
                $localTourney = $this->_golfTournamentProvider->find($id);
                if($localTourney){
                    try{
                        $this->_golfTournamentProvider->play_tournament($localTourney, true, $replayPastRounds);
                    }catch (\Exception $e){
                        __dlog($e);
                    }
                }
            } else {
                $localTourneys = $this->_golfTournamentProvider->get_actives();
                foreach($localTourneys as $t){
                    try{
                        $this->_golfTournamentProvider->play_tournament($t, false, $replayPastRounds);
                    }catch (\Exception $e){
                        __dlog($e);
                    }
                }
            }
            return true;
        }

    }

