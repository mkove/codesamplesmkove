<?php
	
	namespace App\Console\Commands\Play\Golf;
	
	use App\Helpers\SiteHelper;
	use App\Helpers\UserNotificationHelper;
	use App\Providers\Pool\Golf\GolfTournamentProvider;
	use Illuminate\Console\Command;
	
	
	class SetActiveTournamentCommand extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
 
		private $_golfTournamentProvider;
 
		
		protected $signature = 'play:next-golf';
		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Set next tournament thats active';
		
		
		public function __construct(
			GolfTournamentProvider $gtp
		)
		{
			$this->_golfTournamentProvider = $gtp;
			parent::__construct();
		}
		
		
		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		
		
		public function handle()
		{
			$localT = $this->_golfTournamentProvider->get_upcoming();
			$gsId = $this->_golfTournamentProvider->local_gs_to_gs_id($localT->external_id);
			if(!$localT->active){
				$this->_golfTournamentProvider->update($localT->id, ['active' => 1]);
				$buildString = "<h3>".$localT->name." ({$gsId})</h3>"."<p>IS ACTIVE NOW <br><strong>Dates: ".$localT->start_date."</strong> - <strong>".$this->end_date."</strong><br><br>
				<a href='https://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/{$gsId}'>GS Entrants</a>&nbsp|&nbsp<a href='https://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/live'>GS Live</a>
</p>";
				UserNotificationHelper::send_admin($localT->name."({$gsId}) [ACTIVATED]", $buildString);
			}
			return true;
		}
		
	}

