<?php

    namespace App\Console\Commands\Play\Golf;

    use App\Models\Golf;
    use App\Providers\Pool\Golf\GolfTournamentProvider;
    use Illuminate\Console\Command;


    class ProcessPaymentCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */

        protected $signature = 'pay:process-golf';
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Charges comish and locks pools on friday';

        private $_gtp;
        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct(GolfTournamentProvider $gtp)
        {
            $this->_gtp = $gtp;
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $tournes = $this->_gtp->get_current();

            //DO NOT lock Not Started
            if($tournes->status !== 'Not Started'){
                $tournes->load(['golfs','golfs.pool']);
                foreach($tournes->golfs as $golf){
                    //TODO: pay invoice

                    if(!$golf->locked){


//                    TODO:
//                    Process emails here that pool is locked
//                    golf_pool_locked

                        $golf->locked = true;
                        $golf->save();
                    }
                }
            }

        }



    }


