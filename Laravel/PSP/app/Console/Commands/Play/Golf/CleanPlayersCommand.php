<?php

namespace App\Console\Commands\Play\Golf;

use App\Helpers\UserNotificationHelper;
use App\Models\Golf;
use App\Providers\Pool\Golf\GolfTournamentProvider;
use Illuminate\Console\Command;


class CleanPlayersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'play:clean-golf';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs on active pool to remove WD players';

    private $_gtp;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GolfTournamentProvider $gtp)
    {
        $this->_gtp = $gtp;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tournes = $this->_gtp->get_current();
		if($tournes !== 'Not Started'){
			$tournes->load(['golfs', 'tournament_players.player', 'tournament_players', 'tournament_players.rounds', 'tournament_players.rounds.scored_holes']);
			$message = "Following players are no longer playing: <ul>";
			$countPlayers = 0;
			foreach ($tournes->tournament_players as $gtp) {
				$countHoles = 0;
				foreach ($gtp->rounds as $round) {
					$countHoles = $countHoles + $round->scored_holes->count();
				}
				if ($countHoles === 0 && $gtp->status === 'Active') {
					$countPlayers++;
					$message .= "<li>" . $gtp->player->name . " (0 holes): Switching <span style='color:red'>" . $gtp->status . "</span> to <span style='color:orange;'>WD</span> </li>";
					$gtp->status = 'WD';
                $gtp->save();
				}
			}
			
			$message .= "</ul><p>Those players did not play any holes.</p>";
			$subject = $tournes->name . " Withdrawn Players";
			if($countPlayers > 0){
				UserNotificationHelper::send_admin($subject, $message);
			}
		}
    
    }


    private function _can_be_locked($golf, $teeTime)
    {
    }
}
