<?php

    namespace App\Console\Commands\Play\Golf;
	
    use Illuminate\Console\Command;


    class TournamentCommand extends Command
    {
	    /**
	     * The name and signature of the console command.
	     *
	     * @var string
	     */
	 
	
	    protected $signature = 'play:tournament-golf {id?} {--force=}';
	    /**
	     * The console command description.
	     *
	     * @var string
	     */
	    protected $description = '(NOT USED - DEPRECIATED) DOES NOT RUN';
	
	
	    public function __construct(
		  
	    ) {
		  
		    parent::__construct();
	    }
	
	    /**
	     * Execute the console command.
	     *
	     * @return mixed
	     */
	
	
	    public function handle()
	    {
			return false; //we no longer run this
		   
	    }
	
	  
	
    }

    //redis-cli -h private-pspprod-redis-do-user-4179552-0.b.db.ondigitalocean.com -p 25061 -u default -a rakzrprkxrlciq5c flushall