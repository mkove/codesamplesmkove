<?php

    namespace App\Console\Commands;

    use App\GolfPoolRoster;
    use App\GolfUserGroupPlayer;
    use App\Helpers\GolfHelper;
    use App\Models\GolfPlayerLegacy;
    use App\Models\GolfPlayersInTournament;
    use App\Models\GolfPool;
    use App\Models\GolfRanking;
    use App\Models\GolfTournament;
    use App\Models\php;
    use Carbon\Carbon;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use App\GolfPlayerStat;

    class GetGolfTournamentsCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'golf:tournaments';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Get all tourneys';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct() {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle() {
            $golfHelper = new GolfHelper();
            $tourneys = $golfHelper->getTournamentsAsCollection();
            foreach($tourneys as $tourney){
                try {
                    $localTourney = GolfTournament::where('tournament_id',$tourney['tournament_id'])->first();
                    if($localTourney){
                        GolfTournament::where('id', $localTourney->id)->update($tourney);
                    } else {
                        GolfTournament::create($tourney);
                    }
                } catch(\Exception $e){
                    \Log::error($e->getTraceAsString());
                }

            }
        }
    }
