<?php
    namespace App\Console\Commands\EveryMatrix;
    use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
    use App\Models\Config;
    use App\Models\EveryMatrix\EMLog;
    use App\Providers\Broadage\BasketballFeedConnector as BroadAgeFeed;
    use Illuminate\Console\Command;
    class NextUpdateCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'everymatrix:next';
        private $_name;
        private $_url;
        private $_port;
        private $_path;
        private $_builtUrl;
        private $_subConfig;
        private $_batches;
        private $_completed;
        private $_current;
        private $_filename;
        private $_lastRun;
        private $_hadError;
        private $_emLog;
        private $_keepFile;
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Get next request from EM, ping this every 3-5 minutes ';
        public function handle() {

            $this->_emLog = new EMLog();

            $this->_set_config();
            if($this->_subConfig->value != 0){
                $this->_get_data();
                $this->_update_from_file();
            }
            return false;
        }

        private function _set_config(){


            $subsNameConfig = Config::firstOrNew([
                'key' => 'em.subscription_name',
            ]);
            $lastConfig = Config::firstOrNew([
                'key' => 'em.last_run',
            ]);
            $subsUrlConfig = Config::firstOrNew([
                'key' => 'em.subscription_url',
            ]);
            $subsPortConfig = Config::firstOrNew([
                'key' => 'em.subscription_port',
            ]);
            $subsPathConfig = Config::firstOrNew([
                'key' => 'em.subscription_path',
            ]);
            $subConfig = Config::firstOrNew([
                'key' => 'em-readonly.subscription_id',
            ]);

            if(!$subsUrlConfig->exists){
                $subsUrlConfig->value = "http://sept.oddsmatrix.com";
                $subsUrlConfig->save();
            }
            if(!$subsPortConfig->exists){
                $subsPortConfig->value = "8081";
                $subsPortConfig->save();
            }
            if(!$subsPathConfig->exists){
                $subsPathConfig->value = "/xmlfeed";
                $subsPathConfig->save();
            }
            if(!$subConfig->exists){
                $subConfig->value = 0;
                $subConfig->save();
            }

            if(!$lastConfig->exists){
                $lastConfig->label = "Last Feed Ran Date";
                $lastConfig->value = date('Y-m-d G:i:s A');
                $lastConfig->save();
            }

            $this->_lastRun = $lastConfig;
            $this->_name = $subsNameConfig->value;
            $this->_port = $subsPortConfig->value;
            $this->_url = $subsUrlConfig->value;
            $this->_path = (substr($subsPathConfig->value,0,1) === '/') ? $subsPathConfig->value : "/"
                .$subsPathConfig->value;
            $this->_builtUrl = $this->_url.":".$this->_port.$this->_path;
            $this->_subConfig = $subConfig;
            $this->_current = 1;
            $this->_filename = "update/next-".date('Y-m-d_H-i-s');
        }

        private function _get_data(){
            $data = [
                'requestType' => "GetNextUpdateDataRequest",
                'subscriptionId' => $this->_subConfig->value,
            ];
            EMH::send_request($this->_builtUrl, $this->_port, $data, $this->_filename);
            $this->_update_from_file();

            $this->_lastRun->value = date('Y-m-d G:i:s A');
            $this->_lastRun->save();

            return $this->_filename;
        }

        private function _update_from_file(){
            $this->_keepFile = false;
            $this->_hadError = false;
            $data = EMH::read_update_file($this->_filename);
            if($data['error']){
                $this->_emLog->log('subscribe','0','None',$data['message'], true);
                $this->_subConfig->value = 0;
                $this->_subConfig->save();
                return false;
            }
            $batches = $data['data'];
            foreach($batches as $model => $batch){
                $localModel = "\App\Models\EveryMatrix\\".$model;
                foreach($batch['entries'] as $entry){
                    $id = $entry['id'];
                    $type = $entry['type'];
                    unset($entry['type']);
                    $el = $localModel::firstOrNew(['id'=> $id]);
                    if($type == 'delete'){
                        $el->delete();
                    } else {
                        $entry = EMH::fix_bad_data($entry);
                        if(!$el->exists){ //creating new entry
                            if($type == 'update'){
                                $this->_emLog->log('update',$id, $model, "Did Not Find For Update",true);
                            }
                            $missing = EMH::get_missing_columns($el, $entry);
                            if(count($missing)){
                                $this->_keepFile = true;
                                foreach($missing as $miss){
                                    $this->_emLog->log('missing', $id, $model, $miss, true);

                                }
                            }
                            $entry = EMH::do_fillables($el, $entry);
                        }
                        $logData = EMH::update_element_with_data($el, $entry);
                        if($logData['error']){
                            $this->_emLog->log('update',$id, $model, $logData['message'],$logData['error']);
                        }
                    }

                }
            }
            if(!$this->_keepFile){
                $delFile = 'database\everymatrix\\'.$this->_filename;
                try {
                    $delResult = \Storage::disk('base')->delete($delFile);
                    $error = $delResult ? false : true;
                    $this->_emLog->log('next',"0", "None", "Processed ".$this->_filename, $error);
                } catch(\Exception $e){
                    $this->_emLog->log('next',"0", "None", "Failed ".$this->_filename." WITH ".$e->getMessage(), true);
                }
            } else {
                $this->_emLog->log('next',"0", "None", "Had Errors at ".$this->_filename, true);
            }
        }



    }
