<?php
    namespace App\Console\Commands\EveryMatrix;
    use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
    use App\Models\Config;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Artisan;
    use App\Models\EveryMatrix\EMLog;

    class SubscribeCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'everymatrix:subscribe {force=1}';
        private $_name;
        private $_url;
        private $_port;
        private $_path;
        private $_builtUrl;
        private $_subConfig;
        private $_batchId;
        private $_batches;
        private $_batchCompleteConfig;
        private $_emLog;
        private $_force;
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Subscribe to EveryMatrix. Run this only if subscription fails. No more than 24 times per day. Once every 5 minutes. ';

        public function handle() {

            $this->_emLog = new EMLog();
            $this->_set_config();
            $this->_force = $this->argument('force');
            if($this->_force){
                $this->_unsubscribe();
            }
            $this->_subscribe();
            $this->_fetch_dump();
            $this->_parse_dump();
            return true;
        }

        private function _set_config(){

            $dumpCompletedConfig = Config::firstOrCreate([
                'key' => 'em-readonly.dump_completed',
                'label' => "Initial Dump Completed"
            ]);
            $subsNameConfig = Config::firstOrCreate([
                'key' => 'em.subscription_name',
            ]);
            $subsUrlConfig = Config::firstOrCreate([
                'key' => 'em.subscription_url',
            ]);
            $subsPortConfig = Config::firstOrCreate([
                'key' => 'em.subscription_port',
            ]);
            $subsPathConfig = Config::firstOrCreate([
                'key' => 'em.subscription_path',
            ]);
            $subConfig = Config::firstOrCreate([
                'key' => 'em-readonly.subscription_id',
            ]);

            if(!$subsNameConfig->exists){
                $subsNameConfig->value = "Aaron123";
                $subsNameConfig->save();
            }
            if(!$subsUrlConfig->exists){
                $subsUrlConfig->value = "http://sept.oddsmatrix.com";
                $subsUrlConfig->save();
            }
            if(!$subsPortConfig->exists){
                $subsPortConfig->value = "8081";
                $subsPortConfig->save();
            }
            if(!$subsPathConfig->exists){
                $subsPathConfig->value = "/xmlfeed";
                $subsPathConfig->save();
            }
            if(!$subConfig->exists){
                $subConfig->value = 0;
                $subConfig->save();
            }
            if(!$dumpCompletedConfig->exists){
                $dumpCompletedConfig->value = 0;
                $dumpCompletedConfig->save();
            }
            $this->_name = $subsNameConfig->value;
            $this->_port = $subsPortConfig->value;
            $this->_url = $subsUrlConfig->value;
            $this->_path = (substr($subsPathConfig->value,0,1) === '/') ? $subsPathConfig->value : "/"
                .$subsPathConfig->value;
            $this->_builtUrl = $this->_url.":".$this->_port.$this->_path;
            $this->_subConfig = $subConfig;
            $this->_batchId = 1;
            $this->_batchCompleteConfig = $dumpCompletedConfig;
            $this->_batches = 100;
            $this->_batchCompleteConfig->value = false;
            $this->_batchCompleteConfig->save();
        }

        private function _unsubscribe(){
           return Artisan::call('everymatrix:unsubscribe');
        }
        private function _subscribe(){
            $data = [
                'requestType' => "SubscribeRequest",
                'subscriptionSpecificationName' => $this->_name
            ];
            EMH::send_request($this->_builtUrl, $this->_port, $data, 'sub');
            $subscribeResponse = EMH::read_subscribe_xml('sub');
            if($subscribeResponse['error']){
//                dump($subscribeResponse['message']);
                $this->_emLog->log('subscribe','0','None',$subscribeResponse['message'],true);
                return false;
            }
            $id = $subscribeResponse['data'];
            if($id){
                $this->_subConfig->value = $id;
                $this->_subConfig->save();
                $this->_batchCompleteConfig->value = false;
                $this->_batchCompleteConfig->save();
            }
            $this->_emLog->log('subscribe','0','None',$id,false);

            return true;
        }

        private function _fetch_dump(){
            $data = [
                'requestType' => "GetNextInitialDataRequest",
                'subscriptionId' => $this->_subConfig->value
            ];
            while(!$this->_batchCompleteConfig->value){
                $prettyName = sprintf('%03d', $this->_batchId);
                $filename = 'dump/'.$prettyName;
                EMH::send_request($this->_builtUrl, $this->_port, $data, $filename);
                $dumpFileData = EMH::read_dump_file($filename);
                if($dumpFileData['error']){
//                    dump($dumpFileData['message']);
                    $this->_emLog->log('dump','0','None',$dumpFileData['message'],true);
                    return false;
                }
                $dumpData = $dumpFileData['data'];
//                $this->_emLog->log('dump','0','None',"Dump saved: ".$filename,false);
                $this->_batchId = $dumpData['batchId'] + 1; //next batch
                $this->_batches = $dumpData['batchesLeft'];
                $this->_batchCompleteConfig->value = $dumpData['dumpCompleted'];
                $this->_batchCompleteConfig->save();
            }
            return true;
        }

        private function _parse_dump(){
            $exitCode = Artisan::call('everymatrix:parse-dump');
        }
    }
