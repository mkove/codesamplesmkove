<?php
namespace App\Console\Commands\EveryMatrix;
use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
use App\Helpers\Everymatrix\EverymatrixHelper;
use App\Models\Config;
use App\Models\EveryMatrix\EMLog;
use Illuminate\Console\Command;
class ParseDumpData extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    private $_entry;
    private $_fileCount;
    private $_file;
    private $_curModel;
    private $_missingKeys;
    private $_emLog;

    protected $signature = 'everymatrix:parse-dump';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run this after dump is completed';
    private $_keepFile;
    private $_processingConfig;
    private $_processingData;

    public function handle() {
        \DB::connection('psp_everymatrix')->disableQueryLog();
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $this->_entry = 0;
        $this->_fileCount = 0;
        $this->_file = 0;
        $this->_curModel = "";
        $this->_emLog = new EMLog();
        $this->_keepFile = false;
        $this->_processingConfig = null;
        $this->_processingData = null;
        $this->_start_parse();

    }

    private function _start_parse() {
        $this->_processingConfig = Config::where('key','em-readonly.dump_process')->first();
        $this->_processingData = json_decode($this->_processingConfig->value);

        $path = 'database\everymatrix\dump';
        $files = \Storage::disk('base')->allFiles($path);
        $this->_parsing_and_saving($files);
    }

    private function _parsing_and_saving($files){
        $this->_processingData->total = count($files);
        $this->_processingData->current = 0;
        $this->_processingData->finished = false;
        $this->_processingConfig->value = json_encode($this->_processingData);
        $this->_processingConfig->save();
        foreach ($files as $file) {
            //scope large files in there
            $this->_work_file($file);
        }

        $this->_processingData->finished = true;
        $this->_processingConfig->value = json_encode($this->_processingData);
        $this->_processingConfig->save();
        return true;
    }

    private function _work_file($file){
        try {
            $this->_keepFile = false;
            $filename = str_replace('database/everymatrix/', '', $file);
            $dumpFileData = EMH::read_dump_file($filename);
            if(!$dumpFileData){
                $this->_emLog->log('dump','0','None',"Error reading file {$file}",true);
                $dumpFileData = null;
                return null;
            }

            if($dumpFileData['error']){
                $this->_emLog->log('dump','0','None',$dumpFileData['message'],true);
                $dumpFileData = null;
                return false;
            }

            $dumpData = $dumpFileData['data'];
            $model = $dumpData['name'];
            $localModel = "\App\Models\EveryMatrix\\".$model;
            $fCount = count($dumpData['entries']);
            $i = 0;
            $this->_processingData->current++;
            $this->_processingConfig->value = json_encode($this->_processingData);
            $this->_processingConfig->save();
            $insertData = [];
            $updateData = [];
            $el = null;
            foreach($dumpData['entries'] as $entry) {
                $id = intval($entry['id']);
                if(!$el){
                    $el = $localModel::firstOrNew(['id' => $id]);
                }
                $insertData[$id] = $this->_process_entry($entry, $el, $model);
                $updateData[$id] = $this->_process_entry($entry, $el, $model, false);
                $i++;
            }

            $this->_bulk_insert_update($insertData, $updateData, $localModel);
            $insertData = null;
            unset($insertData);
            $dumpFileData = null; //kill for memory
            $dumpData = null; //clean memory
            $insertData = [];

            if(!$this->_keepFile){
//                $this->_delete_dump($filename);
            } else {
//                \Log::info("File had missing elements. Need to keep: ".$filename);
            }
            $this->_processingConfig->value = json_encode($this->_processingData);
            $this->_processingConfig->save();

        } catch (\Exception $e){
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            $this->_emLog->log('dump','0',$localModel, 'Error in insert',true);
        }
        return true;
    }

    private function _bulk_insert_update($insertData, $updateData, $localModel){
        foreach (array_chunk($insertData,1000, true) as $t) {
            $ids = array_keys($t);
            foreach($t as $e){
                $ids[] = $e['id'];
            }
            try {
                $existingRecords = $localModel::whereIn('id', $ids)->pluck('id');
                if ($cnt = $existingRecords->count()) {

                    foreach ($existingRecords as $thisId) {
                        try {
                            $localModel::where('id', $thisId)->update($updateData[$thisId]);
                        } catch (\Exception $e) {
                            \Log::error($e->getMessage());
                            \Log::error($e->getTraceAsString());
                        }
                        unset($t[$thisId]);
                    }
                }
                $ret = $localModel::insert($t);
            }catch(\Illuminate\Database\QueryException $ex){
                \Log::error($ex->getTraceAsString());
                \Log::error($ex->getMessage());
                dd($ex->getMessage());
            } catch(\Exception $e){
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }
        }
        $data = null;
        return true;
    }

    private function _process_entry($entry, &$el, $fillable = true){
        if($fillable){
            $entry = EMH::do_fillables($el, $entry);
        }
        $entry = EMH::fix_bad_data($entry);
        return $entry;
//        return true;
    }

    private function _delete_dump($file){
        $delFile = 'database\everymatrix\\'.$file;
        try {
            $delResult = \Storage::disk('base')->delete($delFile);
        } catch (\Exception $e){
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            $this->_emLog->log('dump',"0", "None", $e->getMessage(), true);
        }
    }
}
