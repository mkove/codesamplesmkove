<?php
    namespace App\Console\Commands\EveryMatrix;
    use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
    use App\Models\Config;
    use App\Models\EveryMatrix\EMLog;
    use App\Models\EveryMatrix\Event;
    use App\Providers\Broadage\BasketballFeedConnector as BroadAgeFeed;
    use Illuminate\Console\Command;
    class PollGamesCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'everymatrix:poll-games';
        protected $description = 'Poll games after each next update';

        public function handle() {

        }


    }
