<?php
    namespace App\Console\Commands\EveryMatrix;
    use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
    use App\Models\Config;
    use App\Models\EveryMatrix\EMLog;
    use App\Providers\Broadage\BasketballFeedConnector as BroadAgeFeed;
    use Illuminate\Console\Command;
    class EMResetDatabaseCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        private $_emLog;

        protected $signature = 'everymatrix:reset';
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'DANGER: THIS WILL CLEAR ALL EVERY MATRIX DATA';

        public function handle() {
            $this->_emLog = new EMLog();
            $this->_emLog->query()->delete();
            return $this->_do_reset();

        }

        private function _do_reset(){
            $models = ['BettingOfferStatus', 'BettingType', 'Currency', 'EntityProperty', 'EntityPropertyType', 'EntityType', 'Event', 'EventAction', 'EventActionDetail', 'EventActionDetailStatus', 'EventActionDetailType', 'EventActionDetailTypeUsage', 'EventActionStatus', 'EventActionType', 'EventActionTypeUsage', 'EventInfo', 'EventInfoStatus', 'EventInfoType', 'EventInfoTypeUsage', 'EventPart', 'EventPartDefaultUsage', 'EventParticipantInfo', 'EventParticipantInfoDetail', 'EventParticipantInfoDetailStatus', 'EventParticipantInfoDetailType', 'EventParticipantInfoDetailTypeUsage', 'EventParticipantInfoStatus', 'EventParticipantInfoType', 'EventParticipantInfoTypeUsage', 'EventParticipantRelation', 'EventStatus', 'EventTemplate', 'EventType', 'Location', 'LocationRelation', 'LocationRelationType', 'LocationType', 'OutcomeStatus', 'OutcomeType', 'OutcomeTypeBettingTypeRelation', 'Participant', 'ParticipantRelation', 'ParticipantRelationType', 'ParticipantRole', 'ParticipantType', 'ParticipantUsage', 'Provider', 'Sport'
            ];
            try{
                foreach($models as $model){

                    $localModel = "App\Models\EveryMatrix\\".$model;
                    $result = $localModel::query()->delete();
                    $this->_emLog->log('reset','0',$model,'Truncated',false);
                }
            } catch (Exception $e){
                \Log::error($e->getMessage());
                return false;
            }
            return true;
        }
    }
