<?php
    namespace App\Console\Commands\EveryMatrix;
    use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
    use App\Models\Config;
    use App\Models\EveryMatrix\EMLog;
    use Illuminate\Console\Command;
    class UnsubscribeCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'everymatrix:unsubscribe';
        private $_name;
        private $_url;
        private $_port;
        private $_path;
        private $_builtUrl;
        private $_subConfig;
        private $_batchId;
        private $_batches;
        private $_batchCompleted;
        private $_emLog;
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'UN-Subscribe to EveryMatrix.';
        public function handle() {
            $this->_emLog = new EMLog();
            $this->_set_config();
            $this->_unsubscribe();
        }

        private function _set_config(){

            $subsNameConfig = Config::firstOrCreate([
                'key' => 'em.subscription_name',
            ]);
            $subsUrlConfig = Config::firstOrCreate([
                'key' => 'em.subscription_url',
            ]);
            $subsPortConfig = Config::firstOrCreate([
                'key' => 'em.subscription_port',
            ]);
            $subsPathConfig = Config::firstOrCreate([
                'key' => 'em.subscription_path',
            ]);
            $subConfig = Config::firstOrCreate([
                'key' => 'em-readonly.subscription_id',
            ]);

            if(!$subsNameConfig->exists){
                $subsNameConfig->value = "Aaron123";
                $subsNameConfig->save();
            }
            if(!$subsUrlConfig->exists){
                $subsUrlConfig->value = "http://sept.oddsmatrix.com";
                $subsUrlConfig->save();
            }
            if(!$subsPortConfig->exists){
                $subsPortConfig->value = "8081";
                $subsPortConfig->save();
            }
            if(!$subsPathConfig->exists){
                $subsPathConfig->value = "/xmlfeed";
                $subsPathConfig->save();
            }
            if(!$subConfig->exists){
                $subConfig->value = 0;
                $subConfig->save();
            }
            $this->_name = $subsNameConfig->value;
            $this->_port = $subsPortConfig->value;
            $this->_url = $subsUrlConfig->value;
            $this->_path = (substr($subsPathConfig->value,0,1) === '/') ? $subsPathConfig->value : "/"
                .$subsPathConfig->value;
            $this->_builtUrl = $this->_url.":".$this->_port.$this->_path;
            $this->_subConfig = $subConfig;
            $this->_batchId = 1;
            $this->_batchCompleted = false;
            $this->_batches = 100;
        }

        private function _unsubscribe(){
            $data = [
                'requestType' => "UnsubscribeRequest",
                'subscriptionSpecificationName' => $this->_name,
                'subscriptionId' => $this->_subConfig->value
            ];

            EMH::send_request($this->_builtUrl, $this->_port, $data, 'unsub');
            $this->_subConfig->value = 0;
            $this->_subConfig->save();
            $this->_emLog->log('subscribe','0','None','Unsubscribed',false);
            return true;
        }

    }
