<?php
    namespace App\Console\Commands\EveryMatrix;
    use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
    use App\Models\Config;
    use App\Models\EveryMatrix\EMLog;
    use App\Providers\Broadage\BasketballFeedConnector as BroadAgeFeed;
    use Illuminate\Console\Command;
    class PingCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'everymatrix:ping';
        private $_name;
        private $_url;
        private $_port;
        private $_path;
        private $_builtUrl;
        private $_subConfig;
        private $_filename;
        private $_lastRun;
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Ping to keep connection alive. Every 1-3 minutes';
        public function handle() {
            $this->_set_config();
            $this->_send_ping();
        }

        private function _set_config(){


            $subsNameConfig = Config::firstOrNew([
                'key' => 'em.subscription_name',
            ]);
            $lastConfig = Config::firstOrNew([
                'key' => 'em.last_run',
            ]);
            $subsUrlConfig = Config::firstOrNew([
                'key' => 'em.subscription_url',
            ]);
            $subsPortConfig = Config::firstOrNew([
                'key' => 'em.subscription_port',
            ]);
            $subsPathConfig = Config::firstOrNew([
                'key' => 'em.subscription_path',
            ]);
            $subConfig = Config::firstOrNew([
                'key' => 'em-readonly.subscription_id',
            ]);

            if(!$subsUrlConfig->exists){
                $subsUrlConfig->value = "http://sept.oddsmatrix.com";
                $subsUrlConfig->save();
            }
            if(!$subsPortConfig->exists){
                $subsPortConfig->value = "8081";
                $subsPortConfig->save();
            }
            if(!$subsPathConfig->exists){
                $subsPathConfig->value = "/xmlfeed";
                $subsPathConfig->save();
            }
            if(!$subConfig->exists){
                $subConfig->value = 0;
                $subConfig->save();
            }

            if(!$lastConfig->exists){
                $lastConfig->label = "Last Feed Ran Date";
                $lastConfig->value = date('Y-m-d G:i:s A');
                $lastConfig->save();
            }

            $this->_lastRun = $lastConfig;
            $this->_name = $subsNameConfig->value;
            $this->_port = $subsPortConfig->value;
            $this->_url = $subsUrlConfig->value;
            $this->_path = (substr($subsPathConfig->value,0,1) === '/') ? $subsPathConfig->value : "/"
                .$subsPathConfig->value;
            $this->_builtUrl = $this->_url.":".$this->_port.$this->_path;
            $this->_subConfig = $subConfig;
        }

        private function _send_ping(){
            $id = date('U');
            $data = [
                'requestType' => "PingRequest",
                'id' => $id,
            ];
            $response = EMH::send_request($this->_builtUrl, $this->_port, $data, 'ping');

        }
    }
