<?php
    namespace App\Console\Commands\EveryMatrix;
    use App\Models\EveryMatrix\EMLog;
    use App\Models\EveryMatrix\Event;
    use App\Models\EveryMatrix\EventPartDefaultUsage;
    use App\Models\Game;
    use App\Models\Team;
    use Illuminate\Console\Command;
    use App\Providers\Game\Game as GameProvider;
    class PopulateFootballCommand extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'everymatrix:populate-football';
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Populate Football Games';

        private $_events;
        private $_league;
        private $_gameProvider;


        public function handle(GameProvider $gameProvider) {
            $this->_gameProvider = $gameProvider;
            $this->_events = $this->_get_events();
            $prettyEvents = $this->_parse_events();
            $this->_populate_games($prettyEvents);
        }

        private function _get_events(){
            return Event::with([
                'type',
                'status',
                'location',
                'template',
                'participants' => function($query){
                    $query->where('countryId', 229)->with([
                        'usage','roles'
                    ]);
                },
                'actions',
                'info',
                'event_part_default_usage',
                'event_part_default_usage.event_type',
                'event_part_default_usage.root_part',
                'actions.info',
                'actions.part',
                'actions.type',
                'actions.status'
            ])->where('sportId', 5)->get();
        }

        private function _parse_events(){
            $prettyData = [];

            foreach($this->_events as $event){
                //ONLY GRAB THE BABIES
                if($event->participants->count() == 2){
                    $teams = $this->_get_teams($event);
                    $parent = $this->_get_parent($event);
                    $game = $this->_create_game($event, $teams, $parent);
                }
            }

        }

        private function _get_teams($event){
            $teams = [
                            'hometeam' => [
                                'id' => '0',
                                'name' => 'Home',
                                'q1' => 0,
                                'q2' => 0,
                                'q3' => 0,
                                'q4' => 0,
                                'ot' => 0,
                                'totalscore' => 0,
                                'handicap' => 0
                            ],
                            'awayteam' => [
                                'id' => '0',
                                'name' => 'Away',
                                'q1' => 0,
                                'q2' => 0,
                                'q3' => 0,
                                'q4' => 0,
                                'ot' => 0,
                                'totalscore' => 0,
                                'handicap' => 0
                            ],
                        ];
            $participants = $event->participants;
            foreach($participants as $participant){
                $homeAway = $this->_get_home_away($participant, $event->id);
                $teams[$homeAway]['id'] = $participant->id;
                    $teams[$homeAway]['name'] = $participant->name;
                    $teams[$homeAway]['q1'] = 0;
                    $teams[$homeAway]['q2'] = 0;
                    $teams[$homeAway]['q3'] = 0;
                    $teams[$homeAway]['q4'] = 0;
                    $teams[$homeAway]['ot'] = 0;
                    $teams[$homeAway]['totalscore'] = 0;
                    $teams[$homeAway]['handicap'] = 0;
            }
            return $teams;
        }

        private function _get_home_away($participant, $eventId){
            foreach($participant->roles()->wherePivot('eventId',$eventId)->get() as $role) {

                if ($role->id == 1) {
                    return 'hometeam';
                }
                if ($role->id == 2) {
                    return 'awayteam';
                }
            }
            return null;
        }

        private function _get_parent($event){
            return $this->_events->where('id', $event->parentId)->first();
        }

        private function _populate_games($prettyGames){
//            $game = Game::firstOrNew(['id'=> 212321212]);
//            dd($game);
//            dd();
//            foreach($prettyGames as $game){
//                dd($game);
//            }
        }

        private function _create_game($event, $teams, $parent){
            $startDate = date('Y-m-d H:i:s', strtotime($event->startTime));
            $y = date('F', strtotime($event->startTime));
            $league = $this->_get_league($parent);
            $tz = trim(date('T', strtotime($event->startTime)));
            $game = [
                "contestID" => $event->id,
                "tournament_id" => $event->id,
                "tournament_name" => $teams['awayteam']['name']." at ".$teams['hometeam']['name'],
                "season" => $this->_get_season($event),
                "sport" => "football",
                "sport_id" => "2",
                "league" => $league,
                "week" => ($league == 'professional') ? $this->_get_week($parent, $event) : 0,
                "timezone" => $tz,
                "time" => $startDate,
                "start" => $startDate,
                "gmt" => $startDate,
                "status" => $this->_get_status($event),
                "round_id" => 0,
                "round" => "",
                "round_name" => "",
                "round_shortname" => "",
                "hometeam_shortname" => "",
                "awayteam_shortname" => "",
                "awayteam_halftime" => 0,
                "hometeam_halftime" => 0,
                "stadium" => "",
                "stadium_id" =>  0,
                "season_id" => 0,
                "hometeam_name" => $teams['hometeam']['name'],
                "hometeam_id" => $teams['hometeam']['id'],
                "hometeam_q1" => $teams['hometeam']['q1'],
                "hometeam_q2" => $teams['hometeam']['q2'],
                "hometeam_q3" => $teams['hometeam']['q3'],
                "hometeam_q4" => $teams['hometeam']['q4'],
                "hometeam_ot" => $teams['hometeam']['ot'],
                "hometeam_totalscore" => $teams['hometeam']['totalscore'],
                "season_year" => $this->_get_season_year($parent),
                "awayteam_name" => $teams['awayteam']['name'],
                "awayteam_id" => $teams['awayteam']['id'],
                "awayteam_q1" => $teams['awayteam']['q1'],
                "awayteam_q2" => $teams['awayteam']['q2'],
                "awayteam_q3" => $teams['awayteam']['q3'],
                "awayteam_q4" => $teams['awayteam']['q4'],
                "awayteam_ot" => $teams['awayteam']['ot'],
                "awayteam_totalscore" => $teams['awayteam']['totalscore'],
                "over_under" => "0.0",
                "hometeam_handicap" => $teams['hometeam']['handicap'],
                "awayteam_handicap" => $teams['awayteam']['handicap'],
                "timer" => "0",
                "ball" => "",
                "current_quarter" => 0,
                "completed_quarter" => 0,
                "home_team_id" => $this->_get_local_team($teams['hometeam']),
                "away_team_id" => $this->_get_local_team($teams['awayteam']),
                ];
                $newGame =  $this->_gameProvider->create_update($game);

        }

        private function _get_season($event){
            $name = $event->name;
     //       dump($event->name);
            return $name;
            $parentId = $event->parentId;
            while($parentId != 0){
                $parentEvent = $event->parent;
                $name = $parentEvent->name;
                $parentId = $parentEvent->parentId;
            }
            return $name;
        }
        private function _get_season_year($event){
            return str_replace('NCAA Football ', '', str_replace('NFL ', '', $event->name));
        }

        private function _get_status($event){
            return ($event->status) ? $event->status->name : "Pending";
        }

        private function _get_league($event){

            if($event->template){
                $this->_league = ($event->template->name == "NFL") ? "professional" : "college";
            }
            return $this->_league;
        }

        private function _get_week($parent, $event){
            $week1 = date('W', strtotime($parent->startTime));
            $gameWeek = date('W', strtotime($event->startTime));
            if($week1 > $gameWeek){ //overlapped to new year add 52 weeks
                return ((($gameWeek + 52) - $week1) + 1);
            } else { // still in same year
                return (($gameWeek - $week1) + 1);
            }
        }

        private function _get_local_team($providerTeam){
            $team = Team::firstOrNew(['provider_id' => $providerTeam['id']]);
            if(!$team->exists){
                $team->name = $providerTeam['name'];
                $team->short_name = $providerTeam['name'];
                $team->alt_name = $providerTeam['name'];
                $team->sport = 'football';
                $team->provider_id = $providerTeam['id'];
                $team->league = $this->_league;
                $team->active = true;
                $team->save();
            }
            return $team->id;
        }

    }
