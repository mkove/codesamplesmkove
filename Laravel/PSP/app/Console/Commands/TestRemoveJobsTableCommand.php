<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
    use Illuminate\Support\Facades\DB;

//    TODO:: split this shit up into separate jobs
	class TestRemoveJobsTableCommand extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		
		protected $signature = 'remove_jobs:test';
		
		
		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Remove jobs table';
		
		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			DB::table('jobs')->truncate();

		}
		

	
	}
