<?php
	
	namespace App\Console\Commands\DataGolf\Golf;
	
	
	
	use App\Helpers\DataGolfHelper;
	use App\Helpers\GolfHelper;
	use App\Helpers\SiteHelper;
	use App\Helpers\UserNotificationHelper;
	use App\Providers\Pool\Golf\GolfPlayerProvider;
	use App\Providers\Pool\Golf\GolfTournamentPlayerProvider;
	use App\Providers\Pool\Golf\GolfTournamentProvider;
	use Illuminate\Console\Command;
	
	class GetData extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'datagolf:golf {type} {--id=}';
		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Get DataGolf Feed Data';
		
		private $_dataGolfHelper;
		private $_golfTournamentProvider;
		private $_golfPlayerProvider;
		private $_golfTournamentPlayerProvider;
		
		private $_message;
		private $_notifyAdminFlag;
		private $_subject;
		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct(
			DataGolfHelper $dataGolfHelper,
			GolfTournamentProvider $golfTournamentProvider,
			GolfPlayerProvider $golfPlayerProvider,
			GolfTournamentPlayerProvider $golfTournamentPlayerProvider
		) {
			$this->_golfTournamentProvider= $golfTournamentProvider;
			$this->_golfPlayerProvider = $golfPlayerProvider;
			$this->_golfTournamentPlayerProvider = $golfTournamentPlayerProvider;
			$this->_dataGolfHelper = $dataGolfHelper;
			parent::__construct();
		}
		
		
		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			$this->_notifyAdminFlag = false;
			$this->_type = $this->argument('type');
			switch ($this->_type) {
				case 'schedule':
					
					$this->_get_schedule();
					break;
				case 'players':
					
					$this->_get_players();
					break;
				case 'rankings':
					
					$this->_get_rankings();
					break;
				case 'field':
					
					$this->_field_updates();
					break;
				case 'live':
					
					
					if(__conf('golf.force_replay_datagolf_previous_rounds', 'boolean',1)){
						for($r=1;$r<5;$r++){
							$this->_get_live($r);
						}
						__set_conf('golf.force_replay_datagolf_previous_rounds','0');
					} else{
						$this->_get_live();
					}
					break;
				case 'activate':
						$this->_activate();
					break;
				case 'finish':
					
					break;
			}
			$this->_notify_admin();
			return true;
		}
		
		private function _get_schedule(){
			$this->_subject = "DATA GOLF SCHEDULE";
			$this->_notifyAdminFlag = true;
			$schedule = $this->_dataGolfHelper->get_schedule();
			$currentYear = $schedule->current_season;
			$type = $schedule->tour;
			foreach($schedule->schedule as $tourney){
				$localTourney = $this->_golfTournamentProvider->get(['event_id' => $tourney->event_id, 'current_season' => $currentYear], true)->first();
				if(!$localTourney){
					
					$createData = [
						'name' => $tourney->event_name,
						'external_id' => "GD-".$tourney->event_id,
						'start_date' => date('Y-m-d', strtotime($tourney->start_date)),
						'end_date' => date('Y-m-d', strtotime($tourney->start_date." + 4 days")),
						'status' => 'Not Started',
						'country' => "NA",
						'venue' =>  $tourney->course,
						'type' => $type,
						'par' => '72',
						'gender' => 'male',
						'purse' => 0,
						'active' => false,
						'first_tee_off_at' => null,
						'config' => '',
						'prize_final' => 0,
						'archived' => false,
						'current_season' => $currentYear,
						'course' => $tourney->course,
						'event_id' => $tourney->event_id,
						'event_name' => $tourney->event_name,
						'location' => $tourney->location,
						'r1_teetime',
						'r2_teetime',
						'r3_teetime',
						'r4_teetime',
						'start_hole',
						'current_round' => 0,
					];
					$return = $this->_golfTournamentProvider->create($createData);
					$notifyMessage = "<strong>".$tourney->event_name."</strong> GD ID: <em style='color:red;'>".$tourney->event_id."</em>";
					$type = "NEW TOURNAMENT";
					$this->_add_message($notifyMessage,$type);
				} else{
					$updateData = [
						'name' => $tourney->event_name,
						'start_date' => date('Y-m-d', strtotime($tourney->start_date)),
						'end_date' => date('Y-m-d', strtotime($tourney->start_date." + 4 days")),
						'venue' =>  $tourney->course,
						'course' => $tourney->course,
						'location' => $tourney->location,
					];
					$return = $this->_golfTournamentProvider->update($localTourney->id, $updateData);
					$updateString = "";
					foreach($updateData as $item => $value){
						$updateString .= "<li><strong>{$item}</strong>: {$value}";
					}
					$notifyMessage = "<ul>".$updateString."</ul>";
					$type = "UPDATED TOURNAMENT";
					$this->_add_message($notifyMessage,$type);
				}
			}
		}
		
		private function _new_status($oldStatus, $newStatus){
			return ($newStatus != $oldStatus);
		}
		
		private function _reset_players_if_status_change(&$tournament,$newStatus = null){
			if($newStatus){
				if($tournament->status !== "Round 4"){
					$notifyMessage = "<strong>".$tournament->name."</strong> PLAYERS HOLE, TODAY";
					$type = "PLAYERS RESET";
					$this->_add_message($notifyMessage,$type);
					$players = $tournament->tournament_players;
					foreach($players as $player){
						$player->hole = null;
						$player->today = null;
						$player->live_today = null;
						$player->save();
					}
				}
			}
		}
		
		private function _get_players()
		{
			$players = $this->_dataGolfHelper->get_players();
			$this->_subject = "DATA GOLF PLAYERS ADDED";
			foreach ($players as $player) {

				$name = GolfHelper::reverse_last_first($player->player_name);
				$localPlayer = $this->_golfPlayerProvider->get(['dg_id' => $player->dg_id], true)->first();
				if (!$localPlayer) {
					try {
						$this->_notifyAdminFlag = true;

						$photo = "no-photo.png";
						$existingPlayers = $this->_golfPlayerProvider->get(['first_name' => $name['first'], 'last_name' => $name['last']], true);
						$existingPlayer = $existingPlayers->where('dg_id', null)->orWhere('dg_id', '')->first();
						if ($existingPlayer) $photo = $existingPlayer->photo;
						// echo ($name['first'] . " " . $name['last'] . " :: " . $localPlayer->id . "=> " . $localPlayer->name . " :: " . $existingPlayer->id . "=> " . $existingPlayer->name . $photo . "\n");

						$createData = [
							'name' => $name['full'],
							'external_id' => "DG-" . $player->dg_id,
							'country' => $player->country,
							'pos' => 9999,
							'avg_points' => 0,
							'points_total' => 0,
							'events' => 0,
							"internal_note" => "DG Added on " . date('F j Y, G:i a'),
							'order' =>  0,
							'photo' => $photo,
							'first_name' => $name['first'],
							'last_name' => $name['last'],
							'middle_name'  => $name['middle'],
							'display_name_mobile'  => $name['mobile'],
							'display_name'  => $name['short'],
							'config' => "",
							'pga_id' => null,
							'dg_id' => $player->dg_id,
							'country_code' => $player->country_code,
							'datagolf_rank' => '9999',
							'owgr_rank' => '9999',
						];
						$return = $this->_golfPlayerProvider->create($createData);
						$notifyMessage = "<strong>" . $return->name . "</strong> GD ID: <em style='color:red;'>" . $return->dg_id . "</em>";
						$type = "NEW GOLFER";
						$this->_add_message($notifyMessage, $type);
					} catch (\Exception $e) {
						\Log::error($e->getMessage());
					}

				} else {

					if (!$localPlayer->photo) {
						$photo = "no-photo.png";
						try {
							$existingPlayers = $this->_golfPlayerProvider->get(['first_name' => $name['first'], 'last_name' => $name['last']], true);
							$existingPlayer = $existingPlayers->where('dg_id', null)->orWhere('dg_id', '')->first();
							if ($existingPlayer) $photo = $existingPlayer->photo;

							$updateData = [
								'photo' => $photo,
							];
							$this->_golfPlayerProvider->update($localPlayer->id, $updateData);
							// echo($name['first']." ".$name['last']." :: ".$localPlayer->id.$localPlayer->name." :: ".$existingPlayer->id.$existingPlayer->name.$photo."\n");

						} catch (\Exception $e) {
							\Log::error($e->getMessage());
						}
					}
				}
			}
		}
		
		private function _get_rankings(){
			$rankings = $this->_dataGolfHelper->get_rankings();
			foreach($rankings->rankings as $player){
				$localPlayer = $this->_golfPlayerProvider->get(['dg_id' => $player->dg_id], true)->first();
				if($localPlayer){
					$updateData = [
						'pos' => $player->owgr_rank,
						'datagolf_rank' => $player->datagolf_rank,
						'owgr_rank' => $player->owgr_rank,
					];
					
					$this->_golfPlayerProvider->update($localPlayer->id, $updateData);
					
				}
			}
		}
		
		private function _field_updates(){
			
			$live = $this->_dataGolfHelper->get_live_field();
			$eventName = $live->event_name;
			$currentRound = $live->current_round;
			$localTournamentQuery = $this->_golfTournamentProvider->get(['name' => $eventName], true);
			$localTournament = $localTournamentQuery->where('event_id','<>', null)->with(['tournament_players'])->first();
			$this->_update_tournament($localTournament, $currentRound);
			$dgTournamentPlayers = $live->field;
			$startDate = $localTournament->start_date;
			$this->_subject = $localTournament->name.": DG PLAYERS ADDED";
			foreach($dgTournamentPlayers as $player){
				$tee1 = date('Y-m-d H:i:s', strtotime($startDate." ".$player->r1_teetime));
				$tee2 = date('Y-m-d H:i:s', strtotime($startDate." ".$player->r2_teetime." +1 days"));
				$tee3 = date('Y-m-d H:i:s', strtotime($startDate." ".$player->r3_teetime." +2 days"));
				$tee4 = date('Y-m-d H:i:s', strtotime($startDate." ".$player->r4_teetime." +3 days"));
				
				$golfPlayer = $this->_golfPlayerProvider->get(['dg_id' => $player->dg_id], true)->first();
				$gtp = $this->_golfTournamentPlayerProvider->get([ 'player_id' => $golfPlayer->id,
					'tournament_id' => $localTournament->id], true)->first();
				if(!$gtp){
					$this->_notifyAdminFlag = true;
					$createData = [
						'player_id' => $golfPlayer->id,
						'tournament_id' => $localTournament->id,
						'pos' => 0,
						'tee_time' => $player->r1_teetime,
						'par' => 0,
						'hole' => 0,
						'start_hole' => $player->start_hole,
						'today' => 0,
						'live_total' => 0,
						'live_today' => 0,
						'score_to_par' => 0,
						'drive_dist_avg' => 0,
						'drive_accuracy_pct' => 0,
						'gir' => 0,
						'putts_gir_avg' => 0,
						'saves' => 0,
						'eagles' => 0,
						'birdies' => 0,
						'pars' => 0,
						'bogeys' => 0,
						'doubles' => 0,
						'total' => 0,
						'winner' => 0,
						'status' => 'Active',
						'rank' => 0,
						'config' => '',
						'manual' => 0,
						'round_1' => null,
						'round_2' => null,
						'round_3' => null,
						'round_4' => null,
						'round_completed' => $currentRound - 1,
						'current_round' => $currentRound,
						'disqualified_round' => null,
						'percent' => 0,
						'prize' => 0,
						'amateur' => $player->am,
						'r1_teetime' => $tee1,
						'r2_teetime' => $tee2,
						'r3_teetime' => $tee3,
						'r4_teetime' => $tee4,
						'position' => 0,
					];
					$this->_golfTournamentPlayerProvider->create($createData);
					$notifyMessage = "<strong>".$player->player_name."</strong> GD ID: <em style='color:red;'>".$player->dg_id."</em>";
					$type = "NEW PLAYER";
					$this->_add_message($notifyMessage,$type);
				} else{
					$updateData = [
//						'pos' => $golfPlayer->position,
//						'position' => $golfPlayer->position,
						'rank' => $golfPlayer->rank,
						'tee_time' => $player->r1_teetime,
						'start_hole' => $player->start_hole,
						'round_completed' => $currentRound - 1,
						'current_round' => $currentRound,
						'amateur' => $player->am,
						'r1_teetime' => $tee1,
						'r2_teetime' => $tee2,
						'r3_teetime' => $tee3,
						'r4_teetime' => $tee4,
					];
					$this->_golfTournamentPlayerProvider->update($gtp->id, $updateData);
				}
			}
		}
		
		private function _update_tournament(&$tournament, $currentRound = null){
			
			$newStatusString = "Round ".$currentRound;
			if($tournament->current_round != $currentRound) {
				$notifyMessage = "<strong>" . $tournament->name . "</strong> CUR ROUND: <em style='color:red;'>" . $currentRound . "</em>";
				$type = "ROUND UPDATED";
				$this->_add_message($notifyMessage, $type);
			}
			$newTeeOff = GolfHelper::get_lock_time($tournament->tournament_players);
			
			if(!$tournament->first_tee_off_at !== $newTeeOff){
				$tournament->first_tee_off_at = $newTeeOff;
				$notifyMessage = "<strong>" . $tournament->name . "</strong> TEE: <em style='color:red;'>" . $newTeeOff . "</em>";
				$type = "TEE OFF LOCK UPDATED";
				$this->_add_message($notifyMessage, $type);
			}
			
			
			$now = date('U');
			$tee = strtotime($tournament->first_tee_off_at);
 			if($now < $tee){
				$tournament->status = "Not Started";
				$tournament->current_round = 0;
			} else {
				 if($this->_new_status($tournament->status, $newStatusString)){
					 $notifyMessage = "<strong>" . $tournament->name . "</strong> NEW STATUS: <em style='color:red;'>" . $newStatusString . "</em>";
					 $type = "TOURNEY STATUS";
					 $this->_add_message($notifyMessage, $type);
					 $this->_reset_players_if_status_change($tournament, $newStatusString);
				 }
				 
				$tournament->current_round = $currentRound;
				$tournament->status = $newStatusString;
			}
			
			$tournament->save();
			//updateprizes
			$this->_golfTournamentPlayerProvider->play_prizes($tournament->tournament_players, $tournament->prizes);
			return true;
		}
		
		private function _activate(){
			$tournaments = $this->_golfTournamentProvider->get_upcoming();
			$this->_subject = "Tournaments Activated: ";
			foreach($tournaments as $t){
				if(!$t->active){
					$this->_notifyAdminFlag = true;
					$this->_golfTournamentProvider->update($t->id, ['active' => 1]);
					$notifyMessage = "<strong>".$t->name."</strong> NEW STATUS: <em style='color:red;'>ACTIVE</em>";
					$type = "STATUS CHANGE";
					$this->_add_message($notifyMessage,$type);
				}
			}
		}
		
		private function _get_live($round = null){
			try{
				$stats = $this->_dataGolfHelper->get_tournament_stats($round);
				$eventName = $stats->event_name;
				$statsRound = $stats->stat_round;
				if($statsRound && $statsRound > 0){
					$localTournamentQuery = $this->_golfTournamentProvider->get(['event_name' => $eventName], true)
					                                                      ->with(['tournament_players','tournament_players.player']);
					$localTournament = $localTournamentQuery->where('event_id','<>', null)->first();
					$dgTournamentPlayers = $stats->live_stats;
					$this->_process_wds($localTournament->tournament_players, $dgTournamentPlayers);
					$roundString = "round_".$statsRound;
					$this->_subject = $localTournament->name."[{$roundString}]: DG PLAYER STATUS UPDATE";
					if($round <= $localTournament->current_round){ //Do NOT play future rounds.
						foreach($dgTournamentPlayers as $player){
							$golfPlayer = $this->_golfPlayerProvider->get(['dg_id' => $player->dg_id], true)->first();
							$gtp = $this->_golfTournamentPlayerProvider->get([ 'player_id' => $golfPlayer->id,
								'tournament_id' => $localTournament->id], true)->first();
							if($gtp){
								$updateData = [];
								$updateData['status'] = 'Active';
								$updateData[$roundString] = $player->today;
								$updateData['total'] = $player->total;
								$updateData['pos'] = (int)str_replace('T', '', $player->position);
								$updateData['position'] = $player->position;
								if(GolfHelper::is_player_out($player->position)){
									$updateData['status'] = str_replace('MDF', 'DNF', $player->position); //we don't have status MDF
								}
								
								if($localTournament->current_round == $statsRound){ //only update hole for current round
									$updateData['hole'] = $player->thru;
									$updateData['today'] = $player->today;
									$updateData['live_today'] = $player->today;
									$updateData['live_total'] = $player->total;
								}
								
								if($gtp->status !== $updateData['status']){
									$this->_notifyAdminFlag = true;
									$notifyMessage = "<strong>".$player->player_name."</strong> NEW STATUS: <em style='color:red;'>".$player->position."</em>";
									$type = "STATUS CHANGE";
									$this->_add_message($notifyMessage,$type);
								}
								$updatedPlayer = $this->_golfTournamentPlayerProvider->update($gtp->id, $updateData);
							}
						}
					}
				}
				
			} catch(\Exception $e){
				\Log::error($e);
			}
			
			
		}
		
		private function _process_wds($localPlayers, $livePlayers){
			$flat = $this->_flatten_live($livePlayers);
			foreach($localPlayers as $gtp){
				try{
					$gp = $gtp->player;
					if(!in_array($gp->dg_id,$flat)){
						if($gtp->status === 'Active'){
							$gtp->status = 'WD';
							$gtp->pos = 0;
							$gtp->position = 0;
							$gtp->total = null;
							$gtp->today = null;
							$gtp->live_total = null;
							$gtp->live_today = null;
							$gtp->save();
						}
						
					}
				} catch (\Exception $e){
					\Log::error($e);
				}
				
			}
		}
		
		private function _flatten_live($players){
			$flat = [];
			foreach($players as $player){
				$flat[$player->dg_id] = $player->dg_id;
			}
			return $flat;
		}
		
		private function _add_message($message, $type = ""){
			$this->_message .= "<tr><td style='padding:20px;'>".$message."</td><td style='padding:20px;'>".$type."</td></tr>";
		}
		
		private function _notify_admin(){
			if($this->_notifyAdminFlag){
				$header = "<table style='width:100%;' border='1'>
						<thead>
						<tr>
							<th style='padding:20px;font-weight: bold;'>Message</th>
							<th style='padding:20px;font-weight:bold;'>Type</th>
						</tr>
						</thead><tbody>";
				$footer = "</tbody></table>";
			
				UserNotificationHelper::send_admin($this->_subject, $header.$this->_message.$footer);
			}
			
		}
	}
