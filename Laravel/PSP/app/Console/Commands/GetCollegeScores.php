<?php

    namespace App\Console\Commands;

    use Illuminate\Console\Command;
    use App\Helpers\Goalserve\Professional\NFL as GS_NFL;
    use App\Providers\Chat\ChatService as Chat;

//    TODO:: split this shit up into separate jobs
    class GetCollegeScores extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        private $_season;

        protected $signature = 'scores:college';


        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Get Game Events';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            $this->_season = [
                'nfl' => [
                    'start' => date('d.m.Y', strtotime("-7 days")),
                    'end' => date('d.m.Y', strtotime("+7 days"))
                ]
            ];
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $this->run_all();
        }

        function run_all()
        {
//            $nflScheduleFileOdds = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/nfl-shedule?date1={$this->_season['nfl']['start']}&date2={$this->_season['nfl']['end']}&showodds=1";
//            $nflScheduleFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/nfl-shedule";
//
//            $nflScoreFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/nfl-scores";
//            $leagueMatch = [
//                'NFL' => ['sport' => 'football','league' => 'professional'],
//                'FBS (Division I-A)'  => ['sport' => 'football','league' => 'college'],
//            ];
//            $this->get_schedule($nflScheduleFile);
//            $this->get_odds($nflScheduleFileOdds);
//            $this->get_score($nflScoreFile);
            return true;
        }

        function get_schedule($file)
        {
            $realGames = GS_NFL::process($file);
            foreach($realGames as $id => $game)
            {
                try{
                    $game['sport'] = 'football';
                    $game['league'] = 'professional';
                    $game['tournament_id'] = strval($id);
                    unset($game['hometeam_handicap']);
                    unset($game['awayteam_handicap']);
                    unset($game['over_under']);
                    $localGame = \App\Models\Game::where('contestID',$game['contestID'])->first();

                    if(!$localGame){
                        $localGame = \App\Models\Game::create($game);
                    } else {
                        if($game['status'] != "Not Started") {


                            $localGame->status = $game['status'];

                            $localGame->hometeam_q1 = $game['hometeam_q1'];
                            $localGame->hometeam_q2 = $game['hometeam_q2'];
                            $localGame->hometeam_q3 = $game['hometeam_q3'];
                            $localGame->hometeam_q4 = $game['hometeam_q4'];
                            $localGame->hometeam_ot = $game['hometeam_ot'];
                            $localGame->hometeam_totalscore = $game['hometeam_totalscore'];
                            $localGame->awayteam_q1 = $game['awayteam_q1'];
                            $localGame->awayteam_q2 = $game['awayteam_q2'];
                            $localGame->awayteam_q3 = $game['awayteam_q3'];
                            $localGame->awayteam_q4 = $game['awayteam_q4'];
                            $localGame->awayteam_ot = $game['awayteam_ot'];
                            $localGame->awayteam_totalscore = $game['awayteam_totalscore'];
                            $localGame->save();
                        }

                    }
                }  catch (\Illuminate\Database\QueryException $e){
                    \Log::error($e->getMessage());
                }
            }
        }

        function get_odds($file)
        {
            $realGamesOdds = GS_NFL::process($file);
            foreach($realGamesOdds as $id => $game)
            {
                try{
                    $localGame = \App\Models\Game::where('contestID',  $game['contestID'])->first();
                    if($game['hometeam_handicap']){
                        $localGame->hometeam_handicap = $game['hometeam_handicap'];
                    }
                    if($game['awayteam_handicap']) {
                        $localGame->awayteam_handicap = $game['awayteam_handicap'];
                    }
                    if($game['over_under']) {
                        $localGame->over_under = $game['over_under'];
                    }
                    $localGame->save();
                }  catch (Exception $e){
                    \Log::error($e->getMessage());

                }
            }
        }

        function get_score($file)
        {
            $xmlRaw = GS_NFL::get_xml($file);
            $xml = simplexml_load_string($xmlRaw);

            foreach($xml->category->match as $match) {
                $contestId = strval($match['contestID']);
                $game = \App\Models\Game::where('contestID',$contestId)->first();
                if($game) {
                    $updateData = $this->_get_update_data($match);
                    $scoreChange = false;
                    $finished = false;
                    $game->timer = $match['timer'];
                    $game->status = $match['status'];
//					$game->save();
                    foreach($updateData as $var => $data) {
                        $game->{$var} = $data;
                    }
                    $game->save();
                    if($game->status != "Not Started") {

                        $res = \App\Models\SchwindyPick::where('game_id',$game->id)
                            ->where('team_id', $game->hometeam_id)
                            ->update([
                                'final' => $game->is_final,
                                'points' => $game->hometeam_totalscore,
                            ]);
                        \App\Models\SchwindyPick::where('game_id',$game->id)
                            ->where('team_id', $game->awayteam_id)
                            ->update([
                                'final' => $game->is_final,
                                'points' => $game->awayteam_totalscore,
                            ]);
                    }
                }
            }
        }

        private function _score_changed($key, $score, $game)
        {
            return ($game->{$key} != $score);
        }

        private function _get_update_data($match)
        {
            $updateData = [];

            $updateData['status'] = strval($match['status']);
            $hometeam = $match->hometeam;
            $updateData['hometeam_q1'] = intval($hometeam['q1']);
            $updateData['hometeam_q2'] = intval($hometeam['q2']);
            $updateData['hometeam_q3'] = intval($hometeam['q3']);
            $updateData['hometeam_q4'] = intval($hometeam['q4']);
            $updateData['hometeam_ot'] = intval($hometeam['ot']);
            $updateData['hometeam_totalscore'] = intval($hometeam['totalscore']);

            $awayteam = $match->awayteam;
            $updateData['awayteam_q1'] = intval($awayteam['q1']);
            $updateData['awayteam_q2'] = intval($awayteam['q2']);
            $updateData['awayteam_q3'] = intval($awayteam['q3']);
            $updateData['awayteam_q4'] = intval($awayteam['q4']);
            $updateData['awayteam_ot'] = intval($awayteam['ot']);
            $updateData['awayteam_totalscore'] = intval($awayteam['totalscore']);
            return $updateData;
        }
    }
