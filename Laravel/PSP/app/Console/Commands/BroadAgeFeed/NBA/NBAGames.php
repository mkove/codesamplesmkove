<?php
    namespace App\Console\Commands\BroadAgeFeed\NBA;
    use App\Providers\Broadage\BasketballFeedConnector as BroadAgeFeed;
    use Illuminate\Console\Command;

    class NBAGames extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'broadage:nbagames {id?}';


        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Get NBA BASKETBALL FUCKING DATA!!!!!!';
        public function handle() {
            $id = $this->argument('id');
            if($id){
                $this->_get_info($id);
            } else {
                $this->_get_fixtures();
            }
        }


        private function _get_fixtures(){
            $matchStatus = [
                'Full Time' => "Final",
                'Finished A.E.T.' => "Final",
                'Scheduled' => "Scheduled"
            ];

            $ncaaBoradAge = new BroadAgeFeed();
            $gameService = resolve('\App\Providers\Game\Game');
            $fixtures = $ncaaBoradAge->get_tournament_fixture(9);
            foreach($fixtures as $fixture){
                try {
                    $hometeam = $fixture->homeTeam;
                    $awayteam = $fixture->awayTeam;
                    $round = isset($fixture->round) ? $fixture->round : null;
                    $status = isset($fixture->status->name) ? $fixture->status->name : "Scheduled";
                    $info = $fixture->info;
                    $stage = $fixture->stage;
                    $statusString = isset($matchStatus[$status]) ? $matchStatus[$status] : $status;
                    $tourneyName = (isset($hometeam->name) && isset($awayteam->name)) ? $awayteam->name." at ".$hometeam->name : "TBD";
                    echo "\n Getting ".$tourneyName;
                    $fixedDate = str_replace('/', '-', $fixture->date);

                    $date = new \DateTime($fixedDate, new \DateTimeZone('America/New_York'));
                    $properTZDate = $date->format('Y-m-d H:i:s');

                    $gameTime = $properTZDate;
                    $gameData = [
                        "time" => $gameTime,
                        "start" => $gameTime,
                        "gmt" => $gameTime,
                        "over_under" => 0,
                        "hometeam_handicap" => 0,
                        "awayteam_handicap" => 0,
                        "timezone" => "EST",
                        "contestID" => isset($fixture->id) ? $fixture->id : 0,
                        "tournament_id" => isset($fixture->id) ? $fixture->id : 0,
                        "week" => 0,
                        "tournament_name" => $tourneyName,
                        "league" => "professional",
                        "sport" => "basketball",
                        "round" =>  ($round && isset($round->name)) ? $round->name : "",
                        "round_name" => ($round && isset($round->name)) ? $round->name : "",
                        "round_shortname" => ($round && isset($round->shortName)) ? $round->shortName : "",
                        "round_id" => ($round && isset($round->id)) ? $round->id : 0,

                        "hometeam_name" => isset($hometeam->name) ? $hometeam->name : "",
                        "hometeam_shortname" => isset($hometeam->mediumName) ? $hometeam->mediumName : "",
//                    "hometeam_mediumname" => isset($hometeam->mediumName) ? $hometeam->mediumName : "",
                        "hometeam_id" => isset($hometeam->id) ? $hometeam->id : 0,
                        "hometeam_q1" => isset($hometeam->score->quarter1) ? $hometeam->score->quarter1 : 0,
                        "hometeam_q2" => isset($hometeam->score->quarter2) ? $hometeam->score->quarter2 : 0,
                        "hometeam_q3" => isset($hometeam->score->quarter3) ? $hometeam->score->quarter3 : 0,
                        "hometeam_q4" => isset($hometeam->score->quarter4) ? $hometeam->score->quarter4 : 0,
                        "hometeam_totalscore"  => isset($hometeam->score->current) ? $hometeam->score->current : 0,
                        "hometeam_halftime"  => isset($hometeam->score->halfTime) ? $hometeam->score->halfTime : 0,
                        "hometeam_ot"  => isset($hometeam->score->overTime) ? $hometeam->score->overTime : 0,
                        "awayteam_ot"  => isset($awayteam->score->overTime) ? $awayteam->score->overTime : 0,
//                 /   "hometeam_currentscore"  => isset($hometeam->score->current) ? $hometeam->score->current : 0,

                        "awayteam_name" => isset($awayteam->name) ? $awayteam->name : "",
                        "awayteam_shortname" => isset($awayteam->mediumName) ? $awayteam->mediumName : "",
//                    "awayteam_mediumname" => isset($awayteam->mediumName) ? $awayteam->mediumName : "",
                        "awayteam_id" => isset($awayteam->id) ? $awayteam->id : 0,
                        "awayteam_q1" => isset($awayteam->score->quarter1) ? $awayteam->score->quarter1 : 0,
                        "awayteam_q2" => isset($awayteam->score->quarter2) ? $awayteam->score->quarter2 : 0,
                        "awayteam_q3" => isset($awayteam->score->quarter3) ? $awayteam->score->quarter3 : 0,
                        "awayteam_q4" => isset($awayteam->score->quarter4) ? $awayteam->score->quarter4 : 0,
                        "awayteam_ot" => isset($awayteam->score->quarter4) ? $awayteam->score->quarter4 : 0,
                        "awayteam_totalscore"  => isset($awayteam->score->current) ? $awayteam->score->current : 0,
                        "awayteam_halftime"  => isset($awayteam->score->halfTime) ? $awayteam->score->halfTime : 0,
//                    "awayteam_currentscore"  => isset($awayteam->score->current) ? $awayteam->score->current : 0,

                        "stadium" => isset($info->stadium->name) ? $info->stadium->name : "",
                        "stadium_id" => isset($info->stadium->id) ? $info->stadium->id : 0,
                        "status"  => $statusString,
                        "season"   => isset($stage->name) ? $stage->name : "",
                        "season_id" => isset($stage->id) ? $stage->id : 0,
                        "current_quarter" => $this->_get_current_quarter($fixture),
                        "completed_quarter" => $this->get_completed_quarter($fixture),
                    ];
                    $game = $gameService->create_update($gameData);
                } catch(\Exception $e){
                    \Log::error($e->getMessage());
	                \Log::error($e->getTraceAsString());
                }

            }
        }

        private function _get_current_quarter($fixture){
            $cht = $fixture->homeTeam->score->current;
            $q1ht = $fixture->homeTeam->score->quarter1;
            $q2ht = $fixture->homeTeam->score->quarter2;
            $q3ht = $fixture->homeTeam->score->quarter3;
            $q4ht = $fixture->homeTeam->score->quarter4;
            $q5ht = $fixture->homeTeam->score->overTime;

            $cat = $fixture->awayTeam->score->current;
            $q1at = $fixture->awayTeam->score->quarter1;
            $q2at = $fixture->awayTeam->score->quarter2;
            $q3at = $fixture->awayTeam->score->quarter3;
            $q4at = $fixture->awayTeam->score->quarter4;
            $q5at = $fixture->awayTeam->score->overTime;

            for($i=5;$i>0;$i--){
                $htVar = "q{$i}ht";
                $atVar = "q{$i}at";
                if(($cht == $$htVar) && ($cat == $$atVar)){
                    return $i;
                }
            }
            return null;
        }

        private function get_completed_quarter($fixture){
            $cht = $fixture->homeTeam->score->current;
            $q1ht = $fixture->homeTeam->score->quarter1;
            $q2ht = $fixture->homeTeam->score->quarter2;
            $q3ht = $fixture->homeTeam->score->quarter3;
            $q4ht = $fixture->homeTeam->score->quarter4;
            $q5ht = $fixture->homeTeam->score->overTime;

            $cat = $fixture->awayTeam->score->current;
            $q1at = $fixture->awayTeam->score->quarter1;
            $q2at = $fixture->awayTeam->score->quarter2;
            $q3at = $fixture->awayTeam->score->quarter3;
            $q4at = $fixture->awayTeam->score->quarter4;
            $q5at = $fixture->awayTeam->score->overTime;

            for($i=5;$i>0;$i--){
                $htVar = "q{$i}ht";
                $atVar = "q{$i}at";

                if(($cht == $$htVar) && ($cat == $$atVar)){
                    return ($i ==1) ? null : $i-1;
                }
            }
            return null;
        }
    }
    //    ?=
