<?php
    namespace App\Console\Commands\BroadAgeFeed;
    use App\Providers\Broadage\BasketballFeedConnector as BroadAgeFeed;
    use Illuminate\Console\Command;
    use App\Helpers\BroadageHelper as BAH;
    class GetBasketballGames extends Command {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'broadage:basketball';
        protected  $_league = "professional";

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Get NBA/NCAA Basketball Data';
        public function handle() {
            $this->_get_fixtures();
        }


        private function _get_fixtures(){
            $matchStatus = [
                'Full Time' => "Final",
                'Finished A.E.T.' => "Final",
                'Scheduled' => "Scheduled"
            ];

            $broadAgeFeed = new BroadAgeFeed();
            $gameService = resolve('\App\Providers\Game\Game');
            $sports = [
                9 => 'professional',
                43 => 'college',
            ];
            foreach($sports as $id => $league){
                $this->_league = $league;
                echo "\nGetting $league";
                $fixtures = $broadAgeFeed->get_tournament_fixture($id);
                foreach($fixtures as $fixture){
                    try {
                        $hometeam = $fixture->homeTeam;
                        $awayteam = $fixture->awayTeam;
                        $round = isset($fixture->round) ? $fixture->round : null;
                        $status = BAH::normalize_status($fixture->status->id);
                        $info = $fixture->info;
                        $stage = $fixture->stage;

                        $tourneyName = (isset($hometeam->name) && isset($awayteam->name)) ? $awayteam->name." at ".$hometeam->name : "TBD";
                        echo "\n[$league] Getting ".$tourneyName;
                        $fixedDate = str_replace('/', '-', $fixture->date);
                        $date = new \DateTime($fixedDate, new \DateTimeZone('America/New_York'));
                        $properTZDate = $date->format('Y-m-d H:i:s');
                        $gameTime = $properTZDate;
                        $hto = isset($hometeam->score->overTime) ? $hometeam->score->overTime : 0;
                        $ato = isset($awayteam->score->overTime) ? $awayteam->score->overTime : 0;
                        $curQuarter = BAH::get_current_quarter_by_status($status, $hto, $ato);
                        $homeTeamTrueScore = $this->_get_true_score($hometeam);
                        $awayTeamTrueScore = $this->_get_true_score($awayteam);
                        $gameData = [
                            "time" => $gameTime,
                            "start" => $gameTime,
                            "gmt" => $gameTime,
                            "over_under" => 0,
                            "hometeam_handicap" => 0,
                            "awayteam_handicap" => 0,
                            "timezone" => "EST",
                            "contestID" => isset($fixture->id) ? $fixture->id : 0,
                            "tournament_id" => isset($fixture->id) ? $fixture->id : 0,
                            "week" => 0,
                            "tournament_name" => $tourneyName,
                            "league" => $league,
                            "sport" => "basketball",
                            "round" =>  ($round && isset($round->name)) ? $round->name : "",
                            "round_name" => ($round && isset($round->name)) ? $round->name : "",
                            "round_shortname" => ($round && isset($round->shortName)) ? $round->shortName : "",
                            "round_id" => ($round && isset($round->id)) ? $round->id : 0,
                            "hometeam_name" => isset($hometeam->name) ? $hometeam->name : "",
                            "hometeam_shortname" => isset($hometeam->mediumName) ? $hometeam->mediumName : "",
//                    "hometeam_mediumname" => isset($hometeam->mediumName) ? $hometeam->mediumName : "",
                            "hometeam_id" => isset($hometeam->id) ? $hometeam->id : 0,
                            "hometeam_q1" => $homeTeamTrueScore['q1'],
                            "hometeam_q2" => $homeTeamTrueScore['q2'],
                            "hometeam_q3" => $homeTeamTrueScore['q3'],
                            "hometeam_q4" => $homeTeamTrueScore['q4'],
                            "hometeam_ot"  => $hto,
                            "hometeam_totalscore"  => isset($hometeam->score->current) ? $hometeam->score->current : 0,
                            "hometeam_halftime"  => isset($hometeam->score->halfTime) ? $hometeam->score->halfTime : 0,
                            "awayteam_ot"  => $ato,
//                 /   "hometeam_currentscore"  => isset($hometeam->score->current) ? $hometeam->score->current : 0,
                            "awayteam_name" => isset($awayteam->name) ? $awayteam->name : "",
                            "awayteam_shortname" => isset($awayteam->mediumName) ? $awayteam->mediumName : "",
//                    "awayteam_mediumname" => isset($awayteam->mediumName) ? $awayteam->mediumName : "",
                            "awayteam_id" => isset($awayteam->id) ? $awayteam->id : 0,
                            "awayteam_q1" => $awayTeamTrueScore['q1'],
                            "awayteam_q2" => $awayTeamTrueScore['q2'],
                            "awayteam_q3" => $awayTeamTrueScore['q3'],
                            "awayteam_q4" => $awayTeamTrueScore['q4'],
                            "awayteam_ot" => $ato,
                            "awayteam_totalscore"  => isset($awayteam->score->current) ? $awayteam->score->current : 0,
                            "awayteam_halftime"  => isset($awayteam->score->halfTime) ? $awayteam->score->halfTime : 0,
//                    "awayteam_currentscore"  => isset($awayteam->score->current) ? $awayteam->score->current : 0,
                            "stadium" => isset($info->stadium->name) ? $info->stadium->name : "",
                            "stadium_id" => isset($info->stadium->id) ? $info->stadium->id : 0,
                            "status"  => $status,
                            "season"   => isset($stage->name) ? $stage->name : "",
                            "season_id" => isset($stage->id) ? $stage->id : 0,
                            "current_quarter" => $curQuarter,
                            "completed_quarter" => ($status == 'Final') ? $curQuarter :  BAH::get_completed_quarter_by_status($status),
                        ];
                        echo "\n[$status] Creating: Cur: ".$gameData['current_quarter']." and Fin: ".$gameData['completed_quarter'];
                        $game = $gameService->create_update($gameData);
                    } catch(\Exception $e){
                        \Log::error($e->getMessage());
                        \Log::error($e->getTraceAsString());
                    }

                }
            }

        }

        private function _get_true_score(&$team){
            $data = [];
            $data['q1'] = isset($team->score->quarter1) ? $team->score->quarter1 : 0;
            $data['q2'] = $data['q1'] + (isset($team->score->quarter2) ? $team->score->quarter2 : 0);
            $data['q3'] = $data['q2'] + (isset($team->score->quarter3) ? $team->score->quarter3 : 0);
            $data['q4'] = $data['q3'] + (isset($team->score->quarter4) ? $team->score->quarter4 : 0);
         //   dump($data);
            return $data;
        }
    }
