<?php

namespace App\Console\Commands;

use App\Helpers\GolfHelper;
use App\Models\GolfRanking;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetGolfRankingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'golf:rankings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get and save rankings each tuesday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{

            $golfHelper = new GolfHelper();
            $playerRankings = $golfHelper->getRankingsAsCollection();
            foreach ($playerRankings as $playerRanking) {
                try {
                    $data = [
                        'player_id' => $playerRanking['id'],
                        'name' => $playerRanking['name'],
                        'pos' => $playerRanking['pos'],
                        'average' => floatval($playerRanking['avg_points']),
                        'total' => floatval($playerRanking['points_total']),
                        'events' => floatval($playerRanking['events']),
                        'created_at' => Carbon::now()
                    ];
                    $rankingPlayer = GolfRanking::where('player_id', $playerRanking['id'])->first();
                    if($rankingPlayer){
                        GolfRanking::update($rankingPlayer->id, $data);
                    } else {
                        GolfRanking::create($data);
                    }
                } catch (\Exception $exception) {
                    \Log::error($exception->getMessage());
                }
            }
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
        }

    }
}
