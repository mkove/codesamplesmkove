<?php
	
	namespace App\Console\Commands;
	
	use App\Events\TestBroadcast;
	use Illuminate\Console\Command;

//    TODO:: split this shit up into separate jobs
	class TestBroadcastCommand extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		
		protected $signature = 'broadcast:test';
		
		
		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Test Redis';
		
		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			$passedHash = md5(date('U'));
			broadcast(new TestBroadcast($passedHash));

		}
		

	
	}
