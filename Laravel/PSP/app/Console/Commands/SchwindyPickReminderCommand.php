<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Pool;
use App\Mail\Pool\Schwindy\PickReminder;

//    TODO:: split this shit up into separate jobs
class SchwindyPickReminderCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */

	protected $signature = 'pick:reminder';


	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Pick Reminder';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		try {
			\Log::info('Pick Reminder handle');

			$pools = Pool::orderBy('id', 'desc')->with([
				'users',
				'invites',
				'commissioner',
				'schwindy',
				'schwindy.picks',
			]);

			$year = date("Y");
			$pools->where('status', 'active');
			$pools->where('type', 2);
			$pools->where('created', 'LIKE', $year . '%');
			$pools = $pools->paginate(5000);

			\Log::info('Pick Reminder handle' . $year);

			foreach ($pools as $pool) {
				$current_week = $pool->schwindy ? $pool->schwindy->current_week : 0;

				if ($current_week != 0) {
					$users = $pool->users;
					$schwindy = $pool->schwindy;

					foreach ($users as $user) {

						$userPicks = $schwindy->picks->where('user_id', $user->id)->where('week', $current_week)->where('schwindy_id', $schwindy->id);
						$picked_count = count($userPicks);

						if (!$picked_count) {
							$email = $user->email;
							$pool = $schwindy->pool;
							$pool_name = $pool->pool_name;


							$subject = "Pick Reminder";
							if (date('w') == 0)
								$message = "You have not yet made picks for<b>" . $pool_name . "</b>.  Be sure to get your picks in before your pool locks at 12:55pm";
							else
								$message = "You have not yet made picks for <b>" . $pool_name . "</b>.  Your deadline to make picks is Sunday at 12:55 Eastern, but If you would like to pick a team from the Thursday Night game it must be submitted before the scheduled kickoff.";
							$sendReceipt = new PickReminder($pool, $schwindy, $subject, $message);
							$sendReceipt->send_pick_save_email($email);
						}
					}
				}
			}
		} catch (\Exception $exception) {
			\Log::error($exception->getMessage());
		}
	}
}
