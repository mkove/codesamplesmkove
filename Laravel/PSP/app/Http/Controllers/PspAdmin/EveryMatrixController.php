<?php

namespace App\Http\Controllers\PspAdmin;

use App\Console\Commands\EveryMatrix\ParseDumpData;
use App\Jobs\EveryMatrixReset;
use App\Models\Config;
use App\Models\EveryMatrix\EMLog;
use \App\Models\EveryMatrix\Event as EMEvent;
use App\Helpers\Everymatrix\EverymatrixHelper;
use App\Http\Controllers\Controller;
use App\Models\EveryMatrix\ParticipantRole;
use App\Models\EveryMatrix\Sport;
use App\Models\Game;
use App\Models\GameEvent;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use App\Helpers\Everymatrix\EverymatrixHelper as EMH;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;


class EveryMatrixController extends Controller
{
    public function index(Request $request)
    {

        $models = EMLog::select('model')->groupBy('model')->get();
        $logs = EMLog::orderBy('created_at', 'desc');

        if ($action = $request->get('action')) {
            $logs = $logs->where('action', $action);
        }

        if ($model = $request->get('model')) {
            $logs = $logs->where('model', $model);
        }

        if ($action = $request->get('error')) {
            $logs = $logs->where('error', true);
        }

        $logs = $logs->paginate('500');

        return view('pspadmin.everymatrix.index', ['files' => [], 'prettyFiles' => [], 'logs' => $logs, 'models' => $models]);
    }

    public function sport($sportId = null)
    {
        if ($sportId) {
            $events = EMEvent::with([

                'type',
                'status',
                'location',
                'participants',
                'participants_infos',
                'actions',
                'event_part_default_usage',
                'event_part_default_usage.event_type',
                'event_part_default_usage.root_part',
                'actions.info',
                'actions.part',
                'actions.type',
                'actions.status'
            ])
                ->orderBy('startTime', 'asc')->where('sportId', $sportId)->get();
        } else {
            $events = [];
        }
        $ids = [5, 8, 2];
        $sports = Sport::whereIn('id', $ids)->orderBy('name', 'asc')->get();
        //            $sports = Sport::get();
        return view('pspadmin.everymatrix-sport', ['events' => $events, 'sportId' => $sportId, 'sports' => $sports]);
    }

    public function parse()
    {
        EMH::read_update_file();
    }

    public function football()
    {
        //            Artisan::call('everymatrix:populate-football');
        //            die();
        //            $participantRoles = ParticipantRole::get();
        //            dd($participantRoles);
        $events = EMEvent::with([

            'type',
            'status',
            'location',
            'participants',
            'participants.usage',
            'participants.roles',
            'participants_infos',
            'actions',
            'event_part_default_usage',
            'event_part_default_usage.event_type',
            'event_part_default_usage.root_part',
            'actions.info',
            'actions.part',
            'actions.type',
            'actions.status'
        ])
            ->orderBy('startTime', 'asc')->where('sportId', 5)->get();
        return view('pspadmin.everymatrix.football', ['events' => $events]);
    }

    public function golf()
    {
        //            Artisan::call('everymatrix:populate-football');
        //            die();
        //            $participantRoles = ParticipantRole::get();
        //            dd($participantRoles);
        $events = EMEvent::with([

            'type',
            'status',
            'location',
            'participants',
            'participants.usage',
            'participants.roles',
            'participants_infos',
            'actions',
            'event_part_default_usage',
            'event_part_default_usage.event_type',
            'event_part_default_usage.root_part',
            'actions.info',
            'actions.part',
            'actions.type',
            'actions.status'
        ])
            ->orderBy('startTime', 'asc')->where('sportId', 2)->get();
        return view('pspadmin.everymatrix.golf', ['events' => $events]);
    }


    public function load(Request $request)
    {
        $file = $request->file;
        $data = EverymatrixHelper::read_dump_file($file);
        //            $content = EverymatrixHelper::grab_file_raw($file);
        //            $entries = EverymatrixHelper::get_entries($content);
        //            $name = EverymatrixHelper::get_file_type_name($entries);

        //            preg_match_all('/((?:^|[A-Z])[a-z]+)/',$name,$matches);
        //            $prettyName = implode(" ", $matches[0]);
        //            $updatable = EverymatrixHelper::parse_update_content($entries);
        //            return response()->json([
        //                'html' => " <h1>".$name ."</h1>".$updatable,
        //                'messages' => [],
        //                'error' => false,
        //            ]);
    }

    public function restart(Request $request)
    {
        $messages = [];
        try {
            $hard = $request->get('hard');
            if ($hard) {
                EveryMatrixReset::dispatchNow();
                $messages[] = [
                    'type'  => 'success',
                    'message'   => "Hard Reset Databse",
                ];
            }
            if ($request->ajax()) {
                return response()->json([
                    'html' => view('pspadmin.everymatrix.partials.dump')->render(),
                    'messages' => $messages,
                    'error' => false,
                ]);
            }
            session()->flash('messages', $messages);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return redirect()->route('dashboard');
    }

    //        public function parse(){
    //
    //            Artisan::call('everymatrix:parse-dump');
    //        }
}
