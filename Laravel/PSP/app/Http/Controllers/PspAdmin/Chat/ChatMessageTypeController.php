<?php

namespace App\Http\Controllers\PspAdmin\Chat;

use App\Models\ChatMessageType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatMessageTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  ChatMessageType $chatMessageType
     * @return \Illuminate\Http\Response
     */
    public function show(ChatMessageType $chatMessageType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  ChatMessageType $chatMessageType
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatMessageType $chatMessageType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  ChatMessageType $chatMessageType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatMessageType $chatMessageType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  ChatMessageType $chatMessageType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatMessageType $chatMessageType)
    {
        //
    }
}
