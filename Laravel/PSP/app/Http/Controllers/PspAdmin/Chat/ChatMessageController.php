<?php

namespace App\Http\Controllers\PspAdmin\Chat;

use App\Models\Chat;
use App\Models\ChatMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatMessageController extends Controller
{
    private $_messages;
    private $_hadError;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chatMessage = $request->except('_method','_token', 'reloadable','reloadable_type');
        $chatMessage = ChatMessage::create($chatMessage);
        $chat = Chat::with(['messages','messages.user'])->find($chatMessage->chat_id);
        $this->_hadError = false;
        if($chatMessage){
            $this->_messages[] = [
                'type' => 'success',
                'message' => "Created!",
            ];
        } else {
            $this->_hadError = true;
            $this->_messages[] = [
                'type' => 'danger',
                'message' => "Failed to update. Try again!",
            ];
        }
        if($request->ajax()) {
            return $this->_build_response($chat, $request);
        }

        return view('pspadmin.chat.index',['chat' => $chatMessage->chat]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  ChatMessage $chatMessage
     * @return \Illuminate\Http\Response
     */
    public function show(ChatMessage $chatMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  ChatMessage $chatMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatMessage $chatMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  ChatMessage $chatMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatMessage $chatMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  ChatMessage $chatMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatMessage $chatMessage)
    {
        //
    }

    private function _build_response($chat, $request ){
        $reloadable = $request->get('reloadable');
        $html = ($reloadable) ?? null;
        return response()->json([
            'html' => ($reloadable) ? view($reloadable,['chat' => $chat])->render() : '',
            'messages' => $this->_messages,
            'error' => $this->_hadError
        ]);
    }
}
