<?php

namespace App\Http\Controllers\PspAdmin\Config;

use App\Helpers\SiteHelper;
use App\Models\SiteContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SiteContentController extends Controller
{

    private $_messages;
    private $_hadError;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = SiteContent::orderBy('section', 'asc')->where('section', '<>', 'info')->where('identifier', '<>', '')->get();
        $sections = SiteContent::select('section')->groupBy('section')->get();
        $orgContent = [];
        try {
            foreach ($contents as $content) {
                $content->used = $this->_used_where($content->identifier);
                //                dump($content->toArray(),$content->identifier);
                $parts = explode('.', $content->identifier);
                $count = count($parts);
                switch ($count) {
                    case 1:
                        $section = $parts[0];
                        $subSection = "legacy-general";
                        $view = "unnamed";
                        $label = $content->identifier;
                        break;
                    case 2:
                        $section = $parts[0];
                        $subSection = "legacy-general";
                        $view = "unnamed";
                        $label = $parts[1];
                        break;
                    case 3:
                        $section = $parts[0];
                        $subSection = $parts[1];
                        $view = "unnamed";
                        $label = $parts[2];
                        break;
                    case 4:
                        $section = $parts[0];
                        $subSection = $parts[1];
                        $view = $parts[2];
                        $label = $parts[3];
                        break;
                    case 5:
                        $section = $parts[0];
                        $subSection = $parts[1];
                        $view = $parts[2];
                        $label = $parts[3] . " " . $parts[4];
                        break;
                }
                //                $section = isset($parts[0]) ? $parts[0] : "General";
                //                $subSection = isset($parts[1]) ? $parts[1] : "General";
                //                $view = isset($parts[2]) ? $parts[2] : "General";
                //                $label = isset($parts[3]) ? $parts[3] : "Label";
                //                $label .= isset($parts[4]) ? $parts[4] : "";
                //                if(count($parts) == 4){
                //                    if($parts[0] == 'squareboard'){
                if (!isset($orgContent[$section])) {
                    $orgContent[$section] = [];
                }
                if (!isset($orgContent[$section][$subSection])) {
                    $orgContent[$section][$subSection] = [];
                }
                if (!isset($orgContent[$section][$subSection][$view])) {
                    $orgContent[$section][$subSection][$view] = [];
                }
                $orgContent[$section][$subSection][$view][$label] = $content;
                //                    }

                //                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return view('pspadmin.config.content.index', ['contents' => $contents, 'sections' => $sections, 'contentData' => $orgContent]);
    }

    public function help_content()
    {
        $contents = SiteContent::orderBy('section', 'asc')->where('section',  'info')->get();
        $contentData = [];
        foreach ($contents as $content) {
            $parts = explode('.', $content->identifier);
            $section = isset($parts[1]) ? $parts[1] : 'General';
            if (!isset($contentData[$section])) {
                $contentData[$section] = [];
            }
            $contentData[$section][] = $content;
        }
        return view('pspadmin.config.content.help', ['contents' => $contents,  'contentData' => $contentData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  SiteContent $content
     * @return \Illuminate\Http\Response
     */
    public function show(SiteContent $content)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  SiteContent $content
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteContent $content)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  SiteContent $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteContent $content)
    {
        $user = Auth::user();
        $contentData = [
            'content' => $request->get('content'),
            'note' => "Updated by " . $user->email . " on " . date('F j, Y G:i A')
        ];
        if ($content->update($contentData)) {
            $this->hadError = false;
            $this->_messages[] = [
                'type' => 'success',
                'message' => "Updated {$content->label}!",
            ];
        } else {
            //                $this->hadError = true;
            //                $this->_messages[] = [
            //                    'type' => 'danger',
            //                    'message' => "Failed to update. Try again!",
            //                ];
        }
        SiteHelper::flush_content();
        if ($request->ajax()) {
            return $this->_build_response($content, $request);
        }

        return view('pspadmin.config.content.edit', ['content' => $content]);
        //
    }

    private function _build_response($content, $request)
    {
        $reloadable = $request->get('reloadable');
        $reloadableType = $request->get('reloadable_type');

        if ($reloadableType == 'index') {
            $html = ($reloadable) ?? null;
            $contents = Content::get();
            return response()->json([
                'html' => ($reloadable) ? view($reloadable, ['contents' => $contents])->render() : '',
                'messages' => $this->_messages,
                'error' => $this->_hadError
            ]);
        } elseif ($reloadableType == 'edit') {
            $html = ($reloadable) ?? null;
            return response()->json([
                'html' => ($reloadable) ? view($reloadable, ['content' => $content])->render() : '',
                'messages' => $this->_messages,
                'error' => $this->_hadError
            ]);
        } else {
            $html = ($reloadable) ?? null;
            return response()->json([
                'html' => ($reloadable) ? view($reloadable, ['content' => $content])->render() : '',
                'messages' => $this->_messages,
                'error' => $this->_hadError
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  SiteContent $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteContent $content)
    {
        $content->delete();
        $this->_messages[] = [
            'type' => 'success',
            'message' => "Deleted",
        ];
        SiteHelper::flush_content();
        return response()->json([
            'html' => '',
            'messages' => null, // $this->_messages,
            'error' => $this->_hadError
        ]);
    }

    private function _used_where($key = '')
    {

        $used = [];
        try {
            foreach (\File::allFiles(resource_path('/views/pspfrontend')) as $file) {
                //                    dump($file->getFilename());
                $contents = \File::get($file);
                if (strpos($contents, $key) !== false) {
                    $used[] = $file->getFilename();
                }
            }
            //            $contents = \File::get($filename);
        } catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        }
        return $used;
    }
}
