<?php

namespace App\Http\Controllers\PspAdmin\Config;

use App\Models\StaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staticPages = StaticPage::orderBy('order', 'asc')->get();
        return view('pspadmin.config.page.index', compact('staticPages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pspadmin.config.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $pageData = $request->except('_token');
            $pageData['user_id'] = Auth::user()->id;

            $pageData['slug'] = $this->_do_slug($pageData['title']);
            StaticPage::create($pageData);
            $messages[] = [
                'type'  => 'success',
                'message'   => "Page Created",
            ];
            session()->flash('messages', $messages);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return redirect()->route('static-page.index');
    }

    private function _do_slug($title)
    {
        $slug = Str::slug($title);
        $i = 0;
        $count = StaticPage::where('slug', $slug)->count();
        while ($count != 0) {
            $i++;
            $slug = Str::slug($title) . "-" . $i;
            $count = StaticPage::where('slug', $slug)->count();
        }
        return $slug;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    public function show(StaticPage $staticPage)
    {
        dd($staticPage);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    //        public function edit($id)
    public function edit($staticPage, Request $request)
    {
        return view('pspadmin.config.page.edit', compact('staticPage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaticPage $staticPage)
    {
        try {
            $pageData = $request->except('_token', '_method');
            $staticPage->update($pageData);
            $messages[] = [
                'type'  => 'success',
                'message'   => "Page Updated",
            ];
            session()->flash('messages', $messages);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return redirect()->route('static-page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        try {
            $staticPage->delete();
            $messages[] = [
                'type'  => 'success',
                'message'   => "Page Deleted",
            ];
            session()->flash('messages', $messages);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return redirect()->route('static-page.index');
    }
}
