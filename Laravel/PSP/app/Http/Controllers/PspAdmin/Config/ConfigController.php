<?php

namespace App\Http\Controllers\PspAdmin\Config;

use App\Helpers\SiteHelper;
use App\Models\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configs = Config::where('active',true)->where('special',false)->get();
        $byCategory = [];
        foreach($configs as $config){
            $parts = explode('.', $config->key);
            
            $mainKey = trim($parts[0]);
            preg_replace( "/\r|\n/", "", $mainKey );

            $subKey = (isset($parts[1])) ? $parts[1] : 'misc';
            $byCategory[$mainKey][$subKey][] = $config;
        }
        return view('pspadmin.config.index',['configs' => $byCategory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    public function show(Config $config)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    public function edit(Config $config)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Config $config)
    {
        $data = $request->except('_token', '_method');
        Config::where('id', $config->id)->update($data);
        $messages[] = [
            'type' => 'success',
            'message' => "Updated <strong>{$config->label}</strong>!",
        ];
        SiteHelper::flush_config();
        $messages[] = [
            'type' => 'info',
            'message' => "Front end cache flushed!",
        ];
        session()->flash('messages', $messages);
        return redirect()->route('config.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Config $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {
        //
    }

    public function flush(){
        SiteHelper::flush();
        $messages[] = [
            'type' => 'info',
            'message' => "Front end cache flushed!",
        ];
        session()->flash('messages', $messages);
        return back();
    }
}
