<?php

namespace App\Http\Controllers\PspAdmin\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
		|--------------------------------------------------------------------------
		| Login Controller
		|--------------------------------------------------------------------------
		|
		| This controller handles authenticating users for the application and
		| redirecting them to your home screen. The controller uses a trait
		| to conveniently provide its functionality to your applications.
		|
		*/

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'login_as']);
    }

    public function logout()
    {
        auth()->logout();
        return redirect('/login'); // send back all errors to the login form;
    }

    public function show_login()
    {
        return view('pspadmin.login');
    }

    //        public function show_signup()
    //        {
    //            return view('pspadmin.signup',['crumbs' => []]);
    //        }

    public function login_as(Request $request)
    {
        $userId = $request->get('user_id');
        $secretToken = $request->get('secretCode');

        $user = User::find($userId);

        if (md5($user->email) == $secretToken) {
            $re = auth()->logout();
            if (auth()->loginUsingId($user->id)) {
                return redirect('/');
            }
        }

        return redirect()->route('psp.login');
    }

    public function do_login(Request $request)
    {
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = \Validator::make($request->all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return redirect('/admin/login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput($request->except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email' => $request->get('email'),
                'password' => $request->get('password'),
                'is_admin' => 'yes'
            );

            // attempt to do the login
            if ($ret = \Auth::attempt($userdata)) {
                // validation successful!
                // redirect them to the secure section or whatever
                return redirect('/admin/dashboard');
                // for now we'll just echo success (even though echoing in a controller is bad)

            } else {
                $validator->errors()->add('nologin', 'Wrong username or password!');
                // validation not successful, send back to form
                return redirect('/login')->withErrors($validator); // send back all errors to the login form;

            }
        }
    }
}
