<?php

    namespace App\Http\Controllers\PspAdmin;
    use App\Http\Controllers\Controller;
    use App\Models\Game;
    use Illuminate\Http\Request;

    class BasketballGameController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        private $_gameService;

        public function __construct(\App\Providers\Game\Game $gameService ){
            $this->_gameService = $gameService;
        }

        public function index()
        {
            //
            $games = Game::where('start', '>', date('Y-m-d'))-> where('sport','basketball')->where('league','college')->orderby('start','asc')->paginate(200);
            return view('pspadmin.game.basketball.college.index', ['games' => $games]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $teams = \App\Helpers\TeamHelper::teams(\App\Models\Game::all());
            return view('pspadmin.game.football.create', ['teams' => $teams]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(\App\Http\Requests\PspAdmin\CreateGameRequest $request)
        {
            $teams = \App\Helpers\TeamHelper::teams(\App\Models\Game::all());
            $game = $request->except('_token');
            $time = $game['time'];
            $date = date('Y-m-d H:i:s', strtotime($time));
            $game['hometeam_name'] = isset($teams[$game['hometeam_id']]) ? $teams[$game['hometeam_id']] : 'Home Team';
            $game['awayteam_name'] = isset($teams[$game['awayteam_id']]) ? $teams[$game['awayteam_id']] : 'Away Team';
            $game['tournament_name'] =$game['awayteam_name'] ." at ".$game['hometeam_name'];
            $game['awayteam_totalscore'] = $game['awayteam_q1'] + $game['awayteam_q2'] + $game['awayteam_q3'] + $game['awayteam_q4'] + $game['awayteam_ot'];
            $game['hometeam_totalscore'] = $game['hometeam_q1'] + $game['hometeam_q2'] + $game['hometeam_q3'] + $game['hometeam_q4'] + $game['hometeam_ot'];
            $game['time'] = $date;
            $game['start'] = $date;
            $game['gmt'] = $date;
            if($this->_gameService->create($game)){
                return redirect('/admin/game/football');
            } else {
                return back()->withInput();
            }

        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(Game $game)
        {

            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit(Game $game)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Game $game, Request $request)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(Game $game)
        {
            //
        }
    }
