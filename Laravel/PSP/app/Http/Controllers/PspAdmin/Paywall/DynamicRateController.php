<?php

namespace App\Http\Controllers\PspAdmin\Paywall;

use App\Http\Controllers\Controller;
use App\Http\Requests\PspAdmin\CreateDynamicRateRequest;
use App\Models\DynamicRate;
use App\Models\PoolType;
use App\Models\Rate;
use App\Providers\Paywall\Invoice\DynamicRateProvider;
use Illuminate\Http\Request;

class DynamicRateController extends Controller
{
    /**
     * @param Rate $rate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private $_dynamicRateProvider;


    public function index(Rate $rate)
    {
        $dynamicRates = $rate->dynamic_rates;
        return view('pspadmin.paywall.rate.dynamic-rate.index', compact('rate', 'dynamicRates'));
    }

    /**
     *
     */
    public function create(Rate $rate)
    {
        return view('pspadmin.paywall.rate.dynamic-rate.create', compact('rate'));
    }

    /**
     * @param Rate $rate
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Rate $rate, CreateDynamicRateRequest  $request)
    {
        $rateData = $request->except('_token');
        $this->_dynamicRateProvider = resolve(DynamicRateProvider::class);
        $this->_dynamicRateProvider->create($rateData);
        $dynamicRates = $rate->dynamic_rates; 

        return redirect()->route('rate.dynamicRate.index', ['rate' => $rate, 'dynamicRates' => $dynamicRates]);
    }

    /**
     * @param Rate $rate
     * @param DynamicRate $dynamicRate
     */
    public function show(Rate $rate, DynamicRate $dynamicRate)
    {
        //
    }

    /**
     * @param Rate $rate
     * @param DynamicRate $dynamicRate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Rate $rate, DynamicRate $dynamicRate)
    {
        return view('pspadmin.paywall.rate.dynamic-rate.edit', compact('dynamicRate', 'rate'));
    }

    /**
     * @param Rate $rate
     * @param DynamicRate $dynamicRate
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Rate $rate, DynamicRate $dynamicRate, Request $request)
    {
        $rateData = $request->except('_token');
        dd($rateData);
        $this->_dynamicRateProvider = resolve(DynamicRateProvider::class);
        $this->_dynamicRateProvider->update($dynamicRate->id, $rateData);
        return redirect()->route('rate.dynamicRate.index', ['rate' => $rate]);
    }

    /**
     * @param Rate $rate
     * @param DynamicRate $dynamicRate
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Rate $rate, DynamicRate $dynamicRate)
    {
        $dynamicRate->delete();
        return redirect()->route('rate.dynamicRate.index', ['rate' => $rate, 'dynamicRate' => $dynamicRate]);
    }
}
