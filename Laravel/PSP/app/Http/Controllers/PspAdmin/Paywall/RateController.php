<?php

namespace App\Http\Controllers\PspAdmin\Paywall;
use App\Http\Controllers\Controller;
use App\Models\PoolType;
use App\Models\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poolTypes = PoolType::get();
        $rates = \App\Models\Rate::with(['pool_type'])->get();
        return view('pspadmin.paywall.rate.index',compact('poolTypes','rates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $poolTypes = PoolType::get();
        return view('pspadmin.paywall.rate.create',compact('poolTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rateData = $request->except('_token');
        Rate::create($rateData);
        return redirect()->route('rate.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function edit(Rate $rate)
    {
        $poolTypes = PoolType::get();
        return view('pspadmin.paywall.rate.edit',compact('poolTypes', 'rate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        $rateData = $request->except('_token', '_method');
        $rate->update($rateData);
        return redirect()->route('rate.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        //
    }
}
