<?php

namespace App\Http\Controllers\PspAdmin\Paywall;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Invoice;
use App\Providers\Paywall\Invoice\DynamicRateProvider;
use Illuminate\Http\Request;
use net\authorize\api\contract\v1 as AnetAPI;

class InvoiceController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $merchAuth;

    public function __construct() {

        $this->merchAuth = new AnetAPI\MerchantAuthenticationType();
        $this->merchAuth->setName(config('authorizenet.login'));
        $this->merchAuth->setTransactionKey(config('authorizenet.tx_key'));
    }

    public function index()
    {
        $invoices = Invoice::with(['discounts', 'payment', 'rate','pool','pool.pool_type'])->orderBy('created_at','desc')->paginate(50);
        return view('pspadmin.paywall.invoice.index',compact('invoices'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        $invoice->load(['pool','pool.golf','pool.golf.entries', 'rate','rate.dynamic_rates']);
        $discounts = Discount::get();
        $pool = $invoice->pool;
        $dynamicRatesProvider = resolve(DynamicRateProvider::class);
        $dynamicRates = ($invoice->rate && $invoice->dynamic_rates) ? $dynamicRatesProvider->get_applicable_rates($invoice->pool, $invoice->rate->dynamic_rates) : collect([]);

        return view('pspadmin.paywall.invoice.edit', compact('invoice', 'discounts', 'dynamicRates', 'pool'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice) {
        $data = $request->except('_token', '_method');
        $invoice->fill($data);
        $messages[] = [
            'type' => 'success',
            'message' => "Invoice was updated",
        ];
        session()->flash('messages', $messages);
        return redirect()->route('invoice.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        $invoice->delete();
        $messages[] = [
            'type' => 'success',
            'message' => "Invoice was deleted",
        ];
        session()->flash('messages', $messages);
        return redirect()->route('invoice.index');
    }
}
