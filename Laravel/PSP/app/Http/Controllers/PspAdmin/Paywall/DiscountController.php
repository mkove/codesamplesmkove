<?php

namespace App\Http\Controllers\PspAdmin\Paywall;
use App\Http\Controllers\Controller;
//use App\Http\Requests\PspAdmin\CreateDiscountRequest;
//use App\Http\Requests\PspAdmin\UpdateDiscountRequest;
use App\Http\Requests\PspAdmin\CreateDiscountRequest;
use App\Http\Requests\PspAdmin\UpdateDiscountRequest;
use App\Models\Discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts = Discount::with(['user'])->get();
        return view('pspadmin.paywall.discount.index',compact('discounts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pspadmin.paywall.discount.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDiscountRequest $request)
    {
        $data = $request->except('_token','_method');
        if($data['start_date']){
            $data['start_date'] = date('Y-m-d', strtotime($data['start_date']));
        } else {
            $data['start_date'] = date('Y-m-d H:i:s');
        }
        if($data['end_date']){
            $data['end_date'] = date('Y-m-d', strtotime($data['end_date']));
        } else {
            $data['end_date'] = date('Y-m-d', strtotime("+1 year"));
        }
        Discount::create($data);
        $messages[] = [
            'type'  => 'success',
            'message'   => "Discount was added",
        ];
        session()->flash('messages', $messages);
        return redirect()->route('discount.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function show(Discount $discount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        return view('pspadmin.paywall.discount.edit', compact('discount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDiscountRequest $request, Discount $discount)
    {
        $data = $request->except('_token', '_method');
        if($data['start_date']){
            $data['start_date'] = date('Y-m-d H:i:s', strtotime($data['start_date']));
        } else {
            $data['start_date'] = date('Y-m-d H:i:s');
        }
        if($data['end_date']){
            $data['end_date'] = date('Y-m-d H:i:s', strtotime($data['end_date']));
        } else {
            $data['end_date'] = date('Y-m-d H:i:s', strtotime("+1 year"));
        }
        Discount::where('id', $discount->id)->update($data);

        $messages[] = [
            'type'  => 'success',
            'message'   => "Discount was updated",
        ];
        session()->flash('messages', $messages);
        return redirect()->route('discount.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount $discount)
    {
        if($discount->used > 0){
            $messages[] = [
                'type'  => 'warning',
                'message'   => "Cannot delete discount that's been used. Maybe disable it?",
            ];
        } else {
            $discount->delete();
            $messages[] = [
                'type'  => 'success',
                'message'   => "Discount was deleted",
            ];
        }

        session()->flash('messages', $messages);
        return redirect()->route('discount.index');
        //
    }
}
