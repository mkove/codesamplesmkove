<?php
    /**
     * Created by PhpStorm.
     * User: mkova
     * Date: 7/22/2018
     * Time: 1:10 PM
     */
    namespace App\Http\Controllers\PspAdmin;
    use App\Http\Controllers\Controller;
    use App\Models\EmailInvite;
    use App\Models\Game;
    use App\Models\GolfTournament;
    use App\Models\Invoice;
    use App\Models\Payment;
    use App\Models\Pool;

    use App\Models\Schwindy;
    use App\Models\SquareBoard;
    use App\Models\SquareWin;
    use App\Models\User;
    use Illuminate\Http\Request;
    use App\Providers\Pool\Pool as PoolProvider;
    use App\Providers\Game\Game as GameProvider;
    use App\Providers\User\User as UserProvider;

    class DashboardController  extends  Controller{


        private $_poolProvider;
        private $_userProvider;
        private $_gameProvider;

        public function __construct(
            PoolProvider $poolProvider,
            GameProvider $gameProvider,
            UserProvider $userProvider
        ) {
            $this->_gameProvider = $gameProvider;
            $this->_userProvider = $userProvider;
            $this->_poolProvider = $poolProvider;

        }

        public function index()
        {
            $tourneys = GolfTournament::where('active',1)->with('golfs')->get();
			$games = Game::where('status','<>','Not Started')
				->where('status','<>', 'Final')
				->where('status','<>', 'Postponed')
				->where('status','<>', 'Canceled')
			             ->where('status','<>','After Over Time')
			             ->with('squareboards')->get();
			
            $counts = [
                'pools' => Pool::where('status', 'active')->count(),
                'users' => User::where('verified', 1)->count(),
                'invoiced' => Invoice::sum('total'),
	            'sales' => Payment::sum('amount')
            ];
			$today = date('Y-m-d H:i:s');
			$counts30 = [
				'pools' => Pool::where('created_at','>', date('Y-m-d H:i:s', strtotime($today." - 30 days")))->where('status', 'active')->count(),
				'users' => User::where('created','>', date('Y-m-d H:i:s', strtotime($today." - 30 days")))->where('verified', 1)->count(),
				'invoiced' => Invoice::where('created_at','>', date('Y-m-d H:i:s', strtotime($today." - 30 days")))->sum('total'),
				'sales' => Payment::where('created_at','>', date('Y-m-d H:i:s', strtotime($today." - 30 days")))->sum('amount')
			];
            $labels = []; // array_keys($chartData);
            $data = []; // array_values($chartData);
            return view('pspadmin.dashboard.index',['pools' => [], 'counts' => $counts, 'count30' => $counts30, 'labels' => $labels, 'data' => $data, 'tourneys' => $tourneys, 'games' => $games]);
        }

        private function _chart_data($items){
            $data = [];
            $returnData = [];
            foreach($items as $item){
                $date = date('M, Y', strtotime($item->created));
                $data[$date][] = $item;
            }
            foreach($data as $date => $pools){
                $returnData[$date] = count($pools);
            }
            return $returnData;
        }

        public function search(Request $request){
            $data = [];
            $term = $request->get('term');
            $take = $request->ajax() ? 5 : null;
            $results = [
                'pools' => $this->_poolProvider->search($term, 200),
                'users' => $this->_userProvider->search($term, $take),
                'games' => $this->_gameProvider->search($term, $take)
            ];
            foreach($results['pools'] as $pool){
                $data[] = [
                    'id' => $pool->id,
                    'icon' =>  ($pool->pool_type->template = 'squareboard') ? 'table' : 'columns',
                    'type' => ucwords($pool->pool_type->name),
                    'label' => strtoupper($pool->pool_type->name).": ".$pool->pool_name,
                    'name' => $pool->pool_name,
                    'value' => route('pool.edit',['pool' => $pool->id]),
                    'description' => "Commissioners: ".$this->_commissioner_string($pool)
                ];
            }
            foreach($results['users'] as $user){
                $data[] = [
                    'id' => $user->id,
                    'icon' => 'user',
                    'type' => "User",
                    'label' => $user->global_display_name." ({$user->email})",
                    'name' => $user->email,
                    'value' => route('user.edit',['user' => $user->id]),
                    'description' => $user->username,
                ];
            }
            foreach($results['games'] as $game){
                $prettyDate = date('D, M j, Y G:i A', strtotime($game->start));
                $data[] = [
                    'id' => $game->id,
                    'icon' => $game->sport.'-ball',
                    'type' => $game->league,
                    'label' =>  "GAME: ".$game->tournament_name." [".$prettyDate."] - ".strtoupper($game->status),
                    'name' => $game->tournament_name,
                    'value' =>   route('game.edit',['game' => $game->id]),
                    'description' => $prettyDate." - ".strtoupper($game->status),
                ];
            }
            if($request->ajax()) {
                return response()->json($data);
            }
            return view('pspadmin.dashboard.search',$results);
        }

        private function __commissioner_string(&$pool, $links = true){
            $string = [];
            foreach($pool->commissioners as $com){
                $string[] = "<a href='".route('user.edit', ['user' => $com->id])."'>".$com->global_display_name."</a>";
            }
            return implode(', ', $string);
        }
    }
