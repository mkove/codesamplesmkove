<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;

class HookController extends Controller
{
    public function mailerlite_sync(Request $request)
    {
        $groupId = __conf('system.mailerlite_group_id', 'text', "107189296");

        $event = isset($request->event) ? $request->event : '';
        if ($event) {
            $event_type = $event->type;
            $event_data = $event->data;
            // new subscriber is added to an account 
            if ($event_type === 'subscriber.create') {
            } // subscriber is added to a group
            else if ($event_type === 'subscriber.add_to_group') {
                $email = $event_data->subscriber->email;
                $user = User::where('email', $email)->first();
                if ($user) {
                    $userData = [
                        'transactional_only' => false
                    ];
                    $user->update($userData);
                }
            } else if ($event_type === 'subscriber.unsubscribe') {
                $email = $event_data->subscriber->email;
                $hook_groupId = $event_data->group->id;
                if ($groupId === $hook_groupId) {
                    $user = User::where('email', $email)->first();
                    if ($user) {
                        $userData = [
                            'transactional_only' => true
                        ];
                        $user->update($userData);
                    }
                }
            } else if ($event_type === 'subscriber.remove_from_group') {
                $email = $event_data->subscriber->email;
                $hook_groupId = $event_data->group->id;
                if ($groupId === $hook_groupId) {
                    $user = User::where('email', $email)->first();
                    if ($user) {
                        $userData = [
                            'transactional_only' => true
                        ];
                        $user->update($userData);
                    }
                }
            }
        }
    }

    public function generateSignature($jsonPayload)
    {
        $apiKey = __conf('system.mailerlite_api_key', 'text', "2bb89e969580cc1b1d52afc5f1801a74");
        return base64_encode(
            hash_hmac('sha256', $jsonPayload, $apiKey, true)
        );
    }
}
