<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class CaptchaServiceController extends Controller
{
    public function index()
    {
        return view('captha_test');
    }

    public function capthcaFormValidate(Request $request)
    {
        $request->validate([ 
            'captcha' => 'required|captcha'
        ]);
        echo "Captcha success";
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

}
