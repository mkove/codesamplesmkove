<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\SiteHelper;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Providers\Email\EmailContentProvider as EmailContentProvider;
use App\Providers\User\User as UserProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
//use Spatie\GoogleTagManager\GoogleTagManagerFacade;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/pools';
    private $_emailContentProvider;
    private $_userProvider;
    private $_poolProvider;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        \App\Providers\Pool\Pool $poolProvider,
        EmailContentProvider  $emailContentProvider,
        UserProvider $userProvider
    )
    {
        $this->_poolProvider = $poolProvider;
        $this->_emailContentProvider = $emailContentProvider;
        $this->_userProvider = $userProvider;
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'email'    => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:12|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
        if(SiteHelper::conf('site.captcha.enabled','boolean', 0)){
            $rules['h-captcha-response'] = 'required|captcha';
        }
        return Validator::make($data, $rules);
    }

	protected function register(Request $request)
	{
		/** @var User $user */

        $rules = [
            'email'    => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:12|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
        // if(SiteHelper::conf('site.captcha.enabled','boolean', 0)){
        //     $rules['h-captcha-response'] = 'required|captcha';
        // }

        // math captcha
        if (SiteHelper::conf('site.captcha.enabled', 'boolean', 0)) {
            $captcha_result = null;
            $captcha_result = $request->validate([
                'captcha' => 'required|captcha'
            ]);
            if (!$captcha_result)
                return;
        }
        // math captcha
		$gtmData  = ['verify' => null ];
		$validatedData = $request->validate($rules);
		try {
			$dataLayer = null;
            $autoVerify = SiteHelper::conf('site.auto_verify_registration','boolean','1');
            if(!$autoVerify){
                $skipVerify = (session()->get('skipVerify')) ?? false;
            } else {
	            $gtmData['verify'] = 1;
                $skipVerify = true;
            }
            $phone = $request->get('phone');
            $phone = ($phone) ? preg_replace("/[^0-9]/", "", $phone ) : "";
            $parts = explode('@', $validatedData['email']);
			$validatedData['password']        = bcrypt($validatedData['password']);
            $validatedData['global_display_name'] = isset($parts[0]) ? $parts[0] : '';
			$validatedData['name']  = "";
			$validatedData['first']  = $request->get('first_name');
			$validatedData['last']  = $request->get('last_name');
			$validatedData['phone']  = $phone;
			$validatedData['is_admin']  = 'no';
            $validatedData['status']  = 'active';
            $validatedData['verified']  = $skipVerify;
            $validatedData['verify_hash'] = md5($validatedData['email']."-".date('U'));
			$validatedData['timezone'] = 'America/New_York';
            $validatedData['tfa_code'] = $this->_userProvider->generate_tfa();
            $validatedData['verify_expire_time'] = Carbon::now();
            $validatedData['rate_limit'] = __conf('system.default_user_email_rate_limit', 'text', '250');

            if($this->_userProvider->user_exists($validatedData['email'], $validatedData['username'])){
                return redirect()->back()->with('message', 'User with this email or username already exist.');
            }

            $user = $this->_userProvider->create($validatedData);
			
			
			__gtm_push('register', ['new' => 1]);
			
//            GoogleTagManagerFacade::flash('user_id', $user->id);

            auth()->loginUsingId($user->id);
            if($user->verified){
				__gtm_push('verified', ['verified' => 1]);
	        
                session(['convert_user_id' => $user->id]);
                if($autoVerify){
                    SiteHelper::add_config_notice('user.notifications.verification.you_were_verified', "Successfully created a new account.");
                    $data = [
                        'user' => $user,
                        'current_user' => $user,
                    ];
                    $this->_emailContentProvider->process_email('users_verified', $user->email, $data);
                }
                $this->_userProvider->redirect_user_on_sign_in();
            }
            session(['unverified_user_id' => $user->id]);
            $data = [
                'user' => $user,
                'current_user' => $user,
            ];
            $this->_emailContentProvider->process_email('users_verify', $user->email, $data);

            return redirect()->route('psp.user.unverified');
		} catch (\Exception $exception) {
            \Log::error($exception->getTraceAsString());
			return redirect()->back()->with('message', 'Unable to create new user.');
		}
        return redirect()->route('psp.user.unverified'); //send all new users here

	}

	private function _verify_captcha(){

    }
}
