<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Models\User;
// use Mail; 
use Hash;
use Illuminate\Support\Str;
use App\Helpers\SiteHelper;
use App\Mail\SendResetEmail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    // use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $meta = [
            'title' => __c('website.seo.password-reset.title', ''),
            'description' => __c('website.seo.password-reset.description', ''),
            'og_title' => __c('website.seo.password-reset.og-title', ''),
            'og_description' => __c('website.seo.password-reset.og-description', ''),
            'keywords' => __c('website.seo.password-reset.keywords', ''),
            'robots' => __c('website.seo.password-reset.robots', "index,follow")
        ];
        return view('auth.passwords.email', ['meta' => $meta]);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function submitForgetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        $sendEmail = new SendResetEmail('', 'Reset Password', ['token' => $token]);
        if ($sendEmail->send_reset_email($request->email)) {
            $message = 'We have e-mailed your password reset link!';
        } else {
            $message = 'Please check your email address';
        }

        return view('auth.passwords.email', ['message' => $message]);
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function showResetPasswordForm($token)
    {
        return view('auth.passwords.reset', ['token' => $token]);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function submitResetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $updatePassword = DB::table('password_resets')
            ->where([
                'email' => $request->email,
                'token' => $request->token
            ])
            ->first();

        if (!$updatePassword) {
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $user = User::where('email', $request->email)
            ->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email' => $request->email])->delete();

        $messages[] = [
            'type'  => 'success',
            'message'   => "Your password has been changed!",
        ];
        session()->flash('messages', $messages);

        return redirect('/login');
    }
}
