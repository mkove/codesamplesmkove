<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\SiteHelper;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/pool';
    public $crumbs;
    private $_poolProvider;
    private $_userProvider;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        \App\Providers\Pool\Pool $poolProvider,
        \App\Providers\User\User $userProvider
    )
    {
        $this->_poolProvider = $poolProvider;
        $this->_userProvider = $userProvider;
        $this->middleware('guest')->except(['logout']);
    }

    public function logout()
    {
        auth()->logout();
	    return redirect('/login');// send back all errors to the login form;
    }

    public function show_login()
    {
        $meta = [
            'title' => __c('website.seo.login.title', ''),
            'description' => __c('website.seo.login.description', ''),
            'og_title' => __c('website.seo.login.og-title', ''),
            'og_description' => __c('website.seo.login.og-description', ''),
            'keywords' => __c('website.seo.login.keywords', ''),
            'robots' => __c('website.seo.login.robots', "index,follow")
        ];
        return view('pspfrontend.login', ['crumbs' => null, 'meta' => $meta]);
    }

    public function show_signup()
    {
        $meta = [
            'title' => __c('website.seo.signup.title', ''),
            'description' => __c('website.seo.signup.description', ''),
            'og_title' => __c('website.seo.signup.og-title', ''),
            'og_description' => __c('website.seo.signup.og-description', ''),
            'keywords' => __c('website.seo.signup.keywords', ''),
            'robots' => __c('website.seo.signup.robots', "index,follow")
        ];
        return view('pspfrontend.signup', ['crumbs' => [], 'meta' => $meta]);
    }


    public function do_login(Request $request) {
        $login = request()->input('login');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $userdata = array(
            $fieldType => $request->get('login'),
            'password' => $request->get('password')
        );
	    $rules = array(
            'login' => 'required', // make sure the email is an actual email
		    'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
	    );

        // // math captcha
        // if (SiteHelper::conf('site.captcha.enabled', 'boolean', 0)) {
        //     $captcha_result = null;
        //     $captcha_result = $request->validate([
        //         'captcha' => 'required|captcha'
        //     ]);
        //     if (!$captcha_result)
        //         return;
        // }
        // // math captcha

	    // run the validation rules on the inputs from the form
	    $validator = \Validator::make($request->all(), $rules);

	    // if the validator fails, redirect back to the form
	    if ($validator->fails()) {
		    return redirect('/login')
			    ->withErrors($validator)// send back all errors to the login form
			    ->withInput($request->except('password')); // send back the input (not the password) so that we can repopulate the form
	    } else {
		    // attempt to do the login
		    if ($ret = \Auth::attempt($userdata)) {
                $autoVerify = session()->get('skipVerify');
                return $this->_userProvider->redirect_user_on_sign_in();

		    } else {
			    $validator->errors()->add('nologin', 'Wrong username or password!');
			    // validation not successful, send back to form
			    return redirect()->back()->withErrors($validator)->withInput($request->except('password'));// send back all errors to the login form;

		    }
	    }
    }
}
