<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 7/27/2018
 * Time: 10:04 PM
 */

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Providers\Email\EmailContentProvider as EmailContentProvider;
use App\Providers\User\User as UserProvider;

class RegisterController extends Controller
{

	private $_emailContentProvider;
	private $_userProvider;

	public function __construct(EmailContentProvider  $emailContentProvider, UserProvider $userProvider)
	{
		$this->_emailContentProvider = $emailContentProvider;
		$this->_userProvider = $userProvider;
	}

	public function index()
	{
		//register form
	}

	public function join($code)
	{
		session(['inviteCode' => $code]);
		return redirect()->route('psp.user.pools');
	}

	public function unverified()
	{
		$user = \Auth::user();
		if ($user) {
			if (!$user->verified) {
				return view('pspfrontend.user.unverified', ['user' => $user]);
			}
			return redirect()->route('psp.user.pools');
		}
		return redirect('/');
	}

	public function process_registration(Request $request)
	{
	}

	public function login_register(Request $request)
	{
		$poolId = $request->pool_id;
		$email = strtolower($request->email);
		$pool = \App\Models\Pool::where('id', $poolId)->with('users')->first();
		if (!$pool) {
			//TODO: you did not pass proper pool id
			return redirect("Location: /login");
		}
		if ($loggedInUser = $_SESSION['profile']) {
			//if user is logged in, let them join
			return redirect('/pool/' . $pool->id . "/join/" . $loggedInUser['id']);
		}

		$invite = \App\Models\EmailInvite::where('email', $email)->where('pool_id', $poolId)->first();
		//no loggin form for logged in users

		$user = $this->_process_login_register($request);
		if ($user) {
			if ($pool->can_join($user['email'])) {
				try {
					//TODO: notify that joined and redirect with message
					$pool->users()->attach($user['id']);
					$invite->accepted = 'yes';
					$invite->save();
					return redirect('/pool/' . $pool->id); // go play now
				} catch (\Exception $e) {
					\Log::error(":" . $e->getMessage());
					return redirect('/dashboard');
				}
			} else {
				//TODO: notify logged in user cannot joined.
				return redirect('/dashboard'); // go play now
			}
		} else {
			//TODO: flash that user cannot join this pool and needs a password
			return redirect('/pool/' . $pool->id . '/join/' . $user['id']); // go play now

		}
	}

	public function login()
	{
	}

	public function process_login(Request $request)
	{
	}

	public function join_invite($poolId, Request $request)
	{
	}

	private function _process_login_register(Request $request)
	{
		$user = \Auth::user();
		if ($user) {
			\Auth::logout();
			$user = null;
		}

		$password = $request->password;
		$password2 = $request->password1;
		$email = strtolower($request->email);
		$pass = md5($password);

		if (\Auth::attempt(['email' => $email, 'password' => $password, 'status' => 'active'])) {
			$user = \Auth::user();
			if (!$user->verified) {
				return redirect()->route('psp.unverified');
			}
		} else {
			return redirect('/login');
		}

		try {
			//			$user = \App\Models\User::firstOrNew(['email' => $email, 'status' => 'active']);
			if ($user->exists && $user->password == $pass) {
				$_SESSION['profile'] = $user->toArray();
			} elseif ($user->exists) {
				//bad password
				$user = null;
				unset($_SESSION['profile']);
				//todo: flash session that it's bad login
			} else {
				$createData = [
					'email' => strtolower(trim($email)),
					'password' => md5($password),
					'is_admin' => 'no',
					'verified' => false,
					'rate_limit' => __conf('system.default_user_email_rate_limit', 'text', '250'),
					'verify_hash' => md5($user->email . "-" . date('U')),
				];
				$user = $this->_userProvider->create($createData);
				$data = [
					'user' => $user,
					'current_user' => $user,
					'pool' => null,
				];
				$this->_emailContentProvider->process_email('users_verify', $user->email, $data);

				//				$_SESSION['profile'] = $user->toArray();
				//todo: send user welcome email
			}
		} catch (\Exception $e) {
			\Log::error(":" . $e->getMessage());
		}

		return $user;
	}
}
