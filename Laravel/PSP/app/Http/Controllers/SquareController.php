<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 7/22/2018
 * Time: 1:09 PM
 */

namespace App\Http\Controllers;

use App\Models\Square;
use App\Models\SquareWin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SquareController  extends Controller
{

    private $_user;
    private $_squareBoard;

    public function toggle($poolId, $squareBoardId, Request $request)
    {
        //Get square
        $squareId = $request->get('square_id');
        $userId = $request->get('user_id');
        $user = User::find($userId);
        $op = $request->get('op');
        $currentUser = Auth::user();
        $square = Square::with(
            [
                'user',
                'squareboard',
                'squareboard.pool',
                'squareboard.squares',
                'user.squares' => function ($query) {
                    $query->where('squares.squareboard_id', 'squareboard.id');
                }
            ]
        )->find($squareId);
        if (!$square) {
            $resp = ['type' => 'danger', 'error' => 1, 'message' => "Please pick square first!", 'square' => null, 'squareboard' => null, 'remaining' => null];
            return \Response::json($resp);
        }

        try {
            $squareBoard = $square->squareboard;
            $pool = $squareBoard->pool;
            $userSquareCount = $squareBoard->squares->where('user_id', $userId)->count();
            $userAllowed =  ($squareBoard->max_per_user - $userSquareCount);

            if ($op == 'add') {
                if ($square->user) {
                    if (!$square) {
                        $resp = ['type' => 'danger', 'error' => 1, 'message' => "Square is taken!", 'square' => null, 'squareboard' => null, 'remaining' => null];
                        return \Response::json($resp);
                    }
                } else {
                    if ($userAllowed > 0) {
                        //create square
                        $data = [
                            'custom_short_label' => $request->get('custom_short_label'),
                            'custom_label' => $request->get('custom_label'),
                            'comish' => $request->get('pick_users'),
                            'user_id' => ($userId !== $currentUser->id) ? $currentUser->id : $userId
                        ];
                        $this->_pick($square, $user, $data);
                        $userAllowed--;
                    } else {
                        $resp = ['type' => 'danger', 'error' => 1, 'message' => "No squares left!", 'square' => null, 'squareboard' => null, 'remaining' => null];
                        return \Response::json($resp);
                    }
                }
            } elseif ($op == 'remove') {
                //my square
                if ($this->_my_square($square, $currentUser)) {
                    $this->_unpick($square, $user);
                    $userAllowed++;
                } else {
                    if ($request->get('pick_users') && $pool->is_commissioner($currentUser->id)) {
                        $this->_unpick($square, $user);
                        $userAllowed++;
                    } else {
                        $resp = ['type' => 'danger', 'error' => 1, 'message' => "Only commissioner can do this!", 'square' => null, 'squareboard' => null, 'remaining' => null];
                        return \Response::json($resp);
                    }
                }
            }
            $square->refresh();
            $resp = ['type' => 'success', 'error' => 0, 'message' => "ok!", 'square' => $square, 'squareboard' => $squareBoard, 'squaresLeft' => $userAllowed];
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            $resp = ['type' => 'danger', 'error' => 1, 'message' => "failed!", 'square' => null, 'squareboard' => null, 'remaining' => null];
        }

        return \Response::json($resp);
    }

    private function _my_square(&$square, &$curUser)
    {
        return ($square->user_id == $curUser->id);
    }

    private function _pick(&$square, &$user, $data)
    {
        try {
            $square->user_id = $user->id;
            $square->label = $data['custom_label'];
            $square->short_label = $data['custom_short_label'];
            $square->populated = ($data['comish']) ? 'commissioner' : 'user';
            $square->populated_by_user_id = $data['user_id'];
            $square->save();
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }

    private function _unpick(&$square, &$user)
    {
        try {
            $square->user_id = null;
            $square->label = "";
            $square->short_label = "";
            $square->populated = 'user';
            $square->populated_by_user_id = $user->id;
            $square->save();
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }

    private function _toggle_square($user, $pool, $request)
    {
        $loggedInId = (auth()->user()) ? auth()->user()->id : null;
        $requestingUserId = $request->user_id;
        $op = $request->op;
        if (($requestingUserId == $loggedInId) || ($pool->is_commissioner($loggedInId))) { //
            if ($op == 'add') {
                $who = ($loggedInId == $requestingUserId) ? 'user' : 'commissioner';
                $returnData = $this->_add($request, $who);
            } elseif ($op == 'remove') {
                $returnData = $this->_remove($request);
            }
        } else { //toggling other id
            $returnData = ['type' => 'danger', 'error' => 1, 'message' => 'This is not allowed to perform this operation', 'square' => null, 'squareboard' => $pool->squareBoard];
        }
        return $returnData;
    }

    private function _add(Request $request, $who = null)
    {
        $loggedInId = (auth()->user()) ? auth()->user()->id : null;
        $pool = $this->_squareBoard->pool;
        $requestingUserId = $request->user_id;
        $user = \App\Models\User::with(['display_names'])->find($requestingUserId);
        $square = \App\Models\Square::find($request->get('square_id'));
        $label = $this->_get_display_name($request->get('label'), $user, false);
        $shortLabel = $this->_get_display_name($request->get('short_label'), $user, true);
        $squareId = $request->square_id;
        $square = \App\Models\Square::where('squareboard_id', $this->_squareBoard->id)->find($squareId);

        if ($square && $square->user_id == null) {
            try {
                $square->user_id = $requestingUserId;
                $square->label = $label;
                $square->short_label = $shortLabel;
                $square->populated = ($requestingUserId != $loggedInId) ? 'commissioner' : 'user';
                $square->populated_by_user_id = $pool->user_id;
                $square->save();
                return ['error' => 0, 'message' => "Updated!", 'type' => 'success',  'square' => $square,  'squareboard' => $pool->squareBoard];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return ['error' => 1, 'message' => "This square is occupied!", 'type' => 'warning',  'square' => $square, 'squareboard' => $pool->squareBoard];
    }

    private function _remove(Request $request)
    {
        $pool = $this->_squareBoard->pool;
        $square = \App\Models\Square::find($request->square_id);
        $pickUsers = $request->get('pick_users');
        $loggedInId = (auth()->user()) ? auth()->user()->id : null;
        if ($square && (($pool->is_commissioner($loggedInId) && $pickUsers) || $square->user_id == $loggedInId)) {
            try {
                $square->user_id = null;
                $square->label = '';
                $square->short_label = '';
                $square->save();
                return ['error' => 0, 'type' => 'info', 'message' => "User was removed!", 'square' => $square, 'squareboard' => $pool->squareBoard];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return ['error' => 1, 'type' => 'warning', 'message' => "Cannot unpick this square!", 'square' => $square, 'squareboard' => $pool->squareBoard];
    }

    private function _get_display_name($id = false, $user = false, $short = true)
    {

        $displayName = false;
        if ($id) {
            $displayName = \App\Models\UserDisplayName::where('user_id', $user->id)->find($id);
        } else {
            $displayName = \App\Models\UserDisplayName::where('user_id', $user->id)->where('primary', true)->first();
        }
        if ($displayName) {
            return ($short) ? $displayName->short_name : $displayName->display_name;
        } else {
            return ($short) ? substr($user->global_display_name, 0, 3) : $user->global_display_name;
        }
    }

    private function _load_user()
    {
        $this->_user = (auth()->user()) ? auth()->user() : null;
    }

    private function _custom_name_taken($label, $short)
    {
        return  \App\Models\Square::where(function ($query) use ($label, $short) {
            $short = strtolower(trim($short));
            $label = strtolower(trim($label));
            $query->where('label', $label)
                ->orWhere('short_label', $short)
                ->orWhere('label', $short)
                ->orWhere('short_label', $label);
        })
            ->where('squareboard_id', $this->_squareBoard->id)
            ->where('user_id', '<>', $this->_user->id)
            ->count();
    }
}
