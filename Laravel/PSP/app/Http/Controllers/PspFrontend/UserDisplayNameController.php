<?php

    namespace App\Http\Controllers\PspFrontEnd;

    use App\Helpers\SiteHelper;
    use App\Models\Game;
    use \App\Providers\Game\Game as GameProvider;
    use App\Models\UserDisplayName;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Providers\User\UserDisplayName as UserDisplayNameProvider;
    use App\Providers\Pool\Pool as PoolProvider;

    class UserDisplayNameController extends Controller
    {
        private $_messages;
        private $_hadError;
        private $_displayNameProvider;
        private $_poolProvider;
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */

        public function __construct(UserDisplayNameProvider $displayNameProvider, PoolProvider $poolProvider) {
            $this->_displayNameProvider = $displayNameProvider;
            $this->_poolProvider = $poolProvider;
        }

        public function index()
        {
            //
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create(Request $request)
        {


        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $user = \Auth::user();
            $displayName = $request->get('displayName');
            $save = $request->get('save');
            $poolId = isset($displayName['pool_id']) ? $displayName['pool_id'] : null;
            $userId = isset($displayName['user_id']) ? $displayName['user_id'] : null;
            $name = isset($displayName['name']) ? $displayName['name'] : null;
            $shortName = isset($displayName['short_name']) ? $displayName['short_name'] : null;
            if($user->id != $userId){
                $pool = $this->_poolProvider->get($poolId);
                if(!$pool){
                    return response()->json(['error' => true, 'message'=> "No pool"]);
                }
                if(!$pool->is_commissioner($user->id)){
                    return response()->json(['error' => true, 'message'=> "You cannot add display name for other user"]);
                }
            }

            $hasPrimary = $this->_displayNameProvider->has_primary($userId);

            $data = [
                'display_name' => $name,
                'short_name' => $shortName,
                'note' => "Created while picking",
                'user_id' => $userId,
                'pool_id' => ($save) ? null : $poolId,
                'primary' => (!$hasPrimary),
            ];
            $displayName = $this->_displayNameProvider->create($data);
            $displayName->selected = boolval($displayName->primary);
            $displayName->pool_only = ($displayName->pool_id);
            return response()->json(['error' => false, 'display_name' => $displayName]);

        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, UserDisplayName $userDisplayName)
        {
            $userDisplayName->load(['user']);
            $userDisplayNameData = $request->except('_method','_token', 'reloadable','reloadable_type');
            $fields = array_keys($userDisplayNameData);
            $this->_hadError = false;
            if(isset($userDisplayNameData['primary'])) {
                //update primary , relaod entire table
                UserDisplayName::where('user_id', $userDisplayName->user_id)
                    ->update(['primary' => 0]);
            }
            if($userDisplayName->update($userDisplayNameData)){
                $this->_messages[] = [
                    'type' => 'success',
                    'message' => "Updated <strong><em>{$userDisplayName->display_name}</em></strong>!<br> Fields: <strong>".ucwords(str_replace('_', ' ', implode(", ", $fields)))."</strong>",
                ];
                $userDisplayName->refresh();

            } else {
                $this->_hadError = true;
                $this->_messages[] = [
                    'type' => 'danger',
                    'message' => "Failed to update. Try again!",
                ];
            }

            if($request->ajax()) {
                return $this->_build_response($userDisplayName, $request);
            }


            SiteHelper::set_help($this->help_title, $this->help_content, 'edit-display-names', 'user');
            return view('pspadmin.user.display-name.edit',[
                'userDisplaName' => $userDisplayName,
                'help_title' => $this->help_title,
                'help_content' => $this->help_content,
            ]);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */

        public function search(Request $request){
            $data = [];
            $term = $request->get('term');
            $label = $request->get('label');
            $userId = $request->get('user_id');
            $userDisplayNames = UserDisplayName::where($label,'like', $term.'%')
                ->where('user_id',$userId)
                ->take(20)->get();
            foreach($userDisplayNames as $userDisplayName){
                $data[] = [
                    'id' => $userDisplayName->display_name,
                    'label' => $userDisplayName->display_name,
                    'value' => $userDisplayName->display_name
                ];
            }
            return response()->json($data);
        }

        public function destroy(UserDisplayName $userDisplayName, Request $request)
        {
            if($userDisplayName->delete()){
                $this->_messages[] = [
                    'type' => 'success',
                    'message' => "DELETED!",
                ];
            } else {
                $this->_hadError = true;
                $this->_messages[] = [
                    'type' => 'danger',
                    'message' => "Failed to update. Try again!",
                ];
            }
            if($request->ajax()) {
                return $this->_build_response($userDisplayName, $request);
            }
            return view('pspadmin.user.display-name.edit',['userDisplaName' => $userDisplayName]);
        }

        private function _build_response($userDisplayName, $request ){
            $reloadable = $request->get('reloadable');
            $reloadableType = $request->get('reloadable_type');

            if ($reloadableType == 'index') {
                $html = ($reloadable) ?? null;
                $userDisplayNames = UserDisplayName::where('user_id', $userDisplayName->user_id)->get();
                return response()->json([
                    'html' => ($reloadable) ? view($reloadable,['userDisplayNames' => $userDisplayNames])->render() : '',
                    'messages' => $this->_messages,
                    'error' => $this->_hadError
                ]);
            } elseif ($reloadableType == 'edit') {
                $html = ($reloadable) ?? null;
                return response()->json([
                    'html' => ($reloadable) ? view($reloadable,['userDisplayName' => $userDisplayName])->render() : '',
                    'messages' => $this->_messages,
                    'error' => $this->_hadError
                ]);
            } else {
                $html = ($reloadable) ?? null;
                return response()->json([
                    'html' => ($reloadable) ? view($reloadable,['userDisplayName' => $userDisplayName])->render() : '',
                    'messages' => $this->_messages,
                    'error' => $this->_hadError
                ]);
            }

        }
    }
