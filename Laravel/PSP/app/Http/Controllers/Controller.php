<?php
namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\SiteContent;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $content_variables;
    protected $config_variables;
    public $help_title;
    public $help_content;

    public function __construct()
    {
        $this->help_content = "Help Content";
        $this->help_title = "Help Title";
//        // Fetch the Site Settings object
//        $this->content_variables = [];
//        $tmp = SiteContent::get();
//        foreach($tmp as $var){
//            $this->content_variables[$var->identifier] = $var;
//        }
//        session(['content_variables' => $this->content_variables]);
//        $this->config_variables = [];
//        $tmp = Config::get();
//        foreach($tmp as $var){
//            $this->config_variables[$var->key] = $var;
//        }
//        session(['config_variables' => $this->config_variables]);


//        View::share('site_settings', $this->site_settings);
    }
}
