<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 7/22/2018
 * Time: 1:10 PM
 */

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;
use App\Helpers\Goalserve\College\Football as GS;



class GameController extends Controller
{
	public function index()
	{


		//            $nflScheduleFileOdds = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/fbs-shedule?date1={$this->_season['nfl']['start']}&date2={$this->_season['nfl']['end']}&showodds=1";
		$collegeFile = "http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/football/fbs-scores";
		$realGames = [];
		try {
			$realGames = GS::process($collegeFile, true);
		} catch (\Exception $e) {
			\Log::error(":" . $e->getMessage());
		}
		$localGames = \App\Models\Game::where('sport', 'football')->where('league', 'college')->get();
		foreach ($realGames as $game) {
			//                if(!$localGame = $localGames->where('contestID', $game['contestID'])->first()){
			//
			//                    try{
			//                        $game['sport'] = 'football';
			//                        $game['league'] = 'college';
			//                        $game['season'] = "Regular";
			//
			//                        $game['tournament_id'] = $game['contestID'];
			//                        $game['start'] = date('Y-m-d H:i:s', strtotime($game['start']));
			//                        $game['time'] = $game['start'];
			//                        $game['gmt'] = $game['start'];
			//                        $game['hometeam_handicap'] = 0;
			//                        $game['awayteam_handicap'] = 0;
			//                        $game['over_under'] = 0;
			//                        $localGame = \App\Models\Game::create($game);
			//                        dump($localGame);
			//                    } catch (\Exception $e)
			//                    {
			//                        dd($e->getMessage(), $e->getTrace(), $game);
			//                    }

			//                }
		}
		// dd($realGames);
	}

	public function show($id)
	{
	}

	public function create()
	{
	}

	public function edit($id)
	{
	}

	public function update($id, $request)
	{
	}

	public function store($request)
	{
	}

	public function delete($id)
	{
	}

	public function get_score($gameId = null)
	{
		$data = [];
		if ($gameId) {
			$game = \App\Models\Game::find($gameId)->first();
			if ($game) {
				$data[$game->id]['game'] = $game->toArray();
				$data[$game->od]['updated_at'] = strtotime($game->updated_at);
				$data[$game->id]['html'] = view('pspfrontend.pool.partials.live', ['game' => $game])->render();
			}
		} else {
			$games = \App\Models\Game::where('status', '<>', 'Final')->where('status', '<>', 'Not Started')->get();
			foreach ($games as $game) {
				$data[$game->id]['game'] = $game->toArray();
				$data[$game->od]['updated_at'] = strtotime($game->updated_at);
				$data[$game->id]['html'] = view('pspfrontend.pool.partials.live', ['game' => $game])->render();
			}
		}
		return response()
			->json([
				'error' => 0,
				'data'  => $data,
			]);
	}

	public function get_games($league, $sport)
	{
		$user = \Auth::user();
		//		    $games = Game::where('id',773689)->get();
		$games = \App\Models\Game::where('sport', $sport)
			->where('league', $league)
			->where('start', '>', date('Y-m-d H:i:s', strtotime('today midnight')));
		if ($league == 'professional' && $sport == 'football') {
			$games = $games->where('season_year', __conf('schwindy.season'));
		}
		//                ->where('awayteam_name','<>','TBD')
		//                ->where('hometeam_name','<>','TBD')
		//                ->where('hometeam_name', '<>','AFC afc')
		//                ->where('awayteam_name', '<>','AFC afc')
		//                ->where('awayteam_name', '<>','NFC NFC')
		//                ->where('awayteam_name', '<>','NFC NFC')
		$games = $games->orderBy('time', 'ASC')
			->get();
		$prettyGames = [];
		foreach ($games as $game) {
			$prettyGames[] = [
				'id' => $game->id,
				'name' => $game->tournament_name,
				'date' => $game->tz_start($user->timezone),
			];
		}
		return response()
			->json([
				'error' => 0,
				'data'  => [
					'games' => $prettyGames
				]
			]);
	}

	public function next_score($id)
	{
		$game = \App\Models\Game::find($id);
		if (!$game->is_final) {

			$curHome = $game->hometeam_totalscore;
			$curAway = $game->awayteam_totalscore;
			$scores = [
				'home_team' => [
					'actual' => $curHome,
					'possible' => []
				],
				'away_team' => [
					'actual' => $curAway,
					'possible' => []
				],
			];
			switch ($game->sport) {
				case 'football':
					$possible = [1, 3, 6, 7];
					foreach ($possible as $score) {
						$scores['home_team']['possible'][] = $curHome + $score;
						$scores['away_team']['possible'][] = $curAway + $score;
					}
					break;
				case 'basketball':
					$possible = [1, 2, 3];
					foreach ($possible as $score) {
						$scores['home_team']['possible'][] = $curHome + $score;
						$scores['away_team']['possible'][] = $curAway + $score;
					}
					break;
			}
			$error = 0;
			$message = null;
		} else {
			$scores = null;
			$error = 1;
			$message = "Game has finished!";
		}

		return response()
			->json([
				'error' => $error,
				'message' => $message,
				'data'  => [
					'scores' => $scores
				]
			]);
	}
}
