<?php
    namespace App\Http\Controllers\Api\Admin;

    use App\Http\Controllers\Controller;
    use App\Models\User;
    use Illuminate\Http\Request;

    class UserApiController extends Controller
    {
        /**
         * The user repository instance.
         */
        protected $users;

        /**
         * Create a new controller instance.
         *
         * @param  UserRepository  $users
         * @return void
         */
        public function __construct(User $users)
        {
            $this->users = $users->where('is_admin','yes')->get();
        }

        public function index(){
            return response()->json($this->users);
        }

        public function show(User $user){
            return response()->json($user);
        }

        public function edit(User $user, Request $request){
            return response()->json($user);
        }

        public function store(Request $request){
//            return response()->json($user);
        }

        public function update(User $user, Request $request){
//            return response()->json($usesr);
        }
    }
