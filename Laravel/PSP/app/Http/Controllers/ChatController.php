<?php
	/**
	 * Created by PhpStorm.
	 * User: mkova
	 * Date: 7/22/2018
	 * Time: 1:09 PM
	 */

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Models\Pool;
	use App\Models\Schwindy;
	use App\Models\SchwindyPick;
	use App\Providers\Chat\ChatService as Chat;
	use App\Events\ChatEvent as ChatEvent;
    use Illuminate\Support\Facades\Auth;

    class ChatController extends Controller {


        private $_poolServiceProvider;

        public function __construct(\App\Providers\Pool\Pool $psp) {
            $this->_poolServiceProvider = $psp;
        }

        public function get($id){

	        $user = Auth::user();
	        $userId = $user ? $user->id : null;
            $chat = Chat::get_by_user_id($id, $userId, 50);
            $poolUser = $this->_poolServiceProvider->get_pool_user($chat->pool_id, $user->id);
            $lastReadId = ($poolUser) ? $poolUser->pivot->last_chat_message_id  : null;
            $participants = [];
            if($chat->pool && $chat->pool->users){
                foreach($chat->pool->users as $pUser){
                    $participants[$pUser->id] = [
                        'display_name' => $pUser->pivot->display_name,
                        'last_message_id' => $pUser->pivot->last_chat_message_id
                    ];
                }
            }
            $chatData = [
                'id' => $chat->id,
                'me' => [
                    'id' => $user->id,
                    'display_name' => $participants[$userId]['display_name'],
                    'last_message_id' => $participants[$userId]['last_message_id'],
                ],
                'messages' => [],
                'collapse' => true,
            ];
            $unread = 0;
            $reversed =$chat->messages->reverse();
            foreach($reversed as $message){
                if($lastReadId < $message->id){
                    $unread++;
                }
                $chatData['messages'][] = [
                    'time' => date('M j, Y G:i A', strtotime($message->created_at)),
                    'last_read_id' => $lastReadId,
                    'id' => $message->id,
                    'user_id' => $message->user_id,
                    'display_name' => isset($participants[$message->user_id]['display_name']) ? $participants[$message->user_id]['display_name'] : "SYSTEM",
                    'message' => $message->message,
                    'is_commissioner' => $message->is_commissioner,
                    'visible' => $message->visible,
                    'read' => ($lastReadId >= $message->id),
                    'mine' => ($message->user_id == $userId)
                ];
            }
            $chatData['unread'] = $unread;
            $data = [
                'chat' => $chatData,
                'errors' => []
            ];
            return response()->json($data);
        }

		public function send($id, Request $request)
		{
		    $message = $request->get('message');
		    $user = auth()->user();
			$userId = ($user) ? $user->id : null;
			$chat = Chat::get_by_user_id($id, $userId);
            $msg = null;
            if($chat) {
                $pool = $chat->pool;
                $poolUser = $pool->users->where('id', $userId)->first();

                if($poolUser){
                    $displayName = ($poolUser->pivot->display_name) ?? $user->global_display_name;
                } else {
                    $displayName = $user->global_display_name;
                }
                $msg = Chat::send($chat, $userId, $message, $pool->is_commissioner($userId), null, $displayName);
                $msgdata = [
                    'time' => date('M j, Y G:i A', strtotime($msg->created_at)),
                    'display_name' => $displayName,
                    'id' => $msg->id,
                    'is_commissioner' => $poolUser->pivot->commissioner,
                    'message' => $msg->message,
                    'mine' => ($user->id == $msg->user_id),
                    'read' => false,
                    'user_id' => $msg->user_id,
                    'visible' => true,
                ];
                broadcast(new ChatEvent($chat->id, $msgdata));
                $this->_update_last_read($chat, $user,$msg->id);
                return response()->json(['message'=>$msgdata, 'error' => false]);
            }
            return response()->json(['message'=> [], 'error' => false]);

		}

		public function read($id){
        	try{
		        $user = auth()->user();
		        $userId = ($user) ? $user->id : null;
		        $chat = Chat::get_by_user_id($id, $userId);
		        $latestMessage = $chat->messages->first(); //because they are ordered in reverse...which is fuckin WERIRD!
		        $this->_update_last_read($chat, $user, $latestMessage->id);
	        } catch (\Exception $e){
        		\Log::error($e->getMessage());
	        }
            
            return response()->json(['unread' => 0, 'error' => false]);
        }

        private function _update_last_read(&$chat, &$user, $lastId = null){
            return Chat::update_last_read_id($chat, $user, $lastId);
        }
	}


