<?php
    namespace App\Http\Requests\PspAdmin;

    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Support\Facades\Log;

    class UpdateUserRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                "username" => "sometimes|required|unique:users",
                "email" => "sometimes|required|unique:users",

            ];
        }

        protected function prepareForValidation()
        {
            if ($this->has('phone')){
                $this->merge([
                    'phone' => preg_replace("/[^0-9]/", '', $this->phone)
                ]);
            }
//                $this->merge(['phone'=>'Mr '.$this->first_name]);
        }


    }
