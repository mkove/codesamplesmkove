<?php

    namespace App\Http\Requests;
    use Illuminate\Http\Request;
    use Illuminate\Validation\Rule;
    use Illuminate\Foundation\Http\FormRequest;

    class CreatePoolRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return \Auth::check();

        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules(Request $request)
        {
            $rules = [];
            return $rules + [
                "name" => "required",
                "type" => "required",
                "password" => "required",
            ];
        }

        public function messages()
        {
            return [
                'game_id.required' => "Game is REQUIRED for this pool type",
                'type.required' => "Type is REQUIRED",
                'password.required' => "Password must set"
            ];
        }
    }
