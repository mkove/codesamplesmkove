<?php
    namespace App\Exports;
    use App\Models\Pool;
    use App\Models\User;
    use Maatwebsite\Excel\Concerns\FromArray;

    class SquareboardRosterExport implements FromArray
    {

        protected $users;
        public function __construct(array $users) {
            $this->users = $users;
        }

        public function array(): array
        {
            return $this->users;
        }
    }
