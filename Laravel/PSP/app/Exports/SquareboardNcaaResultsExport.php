<?php
    namespace App\Exports;
    use App\Models\Pool;
    use App\Models\User;
    use Maatwebsite\Excel\Concerns\FromArray;

    class SquareboardNcaaResultsExport implements FromArray
    {

        protected $rows;
        public function __construct(array $rows) {
            $this->rows = $rows;
        }

        public function array(): array
        {
            return $this->rows;
        }
    }
