<?php

namespace App\Providers;

use App\Models\BillingPaymentProfile;
use App\Models\DynamicRate;
use App\Models\EmailContent;
use App\Models\EmailInvite;
use App\Models\GameEvent;
use App\Models\Golf;
use App\Models\GolfEntry;
use App\Models\GolfGroup;
use App\Models\GolfPlayer;
use App\Models\GolfPlayerNameVariation;
use App\Models\GolfTournament;
use App\Models\GolfTournamentPlayer;
use App\Models\GolfTournamentPrize;
use App\Models\PageRedirect;
use App\Models\Pool;
use App\Models\PoolType;
use App\Models\Schwindy;
use App\Models\SquareBoard;
use App\Models\StaticPage;
use App\Models\UserBilling;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $pspadminnamespace = '\App\Http\Controllers\PspAdmin';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        Route::model('schwindy', Schwindy::class);
        Route::model('pool', Pool::class);
        Route::model('gameEvent', GameEvent::class);
        Route::model('squareboard', SquareBoard::class);
        Route::model('emailInvite', EmailInvite::class);
        Route::model('pooltype', PoolType::class);
        Route::model('emailContent', EmailContent::class);
        Route::model('static-page', StaticPage::class);
        Route::model('staticPage', StaticPage::class);
        Route::model('dynamicRate', DynamicRate::class);
        Route::model('golf', Golf::class);
        Route::model('golfEntry', GolfEntry::class);
        Route::model('golfGroup', GolfGroup::class);
        Route::model('golfPlayer', GolfPlayer::class);
        Route::model('golfTournament', GolfTournament::class);
        Route::model('golfTournamentPlayer', GolfTournamentPlayer::class);
        Route::model('golfPlayerNameVariation', GolfPlayerNameVariation::class);
        Route::model('pageRedirect', PageRedirect::class);
        Route::model('billing-method', BillingPaymentProfile::class);
        Route::model('userBilling', UserBilling::class);
        Route::model('prize', GolfTournamentPrize::class);
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapFrontEndRoutes([ 'static','user', 'pool', 'golf','squareboard', 'schwindy']);
        $this->mapPspAdminRoutes();
        $this->mapWebRoutes();



        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapPspAdminRoutes()
    {
        Route::middleware('web')
            ->namespace($this->pspadminnamespace)
            ->group(base_path('routes/pspadmin.php'));
    }

    protected function mapFrontEndRoutes($routes = []){

        foreach($routes as $routeFile){
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/pspfrontend/'.$routeFile.'.php'));

        }
    }
}
