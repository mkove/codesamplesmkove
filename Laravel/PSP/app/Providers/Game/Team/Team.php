<?php
    /**
     * Created by PhpStorm.
     * User: mkova
     * Date: 10/12/2018
     * Time: 11:37 AM
     */

    namespace App\Providers\Game\Team;
    use App\Helpers\SiteHelper;
    use App\Repositories\TeamRepository;

    class Team {

        private $_repo;

        public function __construct(
            TeamRepository $repo
        ) {
            $this->_repo = $repo;
        }

        public function find($id) {
            return $this->_repo->find($id);
        }

        public function get($filters = [], $queryOnly = false){
            return $this->_repo->get($filters, $queryOnly);
        }

        public function find_by_provider_id($providerId){
            return $this->_repo->find_by_provider_id($providerId);
        }

        public function create($data){
            return $this->_repo->create($data);
        }

        public function uppdate(&$team, $data){
            return $this->_repo->update($team, $data);
        }

    }
