<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 11:37 AM
 */

namespace App\Providers\Game;

use App\Helpers\SiteHelper;
use App\Providers\GameEvent\GameEvent;

class Game
{

    private $_gameRepo;
    public function __construct(
        \App\Repositories\GameRepository $gameRepo,
        GameEvent $gameEventProvider
    ) {
        $this->_gameRepo = $gameRepo;
        $this->_gameEventProvider = $gameEventProvider;
    }

    public function find($gameId)
    {
        return $this->_gameRepo->find($gameId);
    }

    public function get_by_round_id($roundId = null, $sport = null, $league = null, $queryOnly = false)
    {
        return $this->_gameRepo->get_by_round_id($roundId, $sport, $league, $queryOnly);
    }

    public function get($filters = [], $queryOnly = false)
    {
        return $this->_gameRepo->get($filters, $queryOnly);
    }

    public function create_update($gameData)
    {
        $sport = $gameData['sport'];
        $league = $gameData['league'];
        $contestId = $gameData['tournament_id'];
        if ($game = $this->get_game_by_contest_id($contestId, $sport, $league)) {

            $this->update($game->id, $gameData);
        } else {
            $game = $this->create($gameData);
        }
        $game->refresh();
        return $game;
    }

    public function create($gameData)
    {
        return $this->_gameRepo->create($gameData);
    }

    public function play_event($gameId, $teamId = null, $type = 'TD', $quarter = 'q1', $clock = null)
    {

        //get Game

        try {
            $game = $this->_gameRepo->get(['id' => $gameId])->with(['home_team', 'away_team', 'events'])->first();
            $homeTeamId = $game->home_team->id;
            $awayTeamId = $game->away_team->id;
            $scores = [
                'hometeam' => $game->hometeam_[$quarter],
                'awayteam' => $game->awayteam_[$quarter]
            ];


            $data = [
                'game_id' => $game->id,
                'type' => $type,
                'time' => $clock,
                'clock' => $clock,
                'player_id' => '11111111',
                'pat_pending' => ($type == 'TD'), //ALWAYS PAT PENDING TD
                'event_id' => date('U'),
                'by' => ($homeTeamId == $teamId) ? 'hometeam' : 'awayteam',
                'team_id' => $teamId,
                'action' => "Player Name XX Yd " . ($type == 'TD') ? "Run (Player Name)" : "Field Goal",
                'quarter' => $quarter,
            ];
            $to = ($homeTeamId === $teamId) ? 'hometeam' : 'awayteam';
            $score = 0;
            switch ($type) {

                case 'TD':
                    $data['action'] = "Player Name XX Yd Run (Player Name)";
                    $score = 6;
                    break;
                case 'FG':
                    $data['action'] = "Player Name XX Yd Field Goal";
                    $score = 3;
                    break;
                case 'D2P':
                    $data['action'] = "DEFENSIVE TWO-POINT ATTEMPT. Player Name intercepted the try attempt. ATTEMPT SUCCEEDS.";
                    $score = 2;
                    break;
                case 'SF':
                    $data['action'] = "Player Name Safety";
                    $score = 2;
                    break;
                default:
                    $score = 0;
                    break;
            }
            $scores[$to] = +$score;
            $data['home_score'] = $scores['hometeam'];
            $data['away_score'] = $scores['awayteam'];

            $homeTeamQuarterScore = "hometeam_{$quarter}";
            $awayTeamQuarterScore = "awayteam_{$quarter}";

            $gameData = [
                'clock' => $clock,
            ];
            $gameData[$homeTeamQuarterScore] =  $scores['hometeam'];
            $gameData[$awayTeamQuarterScore] =  $scores['awayteam'];
            $gameData['hometeam_totalscore'] = $scores['hometeam'];
            $gameData['awayteam_totalscore'] = $scores['awayteam'];
            $this->_gameEventProvider->create($data);
        } catch (\Exception $e) {
            \Log::error("play_event : " . $e->getMessage());
        }
        return $this->_gameRepo->update($game->id, $gameData);
    }

    public function play_pat($gameId, $eventId, $score = 1)
    {
        $game = $this->_gameRepo->find($gameId);
    }

    public function get_game_by_contest_id($contestId, $sport, $league)
    {
        return $this->_gameRepo->get_game_by_contest_id($contestId, $sport, $league);
    }

    public function game_ended($game)
    {
        return $this->_gameRepo->game_ended($game);
    }

    public function game_started($game)
    {
        return $this->_gameRepo->game_started($game);
    }

    public function get_current_quarter($game)
    {
        if ($game) {
            if ($game->status == 'Not Started') {
                return 1;
            } elseif ($game->status == "Final" || $game->status == "After Over Time") {
                return ($game->status == "After Over Time") ? 5 : 4;
            } else {
                return $game->current_quarter;
            }
        }
        return 1;
    }

    public function quarter_score($game, $quarter)
    {
        return $this->_gameRepo->quarter_score($game, $quarter);
    }

    public function get_score($game, $quarter = 'finalscore', $cumulative = true)
    {
        return $this->_gameRepo->get_score($game, $quarter, $cumulative);
    }

    public function update($id, $data = [])
    {
        return $this->_gameRepo->update($id, $data);
    }

    public function get_current_round($sport, $league)
    {
        $rounds = [
            "Prequalifying Round",
            "1st Round",
            "2nd Round",
            "3rd Round",
            "4th Round",
            "Semi Final",
            "Final",
        ];
        foreach ($rounds as $round) {
            if ($this->season_games_played_count($sport, $league, $round) == 0) {
                return $round;
            }
        }
        return null;
    }

    public function get_last_game($sport = null, $league = null, $season = "1st Round")
    {
        return $this->_gameRepo->get_last_game($sport, $league, $season);
    }

    public function get_next_game($sport = null, $league = null, $season = "1st Round")
    {
        return $this->_gameRepo->get_next_game($sport, $league, $season);
    }

    public function active_round_ncaa()
    {
        $counts = [
            '0' => 4,
            '1' => 32,
            '2' => 16,
            '3' => 8,
            '4' => 4,
            '5' => 2,
            '6' => 1
        ];
        $year  = SiteHelper::conf('ncaa.season', 'text', '2019/2020');
        foreach ($counts as $roundId => $cnt) {
            $gameCount = $this->get([
                'sport' => 'basketball',
                'league' => 'college',
                'season_year' => $year,
                'round_id' => $roundId,
                'status' => 'Final',
            ], true)->count();
            if ($gameCount < $cnt) {
                return $roundId;
            }
        }
        return false;
    }

    public function season_games_played_count($sport = null, $league = null, $round = "1st Round")
    {
        return $this->_gameRepo->season_games_played_count($sport, $league, $round);
    }

    public function get_game_round($game)
    {
        return $game->round;
    }

    public function search($term, $take = null)
    {
        return $this->_gameRepo->search($term, $take);
    }

    public function week_played($week, $season, $seasonYear = '2019/2020', $sport = 'football', $league = 'professional')
    {
        return $this->_gameRepo->week_played($week, $season, $seasonYear, $sport, $league);
    }

    public function week_in_play($week, $season, $seasonYear = '2019/2020', $sport = 'football', $league = 'professional')
    {
        return $this->_gameRepo->week_in_play($week, $season, $seasonYear, $sport, $league);
    }

    public function week_not_started($week, $season, $seasonYear = '2019/2020', $sport = 'football', $league = 'professional')
    {
        return $this->_gameRepo->week_not_started($week, $season, $seasonYear, $sport, $league);
    }

    public function build_game_roster(&$games)
    {
        $roster = [];
        foreach ($games as $game) {
            $roster[$game->home_team_id] = $game->hometeam_totalscore;
            $roster[$game->away_team_id] = $game->awayteam_totalscore;
        }
        //put first small number on top
        asort($roster);
        return $roster;
    }

    /**
     * @return GameEvent
     */
    public function build_game(&$game)
    {
        try {
            $hometeam = $game->home_team;
            $awayteam = $game->away_team;
            $gamearr = [
                'id' => $game->id,
                'status' => $game->status,
                'timer' => $game->timer,
                'ball' => $game->ball,
                'pat_pending' => false,
                'awayteam' => [
                    'id' => $awayteam->id,
                    'name' => $awayteam->name,
                    'short_name' => $awayteam->short_name,
                    'q1' => $game->awayteam_q1,
                    'q2' => $game->awayteam_q2,
                    'q3' => $game->awayteam_q3,
                    'q4' => $game->awayteam_q4,
                    'ot' => $game->awayteam_ot,
                    'total' => $game->awayteam_totalscore,
                    'ball' => ($game->ball === 'awayteam')
                ],
                'hometeam' => [
                    'id' => $hometeam->id,
                    'name' => $hometeam->name,
                    'short_name' => $hometeam->short_name,
                    'q1' => $game->hometeam_q1,
                    'q2' => $game->hometeam_q2,
                    'q3' => $game->hometeam_q3,
                    'q4' => $game->hometeam_q4,
                    'ot' => $game->hometeam_ot,
                    'total' => $game->hometeam_totalscore,
                    'ball' => ($game->ball === 'hometeam')
                ]
            ];
        } catch (\Exception $e) {
            \Log::error("build_game : " . $e->getMessage());
        }
        return $gamearr;
    }
}
