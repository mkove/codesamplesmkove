<?php
    /**
     * Created by PhpStorm.
     * User: mkova
     * Date: 10/12/2018
     * Time: 9:13 AM
     */

    namespace App\Providers\EmailInvite;


    class EmailInvite {

        private $_emailInviteRepo;

        public function __construct(
            \App\Repositories\EmailInviteRepository $emailInviteRepo
        ) {
            $this->_emailInviteRepo = $emailInviteRepo;
        }

        public function find($id){
            return $this->_emailInviteRepo->find($id);
        }

        public function get($filters = [], $queryOnly = false){
            return $this->_emailInviteRepo->get($filters, $queryOnly);
        }

        public function create($data) {

            return $this->_emailInviteRepo->create($data);
        }

        public function update($id, $data) {
            return $this->_emailInviteRepo->update($id, $data);
        }

        public function generate_code(){
            return md5(date('U').env('APP_KEY','asdklfadsf3233dkadslfkjad:$@A').rand(1,10000));
        }

        public function is_invited($user, $poolId){
            return $this->_emailInviteRepo->is_invited($user, $poolId);
        }

        public function leave($poolId, $email){
            $entry = $this->_emailInviteRepo->get_by_email_pool_id($email, $poolId);
            if($entry){
                $entry->delete();
                return true;
            }
            return false;
        }

        public function get_by_email_pool_id($email, &$pool){
            return $this->_emailInviteRepo->get_by_email_pool_id($email,$pool->id);
        }

        public function invited_by_commissioner($emailInvite, $pool){
            $userId = $emailInvite->invited_by_user_id;
            if($this->pool->is_commissioner($userId)){
                return true;
            }
            return false;
        } 
    }
