<?php
    namespace App\Providers\Paywall\Invoice;
    use App\Repositories\PaymentRepository;

    class Payment {

        private $_paymentRepo;

        public function __construct(PaymentRepository $paymentRepo) {
            $this->_paymentRepo = $paymentRepo;
        }

        public function find($id){
            return $this->_paymentRepo->find($id);
        }

        public function get($filters = [], $queryOnly = false){
            return $this->_paymentRepo->get($filters, $queryOnly);
        }

        public function create($data){
            return $this->_paymentRepo->create($data);
        }

        public function update($id, $data){
            return $this->_paymentRepo->update($id, $data);
        }
    }
