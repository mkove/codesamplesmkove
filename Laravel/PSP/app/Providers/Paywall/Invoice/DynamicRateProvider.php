<?php
    namespace App\Providers\Paywall\Invoice;
    use App\Repositories\DynamicRateRepository;

    class DynamicRateProvider {

        private $_dynamicRateRepo;

        public function __construct(DynamicRateRepository $dynamicRateRepo) {
            $this->_dynamicRateRepo = $dynamicRateRepo;
        }

        public function find($id){
            return $this->_dynamicRateRepo->find($id);
        }

        public function get($filters = [], $queryOnly = false){
            return $this->_dynamicRateRepo->get($filters, $queryOnly);
        }

        public function create($data){
            return $this->_dynamicRateRepo->create($data);
        }

        public function update($id, $data){
            return $this->_dynamicRateRepo->update($id, $data);
        } 

        public function get_applicable_rates(&$pool, $dynamicRates){
            $userCount = $pool->users->count();
            $entryCount = ($pool->golf) ? $pool->golf->entries->count() : 0;
            $rates = [];
            $total = 0;
            $default = null;
            foreach($dynamicRates as $dynamicRate){
                $cnt = ($dynamicRate->per == 'user') ? $userCount : $entryCount;
                if($this->_has_min($cnt, $dynamicRate->min_items) && $this->_has_max($cnt, $dynamicRate->max_items) && !$this->_is_default($dynamicRate)){
                    $rates[] = $dynamicRate;
                } elseif($this->_is_default($dynamicRate)){
                    $default = $dynamicRate;
                }
            }
            if(!count($rates) && $default){
                $rates[] = $default; //fallback to default
            }
            return collect($rates);
        }

        public function calculate_total(&$pool, $rates){
            $userCount = $pool->users->count();
            $entryCount = ($pool->golf) ? $pool->golf->entries->count() : 0;
            $total = 0;
            foreach($rates as $dynamicRate){
                $cnt = ($dynamicRate->per == 'user') ? $userCount : $entryCount;
                $rateTotal = ($dynamicRate->is_flat) ? $dynamicRate->amount : ($dynamicRate->amount * $cnt);
                if($dynamicRate->max){
                    if($rateTotal < $dynamicRate->max){
                        $total = $total + $rateTotal;
                    } else {
                        $total = $total + $dynamicRate->max; //cap
                    }
                } else {
                    $total = $total + $rateTotal;
                }
            }
            return $total;
        }

        private function _is_default($dynamicRate){
            return (!$dynamicRate->min_items && !$dynamicRate->max_items);
        }
        private function _has_min($count, $min){
            return (!$min || ($count >= $min));
        }

        private function _has_max($count, $max){
            return (!$max || ($count <= $max));
        }


    }
