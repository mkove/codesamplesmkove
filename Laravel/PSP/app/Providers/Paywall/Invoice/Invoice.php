<?php

namespace App\Providers\Paywall\Invoice;

use App\Models\Rate;
use App\Repositories\InvoiceRepository;
use App\Providers\Paywall\Invoice\Rate as RateProvider;

class Invoice
{

    private $_invoiceRepo;
    public $rateProvider;

    public function __construct(
        InvoiceRepository $invoiceRepo,
        RateProvider $rateProvider
    ) {
        $this->_invoiceRepo = $invoiceRepo;
        $this->rateProvider = $rateProvider;
    }

    public function update($id, $data)
    {
        $invoice = $this->_invoiceRepo->update($id, $data);
        return $invoice;
    }

    public function update2($id, $pool)
    {
        if (!$pool->invoice) {
            $invoice = $this->create($pool);
            $id = $invoice->id;
        }
        $rate = $this->rateProvider->get(['pool_type_id' => $pool->type], true)->first();

        if ($rate) {
            // ['flat', 'per_player','per_game']);
            if ($rate->type == 'flat') {
                $sub = $rate->amount;
                $total = $rate->amount;
            } elseif ($rate->type == 'per_player') {
                $sub = $rate->amount * $pool->users->count();
                $total = $rate->amount * $pool->users->count();
            } else {
                $sub = $rate->amount;
                $total = $rate->amount;
            }
            $invoiceData = [
                'sub_total' => $sub,
                'total' => $total,
                'admin_override' => false,
                'paid' => false,
                'paid_at' => null,
                'description' => "",
            ];
            $invoice = $this->_invoiceRepo->update($id, $invoiceData);
            return $invoice;
        } else {
            $invoiceData = [
                'rate_id' => null,
                'name' => "Pool Invoice (FREE)",
                'tax' => 0,
                'sub_total' => 0,
                'total' => 0,
                'notes' => "No matching rate exists",
                'description' => "General game invoice",
            ];
            $invoice = $this->_invoiceRepo->update($id, $invoiceData);
            return $invoice;
        }
    }

    public function create($pool)
    {
        if (!$pool->invoice) { //no need to create a new invoice if one exists

            try {
                $totals = $this->_get_invoice_totals($pool);
                $rate = $totals['rate'];
                $sub = $totals['sub'];
                $total = $totals['total'];
                $tax = $totals['tax'];

                if ($rate) {
                    $invoiceData = [
                        'pool_id' => $pool->id,
                        'rate_id' => $rate->id,
                        'name' => $rate->name,
                        'tax' => $tax,
                        'sub_total' => $sub,
                        'total' => $total,
                        'admin_override' => false,
                        'paid' => false,
                        'paid_at' => null,
                        'notes' => "Invoice",
                        'description' => "&nbsp;",
                        'status' => 'created'
                    ];
                    $invoice = $this->_invoiceRepo->create($invoiceData);
                } else {
                    $invoiceData = [
                        'pool_id' => $pool->id,
                        'rate_id' => null,
                        'name' => "Pool Invoice (FREE)",
                        'tax' => $tax,
                        'sub_total' => $sub,
                        'total' => $total,
                        'admin_override' => false,
                        'paid' => true,
                        'paid_at' => date('Y-m-d'),
                        'notes' => "No matching rate exists",
                        'description' => "General game invoice",
                    ];
                    $invoice = $this->_invoiceRepo->create($invoiceData);
                }
                return $invoice;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $pool->invoice;
    }

    public function re_invoice(&$invoice, $pool, $adminOverride = false)
    {
        $totals = $this->_get_invoice_totals($pool);
        $rate = $totals['rate'];
        $sub = $totals['sub'];
        $total = $totals['total'];
        $tax = $totals['tax'];
        if ($rate) {
            $invoiceData = [
                'pool_id' => $pool->id,
                'rate_id' => $rate->id,
                'name' => $rate->name,
                'tax' => $tax,
                'sub_total' => $sub,
                'total' => $total,
                'admin_override' => $adminOverride,
            ];
        } else {
            $invoiceData = [
                'pool_id' => $pool->id,
                'rate_id' => null,
                'name' => "Pool Invoice (FREE)",
                'tax' => $tax,
                'sub_total' => $sub,
                'total' => $total,
                'admin_override' => $adminOverride,
                'status' => 'paid',
                'paid_at' => date('Y-m-d'),
                'notes' => "No matching rate exists",
                'description' => "General game invoice",
            ];
        }
        $invoice = $this->_invoiceRepo->update($invoice->id, $invoiceData);
        return $invoice;
    }

    public function apply_discount(&$invoice, $discount)
    {
        try {
            $total = $invoice->total;
            if ($discount) {
                if ($discount->type == 'percent') {
                    $percent = $discount->amount;
                    $total = $total - ($percent / $total) * $total;
                } else if ($discount->type == 'flat') {
                    $total = $total - $discount->amount;
                }
            }
            if ($total < 0) {
                $total = 0;
            }
            $invoice->total = $total;
            $invoice->save();
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public function prep_gtm_ecom_data()
    {
        //TODO: prep for GTAG message
    }

    private function _get_invoice_totals(&$pool)
    {
        $returnData = [
            'rate' => null,
            'tax' => null,
            'sub' => 0,
            'total' => 0,
        ];
        try {
            $rate = $this->rateProvider->get(['pool_type_id' => $pool->type], true)->with('dynamic_rates')->first();
            if ($rate) {
                if ($rate->type == 'flat') {
                    $sub = $rate->amount;
                    $total = $rate->amount;
                } elseif ($rate->type == 'per_player') {
                    $sub = $rate->amount * $pool->users->count();
                    $total = $rate->amount * $pool->users->count();
                } else {
                    $sub = $rate->amount;
                    $total = $rate->amount;
                }
                if ($rate->dynamic_rates->count()) {
                    $dynamicRateProvider = resolve(DynamicRateProvider::class);
                    $applicableRates = $dynamicRateProvider->get_applicable_rates($pool, $rate->dynamic_rates);
                    $rateTotal = $dynamicRateProvider->calculate_total($pool, $applicableRates);
                    $total = $total + $rateTotal;
                }
                $returnData['rate'] = $rate;
                $returnData['tax'] = null;
                $returnData['sub'] = $sub;
                $returnData['total'] = $total;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return $returnData;
    }
}
