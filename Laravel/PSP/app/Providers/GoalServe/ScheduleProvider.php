<?php

	namespace App\Providers\GoalServe;

    use Illuminate\Support\ServiceProvider;

    class ScheduleProvider extends ServiceProvider
    {
        /**
         * Register bindings in the container.
         *
         * @return void
         */
        public function register()
        {
            $this->app->singleton(ScheduleProvider::class, function ($app) {
                return new ScheduleProvider();
            });
        }
    }
