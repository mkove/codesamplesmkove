<?php namespace App\Providers;
    use Illuminate\Support\Collection;

    class Breadcrumb
    {

        private $_crumbs;
        public $obj = true;


        public function get() : Collection{
            return collect($this->_crumbs);
        }

        public function push($label, $link= "#", $active = false){
            $this->_crumbs[] =  [
                'link' => $link,
                'label' => $label,
                'active' => $active
            ];
        }

    }
