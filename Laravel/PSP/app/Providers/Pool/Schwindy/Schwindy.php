<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 9:14 AM
 */

namespace App\Providers\Pool\Schwindy;


use App\Helpers\GameHelper;
use App\Helpers\SchwindyHelper as SH;
use App\Helpers\SiteHelper;
use App\Helpers\TeamHelper;
use App\Helpers\PoolHelper;
use App\Helpers\UserNotificationHelper;
use App\Models\Config;
use App\Models\SchwindyPick;
use App\Models\Team;
use App\Providers\Game\Game as GameProvider;
use App\Providers\Game\Team\Team as TeamProvider;
use App\Providers\Pool\Schwindy\SchwindyPick as SchwindyPickProvider;

class Schwindy
{

    private $_repo;
    private $_gameProvider;
    private $_schwindyPickProvider;
    private $_teamProvider;

    public function __construct(
        \App\Repositories\SchwindyRepository $repo,
        GameProvider $gameProvider,
        SchwindyPickProvider $schwindyPickProvider,
        TeamProvider $teamProvider
    ) {
        $this->_gameProvider = $gameProvider;
        $this->_schwindyPickProvider = $schwindyPickProvider;
        $this->_teamProvider = $teamProvider;
        $this->_repo = $repo;
        $this->_current_season = SiteHelper::conf('schwindy.season', 'text', '2019/2020');
    }

    public function get($id = null)
    {
        return $this->_repo->get($id);
    }

    public function get_incomplete()
    {
        return $this->_repo->get_incomplete();
    }

    public function create($data)
    {
        if (!isset($data['config']) || $data['config'] == "") {
            $data['config'] = $this->create_config();
        }
        return $this->_repo->create($data);
    }

    public function leave($id, $userId)
    {
        $schwindy = $this->get($id);
        foreach ($schwindy->picks->where('user_id', $userId) as $pick) {
            $pick->delete();
        }
        return true;
    }

    public function get_pools($id, $user = false)
    {
        //            $poolProvider->get_pool($id, $user, true);
    }

    public function update($id, $data)
    {
        if (!isset($data['config'])) {
            $data['config'] = $this->create_config();
        } else {
            $data['config'] = $this->create_config($data['config']);
        }

        return $this->_repo->update($id, $data);
    }

    public function create_config($data = [])
    {
        $config = Config::where('key', 'schwindy.season')->first();
        $schwindyConfig = array(
            'start_week' => (isset($data['start_week'])) ? $data['start_week'] : 1,
            'end_week' => (isset($data['end_week'])) ? $data['end_week'] : __conf('schwindy.max_week', 'text', '18'),
            'season_year' => (isset($data['season_year'])) ? $data['season_year'] : $config->value,
            'week_at_start' => (isset($data['week_at_start']))  ? $data['week_at_start'] : 1,
            'weeks' => [],
            'total_picks' => (isset($data['total_picks'])) ? $data['total_picks'] : 32,
            'number_of_team_used' => 1,
            'locked' => []
        );
        for ($i = 0; $i < __conf('schwindy.max_week', 'text', '18'); $i++) {
            $schwindyConfig['weeks'][$i] = [
                'week_number' => $i + 1,
                'active' => isset($data['weeks'][$i]['active']) ? $data['weeks'][$i]['active'] : true,
                'force' => isset($data['weeks'][$i]['force']) ? $data['weeks'][$i]['force'] : true,
                'min_picks' => isset($data['weeks'][$i]['min_picks']) ? $data['weeks'][$i]['min_picks'] : 2,
                'max_picks' => isset($data['weeks'][$i]['max_picks']) ? $data['weeks'][$i]['max_picks'] : 2,
                'once' => isset($data['weeks'][$i]['once']) ? $data['weeks'][$i]['once'] : true,
            ];
            $schwindyConfig['locked'][] = ['week' => $i + 1, 'status' => 'none'];
        }
        return $schwindyConfig;
    }

    public function get_schwindy_for_vue($schwindy, $user, $pickUsers)
    {
        $me = \Auth::user();
        $config = $schwindy->config;
        unset($config->weeks);

        $schwindyData = $schwindy->toArray();
        $schwindyData['config'] = $config;
        $schwindyData['is_commissioner'] = $schwindy->pool->is_commissioner($me->id);
        $userData = $user->toArray();
        unset($schwindyData['pool']);
        unset($userData['schwindy_picks']);


        $schwindyData['user'] = $userData;
        $schwindyData['user_id'] = $user->id;
        $schwindyData['users'] = ($schwindyData['is_commissioner'] && $pickUsers) ? $schwindy->pool->unexposed_users() : [];
        $weeks = [];
        $userPicks = $schwindy->picks->where('user_id', $user->id);

        $gameFilters = [
            'sport' => 'football',
            'league' => 'professional',
            'season_year' => $schwindy->config->season_year,
            'season' => 'Regular Season',
        ];
        $gameQuery = $this->_gameProvider
            ->get($gameFilters, true)
            ->with(['home_team', 'away_team'])
            ->orderBy('time', 'ASC')->get();
        $weeklyGames = GameHelper::get_weekly_games($gameQuery);

        foreach ($schwindy->config->weeks as $week) {
            try {
                $week->games = [];
                $week->pick_count = 0;

                foreach ($weeklyGames[$week->week_number] as $game) {
                    try {
                        $thisGame = $game->toArray();
                        $thisGame['home_team']['win_loss'] = TeamHelper::wld($game->home_team_id, $gameQuery);
                        $thisGame['away_team']['win_loss'] = TeamHelper::wld($game->away_team_id, $gameQuery);

                        $thisGame['home_team']['pick'] = $userPicks->where('week', $week->week_number)
                            ->where('team_id', $thisGame['home_team_id'])
                            ->where('game_id', $thisGame['id'])
                            ->first();
                        if ($thisGame['home_team']['pick']) {
                            $week->pick_count++;
                        }
                        $teamUsedCnt = $userPicks->where('team_id', $thisGame['home_team_id'])
                            ->count();
                        $thisGame['home_team']['can_use'] = (!SH::is_team_over_max($teamUsedCnt, $schwindy->config));

                        $thisGame['away_team']['pick'] = $userPicks->where('week', $week->week_number)
                            ->where('team_id', $thisGame['away_team_id'])
                            ->where('game_id', $thisGame['id'])
                            ->first();
                        $teamUsedCnt = $userPicks->where('team_id', $thisGame['away_team_id'])
                            ->count();
                        $thisGame['away_team']['can_use'] = (!SH::is_team_over_max($teamUsedCnt, $schwindy->config));
                        if ($thisGame['away_team']['pick']) {
                            $week->pick_count++;
                        }
                        //                    $thisGame['locked'] = SH::picks_locked($schwindy, $game);
                        $week->games[] = $thisGame;
                    } catch (\Exception $e1) {
                        \Log::error($e1->getMessage());
                        \Log::error($e1->getTraceAsString());
                    }
                }

                $week->auto_assign = SH::num_of_autoassign($week->week_number, $userPicks, $schwindy->config);
                //                $week->games = $this->weeklyGames[$week->week_number];
                $weeks[] = $week;
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }
        }
        $totalPicks = $userPicks->count();
        $schwindyData['total_picks'] = $totalPicks;
        $schwindyData['teams_left'] = SH::num_of_teams_remaining($userPicks, $schwindy->config);
        $schwindyData['weeks'] = $weeks;
        $schwindyData['win_loss'] = TeamHelper::win_loss($gameQuery);


        return $schwindyData;
    }

    public function shit_picker(&$schwindy, $user)
    {
        //            $userPicks = $schwindy->picks->where('user_id', $user->id);
        //
    }

    public function toggle_lock($type = 'game', $locked = false, $param = null)
    {
        try {
            if ($type == 'game') {
                $this->_gameProvider->update($param, ['locked' => $locked]);
            } else {
                $filters = [
                    'season' => 'Regular Season',
                    'sport' => 'football',
                    'league' => 'professional',
                    'season_year' => SiteHelper::conf('schwindy.season'),
                    'locked' => false,
                    'week' => ($param) ?? SiteHelper::conf('schwindy.current_week', 'number', '1'),
                ];
                $games = $this->_gameProvider->get($filters);
                foreach ($games as $game) {
                    $this->_gameProvider->update($game->id, ['locked' => $locked]);
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public function toggle_visible()
    {
    }

    public function play_schwindy(&$schwindy, $week, $force = false, $setWeek = true)
    {
        if ($force) {
            $picks = $this->_schwindyPickProvider->get(['schwindy_id' => $schwindy->id, 'week' => $week], false);
            foreach ($picks as $pick) {
                $pickUpdateData = ['final' => false];
                $this->_schwindyPickProvider->update($pick->id, $pickUpdateData);
            }
            $pick = null; //collect
        }
        $weeklyGames = $this->_gameProvider->get([
            'sport' => 'football',
            'league' => 'professional',
            'season_year' => $this->_current_season,
            'season' => 'Regular Season',
            'week' => $week,
        ]);
        $gamesLeftToPlay = $weeklyGames->count();
        foreach ($weeklyGames as $game) {
            try {
                if ($game->is_final) {
                    $gamesLeftToPlay--;
                    $picks = $this->_schwindyPickProvider->get([
                        'week' => $week,
                        'game_id' => $game->id,
                        'schwindy_id' => $schwindy->id,
                        'final' => false,
                    ], true)->with(['game', 'user'])->get();
                    foreach ($picks as $pick) {
                        try {
                            if ($game->home_team_id == $pick->team_id) {
                                $points = $game->hometeam_totalscore;
                            } else {
                                $points = $game->awayteam_totalscore;
                            }
                            $pickUpdateData = [
                                'locked' => true,
                                'points' => $points,
                                'final' => true,
                                'visible' => true,
                            ];
                            $this->_schwindyPickProvider->update($pick->id, $pickUpdateData);
                        } catch (\Exception $e) {
                            \Log::error($e->getMessage());
                            \Log::error($e->getTraceAsString());
                        }
                    }
                }
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }
        }
        ////            \Log::info("\nCurrent week ".$week.
        ////                        "\n Left to play ".$gamesLeftToPlay
        //            );
        $schwindy->refresh();

        if ($gamesLeftToPlay === 0) {

            try {
                $this->shit_pick($schwindy, $week);
                if ($setWeek) {
                    $currentWeek = $schwindy->current_week;
                    $currentWeek++;
                    if ($currentWeek <= __conf('schwindy.max_week', 'text', '18')) {
                        $schwindyUpdate = [
                            'current_week' => $currentWeek,
                        ];
                        $this->_repo->update($schwindy->id, $schwindyUpdate);
                        $schwindy->refresh();
                    }
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
    }

    public function shit_pick(&$schwindy, $week)
    {

        $weeklyGames = $this->_gameProvider->get([
            'sport' => 'football',
            'league' => 'professional',
            'season_year' => $this->_current_season,
            'season' => 'Regular Season',
            'week' => $week,
        ]);

        try {
            $teamsByScore = $this->_gameProvider->build_game_roster($weeklyGames);

            $poolUsers = $schwindy->pool->users;
            $config = $schwindy->config;
            foreach ($poolUsers as $user) {
                $this->_shit_pick_user($schwindy, $user, $teamsByScore, $week);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }

    private function _shit_pick_user(&$schwindy, &$user, &$teamsByScore, $week)
    {
        try {
            $skip = array(); //set blank slate for teams that we will skip after shit pick them.
            $forcePickCount = $this->_get_forcepick_count($user, $schwindy, $week);
            if ($forcePickCount > 0) {
                for ($i = 1; $i <= $forcePickCount; $i++) {
                    $pick = $this->_pick_team($user, $schwindy, $teamsByScore, $skip, $week);
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
    }

    private function _get_forcepick_count(&$user, &$schwindy, $week)
    {
        try {
            $config = $schwindy->config;
            $allUserPicks = $this->_schwindyPickProvider->get(['user_id' => $user->id, 'schwindy_id' => $schwindy->id]);
            $thisWeekUserPicks = $this->_schwindyPickProvider->get(['user_id' => $user->id, 'schwindy_id' => $schwindy->id, 'week' => $week]);
            $weeklyUserCount = $thisWeekUserPicks->count();
            $weeklyMin = SH::get_week_min($schwindy->current_week, $config);
            $forcePick = ($weeklyMin - $weeklyUserCount);
            if ($forcePick > 0) {
                //                \Log::info(
                //                    "\nUser ".$user->global_display_name.
                //                    "\nPool ".$schwindy->pool->pool_name.
                //                    "\nMin set to  ".$weeklyMin.
                //                    "\nWeekly Count ".$weeklyUserCount.
                //                    "\n_____________________".
                //                    "\nABout to get shit picked ".$forcePick);
                return $forcePick;
            } else {
                $canStillPick = SH::sum_max_picks($config->weeks);
                $alreadyPicked = SH::num_of_teams_picked($allUserPicks);
                $needsToPick = $config->total_picks;

                $shitPicksThisWeek = $canStillPick - ($needsToPick - $alreadyPicked);
                //missing N number of picks
                if ($shitPicksThisWeek < 0) {
                    //get actual number of missing
                    $shitPicksThisWeek = abs($shitPicksThisWeek);
                    //let's say missing 2
                    $weeklyMax = SH::get_week_max($schwindy->current_week, $schwindy->config);
                    //MAX yo ucan force pick this week
                    $maxPick = ($weeklyMax < $shitPicksThisWeek) ? $weeklyMax : $shitPicksThisWeek;
                    //prevent picking over max if already picked something.
                    $maxPick = $maxPick - $weeklyUserCount;
                    if ($maxPick > 0) {
                        //                        \Log::info(
                        //                            "\nUser ".$user->global_display_name.
                        //                            "\nPool ".$schwindy->pool->pool_name.
                        //                            "\nSlots Left ".$canStillPick.
                        //                            "\nNeeds To Pick ".$needsToPick.
                        //                            "\nAlready Picked ".$alreadyPicked.
                        //                            "\nShit Pick Missing ".$shitPicksThisWeek.
                        //                            "\nWeekly Count ".$weeklyUserCount.
                        //                        "\n_____________________".
                        //                        "\nABout to get shit picked ".$maxPick);
                        return $maxPick;
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return 0;
    }

    private function _pick_team(&$user, &$schwindy, &$roster, &$skip, $week)
    {
        // tempcode disable autopick
        if (!$schwindy->autopick_pending) return;

        foreach ($roster as $teamId => $points) {
            try {
                $userPicks = $this->_schwindyPickProvider->get(['user_id' => $user->id, 'schwindy_id' => $schwindy->id]);
                $teamPickCount = SH::num_team_picked($teamId, $userPicks);
                //do not re-assign same shit team twice, use _skip var.
                if (SH::is_team_over_max($teamPickCount, $schwindy->config)) {
                    array_push($skip, $teamId);
                }

                if (!in_array($teamId, $skip)) {
                    //Create pick and call it a day
                    $team = $this->_teamProvider->find($teamId);
                    $gameFilters = [
                        'sport' => 'football',
                        'league' => 'professional',
                        'season_year' => $schwindy->config->season_year,
                        'season' => 'Regular Season',
                        'week' => $week
                    ];
                    //GET SPECIFIC GAME by TEAM ID AND WEEK
                    $game = $this->_gameProvider
                        ->get($gameFilters, true)
                        ->where(function ($query) use ($teamId) {
                            $query->where('home_team_id', $teamId)->orWhere('away_team_id', $teamId);
                        })
                        ->first();

                    $pickData = [
                        'schwindy_id' => $schwindy->id,
                        'team_id' => $teamId,
                        'provider_team_id' => $team->provider_id,
                        'user_id' => $user->id,
                        'game_id' => $game->id,
                        'week' => $week,
                        'autopick' => true,
                        'locked' => true,
                        'points' => $points,
                        'final' => true,
                        'visible' => true,
                        'team_name' => $team->name,
                        'team_shortname' => $team->short_name
                    ];
                    $pick = $this->_schwindyPickProvider->create($pickData);
                    array_push($skip, $teamId);
                    return $pick;
                    break; // exit loop,
                }
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
                return null;
            }
        }

        return null;
    }

    public function count_entries($user)
    {
        // tempcode.  current schwindy returns 1 entry
        return 1;
        return $user->schwindy_picks->count();
    }

    public function build_named_entries($user, $isGolf = false)
    {
        $uniqueEntries = [];
        //            foreach($user->squares as $entry){
        //                $name = strtolower(trim($entry->label));
        //                if(!isset($uniqueEntries[$name])){
        //                    $uniqueEntries[$name] = [
        //                        'label' => $entry->label,
        //                        'count' => 1
        //                    ];
        //                } else {
        //                    $uniqueEntries[$name]['count']++;
        //                }
        //            }
        //            return array_values($uniqueEntries);
        //            TODO: Schwindy roster needs entries
        return $uniqueEntries;
    }

    public function create_roster_for_export(&$schwindy, $users)
    {
        $pool = $schwindy->pool;
        $rows = [];
        $rows[] = [
            __c('squareboard.roster.content.table_head_confirm', "Confirm"),
            __c('squareboard.roster.content.table_head_username', "User Name"),
            __c('squareboard.roster.content.table_head_email', "Email"),
            __c('squareboard.roster.content.table_head_phone', "Phone"),
            __c('squareboard.roster.content.table_head_display_name', "Display Name"),
            __c('squareboard.roster.content.table_head_entries', "Entries"),
            __c('squareboard.roster.content.table_head_commissioner', "Commissioner"),
            __c('squareboard.roster.content.table_head_commissioner_note', "Commissioner Note"),
            __c('squareboard.roster.content.table_head_user_note', "User Note"),
        ];
        foreach ($users as $poolUser) {
            try {
                $uSquares = $poolUser->squares($schwindy->id)->get();
                $squares = [];
                $sum = 0;
                foreach ($uSquares as $square) {
                    if (isset($squares[$square->label])) {
                        $squares[$square->label]++;
                    } else {
                        $squares[$square->label] = 1;
                    }
                }
                $holdArray = [];
                foreach ($squares as $label => $cnt) {
                    $sum = $sum + $cnt;
                    $holdArray[] = $label . "({$cnt})";
                }
                $comNote = PoolHelper::parse_commissioner_note($poolUser->pivot->commissioner_note);
                $rows[] = [
                    'confirmed' => ($poolUser->pivot->verified) ? 'yes' : '',
                    'username' => $poolUser->username,
                    'email' => $poolUser->email,
                    'phone' => $poolUser->phone,
                    'display_names' => implode(',', $holdArray),
                    'entries' => $sum,
                    'commissioner' => ($pool->is_commissioner($poolUser->id)) ? "yes" : "",
                    'commissioner_note' => $comNote['note'],
                    'users_note' => $poolUser->pivot->user_note,
                ];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $rows;
    }
}
