<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 9:13 AM
 */

namespace App\Providers\Pool;

use App\Helpers\PoolHelper;
use App\Helpers\UserNotificationHelper;
use App\Providers\Email\EmailContentProvider;
use App\Providers\Pool\Schwindy\Schwindy as SchwindyProvider;
use App\Providers\Pool\SquareBoard\SquareBoard as SquareBoardProvider;
use App\Providers\Pool\Golf\GolfProvider;
use App\Providers\Game\Game as GameProvider;
use App\Providers\Pool\PoolType as PoolTypeProvider;
use App\Providers\EmailInvite\EmailInvite as EmailInviteProvider;
use App\Repositories\PoolRepository;
use net\authorize\util\Log;
use function Matrix\trace;
use App\Models\User;
use App\Mail\AdminNotice;

class Pool
{

    private $_poolRepo;
    private $_squareboardProvider;
    private $_schwindyProvider;
    private $_golfProvider;
    private $_gameProvider;
    private $_emailContentProvider;
    private $_poolTypeProvider;
    private $_emailInviteProvider;

    public function __construct(
        PoolRepository $poolRepo
    ) {
        $this->_poolRepo = $poolRepo;
    }
    public function find($id)
    {
        return $this->_poolRepo->find($id);
    }

    public function get($id = null, $params = [], $queryOnly = false)
    {
        if ($id) {
            return $this->_poolRepo->find($id);
        }
        return $this->_poolRepo->get($params, $queryOnly);
    }

    public function user_active(&$pool, $userId)
    {
        foreach ($pool->users as $user) {
            if ($user->id == $userId) {
                return true;
            }
        }
        return false;
    }

    public function create($data)
    {
        return $this->_poolRepo->create($data);
    }

    public function create_by_type($typeData = [], $userId = null)
    {
        try {
            $pool_type_name = "";
            $leagues = (isset($typeData['league'])) ? $typeData['league'] : "";
            $puid = $this->create_puid();
            $data = [
                'league' => $this->_get_league_id($leagues),
                'event' => 0,
                'type' => $typeData['type'],
                'commissioner_id' =>  $userId,
                'pool_name' => $typeData['name'],
                'pool_password' => $typeData['password'],
                'description' => " ",
                'status' => 'active',
                'puid' => $puid,
                'hash' => md5($puid),
                'allow_user_invite' => 'auto'
            ];
            $pool = $this->_poolRepo->create($data);
            switch ($pool->type) {
                case 1:
                    $gameId = (isset($typeData["game_id"])) ? $typeData["game_id"] : null;
                    $this->_gameProvider = resolve(GameProvider::class);
                    $this->_squareboardProvider = resolve(SquareBoardProvider::class);
                    if ($gameId) {
                        $game = $this->_gameProvider->find($gameId);
                    } else {
                        $game = null;
                    }
                    $squareBoard =  $this->_squareboardProvider->create($pool, $game);
                    $pool_type_name = "Regular Square";
                    break;

                case 2:
                    $schwindyData = [
                        'label' => "",
                        'deadline' => "",
                        'config' => "",
                        'weekly_bonus' => false,
                        'pool_id' => $pool->id,
                        'autopick_pending' => false,
                        'week_played' => false,
                    ];
                    $this->_schwindyProvider = resolve(SchwindyProvider::class);
                    $schwindy = $this->_schwindyProvider->create($schwindyData);
                    $pool_type_name = "Schwindy";
                    break;

                case 3:
                    $boardData = [
                        'hometeam_name' => "Home Team",
                        'awayteam_name' => "Away Team",
                        'label' => $pool->pool_name,
                    ];
                    $this->_squareboardProvider = resolve(SquareBoardProvider::class);
                    $squareBoard = $this->_squareboardProvider->create_blank($pool, $boardData);
                    $pool_type_name = "blank";
                    break;

                case 4:
                    $this->_squareboardProvider = resolve(SquareBoardProvider::class);
                    $boardData = [
                        'hometeam_name' => "Losing Team",
                        'awayteam_name' => "Winning Team",
                        'label' => $pool->pool_name,
                    ];
                    $this->_squareboardProvider->create_ncaa_game($pool, $boardData);
                    $pool_type_name = "NCAA";
                    break;

                case 5:

                    $this->_golfProvider = resolve(GolfProvider::class);
                    //CREATE GOLF DATA
                    //                    TODO:: create golf pool repo and provider and create it properly
                    //                    TODO:: create pool_group repo and provider and update it properly
                    $numOfGroups = (isset($typeData['number_of_groups'])) ? $typeData['number_of_groups'] : 3;
                    $golfData = [
                        'golf_tournament_id' => $typeData['tournament_id'],
                        'pool_id' => $pool->id,
                        'number_of_groups' => $numOfGroups,
                        'number_of_winners' => (isset($typeData['number_of_winners'])) ? $typeData['number_of_winners'] : "2",
                        'total_per_entry' => (isset($typeData['total_per_entry'])) ? $typeData['total_per_entry'] : "2",
                        'toward_score' => (isset($typeData['toward_score'])) ? $typeData['toward_score'] : "2",
                        'max_pool_per_user' => (isset($typeData['max_pool_per_user'])) ? $typeData['max_pool_per_user'] : "2",
                        'lower_to_higher' => (isset($typeData['lower_to_higher'])) ? $typeData['lower_to_higher'] : "0",
                        'winner' => (isset($typeData['winner'])) ? $typeData['winner'] : "1",
                        'win_type' => (isset($typeData['win_type'])) ? $typeData['win_type'] : "final",
                        'config' => $this->_golfProvider->build_config($numOfGroups),
                        'pickable' => false,
                    ];
                    $golfPool = $this->_golfProvider->create($golfData);
                    $pool_type_name = "Golf";

                    break;
            }

            // send admin email
            $toEmail = User::where('is_admin', 'yes')->get(['email']);
            $commissioner_user = \Auth::user();
            $message = $commissioner_user->global_display_name . " ( " . $commissioner_user->email . " ) started a <b>" . $data['pool_name'] . " (" . $pool_type_name . ")</b>";
            UserNotificationHelper::send_admin("User created pool", $message);
        } catch (\Exception $e) {
            //                \Log::info("NO POOL HERE");
            \Log::error($e->getMessage());
            $pool = null;
        }

        return $pool;
    }

    public function find_by_puid($puid)
    {
        return $this->_poolRepo->find_by_puid($puid);
    }

    public function find_by_hash($hash)
    {
        return $this->_poolRepo->find_by_hash(strtolower($hash));
    }

    public function update($id, $data)
    {
        return $this->_poolRepo->update($id, $data);
    }

    public function get_pool_user($id, $userId)
    {
        $pool = $this->_poolRepo->get(['id' => $id], true)->with([
            'users' => function ($query) use ($userId) {
                $query->where('users.id', $userId);
            }
        ])->first();
        return ($pool) ? $pool->users->first() : null;
    }

    public function can_user_join($pool, $user)
    {

        //I AM LOGGED IN AND ALREADY A MEMBER
        if ($pool->already_member($user->id) || $pool->i_am_invited($user->email) || $pool->banned_member($user->id)) {
            return false;
        }
        return true;
    }

    public function add_user(&$pool, $user, $status = 'active', $commissioner = false, $displayName = null)
    {

        //            dd($pool)
        $this->_emailInviteProvider = resolve(EmailInviteProvider::class);
        $data[$user->id] = [
            'status' => $status,
            'commissioner' => $commissioner,
            'approved' => ($pool->allow_user_invite === 'auto' || $commissioner),
            'display_name' => ($displayName) ?? $user->global_display_name
        ];
        //            dd($pool, $data, $commissioner);
        $this->_emailContentProvider = resolve(EmailContentProvider::class);
        $updateUser = $this->update_user($pool, $data);
        $invitedUser = $this->_emailInviteProvider->get(['email' => $user->email, 'pool_id' => $pool->id], true)->first();
        if ($invitedUser) {
            $this->_emailInviteProvider->update($invitedUser->id, ['accepted' => 'yes', 'pool_users_id' => $user->id]);
        }
        if (!$data[$user->id]['approved']) {
            $data = [
                'user' => ($user) ? $user : null,
                'pool' => $pool,
                'email_invite' => $invitedUser,
                'emailInvite' => $invitedUser,
                'commissioner.message' => ($invitedUser) ? $invitedUser->message : "",
                'current_user' => \Auth::user(),
            ];
            foreach ($pool->commissioners as $commissioner) {
                $this->_emailContentProvider->process_email('pool_new_member_approval_needed', $commissioner->email, $data);
            }
        }
        return $updateUser;
    }

    public function approve_user(&$pool, $user)
    {
        try {
            $this->_emailInviteProvider = resolve(EmailInviteProvider::class);
            $this->_emailContentProvider = resolve(EmailContentProvider::class);
            $data[$user->id] = [
                'approved' => true,
                'status' => 'active'
            ];
            $updateUser = $this->update_user($pool, $data);
            $invitedUser = $this->_emailInviteProvider->get(['email' => $user->email, 'pool_id' => $pool->id], true)->first();
            if ($invitedUser) {
                $this->_emailInviteProvider->update($invitedUser->id, [
                    'pool_users_id' => $user->id,
                    'approved' => true
                ]);
            }
            $data = [
                'user' => ($user) ? $user : null,
                'pool' => $pool,
                'email_invite' => $invitedUser,
                'emailInvite' => $invitedUser,
                'commissioner.message' => ($invitedUser) ? $invitedUser->message : "",
                'current_user' => \Auth::user(),
            ];
            $this->_emailContentProvider->process_email('pool_member_approved_notice', $user->email, $data);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public function reject_user(&$pool, $user)
    {

        $data[$user->id] = [
            'approved' => false,
            'status' => 'disabled',
        ];
        $updateUser = $this->update_user($pool, $data);
        try {
            $this->_emailInviteProvider = resolve(EmailInviteProvider::class);
            $this->_emailContentProvider = resolve(EmailContentProvider::class);
            $invitedUser = $this->_emailInviteProvider->get(['email' => $user->email, 'pool_id' => $pool->id, 'approved' => false], true)->first();
            if ($invitedUser) {
                $this->_emailInviteProvider->update($invitedUser->id, ['accepted' => 'no', 'pool_users_id' => $user->id]);
            }
            $data = [
                'user' => ($user) ? $user : null,
                'pool' => $pool,
                'email_invite' => $invitedUser,
                'emailInvite' => $invitedUser,
                'commissioner.message' => ($invitedUser) ? $invitedUser->message : "",
                'current_user' => \Auth::user(),
            ];
            $this->_emailContentProvider->process_email('pool_member_reject_notice', $user->email, $data);
        } catch (\Exception $e) {
        }
        return $updateUser;
    }

    public function remove_user(&$pool, $userId)
    {
        //            TODO: do removal not this shit
        //            $data[$userId] = ['status' => 'disabled', 'commissioner' => false];
        //            return $this->update_user($pool, $data);
        return true;
    }

    public function update_user(&$pool, $data)
    {

        return $pool->all_users()->syncWithoutDetaching($data);
    }

    public function make_commissioner(&$pool, $userId)
    {
        $data[$userId] = ['status' => 'active', 'commissioner' => true];
        return $this->update_user($pool, $data);
    }

    public function remove_commissioner(&$pool, $userId)
    {
        $data[$userId] = ['commissioner' => false];
        return $this->update_user($pool, $data);
    }

    public function password_correct(&$pool, $poolPassword = null)
    {
        return ($poolPassword == $pool->pool_password);
    }

    public function is_invited(&$pool, $user = null)
    {
        //   dump($pool->invites->where('email', strtolower(trim($user)))->orWhere('pool_users_id', $user)->get());

        return ($pool->invites()->where('email', strtolower(trim($user)))->orWhere('pool_users_id', $user)->count() > 0);
    }

    public function get_commissioner_pool($id, $userId)
    {
        $pool = $this->_poolRepo->get_commissioner_pool($id, $userId);
        if (!$pool) return null;
        return $pool;
    }

    public function get_pool($id = null, $user = false, $queryOnly = false)
    {
        return $this->_poolRepo->get_pool($id, $user, $queryOnly);
    }

    public function get_pool_type($pool)
    {
        $this->_poolTypeProvider = resolve(PoolTypeProvider::class);
        return $this->_poolTypeProvider->find($pool->type);
    }

    public function is_pool_commissioner(&$pool, $userId)
    {
        return $this->_poolRepo->is_pool_commissioner($pool, $userId);
    }

    public function redirect_pool_type($pool, $page = null, $redirectTo = '/pool')
    {
        $startTime = null;
        //            __dump_time($startTime, "STARTED");
        if (!$pool) {
            \Site::add_notices("Sorry, this pool does not exist", 'danger');
            return redirect($redirectTo);
        }
        $type = $this->get_pool_type($pool);
        //            __dump_time($startTime, "GoT TYP");
        if (!$type) {
            \Site::add_notices("Sorry, this pool does not exist", 'danger');
            return redirect($redirectTo);
        }
        if ($type->template == 'squareboard') {

            return $this->_redirect_to_squareboard($pool, $page, $redirectTo);
        } elseif ($type->template == 'schwindy') {
            return $this->_redirect_to_schwindy($pool, $page, $redirectTo);
        } elseif ($type->template == 'golf') {
            //                __dump_time($startTime, "GOLF");
            $page = $page ?? 'picks';
            return $this->_redirect_to_golf($pool, $page, $redirectTo);
        } else {
            return redirect($redirectTo);
        }
    }

    public function search($term, $take = null)
    {
        return $this->_poolRepo->search($term, $take);
    }

    private function _redirect_to_schwindy($pool, $page = null, $redirectTo = "/")
    {
        $schwindy = $pool->schwindy;
        if (!$schwindy) {
            \Site::add_notices("Sorry, this Schwindy Game does not exist", 'danger');
            return redirect($redirectTo);
        }
        if ($page) {
            return redirect('/schwindy/' . $schwindy->id . '/' . $page);
        }
        return redirect()->route('psp.schwindy.show', ['schwindy' => $schwindy->id]);
    }

    private function _redirect_to_squareboard($pool, $page = null, $redirectTo = "/")
    {
        $squareBoard = $pool->squareboard;
        if (!$squareBoard) {
            \Site::add_notices("Sorry, this Square Game does not exist", 'danger');
            return redirect($redirectTo);
        }
        if ($squareBoard->game) {
            if (!$squareBoard->game->is_not_started && $page != 'edit-create') $page = 'live2';
        }
        if ($page == 'edit-create') $page = 'edit';

        if (!$page) {
            if ($squareBoard->numbers->count() == 0) {
                $page = 'pick';
            }
        }

        return redirect('/pool/' . $pool->id . "/squareboard/" . $squareBoard->id . "/" . $page);
    }

    private function _redirect_to_golf($pool, $page = "pick", $redirectTo = "/")
    {
        $golfPool = $pool->golf;
        if (!$golfPool) {
            \Site::add_notices("Sorry, this Golf pool does not exist", 'danger');
            return redirect($redirectTo);
        }
        $route = route('psp.golf.' . $page, ['golf' => $golfPool->id]);
        //            \Log::info("Sending them to ".'psp.golf.'.$route);
        return redirect()->to($route); //
    }

    private function _get_sport($data)
    {
        $league = explode('|', $data);
        return isset($league[1]) ? $league[1] : null;
    }

    private function _get_league_id($data): int
    {
        $league = explode('|', $data);
        $leagueId = 0;
        if (isset($league[0])) {
            $leagueId = ($league[0] == 'professional') ? 1 : 2;
        }
        return $leagueId;
    }

    private function _get_game_by_sport($gameId, $sport)
    {
        switch ($sport) {
            case 'basketball':
                $basketballGameProvider = resolve('App\Providers\Game\BasketballGame');
                return $basketballGameProvider->find($gameId);
                break;

            case 'football':
                return $this->_gameProvider->find($gameId);
                break;
            default:
                return null;
                break;
        }
    }

    public function create_puid()
    {
        $puid = $this->_generate_puid();
        while ($this->find_by_puid($puid)) {
            try {
                $puid = $this->_generate_puid();
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $puid;
    }

    public function leave($id, $userId)
    {
        try {
            $pool = $this->get($id);
            $pool->all_users()->detach($userId);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public function update_payment_profile($id, $paymentProfileId)
    {
        try {
            $pool = $this->find($id);
            $pool->billing_payment_profile()->sync([$paymentProfileId]);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        //            dump($pool->billing_payment_profile()->get());
        return true;
    }

    public function cancel_pool(&$pool)
    {
        $pool->load(['all_users']);
        $updateData = ['status' => 'disabled'];
        $this->update($pool->id, $updateData);
        return true;
        //            foreach($pool->all_users as $user){
        //
        //            }
    }

    public function verify_user($pool, $userId)
    {
        $poolUser = $pool->users->where('id', $userId)->first();
        if (!$poolUser) {
            return false;
        }
        $poolUser->pivot->verified = true;
        $poolUser->pivot->save();
        return true;
    }

    public function unverify_user($pool, $userId)
    {
        $poolUser = $pool->users->where('id', $userId)->first();
        if (!$poolUser) {
            return false;
        }
        $poolUser->pivot->verified = false;
        $poolUser->pivot->save();
        return true;
    }

    private function _generate_puid()
    {
        $charItems[] = chr(rand(97, 122)) . chr(rand(97, 122)) . chr(rand(97, 122));
        $charItems[] = rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9);
        return strtoupper(implode('-', $charItems));
    }

    public function build_roster($id, $type = null)
    {

        $roster = [];
        $isGolf = false;
        try {
            switch ($type) {
                case 'squareboard':
                    $pool = $this->_poolRepo
                        ->get(['id' => $id], true)->with(['squareBoard'])->first();
                    $squareBoardId = $pool->squareBoard->id;

                    $pool->load(['all_users', 'all_users.squares' => function ($query) use ($squareBoardId) {
                        $query->where('squares.squareboard_id', $squareBoardId);
                    }]);
                    $provider = resolve(SquareBoardProvider::class);
                    break;

                case 'golf':
                    $isGolf = true;
                    $pool = $this->_poolRepo
                        ->get(['id' => $id], true)->with(['golf'])->first();
                    $golfId = $pool->golf->id;

                    $pool->load(['all_users', 'all_users.entries' => function ($query) use ($golfId) {
                        $query->where('golf_entries.golf_id', $golfId);
                    }]);
                    $provider = resolve(GolfProvider::class);
                    break;
                case 'schwindy':
                    $pool = $this->_poolRepo
                        ->get(['id' => $id], true)->with(['schwindy'])->first();
                    // $schwindyId = $pool->schwindy->id;
                    // $pool->load(['all_users', 'all_users.schwindy' => function($query) use ($schwindyId) {
                    //     $query->where('schwindy_picks.schwindy_id', $schwindyId);
                    // }]); 

                    $provider = resolve(SchwindyProvider::class);
                    break;
            }

            foreach ($pool->all_users  as $poolUser) {
                $user = $this->_build_user_roster($poolUser);
                $user['entry_count'] = $provider->count_entries($poolUser);
                $user['entries'] = $provider->build_named_entries($poolUser, $isGolf);
                $user['creator'] = $pool->is_creator($poolUser->id);
                $user['is_golf'] = $isGolf;
                $roster[] = $user;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $roster;
    }

    private function _build_user_roster($poolUser)
    {
        return  [
            'id' => $poolUser->id,
            'name' => trim($poolUser->first . " " . $poolUser->last),
            'username' => $poolUser->username,
            'display_name' => $poolUser->global_display_name,
            'email' => $poolUser->email,
            'phone' => $poolUser->phone,
            'user_status' => $poolUser->status,
            'status' => $poolUser->pivot->status,
            'verified' => $poolUser->pivot->verified,
            'approved' => $poolUser->pivot->approved,
            'note' => PoolHelper::parse_commissioner_note($poolUser->pivot->commissioner_note),
            'users_note' => $poolUser->pivot->user_note,
            'commissioner' => $poolUser->pivot->commissioner,
        ];
    }
}
