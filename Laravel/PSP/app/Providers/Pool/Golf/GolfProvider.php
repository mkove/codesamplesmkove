<?php

namespace App\Providers\Pool\Golf;

use App\Helpers\GolfHelper;
use App\Helpers\PoolHelper;
use App\Helpers\SiteHelper;
use App\Models\GolfPlayer;
use App\Models\GolfTournament;
use App\Models\GolfTournamentPlayer;
use App\Providers\Email\EmailContentProvider;
use App\Repositories\GolfRepository;
use App\Repositories\GolfTournamentPlayerRepository;

class GolfProvider
{

    private $_golfRepo;
    private $_golfTournamentPlayerProvider;
    private $_golfEntryProvider;

    public function __construct(
        GolfRepository $repo
    ) {
        $this->_golfRepo = $repo;
    }

    public function find($id)
    {
        return $this->_golfRepo->find($id);
    }

    public function create($data)
    {
        return $this->_golfRepo->create($data);
    }

    public function update($id, $data)
    {
        return $this->_golfRepo->update($id, $data);
    }

    public function update_config(&$golf, $config, $returnUpdates = false)
    {
        return $this->_golfRepo->update_config($golf, $config, $returnUpdates);
    }

    public function get($filters = [], $queryOnly = false)
    {
        return $this->_golfRepo->get($filters, $queryOnly);
    }

    public function create_standings_for_export(&$golf, $handicaps, $type = 'csv', $restrict = false, $userId = null)
    {
        $rows = [];
        $separator = ($type == 'csv') ? " / " : " / ";
        $head = [
            'Pos',
            'Entry',
            'status',
            'Win',
            'R1',
            'R2',
            'R3',
            'R4',
            'Tot',
        ];
        foreach ($golf->config->groups as $group) {
            if ($group->enabled) {
                array_push($head, "Group " . $group->name);
            }
        }
        $rows[] = $head;
        $standings = $this->get_standings($golf, $handicaps, $restrict, $userId);
        foreach ($standings as $standing) {
            $row = [
                $standing['pos'],
                $standing['name'],
                $standing['status'],
                ($standing['pool_winner'] ? "YES" : ""),
                $standing['round_1'],
                $standing['round_2'],
                $standing['round_3'],
                $standing['round_4'],
                $standing['total'],
            ];
            foreach ($standing['groups'] as $group) {
                $groupArr = [];
                foreach ($group['players'] as $player) {
                    $handicapped = ($player['handicap'] !== 0) ? " ({$player['handicap']})" : "";
                    $groupArr[] = $player['display_name'] . $handicapped;
                }
                array_push($row, implode($separator, $groupArr));
            }
            $rows[] = $row;
        }
        return $rows;
    }

    public function create_roster_for_export(&$golf, &$users)
    {
        $rows = [];
        $rows[] = [
            'CONFIRM', 'COMISH', 'USER NAME', 'NAME', 'EMAIL', 'PHONE', 'ENTRIES',    'ENTRIES COUNT', 'USER NOTE', 'NOTES'
        ];


        foreach ($users as $user) {
            try {
                $userEntries = $user->entries()->where('golf_id', $golf->id)->get()->pluck('name')->toArray();
                $entryString = implode(',', $userEntries);
                $comNote = PoolHelper::parse_commissioner_note($user->pivot->commissioner_note);
                $rows[] = [
                    $user->pivot->verified ? "Yes" : "",
                    ($golf->pool->is_commissioner($user->id)) ? 'Yes' : '',
                    $user->username,
                    $user->first . " " . $user->last,
                    $user->email,
                    $user->phone,
                    $entryString,
                    count($userEntries),
                    $user->pivot->user_note,
                    $comNote['note'],
                ];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $rows;
    }

    public function create_picks_for_export(&$golf, $handicaps, $type = 'csv', $restrict = false, $userId = null)
    {
        $rows = [];
        $separator = ($type == 'csv') ? " / " : " / ";
        $head = [
            'Entry', 
        ];
        foreach ($golf->config->groups as $group) {
            if ($group->enabled) {
                array_push($head, "Group " . $group->name);
            }
        }
        $rows[] = $head;
        $standings = $this->get_picks($golf, $handicaps, $restrict, $userId);

        foreach ($standings as $standing) {
            $row = [
                $standing['name'],
            ];
            foreach ($standing['groups'] as $group) {
                $groupArr = [];
                foreach ($group['players'] as $player) {
                    $handicapped = ($player['handicap'] !== 0) ? " ({$player['handicap']})" : "";
                    $groupArr[] = $player['display_name'] . $handicapped;
                }
                array_push($row, implode($separator, $groupArr));
            }
            $rows[] = $row;
        }
        return $rows;
    }

    public function process_group_changes(&$before, &$after)
    {
        $newPlayers = [];
        $removedPlayers = [];
        $groupChanges = [];
        foreach ($before as $bgtp) {
            $stayed = $after->first(function ($player) use ($bgtp) {
                return ($player->id === $bgtp->id);
            });
            if ($stayed) {
                if ($stayed->pivot->group_number !== $bgtp->pivot->group_number) {
                    $groupChanges[] = [
                        'old' => GolfHelper::group_number_to_letter($bgtp->pivot->group_number),
                        'new' => GolfHelper::group_number_to_letter($stayed->pivot->group_number),
                        'name' => $stayed->player->name,
                        'player' => $stayed
                    ];
                }
            } else {
                $removedPlayers[] = [
                    'name' => $bgtp->player->name,
                    'player' => $bgtp,
                    'group' => GolfHelper::group_number_to_letter($bgtp->pivot->group_number),
                ];
            }
        }
        foreach ($after as $agtp) {
            $stayed = $before->first(function ($player) use ($agtp) {
                return ($player->id === $agtp->id);
            });
            if (!$stayed) {
                $newPlayers[] = [
                    'name' => $agtp->player->name,
                    'player' => $agtp,
                    'group' => GolfHelper::group_number_to_letter($agtp->pivot->group_number),
                ];
            }
        }
        return [
            'new' => $newPlayers,
            'removed' => $removedPlayers,
            'group_change' => $groupChanges
        ];
    }

    public function picks_count(&$golf)
    {
        $golfEntryProvider = resolve(GolfEntryProvider::class);
        return $golfEntryProvider->has_picks($golf);
    }

    public function picks_allowed(&$golf, $commissioner = false)
    {

        // return true;
        //            dd($golf->locked, )
        if (!$golf->locked || __conf('golf.enable.override_locked_pools', 'boolean', 1)) {
            if ($golf->pickable || $commissioner) {
                return true;
            }
        }
        return false;
    }

    public function load_full_golf(&$golf, $user = null, $simple = false)
    {

        $startTime = null;
        __dump_time($startTime, "STARTED");
        $data = [
            'user' => null,
            'pool' => null,
            'golf' => null,
            'tournament' => null,
            'invoice' =>  null,
            'chat' => null,
            'entries' => collect([]),
            'user_entries' => collect([]),
            'user_picks' => [],
            'handicaps' => [],
            'isCommissioner' => false,

        ];
        try {

            if (!$golf) {
                return $data;
            }
            //                dump($golf);
            __dump_time($startTime, "PRE");
            $this->_load_golf_instance($golf, $simple);
            __dump_time($startTime, "POST");
            if ($golf) {
                $pool = $golf->pool;
                $entries = $golf->entries;
                $groupedTournamentPlayers = $golf->grouped_tournament_players;
                if ($golf->entry_lock)
                    $userEntries = $entries;
                else
                    $userEntries = ($user) ? $entries->where('user_id', $user->id) : [];
                $data['user'] = $user;
                $data['pool'] = $pool;
                $data['golf'] = $golf;
                $data['tournament'] = $golf->tournament;
                $data['invoice'] = $pool->invoice;
                $data['chat'] = $pool->chat;
                $data['entries'] = $golf->entries;
                $data['user_entries'] = $userEntries;
                $data['isCommissioner'] = ($user) ? $pool->is_commissioner($user->id) : false;
                foreach ($userEntries as $entry) {
                    foreach ($entry->groups as $group) {
                        foreach ($group->tournament_players as $gtp) {
                            $data['user_picks'][$gtp->id] = $gtp;
                        }
                    }
                }
                foreach ($groupedTournamentPlayers as $gtp) {
                    if ($gtp->pivot->handicap) {
                        $data['handicaps'][$gtp->id] = $gtp->pivot->handicap;
                    }
                }
            }
            __dump_time($startTime, "WOWO");
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return $data;
        }
        return $data;
    }

    private function _load_golf_instance(&$golf, $simple = false)
    {

        $startTime = null;
        __dump_time($startTime, "INSTANCE START ");

        $toload = [];
        if ($simple) {
            $relationships = [
                'pool',
                'pool.chat',
                'pool.chat.messages',
                'grouped_tournament_players',
                'pool.users',
                'pool.invoice',
                'entries',
                'entries.groups',
                'entries.groups.tournament_players.player',
                'tournament',
                'tournament.tournament_players',
                'tournament.tournament_players.player',
                'grouped_tournament_players',
                'grouped_tournament_players.player',
            ];
        } else {
            $relationships = [
                'pool',
                'pool.chat',
                'pool.chat.messages',
                'grouped_tournament_players',
                'pool.users',
                'pool.users.entries',
                'pool.users.entries.groups',
                'pool.users.entries.groups.tournament_players',
                'pool.users.entries.groups.tournament_players.player',
                'pool.users.entries.groups.tournament_players.rounds',
                'pool.users.entries.groups.tournament_players.rounds.holes',
                'pool.users.entries.groups.tournament_players.rounds.scored_holes',
                'pool.invoice',
                'pool.invoice.rate',
                'pool.invoice.discounts',
                'pool.invoice.rate.dynamic_rates',
                'entries',
                'entries.groups',
                'entries.groups.tournament_players.player',
                'entries.groups.tournament_players.rounds',
                'entries.groups.tournament_players.rounds.holes',
                'tournament',
                'tournament.tournament_players',
                'tournament.tournament_players.player',
                'tournament.tournament_players.rounds',
                'tournament.tournament_players.groups.tournament_players',
                'tournament.tournament_players.groups.tournament_players.player',
                'tournament.tournament_players.groups.tournament_players.rounds',
                'tournament.tournament_players.groups.tournament_players.rounds.holes',
                'tournament.tournament_players.groups.tournament_players.rounds.scored_holes',
                'grouped_tournament_players',
                'grouped_tournament_players.rounds',
                'grouped_tournament_players.player',
            ];
        }

        foreach ($relationships as $rel) {
            if (!$golf->relationLoaded($rel)) {
                $toload[] = $rel;
            }
        }

        $toLoad['entries.groups.tournament_players'] = function ($query) {
            $query->orderBy('total', 'asc')->orderBy('status', 'asc');
        };
        $golf->load($toload);
        __dump_time($startTime, "INSTANCE LOADED ");
    }

    public function load_players_live(&$golf, &$tournament, $handicaps = [], $roundOverride = null)
    {
        $tournamentInProgress = $tournament->in_progress;

        if (!$golf || !$tournament) {
            return [];
        }
        $golf->load(['grouped_tournament_players']);
        $allPlayers = [];
        $allCutPlayers = [];
        $this->_golfTournamentPlayerProvider = resolve(GolfTournamentPlayerProvider::class);
        $this->_golfEntryProvider = resolve(GolfEntryProvider::class);
        $timeStamp = date('U');
        //            $tournament->load(['tournament_players','tournament_players.rounds','tournament_players.rounds.scored_holes']);
        $currentRound = ($roundOverride) ?? $tournament->tournament_current_round; // GolfHelper::get_tournament_current_round($tournament);
        foreach ($tournament->tournament_players->sortBy('pos') as $tp) {
            try {
                $data = $this->_golfEntryProvider->apply_handicap_to_player($tp, 'total', $handicaps);
                $groupedPlayer = $golf->grouped_tournament_players->where('id', $tp->id)->first();


                $percent = ($groupedPlayer) ? floatval($groupedPlayer->pivot->percent) : null; // $this->_golfTournamentPlayerProvider->player_percent($tp->id, $golf, $golf->entries);
                $countCuts = $golf->count_cut;
                $handicap = $data['handicap'];
                $magicRounds = $this->_golfTournamentPlayerProvider->do_magic_rounds_with_cuts($tp, $countCuts, $handicap);
                $rounds = [
                    '1' => $magicRounds['round_1'],
                    '2' => $magicRounds['round_2'],
                    '3' => $magicRounds['round_3'],
                    '4' => $magicRounds['round_4'],
                ];

                $today = isset($rounds[$currentRound]) ? intval($rounds[$currentRound]) : 0;

                $playerData = [
                    'id' => $tp->id,
                    'pos' => $tp->pos,
                    'name' => $tp->player->name,
                    'display_name' => $tp->player->display_name ? $tp->player->display_name : $tp->player->name,
                    'short_name' => $tp->player->display_name_mobile ? $tp->player->display_name_mobile : $tp->player->name,
                    'percent' => $percent,
                    'today' => $today,
                    'hole' => ($tp->status !== 'Active') ? "CUT" : $tp->hole,
                    'tee' => $tp->tee_time,
                    'total' => $magicRounds['total'],
                    'handicap_total' => $magicRounds['handicap_total'],
                    'status' => $tp->status,
                    'start_hole' => ((int)$tp->start_hole > 0) ? (int)$tp->start_hole : null, // ($tp->start_hole !== 1) ? $tp->start_hole : null,
                    'r1' => $magicRounds['round_1'],
                    'r2' => $magicRounds['round_2'],
                    'r3' => $magicRounds['round_3'],
                    'r4' => $magicRounds['round_4'],
                    'round_1' => $tp->round_1,
                    'round_2' => $tp->round_2,
                    'round_3' => $tp->round_3,
                    'round_4' => $tp->round_4,
                    'real_total' => $tp->total,
                    'count_cut_label' => ($countCuts && $tp->status === 'CUT') ? __c('golf.live.content.count_cut_label', '(c)') : null,
                    'prize' => (int)$tp->prize,
                    'prize_format' => number_format($tp->prize, 0, '.', ','),
                    'handicap' => $data['handicap'],
                    'handicapped' => $data['handicapped'],
                    'disqualified_round' => $tp->disqualified_round,
                ];
                if (!$tournamentInProgress) {
                }
                if ($tp->status != 'Active') {
                    $allCutPlayers[$tp->id] = $playerData;
                } else {
                    $allPlayers[$tp->id] = $playerData;
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        uasort($allPlayers, function ($a, $b) {
            return $a['total'] <=> $b['total'];
        });
        //            $tie = null;
        //            $lastPId = null;
        //            $swingBack = false;
        //            foreach ($allPlayers as $pid => &$p){
        //                $curPos = $p['pos'];
        //                $isTied = $curPos === $tie && $curPos !== 9999;
        //                $p['tpos'] = ($isTied) ? "<small>T</small>".$p['pos'] : $p['pos'];
        //                $swingBack = ($isTied);
        //                $tie = $curPos;
        //                if($swingBack){
        //                    $allPlayers[$lastPId]['tpos'] = "<small>T</small>".$allPlayers[$lastPId]['pos'];
        //                    $swingBack = false;
        //                }
        //                $lastPId = $pid;
        //            }
        $allPlayers = array_values($allPlayers);
        $this->_sort_player_pos($allPlayers);
        $allPlayers = $allPlayers + $allCutPlayers;
        return array_values($allPlayers);
    }

    public function leave(&$golf, $userId)
    {
        $entries = $golf->entries->where('user_id', $userId);
        foreach ($entries as $entry) {
            $entry->delete();
        }
        return true;
    }

    public function load_entries_live(&$golf, &$handicaps = [], $roundOverride = null)
    {

        $start_time = microtime();
        $this->_golfTournamentPlayerProvider = resolve(GolfTournamentPlayerProvider::class);
        $this->_golfEntryProvider = resolve(GolfEntryProvider::class);
        $pool = $golf->pool;
        $tournament = $golf->tournament;
        $usersEntries = [];
        $towardScore = $golf->toward_score;
        $par = $tournament->par;
        $currentRound = ($roundOverride) ?? $tournament->tournament_current_round;
        //            __dump_time($start_time);
        foreach ($pool->users as $user) {
            foreach ($user->entries->where('golf_id', $golf->id) as $entry) {
                try {
                    $usersEntries[] = $this->_golfEntryProvider->build_vue_live_entry($entry, $currentRound, $golf->type);
                } catch (\Exception $e) {
                    \Log::error(":" . $e->getMessage());
                }
            }
        }

        $sort = ($golf->type === 'score') ? 'total_score' : 'prize_total';
        //            $sort = () 'total_score';
        $this->_sort_entry_pos($usersEntries, $sort, $golf->number_of_winners);

        //            $this->_sort_user_entries_by_pos($usersEntries, $golf->type);



        return $usersEntries;
    }

    private function _sort_user_entries_by_pos(&$usersEntries, $type = 'score')
    {

        $sort = ($type == 'score') ? 'total' : 'prize_total';
        usort($usersEntries, function ($a, $b) use ($sort) {
            return $a[$sort] <=> $b[$sort];
        });
        $total = null;
        $pos = 0;
        $realPos = 0;
        $tie = 0;
        foreach ($usersEntries as &$userEntry) {
            if ($userEntry['status'] === 'active') {
                if ($total != $userEntry[$sort]) {
                    if ($tie > 0) {
                        $realPos = $realPos + $tie;
                        $realPos--;
                        //                            $realPos  = $realPos - $tie;
                    } else {
                        $realPos++;
                        //                            dump($realPos." - tie ".$tie);
                    }
                    $tie = 0;
                    $pos = $realPos;
                } else {
                    $realPos++;
                    $tie++;
                    $pos = $realPos - $tie;
                }

                $userEntry['pos'] = $pos;
                $total = $userEntry[$sort];
            } else {
                $userEntry['pos'] = null;
            }
        }
    }

    private function _has_handicap($players)
    {
        if (isset($players['active'])) {
            foreach ($players['active'] as $player) {
                if ($player['handicapped']) {
                    return true;
                }
            }
        }
        if (isset($players['cut'])) {
            foreach ($players['cut'] as $player) {
                if ($player['handicapped']) {
                    return true;
                }
            }
        }
        return false;
    }

    private function _get_total(&$players, $towardsScore)
    {
        $total = null;
        usort($players, function ($a, $b) {
            return $a['total'] <=> $b['total'];
        });
        foreach ($players as $player) {
            if ($player['status'] == 'Active' && $player['toward_score']) {
                $total = $total + $player['plays']['total'];
            }
        }
        return $total;
    }

    public function get_standings(&$golf, &$handicaps = [], $restrict = false, $userId = null)
    {
        $startTime = microtime(true);
        __dump_time($startTime, "started");
        $tournament = $golf->tournament;
        $currentRound = (int)$tournament->tournament_current_round;
        $allPlayers = $tournament->tournament_players;
        $players = $this->_build_players($allPlayers, $handicaps);
        __dump_time($startTime, "players built");
        $entries = [];
        $gEntries = $golf->entries;
        $isPrize = ($golf->type === 'prize');
        foreach ($gEntries as $entry) {
            try {
                __dump_time($startTime, "Entry Enter");

                $builtRounds = $this->_build_rounds($entry, $players, $isPrize);
                __dump_time($startTime, "round built");

                $entries[] = [
                    'id' => $entry->id,
                    'name' => $entry->name,
                    'total_score' => $entry->total,
                    'prize_total' => (int)$entry->prize_total,
                    'total_prize' => $entry->prize_total,
                    'formatted_prize' => number_format($entry->prize_total),
                    'user_id' => $entry->user_id,
                    'status' => $entry->status,
                    'round_1' => isset($builtRounds['round_1']['score']) ? floatval($builtRounds['round_1']['score']) : 0,
                    'round_2' => isset($builtRounds['round_2']['score']) ? floatval($builtRounds['round_2']['score']) : 0,
                    'round_3' => isset($builtRounds['round_3']['score']) ? floatval($builtRounds['round_3']['score']) : 0,
                    'round_4' => isset($builtRounds['round_4']['score']) ? floatval($builtRounds['round_4']['score']) : 0,
                    'total' => isset($builtRounds['total']['score']) ? floatval($builtRounds['total']['score']) : 0,
                    'groups' => $this->_build_groups($entry, $players, $restrict, $userId),
                    'rounds' => $builtRounds,
                    'current_round' => $currentRound,
                    'prize_stats' => $entry->prize_stats,
                    'score_stats' => $entry->play_stats,
                    'type' => $golf->type,
                ];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        __dump_time($startTime, "exit loop");

        $sort = ($golf->type === 'score') ? 'total_score' : 'prize_total';
        if (count($entries)) {
            $this->_sort_entry_pos($entries, $sort, $golf->number_of_winners);
        }
        __dump_time($startTime, "sorted");

        return $entries;
    }

    public function get_picks(&$golf, &$handicaps = [], $restrict = false, $userId = null)
    { 
        $tournament = $golf->tournament; 
        $allPlayers = $tournament->tournament_players;
        $players = $this->_build_players($allPlayers, $handicaps);
        
        $entries = [];
        $gEntries = $golf->entries;
        
        foreach ($gEntries as $entry) {
            try { 
                $entries[] = [
                    'id' => $entry->id,
                    'name' => $entry->name, 
                    'user_id' => $entry->user_id,
                    'status' => $entry->status, 
                    'groups' => $this->_build_groups($entry, $players, $restrict, $userId), 
                ];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        } 

        return $entries;
    }

    private function _build_rounds(&$entry, &$players, $isPrize = false)
    {
        $rounds = [];
        try {
            if ($entry && $entry->play_stats) {
                $stats = ($isPrize) ? $entry->prize_stats : $entry->play_stats;
                if ($isPrize) {
                    $this->_do_round_player($stats, $players, 'total', $rounds, 'prize');
                    $rounds['total']['score'] = $entry->total;
                } else {
                    foreach ($stats as $round => $stat) {
                        $this->_do_round_player($stat, $players, $round, $rounds, 'score');
                        $rounds[$round]['score'] = $entry->{$round};
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $rounds;
    }

    private function _do_round_player(&$stat, &$players, $round, &$rounds, $type = 'score')
    {
        $playerTypes = ["winners", "losers", "disqualified"];
        foreach ($playerTypes as $pt) {
            if (is_object($stat) && isset($stat->{$pt})) {
                foreach ($stat->{$pt} as $pid) {
                    $player = isset($players[$pid]) ? $players[$pid] : null;
                    if ($player) {
                        $player['handicapped'] = isset($stat->handicap) ? $stat->handicap : null;
                        $player['round_score'] = $player[$round];
                        $player['prize'] = $player['prize'];
                        $rounds[$round][$pt][] = $player;
                    }
                }
            }
        }
    }

    private function _build_groups(&$entry, &$players, $restrict = false, $userId = null)
    {
        $groups = [];
        if ($entry) {
            foreach ($entry->groups as $group) {
                $groupPlayers = [];
                if (!$restrict || ($restrict && $entry->user_id == $userId)) {
                    foreach ($group->tournament_players as $gtp) {
                        $groupPlayers[] = $players[$gtp->id];
                    }
                }
                $groups[] = [
                    'number' => $group->number,
                    'name' => $group->name,
                    'players' => $groupPlayers,
                ];
            }
        }
        return $groups;
    }

    private function _build_players($allPlayers, $handicaps = [], $countCut = false)
    {
        $players = [];
        //collect playas in nice array to use
        $this->_golfTournamentPlayerProvider = resolve(GolfTournamentPlayerProvider::class);
        foreach ($allPlayers as $player) {
            try {
                $handicap = isset($handicaps[$player->id]) ? $handicaps[$player->id] : 0;
                $magicRounds = $this->_golfTournamentPlayerProvider->do_magic_rounds_with_cuts($player, $countCut, $handicap);
                $playerData = [
                    'id' => $player->id,
                    'last_name' => $player->player->last_name,
                    'display_name_mobile' => $player->player->display_name_mobile ? $player->player->display_name_mobile : $player->player->name,
                    'display_name' => $player->player->display_name ? $player->player->display_name : $player->player->name,
                    'name' => $player->player->name,
                    'round_1' => $magicRounds['round_1'],
                    'round_2' => $magicRounds['round_2'],
                    'round_3' => $magicRounds['round_3'],
                    'round_4' => $magicRounds['round_4'],
                    'total' => $magicRounds['handicap_total'],
                    'count_cut_label' => ($countCut && $player->status === 'CUT') ? __c('golf.live.content.count_cut_label', '(c)') : "",
                    'status' => $player->status,
                    'handicap' => $handicap,
                    'prize' => (float)$player->prize,
                    'pos' => $player->pos,
                ];
                $players[$player->id] = $playerData;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $players;
    }

    public function load_standings(&$golf, &$handicaps = [], $live = false, $userId = null)
    {
        $golfId = $golf->id;
        $golf->load([
            'pool',
            'pool.users',
            'pool.users.entries' => function ($query) use ($golfId) {
                $query->where('golf_id', $golfId)->with(['groups', 'groups.tournament_players', 'groups.tournament_players.player']);
            },
        ]);
        $this->_golfTournamentPlayerProvider = resolve(GolfTournamentPlayerProvider::class);
        $this->_golfEntryProvider = resolve(GolfEntryProvider::class);
        $tourneyPar = $golf->tournament->par ?? 72; //TODO: clean this shit - wtf bro
        $pool = $golf->pool;
        $winData  = [];
        $tournament = $golf->tournament;
        $playersData = [];
        $usersEntries = [];
        $timeStamp = date('U');
        foreach ($pool->users as $user) {
            //IF has user Id load only for that user ... if no user id.. load all
            if (($userId && $user->id == $userId) || !$userId) {
                foreach ($user->entries->where('golf_id', $golf->id) as $entry) {
                    try {
                        $players = $this->_golfEntryProvider->get_entry_players($entry, $golf, $handicaps, true);
                        $entry->picked_players = $players;
                        $allPickedPlayers = isset($entry->picked_players['all']) ? $entry->picked_players['all'] : [];
                        $entry->round_performance = [
                            'round_1' => $this->_golfEntryProvider->dice_round($allPickedPlayers, 1, $golf->toward_score, [], $live),
                            'round_2' => $this->_golfEntryProvider->dice_round($allPickedPlayers, 2, $golf->toward_score, [], $live),
                            'round_3' => $this->_golfEntryProvider->dice_round($allPickedPlayers, 3, $golf->toward_score, [], $live),
                            'round_4' => $this->_golfEntryProvider->dice_round($allPickedPlayers, 4, $golf->toward_score, [], $live),
                            'total' => $this->_golfEntryProvider->dice_total($allPickedPlayers, $golf->toward_score, $handicaps),
                        ];

                        $entryHandicapped = (isset($entry->round_performance['total']['handicapped'])) ? $entry->round_performance['total']['handicapped'] : false;

                        $usersEntries[] = [
                            'id' => $entry->id,
                            'timestamp' => date('U', strtotime($entry->updated_at)),
                            'toward_score' => $golf->toward_score,
                            'entry' => $entry,
                            'total' => $entry->total,
                            'handicapped' => $entryHandicapped,
                            //                            'round_total' => $entry->round_performance['total'],
                            'round_1' => $entry->round_1,
                            'round_2' => $entry->round_2,
                            'round_3' => $entry->round_3,
                            'round_4' => $entry->round_4,
                        ];
                    } catch (\Exception $e) {
                    }
                }
            }
        }
        usort($usersEntries, function ($a, $b) {
            return $a['total'] <=> $b['total'];
        });
        $total = null;
        $pos = 0;
        $realPos = 0;
        $tie = 0;
        foreach ($usersEntries as &$userEntry) {
            if ($userEntry && $userEntry['entry']->status === 'active') {
                if ($total != $userEntry['total']) {
                    if ($tie > 0) {
                        $realPos = $realPos + $tie;
                        $realPos--;
                        //                            $realPos  = $realPos - $tie;
                    } else {
                        $realPos++;
                        //                            dump($realPos." - tie ".$tie);
                    }
                    $tie = 0;
                    $pos = $realPos;
                } else {
                    $realPos++;
                    $tie++;
                    $pos = $realPos - $tie;
                }

                $userEntry['pos'] = $pos;
                $total = $userEntry['total'];
            } else {
                $userEntry['pos'] = null;
            }
            //                unset($userEntry['entry']);
        }
        return [
            'usersEntries' => $usersEntries,
            'playersData' => $playersData,
            'started' => ($tournament->tournament_current_round !== null)
        ];
    }

    public function toggle_open(&$golf)
    {

        $set = !$golf->pickable;
        $this->update($golf->id, ['pickable' => $set]);
        return ['open' => $set];
    }

    public function load_players(&$golf, &$tournament, $handicaps = [])
    {
        $allPlayers = [];
        $allCutPlayers = [];
        $this->_golfTournamentPlayerProvider = resolve(GolfTournamentPlayerProvider::class);
        $this->_golfEntryProvider = resolve(GolfEntryProvider::class);
        $timeStamp = date('U');
        foreach ($tournament->tournament_players->sortBy('pos') as $tp) {
            try {
                                $tp->percent = $this->_golfTournamentPlayerProvider->player_percent($tp->id, $golf, $golf->entries);
                $data = $this->_golfEntryProvider->apply_handicap_to_player($tp, 'total', $handicaps);
                $tp->handicap = $data['handicap'];
                $tp->handicapped = $data['handicapped'];
                $tp->timestamp = date('U', strtotime($tp->updated_at));
                if ($tp->status == 'CUT') {
                    $allCutPlayers[$tp->id] = $tp;
                } else {
                    $allPlayers[$tp->id] = $tp;
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        uasort($allPlayers, function ($a, $b) {
            return $a['total'] <=> $b['total'];
        });

        $total = null;
        $pos = 0;
        $realPos = 0;
        $tie = 0;
        /**
            foreach($allPlayers as &$player)
            {
                //we in tie
                if($total === $player->total){
                    $player->pos = $realPos;
                    $tie++;
                } else { //we are out of tie
                    $realPos++;
                    $realPos = $realPos + $tie;
                    $tie = 0;
                    $player->pos = $realPos;
                }
                $total = $player->total;
            }
         **/
        $allPlayers = $allPlayers + $allCutPlayers;
        return $allPlayers;
    }

    public function load_percentage(&$golf, &$entries, $sort = 'percent')
    {
        $this->_golfTournamentPlayerProvider = resolve(GolfTournamentPlayerProvider::class);
        $all = [];
        $present = ['statsActive', 'statsCut', 'statsRest'];
        foreach ($present as $name) {
            $$name = [];
        }
        //            $statsActive = [];
        //            $statsCut = [];
        //            $statsRest = [];
        $statistics = [];
        foreach ($golf->grouped_tournament_players as $gtp) {

            try {
                if ($gtp->player) { //this prevents TP without actual players ... not sure how some happenes
                    $grp = GolfHelper::group_number_to_letter($gtp->pivot->group_number);
                    if ($sort === 'percent') {
                        foreach ($present as $name) {
                            if (!isset($$name[$grp])) {
                                $$name[$grp] = [];
                            }
                        }
                        //                        if(!isset($statsActive[$grp])){
                        //                            $statsActive[$grp] = [];
                        //                        }
                        //                        if(!isset($statsCut[$grp])){
                        //                            $statsCut[$grp] = [];
                        //                        }
                        //                        if(!isset($statsRest[$grp])){
                        //                            $statsRest[$grp] = [];
                        //                        }
                        $handicap = $gtp->pivot->handicap;
                        $total = ($handicap) ? $handicap + $gtp->total : $gtp->total;
                        $status = $gtp->status;
                        $statsActive[$grp][$gtp->id] = [
                            'status' => $gtp->status,
                            'player' => $gtp,
                            'group' => $grp,
                            'rank' => $gtp->player->rank,
                            'pos' => $gtp->pos,
                            'percent' => (float)$gtp->pivot->percent,
                            'total' => $total,
                            'prize' => (int)$gtp->prize,
                        ];
                    } else {
                        foreach ($present as $name) {
                            if (!isset($$name[$grp])) {
                                $$name[$grp] = [];
                            }
                        }
                        //                        if(!isset($statsActive[$grp])){
                        //                            $statsActive[$grp] = [];
                        //                        }
                        //                        if(!isset($statsCut[$grp])){
                        //                            $statsCut[$grp] = [];
                        //                        }
                        //                        if(!isset($statsRest[$grp])){
                        //                            $statsRest[$grp] = [];
                        //                        }
                        $handicap = $gtp->pivot->handicap;
                        $total = ($handicap) ? $handicap + $gtp->total : $gtp->total;
                        $status = $gtp->status;
                        switch ($status) {
                            case 'Active':
                                $statsActive[$grp][$gtp->id] = [
                                    'status' => $gtp->status,
                                    'player' => $gtp,
                                    'group' => $grp,
	                                'rank' => $gtp->player->rank,
	                                'pos' => $gtp->pos,
                                    'percent' => (float)$gtp->pivot->percent,
                                    'total' => $total,
                                    'prize' => (int)$gtp->prize,
                                ];
                                break;
                            case 'CUT':
                                $statsCut[$grp][$gtp->id] = [
                                    'status' => $gtp->status,
                                    'player' => $gtp,
                                    'group' => $grp,
	                                'rank' => $gtp->player->rank,
	                                'pos' => $gtp->pos,
                                    'percent' => (float)$gtp->pivot->percent,
                                    'total' => $total,
                                    'prize' => 0,
                                ];
                                break;
                            default:
                                $statsRest[$grp][$gtp->id] = [
                                    'status' => $gtp->status,
                                    'player' => $gtp,
                                    'group' => $grp,
	                                'rank' => $gtp->player->rank,
	                                'pos' => $gtp->pos,
                                    'percent' => (float)$gtp->pivot->percent,
                                    'total' => $total,
                                    'prize' => 0,
                                ];
                                break;
                        }
                    }
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }

        foreach ($present as $name) {
            if (isset($$name)) {
                foreach ($$name as &$grouped) {
                    if (count($grouped)) {
                        uasort($grouped, function ($a, $b) use ($sort) {
                            return ($sort == 'total') ? $a[$sort] <=> $b[$sort]  : $b[$sort] <=> $a[$sort];
                        });
                    }
                }
            }
        }
        foreach ($present as $name) {
            if (isset($$name)) {
                foreach ($$name as $g => $active) {
                    if (!isset($statistics[$g])) {
                        $statistics[$g] = [];
                    }
                    $statistics[$g] = array_merge($statistics[$g], $active);
                }
            }
        }

        return ['statistics' => $statistics, 'statsall' => []];
    }

    public function load_percentage_all(&$golf, &$entries)
    {
        $this->_golfTournamentPlayerProvider = resolve(GolfTournamentPlayerProvider::class);
        $percentPlayers = [];
        $tournament = $golf->tournament;
        //                    $tournament->load(['tournament_players']);
        try {
            foreach ($tournament->tournament_players->sortBy('pos') as $tp) {
                $groupedPlayer = $golf->grouped_tournament_players->where('id', $tp->id)->first();
                //                dd($groupedPlayer, $tp);
                $percentPlayers[] = [
                    'percent' => ($groupedPlayer) ? floatval($groupedPlayer->pivot->percent) : null, // $this->_golfTournamentPlayerProvider->player_percent($tp->id, $golf, $entries),
                    'player' => $tp,
                    'mine' => '',
                    'group' => ($groupedPlayer) ? GolfHelper::group_number_to_letter($groupedPlayer->pivot->group_number) : '',
                ];
            }
            usort($percentPlayers, function ($a, $b) {
                return $a['percent'] < $b['percent'];
            });
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return ['percentPlayers' => $percentPlayers];
    }

    public function configed_groups(&$config)
    {
        $count = 0;
        try {
            foreach ($config->groups as $group) {
                if ($group->enabled) {
                    $count++;
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return $count;
    }

    public function get_group_max(&$config, $groupNumber)
    {

        foreach ($config->groups as $group) {
            if ($group->enabled && $group->number == $groupNumber) {
                return $group->max;
            }
        }
        return 0;
    }

    public function build_edit_golf_vue(&$golf)
    {
        $hasPicks = $this->golf_has_picks($golf);
        $hasGroupedPlayers = $golf->grouped_tournament_players->count();
        return [
            'id' => $golf->id,
            'pool_id' => $golf->pool_id,
            'name' => $golf->pool->pool_name,
            'password' => $golf->pool->pool_password,
            'type' => $golf->type,
            'allow_user_invite' => $golf->pool->allow_user_invite,
            'commissioner_note' => $golf->commissioner_note,
            'description' => $golf->pool->description,
            'rules' => $golf->rules,
            'count_cut' => boolval($golf->count_cut),
            'entry_lock' => boolval($golf->entry_lock),
            'has_grouped_players' => ($hasGroupedPlayers > 0),
            'view_only' => $golf->config->viewonly,
            'locked' => boolval($golf->locked),
            'locks_at' => ($golf->locks_at) ? date('Y-m-d\TH:i:sO', strtotime($golf->locks_at)) : null,
            'lock_type' => $golf->locks_at ? 'custom' : 'default',
            'allow_toward_score_cap' => __conf('golf.enable.allow_prize_towars_score_limit', 'boolean', 0),
            'pickable' => boolval($golf->pickable),
            'lower_to_higher' => $golf->lower_to_higher,
            'max_pool_per_user' => $golf->max_pool_per_user,
            'number_of_groups' => $golf->number_of_groups,
            'number_of_round_winners' => $golf->number_of_round_winners,
            'number_of_winners' => $golf->number_of_winners,
            'total_per_entry' => $golf->total_per_entry,
            'toward_score' => $golf->toward_score,
            'win_type' => $golf->win_type,
            'playoff_stroke' => (bool)$golf->playoff_stroke,
            'winner' => $golf->winner,
            'has_picks' => $hasPicks,
            'preview_url' => route('psp.golf.preview', ['golf' => $golf->id]),
            'pick_count' => ($hasPicks) ? $this->picks_count($golf) : 0,
            'groups' => array_values(json_decode(json_encode($golf->config->groups), true)),
            'config' => $golf->config
        ];
    }

    public function pick_count(&$golf)
    {
        $count = 0;
        foreach ($golf->entries as $entry) {
            foreach ($entry->groups as $group) {
                $count = $count + $group->tournament_players->count();
            }
        }
        return $count;
    }
    public function golf_has_picks(&$golf)
    {
        $count = 0;
        foreach ($golf->entries as $entry) {
            foreach ($entry->groups as $group) {
                if ($group->tournament_players->count() > 0) {
                    return true;
                }
            }
        }
    }

    //TODO: fix this shit
    public function get_golf_vue(&$golf, $userId = null, $commissioner = false)
    {
        $entries = $golf->entries;
        $tournamentPlayers = $golf->grouped_tournament_players;
        $groupedPlayers = [];
        $originalGroups = [];

        try {
            foreach ($tournamentPlayers as $gtp) {
                $gtp->original_group = $gtp->pivot->group_number;
                $originalGroups[$gtp->id] = $gtp->pivot->group_number;
                $groupedPlayers[] = [
                    'group_number' => $gtp->pivot->group_number,
                    'golf_tournament_player' => $gtp,
                    'player_id' => $gtp->player_id,
                    'golf_tournament_player_id' => $gtp->id,
                    'rank' => $gtp->pos,
                    'handicap' => $gtp->pivot->handicap
                ];
            }

            $users = [];
            foreach ($golf->pool->users as $user) {
                if ($commissioner || $user->id == $userId) {
                    $entryList = array_values($entries->where('user_id', $user->id)->toArray());
                    foreach ($entryList as &$entry) {
                        foreach ($entry['groups'] as &$group) {
                            foreach ($group['tournament_players'] as &$gtplayer) {
                                if (isset($originalGroups[$gtplayer['id']])) {
                                    $gtplayer['original_group'] = $originalGroups[$gtplayer['id']];
                                }
                            }
                        }
                    }
                    $users[] = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'username' => $user->username,
                        'global_display_name' => $user->global_display_name,
                        'email' => $user->email,
                        'status' => $user->status,
                        'verified' => $user->verified,
                        'entries' => $entryList,
                        'commissioner' => $commissioner
                    ];
                }
            }

            $tournament = $golf->tournament;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        //            ksort($groupedPlayers);
        $golf->overwrite_lock = (bool)__conf('golf.enable.override_locked_pools', 'boolean', 1);
        return [
            'id' => $golf->id,
            'pickable' => $golf->pickable,
            'name' => $golf->pool_name,
            'number_of_groups' => $golf->number_of_groups,
            'commissioner_note' => $golf->commissioner_note,
            'total_per_entry' => $golf->total_per_entry,
            'toward_score' => $golf->toward_score,
            'max_pool_per_user' => $golf->max_pool_per_user,
            'lower_to_higher' => $golf->lower_to_higher,
            'winner' => $golf->winner,
            'number_of_winners' => $golf->number_of_winners,
            'tournament_id' => $golf->tournament_id,
            'pool_id' => $golf->pool_id,
            'number_of_single_round_winners' => $golf->number_of_single_round_winners,
            'number_of_cumilitative_winners' => $golf->number_of_cumilitative_winners,
            'number_of_winners_each_round' => $golf->number_of_winners_each_round,
            'players' => $groupedPlayers,
            'users' => $users,
            'config' => $golf->config,
            'overwrite_lock' => boolval(__conf('golf.enable.override_locked_pools', 'boolean', 1)),

        ];
    }

    public function get_user_entry($userId, &$golf)
    {
        return $golf->entries->where('user_id', $userId)->toArray();
    }

    public function count_entries($user)
    {
        return $user->entries->count();
    }

    public function build_named_entries($user, $isGolf = true)
    {
        $uniqueEntries = [];
        $eCount = 0;
        foreach ($user->entries as $entry) {
            try {
                $eCount++;
                $name = strtolower(trim($entry->name));
                if (!isset($uniqueEntries[$name])) {
                    $uniqueEntries[$name] = [
                        'label' => $entry->name,
                        'count' => 1
                    ];
                } else {
                    $uniqueEntries[$name]['count']++;
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return array_values($uniqueEntries);
    }

    public function get_golf_players(&$golf)
    {
        return $golf->players->toArray();
    }

    public function toggle_view_only(&$golf, $enable = false, $pin = null)
    {
        try {
            $pin = ($pin) ?? rand(1000, 9999);
            $config = $golf->config;
            $viewOnly = isset($config->viewonly) ? $config->viewonly : json_encode(['enabled' => false, 'pin' => null]);
            $viewOnly->enabled = $enable;
            $viewOnly->pin = $pin;
            $config->viewonly = $viewOnly;
            $golf->config = $config;
            $golf->save();
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public function build_config($numberOfGroups = 4, $configData = [], $viewOnlyData = [])
    {
        $config = [
            'groups' => [],
            'viewonly' => [
                'enabled' => (isset($viewOnlyData['enabled'])) ? $viewOnlyData['enabled'] : false,
                'pin' => (isset($viewOnlyData['pin'])) ? $viewOnlyData['pin'] : null,
            ]
        ];
        for ($g = 1; $g <= 6; $g++) {
            try {
                $config['groups'][$g] = [
                    'name' => GolfHelper::group_number_to_letter($g),
                    'number' => $g,
                    'enabled' => ($g <= $numberOfGroups),
                    'max' => (isset($configData[$g])) ? $configData[$g] : 2,
                ];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $config;
    }

    public function distribute_players(&$golf, $number = 50)
    {
        $splitGroups = $this->_split_ratings($number, $golf->number_of_groups);
        $start = 0;
        $players = [];
        $tournamentId = $golf->tournament->id;
        foreach ($splitGroups as $groupNumber => $count) {
            try {
                //                $golfPlayersIds = GolfPlayer::orderBy('pos', 'asc')->whereHas('golf_plays', function($query) use ($tournamentId){
                //                    $query->where('tournament_id', $tournamentId);
                //                })->offset($start)->limit($count)->pluck('id');
                $attachToGroup = GolfTournamentPlayer::where('tournament_id', $golf->golf_tournament_id)
                    ->select('golf_tournament_players.*', 'golf_players.pos as ppos')
                    ->with('player')
                    ->join('golf_players', 'golf_players.id', '=', 'golf_tournament_players.player_id')
                    ->orderBy('ppos', 'asc')
                    ->offset($start)->limit($count);
                if ($golf->type === 'prize') {
                    //DO NOT attach amateurs to prize
                    $attachToGroup = $attachToGroup->where('amateur', 0);
                    //                        dd($attachToGroup->toSql());
                }
                $attachToGroup = $attachToGroup->get();
                $i = 0;
                foreach ($attachToGroup as $gtp) {
                    $i++;
                    $players[$gtp->id] = ['group_number' => $groupNumber];
                }
                $start = $start + $count;
                $attachToGroup = null;
            } catch (\Exception $e) {
                \Log::error($e->getTraceAsString());
                \Log::error($e->getTraceAsString());
            }
        }
        $golf->grouped_tournament_players()->detach();
        $golf->grouped_tournament_players()->attach($players);
        $golf->refresh();
        return true;
    }

    public function has_groups($golf)
    {
        return ($golf->grouped_tournament_players->count() > 0);
    }

    public function lock($golf)
    {
        $data = ['locked' => true];
        return $this->_golfRepo->update($golf->id, $data);
    }

    public function unlock($golf)
    {
        $data = ['locked' => false];
        return $this->_golfRepo->update($golf->id, $data);
    }

    public function _split_ratings($totalNumber, $numberOfGroups)
    {
        $group = [];
        try {
            switch ($numberOfGroups) {
                case 1:
                    $group = [
                        1 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.1.1', '1'))),
                    ];
                    break;
                case 2:
                    $group = [
                        1 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.2.1', '0.4'))),
                        2 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.2.2', '0.6'))),
                    ];
                    break;
                case 3:
                    $group = [
                        1 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.3.1', '0.2'))),
                        2 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.3.2', '0.35'))),
                        3 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.3.3', '0.45'))),
                    ];
                    break;
                case 4;
                    $group = [
                        1 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.4.1', '0.08'))),
                        2 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.4.2', '0.20'))),
                        3 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.4.3', '0.32'))),
                        4 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.4.4', '0.40'))),
                    ];
                    break;
                case 5;

                    $group = [
                        1 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.5.1', '0.06'))),
                        2 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.5.2', '0.16'))),
                        3 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.5.3', '0.20'))),
                        4 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.5.4', '0.26'))),
                        5 => intval(floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.5.5', '0.32'))),
                    ];
                    break;
                case 6;

                    $group = [
                        1 => floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.6.1', '0.05')),
                        2 => floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.6.2', '0.10')),
                        3 => floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.6.3', '0.15')),
                        4 => floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.6.4', '0.20')),
                        5 => floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.6.5', '0.25')),
                        6 => floor($totalNumber * GolfHelper::get_golf_distribution_config('golf.distribution.6.6', '0.25')),
                    ];

                    break;
            }

            if (array_sum($group) < $totalNumber) {
                $group[1] = $group[1] + ($totalNumber - array_sum($group));
            }
            if (array_sum($group) > $totalNumber) {
                $group[1] = $group[1] - (array_sum($group) - $totalNumber);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }

        return $group;
    }

    public function load_handicaps(&$golf)
    {
        $handicaps = [];
        foreach ($golf->grouped_tournament_players as $gtp) {
            try {
                if ($gtp->pivot->handicap) {
                    $handicaps[$gtp->id] = $gtp->pivot->handicap;
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $handicaps;
    }

    public function get_grouped_player(&$golf, $tournamentPlayerId = null)
    {
        try {
            $groupedPlayers = $golf->grouped_tournament_players;
            if ($tournamentPlayerId) {
                return $groupedPlayers->where('id', $tournamentPlayerId)->first();
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return null;
    }

    public function play_prizes(&$golf)
    {
        try {
            $golfEntryProvider = resolve(GolfEntryProvider::class);
            foreach ($golf->entries as &$entry) {
                $golfEntryProvider->play_prizes($entry, $golf->toward_score);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }

    public function play_trackmeet(&$golf)
    {
        try {
            $golfEntryProvider = resolve(GolfEntryProvider::class);
            foreach ($golf->entries as &$entry) {
                $golfEntryProvider->play_trackmeet($entry, $golf->toward_score);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }

    //SORTER
    private function _sort_entry_pos(&$usersEntries, $sort = 'total', $numOfWinners = 0)
    {
        try {
            $numOfWinners = ($sort === 'total') ? $numOfWinners : 999999;
            usort($usersEntries, function ($a, $b) use ($sort) {
                if ($sort == 'prize_total') {
                    return $b[$sort] <=> $a[$sort];
                }
                return $a[$sort] <=> $b[$sort];
            });
            $total = false;
            $pos = 0;
            $realPos = 0;
            $tie = 0;
            foreach ($usersEntries as &$userEntry) {
                if ($userEntry && $userEntry['status'] === 'active') {
                    $realPos++;
                    if ($total != $userEntry[$sort]) {
                        $tie = 0;
                        $pos = $realPos;
                    } else {
                        $tie++;
                        $pos = $realPos - $tie;
                    }
                    $userEntry['pos'] = $pos;
                    $total = $userEntry[$sort];
                } else {
                    $userEntry['pos'] = null;
                }
                $userEntry['pool_winner'] = ($userEntry['pos'] && $userEntry['pos'] <= $numOfWinners);
            }

            //Do tie:
            $tie = null;
            foreach ($usersEntries as $i => &$userEntry) {
                try {
                    $curPos = $userEntry['pos'];
                    if ($tie === $curPos) {
                        $userEntry['tpos'] = "<small>T</small>" . $userEntry['pos'];
                        if (isset($usersEntries[$i - 1])) {
                            $usersEntries[$i - 1]['tpos'] = "<small>T</small>" . $usersEntries[$i - 1]['pos'];
                        }
                    } else {
                        $userEntry['tpos'] = $userEntry['pos'];
                    }
                    $tie = $curPos;
                } catch (\Exception $e) {
                    \Log::error($e);
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    private function _sort_player_pos(&$players, $sort = 'total', $numOfWinners = 0)
    {
        try {
            $numOfWinners = ($sort === 'total') ? $numOfWinners : 999999;
            usort($players, function ($a, $b) use ($sort) {
                if ($sort == 'prize_total') {
                    return $b[$sort] <=> $a[$sort];
                }
                return $a[$sort] <=> $b[$sort];
            });
            $total = false;
            $pos = 0;
            $realPos = 0;
            $tie = 0;
            foreach ($players as &$player) {
                if ($player && $player['status'] === 'Active') {
                    $realPos++;
                    if ($total != $player[$sort]) {
                        $tie = 0;
                        $pos = $realPos;
                    } else {
                        $tie++;
                        $pos = $realPos - $tie;
                    }
                    $player['pos'] = $pos;
                    $total = $player[$sort];
                } else {
                    $player['pos'] = null;
                }
                //				    $player['pool_winner'] = ($player['pos'] && $player['pos'] <= $numOfWinners);
            }

            //Do tie:
            $tie = null;
            foreach ($players as $i => &$player) {
                try {
                    $curPos = $player['pos'];
                    if ($tie === $curPos) {
                        $player['tpos'] = "<small class='player-tie-t'>T</small>" . $player['pos'];
                        if (isset($players[$i - 1])) {
                            $players[$i - 1]['tpos'] = "<small class='player-tie-t'>T</small>" . $players[$i - 1]['pos'];
                        }
                    } else {
                        $player['tpos'] = $player['pos'];
                    }
                    $tie = $curPos;
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    \Log::error($e->getTraceAsString());
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
    }
    //SIMULATION

}
