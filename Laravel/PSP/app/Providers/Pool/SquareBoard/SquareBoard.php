<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 11:35 AM
 */

namespace App\Providers\Pool\SquareBoard;

use App\Helpers\GameHelper;
use App\Helpers\PoolHelper;
use App\Helpers\SquareHelper;
use App\Helpers\SiteHelper;
use App\Models\Config;
use App\Models\User;
use App\Providers\Pool\SquareBoard\Square\Square;
use App\Providers\Pool\SquareBoard\SquareBoardNumber\SquareBoardNumber;

class SquareBoard
{

    private $_squareBoardRepo;
    private $_gameRepo;

    public function __construct(
        \App\Repositories\SquareBoardRepository $squareBoardRepo,
        \App\Repositories\GameRepository $gameRepo,
        \App\Repositories\SquareRepository $squareRepo
    ) {
        $this->_squareBoardRepo = $squareBoardRepo;
        $this->_gameRepo = $gameRepo;
    }

    public function get($id, $filters = [], $queryOnly = false)
    {
        return $this->_squareBoardRepo->get($id, $filters, $queryOnly);
    }

    public function can_user_pick(&$squareBoard, $squareCount = 0)
    {
        return (($squareBoard->max_per_user - $squareCount) > 0);
    }

    public function create_blank($pool, $boardData)
    {
        $square = resolve('App\Providers\Pool\SquareBoard\Square\Square');
        $squareBoardNumber = resolve('App\Providers\Pool\SquareBoard\SquareBoardNumber\SquareBoardNumber');
        $config = [
            'win_type' => 'final',
            'dd_win_type' => null
        ];
        $data = [
            'x_team_id' => null,
            'y_team_id' => null,
            'sets' => 1,
            'hometeam_name' => $boardData['hometeam_name'],
            'awayteam_name' => $boardData['awayteam_name'],
            'label' => $boardData['label'],
            'motd' => "Winning numbers have not been generated yet",
            'locked' => false,
            'type' => "final",
            'max_per_user' => 1,
            'config' => "",
            'payout_config' => [],
            'allow_picks' => true,
            'active' => 0,
            'game_id' => null,
            'pool_id' => $pool->id,
            'win_type' => 'final',
        ];
        $squareBoard = $this->_squareBoardRepo->create($data);
        for ($x = 1; $x <= 10; $x++) {
            for ($y = 1; $y <= 10; $y++) {
                $square->create($squareBoard, $x, $y);
            }
        }
        return $squareBoard;
    }

    public function create_ncaa_game($pool, $boardData)
    {
        try {
            $squareBoard = $this->create_blank($pool, $boardData);
            $gameServiceProvider = resolve('\App\Providers\Game\Game');
            //            $roundId = $gameServiceProvider->active_round_ncaa();

            //            if($roundId !== false){
            $updateData['hometeam_name'] = "Winning Team";
            $updateData['awayteam_name'] = "Losing Team";
            $updateData['label'] = "NCAA Championship";
            $squareBoard->payout_config = $this->generate_default_payout();

            $squareBoard->save();
            $this->_squareBoardRepo->update($squareBoard->id, $updateData);
            $squareBoard->refresh();
            return $squareBoard;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return null;
        }
        //            }
    }

    public function next_game($sport, $league)
    {

        //            $gameServiceProvider =  new \App\Providers\Game\Game();
        //TODO: Make this dynamic from admin

    }

    public function leave($id, $userId)
    {
        $squareBoard = $this->get($id);
        $squareBoard->load(['squares']);
        $squareProvider = resolve('App\Providers\Pool\SquareBoard\Square\Square');
        foreach ($squareBoard->squares->where('user_id', $userId) as $square) {
            //  dump($square->user_id, "QUIT");
            $squareProvider->update(
                $square->id,
                [
                    'label' => '',
                    'short_label' => '',
                    'user_id' => null,
                    'note' => '',
                ]
            );
        }
        return true;
    }

    public function create($pool, $game)
    {
        try {
            $square = resolve('App\Providers\Pool\SquareBoard\Square\Square');
            $squareBoardNumber = resolve('App\Providers\Pool\SquareBoard\SquareBoardNumber\SquareBoardNumber');
            $config = [
                'win_type' => 'final',
                'dd_win_type' => null
            ];
            $homeTeam = ($game) ? $game->home_team : null;
            $awayTeam = ($game) ? $game->away_team : null;
            //                dump($homeTeam, $awayTeam);
            $data = [
                'x_team_id' => ($game) ? $game->hometeam_id : null,
                'y_team_id' => ($game) ? $game->awayteam_id : null,
                'hometeam_name' => ($homeTeam) ? $homeTeam->name : null,
                'awayteam_name' => ($awayTeam) ? $awayTeam->name : null,
                'game_id' => ($game) ? $game->id : null,
                'label' => $pool->tournament_name,
                'sets' => 1,
                'motd' => "Winning numbers have not been generated yet",
                'locked' => false,
                'type' => "final",
                'max_per_user' => 1,
                'config' => json_encode($config),
                'allow_picks' => true,
                'active' => 0,
                'pool_id' => $pool->id,
                'win_type' => 'final',

            ];
            $squareBoard = $this->_squareBoardRepo->create($data);
            for ($x = 1; $x <= 10; $x++) {
                for ($y = 1; $y <= 10; $y++) {
                    $square->create($squareBoard, $x, $y);
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            $squareBoard = null;
        }

        return $squareBoard;
    }

    public function update(&$squareBoard, $squareBoardUpdateData)
    {
        return $this->_squareBoardRepo->update($squareBoard->id, $squareBoardUpdateData);
    }

    public function find_by_pool_id($poolId)
    {
        return $this->_squareBoardRepo->find_by_pool_id($poolId);
    }

    public function lock_automatically(&$squareBoard, $notify = false)
    {
        if (!$this->is_locked($squareBoard) && $this->has_numbers($squareBoard)) {
            //                $this->generate_automatically($squareBoard);
            return $this->lock($squareBoard, $notify);
        }
        return true;
    }

    public function generate_automatically(&$squareBoard)
    {
        if (!$this->has_numbers($squareBoard) && $this->can_be_locked($squareBoard)) {
            $squareBoardNumberRepo = resolve('App\Repositories\SquareBoardNumberRepository');
            $squareBoardNumberRepo->generate($squareBoard);
        }
        return true;
    }

    public function is_locked($squareBoard, $lock = true)
    {
        return $this->_squareBoardRepo->is_locked($squareBoard);
    }

    public function lock(&$squareBoard, $notify = false)
    {
        if ($notify) {
            //                SiteHelper::add_notices('This board has been locked due to game start time!', 'info');
        }
        return $this->_squareBoardRepo->lock($squareBoard);
    }

    public function unlock(&$squareBoard)
    {
        return $this->_squareBoardRepo->unlock($squareBoard);
    }

    public function read_only($squareBoard)
    {
        return ($squareBoard->numbers->count());
    }

    public function can_be_locked($squareBoard)
    {
        if ($game = $this->get_game($squareBoard)) {
            $gameRepository = resolve('App\Repositories\GameRepository');
            return $gameRepository->game_started($game);
        }
        return null;
    }

    public function get_game($squareBoard)
    {
        $gameRepository = resolve('App\Repositories\GameRepository');

        return $gameRepository->find($squareBoard->game_id);
    }

    public function redirect_locked($squareBoard, $redirectTo = null)
    {
        $redirectTo = ($redirectTo) ?? "/pool/" . $squareBoard->pool_id . "/squareboard/" . $squareBoard->id;
        SiteHelper::add_notices("Sorry, this pool is locked!", 'danger');
        return redirect($redirectTo);
    }

    public function has_numbers($squareBoard)
    {
        return $this->_squareBoardRepo->has_numbers($squareBoard);
    }

    public function generate_numbers(&$squareBoard)
    {
        $squareBoardNumberRepo = resolve('App\Repositories\SquareBoardNumberRepository');
        return $squareBoardNumberRepo->generate($squareBoard);
    }

    public function get_winning_coordinates($scores, $numbers)
    {
        try {
            $coordinates = SquareHelper::coordinates_by_score($scores['hometeam'], $scores['awayteam']);
            $decodedHome = SquareHelper::flip_numbers($numbers->hometeam_numbers);
            $decodedAway = SquareHelper::flip_numbers($numbers->awayteam_numbers);
            $square = SquareHelper::winning_square_coordinates_by_score_coordinates($coordinates['hometeam'], $coordinates['awayteam'], $decodedHome, $decodedAway);
            return $square;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return null;
        }
    }

    public function get_winning_square(&$squareBoard, $quarter = 'finalscore')
    {
        try {
            $squareBoardNumberRepo = resolve('App\Repositories\SquareBoardNumberRepository');
            $set = $squareBoardNumberRepo->get_by_quarter($squareBoard, $quarter);
            if (!isset($set->hometeam_numbers)) return false;
            $game = $this->get_game($squareBoard);
            $gameRepository = resolve('App\Repositories\GameRepository');
            $score = $gameRepository->get_score($game, $quarter, true);
            $coordinatedScore =  \App\Helpers\SquareHelper::coordinates_by_score($score['hometeam_score'], $score['awayteam_score']);
            $squareService = resolve('\App\Providers\Pool\SquareBoard\Square\Square');
            $thisNumX = $squareBoardNumberRepo->decode($set->hometeam_numbers);
            $thisNumY = $squareBoardNumberRepo->decode($set->awayteam_numbers);

            $winData = [
                'hometeam_numbers' => [
                    'indexed' => $thisNumX,
                    'scored' =>  array_flip($thisNumX),
                ],
                'awayteam_numbers' => [
                    'indexed' => $thisNumY,
                    'scored' => array_flip($thisNumY),
                ],
                'quarter_count' => $set->count,
                'quarter_label' => $set->label
            ];

            $homeTeamCoordinate = $winData['hometeam_numbers']['scored'][$coordinatedScore['hometeam']];
            $awayTeamCoordinate = $winData['awayteam_numbers']['scored'][$coordinatedScore['awayteam']];
            $wt = $squareService->get_by_coordinates($squareBoard, $homeTeamCoordinate, $awayTeamCoordinate);
            if (!$wt) return null;
            $data  = [
                'square_id' => $wt->id,
                'squareboard_id' => $squareBoard->id,
                'coordinate_x' => $homeTeamCoordinate,
                'coordinate_y' => $awayTeamCoordinate,
                'home_score' => $score['hometeam_score'],
                'away_score' => $score['awayteam_score'],
                'win_type' => $quarter,
            ];
            return $data;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return null;
        }
        //
        //            switch($quarter){
        //
        //                case 'q1':
        //
        //                break;
        //
        //                case 'q2':
        //
        //                break;
        //                case 'q3':
        //
        //                break;
        //                case 'finalscore':
        //
        //                break;
        //            }
    }

    public function quarter_win(&$squareBoard, $quarter = 'q1')
    {
    }

    private function _payout_id_to_string($id, $type = 'final')
    {
        $payouts = [
            0 => ['final' => 'pre'],
            1 => ['final' => 'first'],
            2 => ['final' => 'second'],
            3 => ['final' => 'third'],
            4 => ['final' => 'fourth'],
            5 => ['final' => 'semi', 'half' => 'semi_half'],
            6 => ['final' => 'championship', 'half' => 'championship_half']
        ];
        return $payouts[$id][$type];
    }
    private function _payout_to_id($short)
    {
        $payouts = [
            'pre' => 0,
            'first' => 1,
            'second' => 2,
            'third' => 3,
            'fourth' => 4,
            'semi' => 5,
            'championship' => 6
        ];
        return $payouts[$short];
    }

    public function get_ncaa_games_results($squareBoard)
    {
        $rows = [];
        try {
            $sbId = $squareBoard->id;
            $ncaaGames  = $this->_gameRepo->get([
                'sport' => 'basketball',
                'league' => 'college',
                'season_year' => SiteHelper::conf('ncaa.season', 'text', '2019/2020'),
            ], true)->with(
                [
                    'ncaa_winners' => function ($query) use ($sbId) {
                        $query->with('square')->whereHas('square', function ($query2) use ($sbId) {
                            $query2->where('squareboard_id', $sbId);
                        });
                    },
                    'ncaa_winners.square'
                ]
            )->orderBy('start', 'asc')->get();
            //            dd($/ncaaGames);
            foreach ($ncaaGames as $game) {
                //let's process half
                if (($game->round_id === 5 || $game->round_id === 6) && $this->round_enabled($squareBoard, $game->round_id, 'half') && $game->is_final) {
                    $rows[] = $this->_build_ncaa_result_row($game, 'half');
                    //                    $halfGameWinner =$game->ncaa_winners->first(function($item){
                    //                            return $item->win_type == 'half';
                    //                    });
                    //                    $loseWinTeams = GameHelper::get_lose_win_teams($game,'q2');
                    //                    $rows[] = [
                    //                        'round_id' => $game->round_id,
                    //                        'win_type' => 'half',
                    //                        'round_label' => __c('squareboard.ncaa.results.semi_half_label', "Semi Half-Time"),
                    //                        'round_short_label' => __c('squareboard.ncaa.results.semi_half_mobile_label', "Semi H/T"),
                    //                        'winner' => ($halfGameWinner) ? $halfGameWinner->square->label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                    //                        'winner_short_label' => ($halfGameWinner) ? $halfGameWinner->square->short_label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                    //                        'square' => ($halfGameWinner) ? $halfGameWinner->x.",".$halfGameWinner->y : SquareHelper::string_coordinates_by_score($loseWinTeams['winning']['score'], $loseWinTeams['losing']['score']),
                    //                        'winning_team' => $loseWinTeams['winning']['team'],
                    //                        'losing_team' => $loseWinTeams['losing']['team'],
                    //                        'winning_score' => $loseWinTeams['winning']['score'],
                    //                        'losing_score' => $loseWinTeams['losing']['score'],
                    //                    ];
                }
                //                if($game->round_id === 6 && $this->round_enabled($squareBoard, $game->round_id, 'half')){
                //                    $halfGameWinner =$game->ncaa_winners->first(function($item){
                //                        return $item->win_type == 'half';
                //                    });
                //                    $loseWinTeams = GameHelper::get_lose_win_teams($game,'q2');
                //                    $rows[] = [
                //                        'round_id' => $game->round_id,
                //                        'win_type' => 'half',
                //                        'round_label' => __c('squareboard.ncaa.results.championship_half_label', "Championship Half/Time"),
                //                        'round_short_label' => __c('squareboard.ncaa.results.championship_half_mobile_label', "Championship H/T"),
                //                        'winner' => ($halfGameWinner) ? $halfGameWinner->square->label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                //                        'winner_short_label' => ($halfGameWinner) ? $halfGameWinner->square->short_label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                //                        'square' => ($halfGameWinner) ? $halfGameWinner->x.",".$halfGameWinner->y : SquareHelper::string_coordinates_by_score($loseWinTeams['winning']['score'], $loseWinTeams['losing']['score']),
                //                        'winning_team' => $loseWinTeams['winning']['team'],
                //                        'losing_team' => $loseWinTeams['losing']['team'],
                //                        'winning_score' => $loseWinTeams['winning']['score'],
                //                        'losing_score' => $loseWinTeams['losing']['score'],
                //                    ];
                //                }
                if ($this->round_enabled($squareBoard, $game->round_id, 'final')) {
                    $rows[] = $this->_build_ncaa_result_row($game, 'final');
                    //                    $roundPrettyName = $this->_payout_id_to_string($game->round_id, 'final');
                    //                    $gameWinner =$game->ncaa_winners->first(function($item){
                    //                        return $item->win_type == 'final';
                    //                    });
                    //                    $loseWinTeams = GameHelper::get_lose_win_teams($game,'totalscore');
                    //                    $rows[] = [
                    //                        'round_id' => $game->round_id,
                    //                        'win_type' => 'final',
                    //                        'round_label' => __c('squareboard.ncaa.results.'.$roundPrettyName.'_label', $roundPrettyName),
                    //                        'round_short_label' => __c('squareboard.ncaa.results.'.$roundPrettyName.'_mobile_label', $roundPrettyName),
                    //                        'winner' => ($gameWinner) ? $gameWinner->square->label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                    //                        'winner_short_label' => ($gameWinner) ? $gameWinner->square->short_label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                    //                        'square' => ($gameWinner) ? $gameWinner->x . "," . $gameWinner->y : SquareHelper::string_coordinates_by_score($loseWinTeams['winning']['score'], $loseWinTeams['losing']['score']),
                    //                        'winning_team' => $loseWinTeams['winning']['team'],
                    //                        'losing_team' => $loseWinTeams['losing']['team'],
                    //                        'winning_score' => $loseWinTeams['winning']['score'],
                    //                        'losing_score' => $loseWinTeams['losing']['score'],
                    //                    ];
                }
            }
            return $rows;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return null;
        }
    }

    private function _build_ncaa_result_row(&$game, $winType = 'final')
    {
        try {
            $gameWinner = $game->ncaa_winners->first(function ($item) use ($winType) {
                return $item->win_type == $winType;
            });

            $quarter = ($winType === 'final') ? "totalscore" : "q2";
            $loseWinTeams = GameHelper::get_lose_win_teams($game, $quarter);
            $label = $this->_payout_id_to_string($game->round_id, $winType);
            $slugged = str_replace(' ', '_', $label);
            $data = [
                'is_not_started' => $game->is_not_started,
                'is_final' => $game->is_final,
                'round_id' => $game->round_id,
                'win_type' => $winType,
                'round_label' => __c('squareboard.ncaa.results.' . $slugged . '_label', $label),
                'round_short_label' => __c('squareboard.ncaa.results.' . $slugged . '_mobile_label', $label),
                'winner' => ($gameWinner) ? $gameWinner->square->label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                'winner_short_label' => ($gameWinner) ? $gameWinner->square->short_label : __c('squareboard.ncaa.results.no_winner_label', "-"),
                'square' => ($gameWinner) ? $gameWinner->x . "," . $gameWinner->y : SquareHelper::string_coordinates_by_score($loseWinTeams['winning']['score'], $loseWinTeams['losing']['score']),
                'winning_team' => $loseWinTeams['winning']['team'],
                'losing_team' => $loseWinTeams['losing']['team'],
                'winning_score' => $loseWinTeams['winning']['score'],
                'losing_score' => $loseWinTeams['losing']['score'],
            ];
            return $data;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return null;
        }
    }

    public function payouts_by_win(&$squareBoard, $config)
    {
        $winners = SquareHelper::get_ncaa_winners($squareBoard);
        $cummulativeWinners = [];

        foreach ($winners as $winner) {
            try {
                $game = $winner->game;
                $winner->round_id = $game->round_id;
                $winner->save();
                $roundId = $winner->round_id;
                if (!isset($cummulativeWinners[$winner->square->label])) {
                    $cummulativeWinners[$winner->square->label] = [];
                }
                if (!isset($cummulativeWinners[$winner->square->label][$roundId][$winner->win_type])) {
                    $cummulativeWinners[$winner->square->label][$roundId][$winner->win_type] = 0;
                }
                $cummulativeWinners[$winner->square->label][$roundId][$winner->win_type]++;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        $payouts = $squareBoard->payout_config;
        //            $configJson = json_decode($config->value, 1);
        $payoutsByWins = [];
        foreach ($cummulativeWinners as $dn => $wins) {
            foreach ($wins as $id => $subWin) {
                foreach ($subWin as $type => $qty) {
                    try {
                        $group = $this->_payout_id_to_string($id, $type);

                        if ($group && $payouts->payouts->{$group}->enabled) {
                            $payout = isset($payouts->payouts->{$group}) ? $payouts->payouts->{$group} : 0;

                            if (!isset($payoutsByWins[$dn])) {
                                $payoutsByWins[$dn] = [];
                            }
                            if (!isset($payoutsByWins[$dn][$id])) {
                                $payoutsByWins[$dn][$id] = [];
                            }

                            if (!isset($payoutsByWins[$dn][$id][$type])) {
                                $payoutsByWins[$dn][$id][$type] = 0;
                            }

                            if (!isset($payoutsByWins[$dn]['hits'])) {
                                $payoutsByWins[$dn]['hits'] = 0;
                            }
                            if (!isset($payoutsByWins[$dn]['total'])) {
                                $payoutsByWins[$dn]['total'] = 0;
                            }
                            $payoutsByWins[$dn]['hits'] = $payoutsByWins[$dn]['hits'] + (int)$qty;
                            $thisWin = ((int)$qty * (int)$payout->sum);

                            $payoutsByWins[$dn][$id][$type] = ($payoutsByWins[$dn][$id][$type] + $qty);
                            $payoutsByWins[$dn]['total'] = $payoutsByWins[$dn]['total'] +  $thisWin;
                        }
                    } catch (\Exception $e) {
                        //                        dd($e->getMessage(), $group, $payouts);
                    }
                }
            }
        }
        uasort($payoutsByWins, function ($a, $b) {
            return $b['total'] <=> $a['total'];
        });

        $prettyGames = [];
        //            foreach($configJson as $id => $data){
        //                $payoutGroup  =$data['payout_group'];
        //                if(!isset($prettyGames[$payoutGroup])){
        //                    $prettyGames[$payoutGroup] = [
        //                        'ids' => [],
        //                        'label' => $data['psp_label'],
        //                        'order' => $data['order']
        //                    ];
        //                    $prettyGames[$payoutGroup]['ids'][] = $id;
        //                }
        //            }
        $data = [
            'winners' => $cummulativeWinners,
            'payouts' => $payouts,
            //                    'config' => $configJson,
            'payoutByWins' => $payoutsByWins,
            'prettyGames' => $prettyGames,
        ];
        return $data;
    }

    public function get_vue($sqB, $numbers, $pickUsers = false, $encode = true)
    {

        try {
            $squareBoard = $this->get($sqB->id);
            $squareBoard->load([
                'pool',
                'pool.users',
                'pool.users.display_names',
                'game',
                'game.home_team',
                'game.away_team',
                'numbers',
                'squares' => function ($query) {
                    $query->orderBy('squares.y', 'asc')->orderBy('squares.x', 'asc');
                },
                'squares.user',
                'squares.square_wins',
                'squares.square_wins.square',
                'squares.square_wins.square.user',
                'squares.square_wins.number',
            ]);
            //            $cleanBoard = \App\Models\SquareBoard::with([
            //                ''
            //            ])
            $cleanBoard = clone $squareBoard;

            $userId = \Auth::user()->id;
            $comish = $squareBoard->pool->is_commissioner($userId);
            unset($cleanBoard->pool);
            $poolUsers = $squareBoard->pool->users;
            $thisUser = null;
            $openSquareCount = 100;
            foreach ($cleanBoard->squares as &$square) {
                $square->pending = false;
                if ($square->user_id) {
                    $openSquareCount--;
                }
            }
            $poolUserArray = [];
            foreach ($poolUsers as &$user) {
                $userItem = [
                    'current_pick_count' => $squareBoard->squares->where('user_id', $user->id)->count(),
                    'picks_remaining' => intval($squareBoard->max_per_user) - $user->current_pick_count,
                    'email' => $user->email,
                    'first' => $user->first,
                    'global_display_name' => $user->global_display_name,
                    'id' => $user->id,
                    'is_admin' => $user->is_admin,
                    'last' => $user->last,
                    'name' => $user->name,
                ];
                //                $user->current_pick_count = $squareBoard->squares->where('user_id',$user->id)->count();
                //                $user->picks_remaining = intval($squareBoard->max_per_user) - $user->current_pick_count;
                $cleanDisplayNames = [];
                foreach ($user->display_names as $k => $dn) {
                    if ($dn->pool_id === null || $dn->pool_id === $squareBoard->pool_id) {
                        $displayName = $dn->toArray();
                        $displayName['pool_only'] = ($dn->pool_id !== null);
                        $displayName['selected'] = boolval($displayName['primary']);
                        array_push($cleanDisplayNames, $displayName);
                    }
                }
                usort($cleanDisplayNames, function ($a, $b) {
                    return $a['display_name'] <=> $b['display_name'];
                });
                //                $cleanDisplayNames

                $userItem['display_names'] = $cleanDisplayNames;
                if ($userItem['id'] == $userId) {
                    $thisUser = $userItem;
                }
                $poolUserArray[] = $userItem;
            }
            $board = [
                'board' => $cleanBoard,
                'numbers' => $numbers,
                'users' => $poolUserArray,
                'pick_users' => $pickUsers,
                'user' =>   $thisUser,
                'commissioner' => $comish,
                'open_squares' => $openSquareCount,
            ];
            return ($encode) ? json_encode($board) : $board;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return null;
        }
    }

    public function generate_default_payout($payouts = null)
    {
        try {
            $payout = ['payouts' => [
                'pre' => [
                    'gameCount' => 4,
                    'enabled' => ($payouts && isset($payouts->pre->enabled)) ? $payouts->pre->enabled : __conf('ncaa.enable_pre_default', 'boolean', false),
                    'sum' => ($payouts && isset($payouts->pre->sum)) ? $payouts->pre->sum : 0,
                    'total' => ($payouts && isset($payouts->pre->total)) ? $payouts->pre->total : 0,
                    'name' => __c('squareboard.payout.content.pre_title', "Prequalifying", [], false),
                    'short' => __c('squareboard.payout.content.pre_short_title', "pre", [], false),
                    'round_id'  => 0,
                ],
                'first' => [
                    'gameCount' => 32,
                    'enabled' => ($payouts && isset($payouts->first->enabled)) ? $payouts->first->enabled : __conf('ncaa.enable_1st_default', 'boolean', true),
                    'sum' => ($payouts && isset($payouts->first->sum)) ? $payouts->first->sum : 0,
                    'total' => ($payouts && isset($payouts->first->total)) ? $payouts->first->total : 0,
                    'name' => __c('squareboard.payout.content.first_title', "1st", [], false),
                    'short' => __c('squareboard.payout.content.first_short_title', "1", [], false),
                    'round_id'  => 1,
                ],
                'second' => [
                    'gameCount' => 16,
                    'enabled' => ($payouts && isset($payouts->second->enabled)) ? $payouts->second->enabled : __conf('ncaa.enable_2nd_default', 'boolean', true),
                    'sum' => ($payouts && isset($payouts->second->sum)) ? $payouts->second->sum : 0,
                    'total' => ($payouts && isset($payouts->second->total)) ? $payouts->second->total : 0,
                    'name' => __c('squareboard.payout.content.second_title', "2nd", [], false),
                    'short' => __c('squareboard.payout.content.second_short_title', "2", [], false),
                    'round_id'  => 2
                ],
                'third' => [
                    'gameCount' => 8,
                    'enabled' => ($payouts && isset($payouts->third->enabled)) ? $payouts->third->enabled : __conf('ncaa.enable_3rd_default', 'boolean', true),
                    'sum' => ($payouts && isset($payouts->third->sum)) ? $payouts->third->sum : 0,
                    'total' => ($payouts && isset($payouts->third->total)) ? $payouts->third->total : 0,
                    'name' => __c('squareboard.payout.content.third_title', "3rd", [], false),
                    'short' => __c('squareboard.payout.content.third_short_title', "3", [], false),
                    'round_id'  => 3

                ],
                'fourth' => [
                    'gameCount' => 4,
                    'enabled' => ($payouts && isset($payouts->fourth->enabled)) ? $payouts->fourth->enabled : __conf('ncaa.enable_4th_default', 'boolean', true),
                    'sum' => ($payouts && isset($payouts->fourth->sum)) ? $payouts->fourth->sum : 0,
                    'total' => ($payouts && isset($payouts->fourth->total)) ? $payouts->fourth->total : 0,
                    'name' => __c('squareboard.payout.content.fourth_title', "4th", [], false),
                    'short' => __c('squareboard.payout.content.fourth_short_title', "4", [], false),
                    'round_id'  => 4,
                ],
                'semi_half' => [
                    'gameCount' => 2,
                    'enabled' => ($payouts && isset($payouts->semi_half->enabled)) ? $payouts->semi_half->enabled : __conf('ncaa.enable_semi_half_default', 'boolean', false),
                    'sum' => ($payouts && isset($payouts->semi_half->sum)) ? $payouts->semi_half->sum : 0,
                    'total' => ($payouts && isset($payouts->semi_half->total)) ? $payouts->semi_half->total : 0,
                    'name' => __c('squareboard.payout.content.semi_half_title', "Semi Half-Time", [], false),
                    'short' => __c('squareboard.payout.content.semi_half_short_title', "Semi H/T", [], false),
                    'round_id'  => 5
                ],
                'semi' => [
                    'gameCount' => 2,
                    'enabled' => ($payouts && isset($payouts->semi->enabled)) ? $payouts->semi->enabled : __conf('ncaa.enable_semi_final_default', 'boolean', true),
                    'sum' => ($payouts && isset($payouts->semi->sum)) ? $payouts->semi->sum : 0,
                    'total' => ($payouts && isset($payouts->semi->total)) ? $payouts->semi->total : 0,
                    'name' => __c('squareboard.payout.content.semi_title', "Semi Final", [], false),
                    'short' => __c('squareboard.payout.content.semi_short_title', "Semi", [], false),
                    'round_id'  => 5
                ],
                'championship_half' => [
                    'gameCount' => 1,
                    'enabled' => ($payouts && isset($payouts->championship_half->enabled)) ? $payouts->championship_half->enabled : __conf('ncaa.enable_final_half_default', 'boolean', false),
                    'sum' => ($payouts && isset($payouts->championship_half->sum)) ? $payouts->championship_half->sum : 0,
                    'total' => ($payouts && isset($payouts->championship_half->total)) ? $payouts->championship_half->total : 0,
                    'name' => __c('squareboard.payout.content.final_half_title', "Championship Half-Time", [], false),
                    'short' => __c('squareboard.payout.content.final_half_short_title', "Final H/T", [], false),
                    'round_id'  => 6
                ],
                'championship' => [
                    'gameCount' => 1,
                    'enabled' => ($payouts && isset($payouts->championship->enabled)) ? $payouts->championship->enabled : __conf('ncaa.enable_final_default', 'boolean', true),
                    'sum' => ($payouts && isset($payouts->championship->sum)) ? $payouts->championship->sum : 0,
                    'total' => ($payouts && isset($payouts->championship->total)) ? $payouts->championship->total : 0,
                    'name' => __c('squareboard.payout.content.final_title', "Championship", [], false),
                    'short' => __c('squareboard.payout.content.final_short_title', "Final", [], false),
                    'round_id'  => 6
                ]
            ]];
            return $payout;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return null;
        }
    }

    public function create_roster_for_export(&$squareBoard, $users)
    {
        $pool = $squareBoard->pool;
        $rows = [];
        $rows[] = [
            __c('squareboard.roster.content.table_head_confirm', "Confirm"),
            __c('squareboard.roster.content.table_head_username', "User Name"),
            __c('squareboard.roster.content.table_head_email', "Email"),
            __c('squareboard.roster.content.table_head_phone', "Phone"),
            __c('squareboard.roster.content.table_head_display_name', "Display Name"),
            __c('squareboard.roster.content.table_head_entries', "Entries"),
            __c('squareboard.roster.content.table_head_commissioner', "Commissioner"),
            __c('squareboard.roster.content.table_head_commissioner_note', "Commissioner Note"),
            __c('squareboard.roster.content.table_head_user_note', "User Note"),
        ];
        foreach ($users as $poolUser) {
            try {
                $uSquares = $poolUser->squares($squareBoard->id)->get();
                $squares = [];
                $sum = 0;
                foreach ($uSquares as $square) {
                    if (isset($squares[$square->label])) {
                        $squares[$square->label]++;
                    } else {
                        $squares[$square->label] = 1;
                    }
                }
                $holdArray = [];
                foreach ($squares as $label => $cnt) {
                    $sum = $sum + $cnt;
                    $holdArray[] = $label . "({$cnt})";
                }
                $comNote = PoolHelper::parse_commissioner_note($poolUser->pivot->commissioner_note);
                $rows[] = [
                    'confirmed' => ($poolUser->pivot->verified) ? 'yes' : '',
                    'username' => $poolUser->username,
                    'email' => $poolUser->email,
                    'phone' => $poolUser->phone,
                    'display_names' => implode(',', $holdArray),
                    'entries' => $sum,
                    'commissioner' => ($pool->is_commissioner($poolUser->id)) ? "yes" : "",
                    'commissioner_note' => $comNote['note'],
                    'users_note' => $poolUser->pivot->user_note,
                ];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $rows;
    }

    public function round_enabled(&$squareBoard, $roundId, $type = 'final')
    {
        try {
            $payoutConfig = $squareBoard->payout_config;
            $payouts = $payoutConfig->payouts;
            switch ($roundId) {

                case 0:
                    return $payouts->pre->enabled;
                    break;
                case 1:
                    return $payouts->first->enabled;
                    break;
                case 2:
                    return $payouts->second->enabled;
                    break;
                case 3:
                    return $payouts->third->enabled;
                    break;
                case 4:
                    return $payouts->fourth->enabled;
                    break;
                case 5:
                    if ($type === 'final') {
                        return $payouts->semi->enabled;
                    }
                    if ($type === 'half') {
                        return $payouts->semi_half->enabled;
                    }
                    break;
                case 6:
                    if ($type === 'final') {
                        return $payouts->championship->enabled;
                    }
                    if ($type === 'half') {
                        return $payouts->championship_half->enabled;
                    }
                    break;
                default:
                    return false;
                    break;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return false;
        }
    }

    public function get_projections(&$squareBoard, $numbers)
    {
        $squaresToWin = [
            'potential' => null,
            'onescore' => [],
        ];
        $winScores = [];

        try {
            $game = $squareBoard->game;
            if ($game) {
                $squares = $squareBoard->squares;
                $homeScore = $game->hometeam_totalscore;
                $awayScore = $game->awayteam_totalscore;
                $awayTeam = $game->away_team;
                $homeTeam = $game->home_team;
                $currentScores = [
                    'hometeam' => $homeScore,
                    'awayteam' => $awayScore,
                ];

                $potentialSquare = SquareHelper::get_squares_by_scores($currentScores, $numbers, $squares);
                $squaresToWin['potential'] = ($potentialSquare) ? $potentialSquare->id : null;
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 7, $awayScore, 'hometeam', false);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 3, $awayScore, 'hometeam', false);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 6, $awayScore, 'hometeam', true);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 8, $awayScore, 'hometeam', true);

                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 2, 'awayteam', true, true);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 7, 'awayteam', false);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 6, 'awayteam', true);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 8, 'awayteam', true);


                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 3, 'awayteam', false);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 7, 'awayteam', false);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 6, 'awayteam', true);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore, $awayScore + 8, 'awayteam', true);

                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 7, $awayScore, 'hometeam', false);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 2, $awayScore, 'hometeam', true, true);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 6, $awayScore, 'hometeam', true);
                $winScores[] = $this->_project_score($game->home_team, $game->away_team, $homeScore + 8, $awayScore, 'hometeam', true);

                foreach ($winScores as $winScore) {
                    $square = SquareHelper::get_squares_by_scores($winScore, $numbers, $squares);
                    if ($square) {
                        $squaresToWin['onescore'][] =
                            [
                                'id' => $square->id,
                                'team' => $winScore['team'],
                                'style' => $winScore['style'],
                                'class' => $winScore['class']
                            ];
                    }
                }

                //                $squaresToWin['team'] = ($game->ball == 'hometeam') ? $homeTeam : $awayTeam;
                //                $squaresToWin['current'] = SquareHelper::get_squares_by_scores([
                //                    'hometeam'=> $game->hometeam_totalscore,
                //                    'awayteam' => $game->awayteam_totalscore
                //                ], $numbers,$squareBoard->squares);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return $squaresToWin;
    }

    private function _project_score($home, $away, $homeScore, $awayScore, $highlight = 'hometeam', $outline = true, $dotted = false)
    {

        $subClass = "";
        if ($outline) {
            $subClass = $dotted ? "2-points" : "6-8";
        } else {
            $subClass = "3-7";
        }
        $classRoot = $highlight;

        $color = ($highlight == 'hometeam') ? $home->primary_color : $away->primary_color;
        $inverseColor = __invert_color($color);
        $style =   ($outline) ? "/*LEGACY*/  border:4px $color solid; color: $color !important; " : " /*LEGACY*/ background: " . $color . " !important; color: " . $inverseColor . " !important;";
        return [
            'hometeam' => $homeScore,
            'awayteam' => $awayScore,
            'home' => $home,
            'away' => $away,
            'team' => null,
            'style' => $style,
            'class' => $classRoot . "-" . $subClass,
        ];
    }

    public function count_entries($user)
    {
        return $user->squares->count();
    }

    public function build_named_entries($user, $isGolf = false)
    {
        $uniqueEntries = [];
        foreach ($user->squares as $entry) {
            try {
                $name = strtolower(trim($entry->label));
                if (!isset($uniqueEntries[$name])) {
                    $uniqueEntries[$name] = [
                        'label' => $entry->label,
                        'count' => 1
                    ];
                } else {
                    $uniqueEntries[$name]['count']++;
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return array_values($uniqueEntries);
    }

    public function build_live_vue($squareBoard)
    {
        $numbers = [
            'hometeam' => $squareBoard->hometeam_numbers,
            'awayteam' => $squareBoard->hometeam_numbers,
            'count' => $squareBoard->count
        ];
    }

    public function build_squareboard_squares(&$squareBoard, &$possibleWins, &$game)
    {

        $squares = [];
        $patPending = false;
        try {
            foreach ($squareBoard->squares as $square) {
                $squareId = $square->id;
                $squareClass = "cell ";
                $squareClass .= ($square->label) ? " " . md5($square->label) . " " : "";
                $squareClass .= ($square->user_id) ? " picked picked-by-" . $square->user_id . " " : " cell-empty ";
                $squareClass .= ($square->square_wins->count() && $square->user) ? ' cell-pastwinner ' : '';
                $squareClass .= ($square->square_wins->count() > 1) ? " cell-pastwiner-trophy " : "";
                $wins = [];
                foreach ($square->square_wins as $sw) {
                    $wins[] = $sw->id;
                }
                if ($game->status != 'Halftime') {
                    if ($possibleWins['potential'] == $squareId) {
                        $squareClass .= " game-current-score ";
                    }
                    $oneScores = $possibleWins['onescore'];

                    foreach ($oneScores as $oneScore) {
                        if ($squareId == $oneScore['id']) {
                            $squareClass .= " " . $oneScore['class'] . " ";
                        }
                    }
                }


                $squares[] = [
                    'id' => $squareId,
                    'x' => $square->x,
                    'y' => $square->y,
                    'wins' => count($wins), //trophy
                    'class' => $squareClass,
                    'name' => $square->label,
                    'short_name' => $square->short_label,
                    'user_id' => $square->user_id
                ];
            };
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return $squares;
    }
}
