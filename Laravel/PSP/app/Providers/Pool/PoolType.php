<?php
    /**
     * Created by PhpStorm.
     * User: mkova
     * Date: 10/12/2018
     * Time: 9:13 AM
     */

    namespace App\Providers\Pool;


    class PoolType {
        private $_repo;

        public function __construct(
            \App\Repositories\PoolTypeRepository $poolTypeRepo
        )
        {
            $this->_repo = $poolTypeRepo;
        }

        public function get(){
            return $this->_repo->get();
        }

        public function find($id){
            return $this->_repo->find($id);
        }
    }
