<?php

namespace App\Providers;

use App\Models\SiteContent;
use Illuminate\Support\ServiceProvider;

class SiteHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
//            if(!$this->siteContent)
//            $view->with('siteContent', SiteContent::all());
        });


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path('Helpers/SiteHelper.php');
    }
}
