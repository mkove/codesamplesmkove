<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 11:37 AM
 */

namespace App\Providers\Email;

use App\Helpers\SiteHelper;
use App\Mail\AdminNotice;
use App\Mail\Pool\SendPool;
use App\Mail\Pool\Golf\SendGolf;
use App\Mail\Pool\Squareboard\SendSquare;
use App\Mail\User\SendUser;
use App\Repositories\EmailContentRepository;

class EmailContentProvider
{

    private $_repo;
    public function __construct(
        EmailContentRepository $repo
    ) {
        $this->_repo = $repo;
    }

    public function find($id)
    {
        return $this->_repo->find($id);
    }

    public function load_all(&$emailContent, $data = [])
    {
        $models = $this->load_models($emailContent);
        $hydrated = $this->hydrate_models($models, $data);
        foreach ($models as $key => $model) {
            $models['data'] = isset($hydrated[$key]) ? $hydrated[$key] : [];
        }
        return $models;
    }

    public function process_email($trigger, $to = null, $data = [], $user_id = 0, $pool_id = 0, $delay = null)
    {
        if ($to) {
            $emailContent = $this->find_by_trigger($trigger);
            if ($emailContent) {
                return $this->send_email($to, $emailContent, $data, $user_id, $pool_id, $delay);
            }
        }
        return false;
    }
    public function load_models(&$emailContent)
    {
        $models = [];
        $config = $emailContent->config;
        $modelPrefix = 'App\\Models\\';
        if (isset($config->models)) {
            foreach ($config->models as $model) {
                $modelString = $modelPrefix . $model;
                $models[strtolower($model)] = resolve($modelString);
            }
        }

        return $models;
    }

    public function hydrate_models($models, $data = [])
    {
        $hydreated = [];
        foreach ($models as $key => $model) {
            $key = strtolower($key);
            if (isset($data[$key])) {
                $count = null;
                foreach ($data[$key]['params'] as $params) {
                    $model = $model->where($params['column'], $params['op'], $params['value']);
                }
                if ($data[$key]['count']) {
                    $hydreated[$key] = $model->count();
                } else {
                    if ($data[$key]['verb'] == 'first') {
                        $hydreated[$key] = $model->first();
                    } else {
                        $hydreated[$key] = $model->get();
                    }
                }
            }
        }
        return $hydreated;
    }

    public function build_email($email, $data)
    {
        $config = $email;
        $subject = $email->subject;
        $replacements = preg_match_all('/\{(.*?)\}/', $subject, $matches);
    }

    public function find_by_trigger($trigger)
    {
        return $this->_repo->find_by_trigger($trigger);
    }

    public function get($filters = [], $queryOnly = false)
    {
        return $this->_repo->get($filters, $queryOnly);
    }

    public function create($data)
    {
        return $this->_repo->create($data);
    }

    public function update($id, $data = [])
    {
        return $this->_repo->update($id, $data);
    }

    public function prep_raw_email($emailContent, $data)
    {
        $subject = $this->_replace_vars($emailContent, $emailContent->subject, $data);
        $content = $this->_replace_vars($emailContent, $emailContent->content, $data);

        $data['subject'] = $subject;
        $data['content'] = $content;
        return $data;
    }

    public function send_admin($toEmail, $subject, $message, $delay = false)
    {
        try {
            // $emailSender = \Mail::to($toEmail);
            // $mailable = ($delay) ? $emailSender->later($delay, new AdminNotice($subject, $message)) : $emailSender->queue(new AdminNotice($subject, $message));

            $adminNotice = new AdminNotice($subject, $message);
            $adminNotice->send_admin_email($toEmail);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            return false;
        }
        return true;
    }


    public function send_email($toEmail, $emailContent, $data, $user_id = 0, $pool_id = 0, $delay = false)
    {
        $subject = $this->_replace_vars($emailContent, $emailContent->subject, $data);
        $content = $this->_build_html_elements($emailContent->content);
        $content = $this->_replace_vars($emailContent, $content, $data);

        $data['subject'] = $subject;
        $data['content'] = $content;

        try {
            $sendEmails = __conf('system.send_emails_to_users', 'boolean', 1);

            if ($sendEmails) {
                $haltSubject = "";
                // $emailSender = \Mail::to($toEmail);
            } else {
                $haltSubject = "(HALTED)";
                // $emailSender = \Mail::to("mkovalch@gmail.com");
                $toEmail = "mkovalch@gmail.com";
            }

            $subject = $subject . $haltSubject;

            $when = ($delay) ? now()->addMinutes($delay) : null;

            switch ($emailContent->section) {
                case 'golf':
                    // $mailable = ($delay) ? $emailSender->later($delay, new SendGolf($emailContent->trigger_label, $subject, $data)) : $emailSender->queue(new SendGolf($emailContent->trigger_label, $subject, $data));

                    $sendGolf = new SendGolf($emailContent->trigger_label, $subject, $data);
                    $sendGolf->send_golf_email($toEmail);

                    break;
                case 'squareboard':
                    // $mailable =  ($delay) ? $emailSender->later($delay, new SendSquare($emailContent->trigger_label, $subject, $data)) : $emailSender->queue(new SendSquare($emailContent->trigger_label, $subject, $data));

                    $sendSquare = new SendSquare($emailContent->trigger_label, $subject, $data);
                    $sendSquare->send_square_email($toEmail);

                    return true;
                    break;
                case 'user':
                    // $mailable =  ($delay) ? $emailSender->later($delay,new SendUser($emailContent->trigger_label, $subject, $data)) : $emailSender->queue(new SendUser($emailContent->trigger_label, $subject, $data));

                    $sendUser = new SendUser($emailContent->trigger_label, $subject, $data);
                    $sendUser->send_user_email($toEmail);

                    return true;
                    break;
                case 'pool':
                    // $mailable =  ($delay) ? $emailSender->later($delay, new SendPool($emailContent->trigger_label, $subject, $data)) : $emailSender->queue(new SendPool($emailContent->trigger_label, $subject, $data));

                    $sendPool = new SendPool($emailContent->trigger_label, $subject, $data, $user_id, $pool_id);
                    $sendPool->send_pool_email($toEmail);

                    return true;
                    break;
            }
            return true;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            return false;
        }
    } 

    private function _build_html_elements($text)
    {
        preg_match_all('/\[(.*?)=(.*?)\](.*?)\[\/button\]/', $text, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            try {
                $stringToReplace = (isset($match[0])) ? $match[0] : null;
                $type = (isset($match[1])) ? $match[1] : null;
                $urlVar = (isset($match[2])) ? $match[2] : url('/');
                $label = (isset($match[3])) ? $match[3] : 'Visit Premier Sports Pools';
                $html = "<br><br><a href='{$urlVar}' target='_blank' class='btn-regular' >
                        {$label}</a><br><br>";
                $text = str_replace($stringToReplace, $html, $text);
            } catch (\Exception $e) {
                \Log::error("_build_html_elements : " . $e->getMessage());
            }
        }
        return $text;
    }


    private function _replace_vars($emailContent, $text, $data = [])
    {
        $constant = [
            'site_name' => 'Premier Sports Pools',
            'site_url' => 'https://premiersportspools.com'
        ];
        foreach ($constant as $k => $v) {
            $search = '{' . $k . '}';
            $text = str_replace($search, $v, $text);
        }
        if (isset($emailContent->config->vars)) {
            foreach ($emailContent->config->vars as $varName) {
                try {
                    $parts = explode('.', $varName);
                    $column = (isset($parts[1])) ? $parts[1] : null;
                    if ($column) {
                        $model = isset($data[$parts[0]]) ? $data[$parts[0]] : null;
                        $column2 = (isset($parts[2]) && isset($data[$parts[2]])) ? $data[$parts[2]] : null;
                        if ($column2) {
                            $search = '{' . $varName . '}';
                            $parentMod = $model->{$column};
                            if ($column2 == 'count') {
                                $text = str_replace($search, $parentMod->count(), $text);
                            } else {
                                $text = str_replace($search, $parentMod->{$column2}, $text);
                            }
                        } else {
                            if ($model) {
                                $search = '{' . $varName . '}';
                                $text = str_replace($search, $model->{$column}, $text);
                            }
                        }
                    }
                } catch (\Exception $e) {
                    \Log::error("_replace_vars : " . $e->getMessage());
                }
            }
        }

        return $text;
    }
}
