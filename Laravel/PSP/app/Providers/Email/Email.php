<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 9:13 AM
 */

namespace App\Providers\Email;

class Email
{

    private $_mailRepo;

    public function __construct()
    {
    }

    public function send_invite($emailInvite, $messageToUser = "")
    {
        if ($emailInvite) {
            try {
                $pool = $emailInvite->pool;
                $comishUser = \Auth::user();
                $displayName = $pool->users->where('id', $comishUser->id)->first()->pivot->display_name;
                $subject = $displayName . " invited you to " . $pool->pool_name;

                $poolInvites  =  new \App\Mail\PoolInvites($emailInvite, $messageToUser, $subject);
                $poolInvites->send_invite_email($emailInvite->email);

                return true;
            } catch (\Exception $e) {
                \Log::error("send_invite : " . $e->getMessage());
                \Log::error($e->getTraceAsString());
                return false;
            }
        }
        return false;
    }
}
