<?php

namespace App\Providers\GameEvent;

class GameEvent
{

    private $_gameRepo;

    public function __construct(
        \App\Repositories\GameEventRepository $gameEventRepo
    ) {
        $this->_gameEventRepo = $gameEventRepo;
    }

    public function create($data)
    {
        return $this->_gameEventRepo->create($data);
    }


    public function create_update($eventId, $data)
    {

        //TODO: watch what feed shows in mid
        //            $currentEvent = $this->_gameEventRepo->get_by_event_id($eventId);

        return $this->_gameEventRepo->create_update($eventId, $data);
    }

    public function get_events_by_game_id($gameId, $result = false)
    {
        return $this->_gameEventRepo->events_by_game_id($gameId, $result);
    }

    public function score_at($gameId, $quarter = 'q4', $timeSeconds = null)
    {
        $events = $this->get_events_by_game_id($gameId, true);
        $scores = [
            'hometeam' => 0,
            'awayteam' => 0,
        ];

        try {
            switch ($quarter) {
                case '1':
                case 'q1':
                    $event = $events->where('game_id', $gameId)
                        ->where('quarter', 'q1')
                        ->where('clock', '>=', $timeSeconds)
                        ->orderBy('clock', 'asc')
                        ->first();
                    if ($event) {
                        $scores = [
                            'hometeam' => $event->home_score,
                            'awayteam' => $event->away_score,
                        ];
                    }
                    break;
                case '2':
                case 'q2':
                    $event = $events->where('game_id', $gameId)
                        ->where('quarter', 'q2')
                        ->where('clock', '>=', $timeSeconds)
                        ->orderBy('clock', 'asc')
                        ->first();
                    if ($event) {
                        $scores = [
                            'hometeam' => $event->home_score,
                            'awayteam' => $event->away_score,
                        ];
                    } else {
                        $scores = $this->score_at($gameId, 'q1', 0); //0 because we need top of the q1
                    }
                    break;
                case '3':
                case 'q3':
                    $event = $events->where('game_id', $gameId)
                        ->where('quarter', 'q3')
                        ->where('clock', '>=', $timeSeconds)
                        ->orderBy('clock', 'asc')
                        ->first();
                    if ($event) {
                        $scores = [
                            'hometeam' => $event->home_score,
                            'awayteam' => $event->away_score,
                        ];
                    } else {
                        $scores = $this->score_at($gameId, 'q2', 0);
                    }
                    break;
                case '4':
                case 'q4':
                    $event = $events->where('game_id', $gameId)
                        ->where('quarter', 'q4')
                        ->where('clock', '>=', $timeSeconds)
                        ->orderBy('clock', 'asc')
                        ->first();
                    if ($event) {
                        $scores = [
                            'hometeam' => $event->home_score,
                            'awayteam' => $event->away_score,
                        ];
                    } else {
                        $scores = $this->score_at($gameId, 'q3', 0);
                    }
                    break;
                case '5':
                case 'ot':
                    $event = $events->where('game_id', $gameId)
                        ->where('quarter', 'ot')
                        ->where('clock', '>=', $timeSeconds)
                        ->orderBy('clock', 'asc')
                        ->first();
                    if ($event) {
                        $scores = [
                            'hometeam' => $event->home_score,
                            'awayteam' => $event->away_score,
                        ];
                    } else {
                        $scores = $this->score_at($gameId, 'q4', 0);
                    }
                    break;

                case 'finalscore':
                    $event = $events->where('game_id', $gameId)
                        ->where('quarter', 'ot')
                        ->where('clock', '>=', $timeSeconds)
                        ->orderBy('clock', 'asc')
                        ->first();

                    if ($event) {
                        $scores = [
                            'hometeam' => $event->home_score,
                            'awayteam' => $event->away_score,
                        ];
                    } else {
                        $scores = $this->score_at($gameId, 'ot', 0);
                    }
                    break;
            }
        } catch (\Exception $e) {
            \Log::error("score_at : " . $e->getMessage());
        }
        return $scores;
    }
}
