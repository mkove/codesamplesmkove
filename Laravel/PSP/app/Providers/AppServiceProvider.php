<?php

namespace App\Providers;

use App\Hooks\System\CriticalErrorLogging;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Queue\Events\JobFailed;
use Queue;
use App\Helpers\SiteHelper;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Fetch Session Vars for Content
        SiteHelper::refresh_content();
        //Fetch Session vars for config
        SiteHelper::refresh_config();
        try{
         /*   Queue::failing(function (JobFailed $event) {
                $job = $event->job;
                $critLog = new CriticalErrorLogging();
                $critLog->send_slack($job->resolveName()." failed on ".$job->attempts()."/".$job->maxTries()."\n".$event->exception->getMessage());
            });*/
        } catch (\Exception $e){
            \Log::error($e);
        }


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
