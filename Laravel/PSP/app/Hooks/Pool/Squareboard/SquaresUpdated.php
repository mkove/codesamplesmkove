<?php

    namespace App\Hooks\Pool\Squareboard;

    use App\Models\EmailContent;
    use App\Providers\Email\EmailContentProvider;
    use App\Repositories\EmailContentRepository;

    class SquaresUpdated
    {
        /**
         * Create a new message instance.
         *
         * @return void
         */
        private $_emailContentProvider;

        public function __construct(EmailContentProvider $emailContentProvider)
        {
            $this->_emailContentProvider = $emailContentProvider;
        }

        public static function before($data = []){

            $emailContainerProvider = new EmailContentProvider(new EmailContentRepository(new EmailContent()));

        }


        public static function after() {

        }
    }
