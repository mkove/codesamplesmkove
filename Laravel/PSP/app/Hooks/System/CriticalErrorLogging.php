<?php
    namespace App\Hooks\System;

    use GuzzleHttp\Exception\ClientException;
    use GuzzleHttp\RequestOptions;

    class CriticalErrorLogging{

        private $_slackUrl;

        public function __construct(){
            $this->_slackUrl = __conf('system.slack_webhook_url','text', "https://hooks.slack.com/services/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");
        }


//        public function log

        public function send_slack($message){
            try {
                $client = new \GuzzleHttp\Client();

                $data = array(
                    'text'  => $message,
                );
                $request = $client->post($this->_slackUrl,  [
                    RequestOptions::JSON => $data
                ]);

            } catch (ClientException $ce){

                \Log::error($ce->getMessage());
                \Log::error($ce->getResponse());
            }
        }
    }
    //
