<?php

namespace App\Repositories;

/**
 * Class PoolTypeRepository
 * @package App\Repositories
 */
class PoolTypeRepository
{
    public $model;

    public function __construct(\App\Models\PoolType $model)
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model::find($id);
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->model::get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data = [])
    {
        try {
            $ct = $this->model::create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null; 
    }

    /**
     * @param $id
     * @param array $with
     * @return \App\Models\PoolType|\App\Models\PoolType[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function findWith($id, $with = [])
    {
        return $this->model::with($with)->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        try {
            $up = $this->model::where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }

    /**
     * @param $condition
     * @param $data
     * @return mixed
     */
    public function updateMany($condition, $data)
    {
        try {
            $up = $this->model::where($condition)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }

    /**
     * @param $id
     * @param $userId
     * @return mixed
     */
    public function userSquare($id, $userId)
    {
        return $this->model::where('user_id', $userId)->find($id);
    }
}
