<?php

namespace App\Repositories;

use App\Models\GolfPlayer;
use App\Mail\AdminNotice;
use App\Models\User;

class GolfPlayerRepository
{
    public function __construct(GolfPlayer $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $items = $this->model::where('id', '<>', ''); //calling static on non-static
        //            if($filters){
        foreach ($filters as $key => $filter) {
            $items = $items->where($key, $filter);
        }
        //            }
        return ($queryOnly) ? $items : $items->get();
    }

    public function create($data)
    {
//		$ranOn = date('F j, Y G:i a');
//        $toEmail = User::where('is_admin', 'yes')->get(['email']);
//        for ($i = 0; $i < count($toEmail); $i++) {
//			try {
//				$email = $toEmail[$i]['email'];
//				$notice = new AdminNotice("Golf Player Added on ".$ranOn, "GoalServe possible issue: </br> <b>" .
//					$data['name']." | GS ID: ".$data['external_id']."| POS ".$data['pos']."<br>NOTE: ".$data['internal_note'].
//					"</b>");
////				$notice->send_admin_email($email);
//			} catch (\Exception $e){
//				\Log::error($e->getMessage());
//				\Log::error($e->getTraceAsString());
//			}
//
//        }

        return $this->model->create($data);
    }

    public function update($id, $data)
    {
        return $this->model->where('id', $id)->update($data);
    }
}
