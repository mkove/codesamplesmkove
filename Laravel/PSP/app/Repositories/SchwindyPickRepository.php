<?php

namespace App\Repositories;


class SchwindyPickRepository
{

    public $model;

    public function __construct(\App\Models\SchwindyPick $model)
    {
        $this->model = $model;
    }


    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($filters = [], $query = false)
    {
        $picks = $this->model::where('id', '<>', ''); //calling static on non-static
        foreach ($filters as $key => $filter) {
            $picks = $picks->where($key, $filter);
        }
        return ($query) ? $picks : $picks->get();
    }

    public function get_by_game_id_team_id($schwindyId, $gameId, $teamId)
    {
        return $this->model::where('schwindy_id', $schwindyId)->where('game_id', $gameId)->where('team_id', $teamId)->first();
    }

    public function get_picks_by_game_id($gameId)
    {
        return $this->model->where('game_id', $gameId)->get();
    }

    public function create($data = [])
    {
        try {
            $ct = $this->model::create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            $up = $this->model::where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
