<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 10:37 AM
 */

namespace App\Repositories;


use App\Models\UserNotification;

class UserNotificationRepository
{
    public $model;

    public function __construct(UserNotification $model)
    {
        $this->model = $model;
    }

    public function create($userNotificationData = [])
    {
        try {
            $ct = $this->model->create($userNotificationData);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function update($userNotificationId, $userNotificationData = [])
    {
        try {
            $up = $this->model->where('id', $userNotificationId)->update($userNotificationData);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }

    public function get($id = null, $filters = [], $queryOnly = false)
    {
        if ($id) {
            return $this->model->with(['user', 'commissioner'])->find($id);
        }
        $userNotifications = $this->model->with(['user', 'commissioner'])->where('id', '<>', '')->orderBy('created_at', 'desc'); //calling static on non-static
        foreach ($filters as $key => $filter) {
            $userNotifications = $userNotifications->where($key, $filter);
        }
        return ($queryOnly) ? $userNotifications : $userNotifications->get();
    }
}
