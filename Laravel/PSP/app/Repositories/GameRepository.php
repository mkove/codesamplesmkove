<?php

namespace App\Repositories;

use App\Helpers\SiteHelper;

class GameRepository
{

    public $model;

    public function __construct(\App\Models\Game $model)
    {
        $this->model = $model;
    }

    public function load_game($id)
    {

        return $this->find($id);
    }

    public function get_game_by_contest_id($contestId, $sport, $league)
    {
        return $this->model->where('tournament_id', $contestId)->where('sport', $sport)->where('league', $league)->first();
    }

    public function quarter_score($game, $quarter)
    {
        $numericQuarter = SiteHelper::convert_qurater_to_number($quarter, true);

        //normalize Broadage Shennanigans
        $scores = [
            1 => [
                'home' => 'hometeam_q1',
                'away' => 'awayteam_q1',
            ],
            2 => [
                'home' => 'hometeam_q2',
                'away' => 'awayteam_q2',
            ],
            3 => [
                'home' => 'hometeam_q3',
                'away' => 'awayteam_q3',
            ],
            4 => [
                'home' => 'hometeam_q4',
                'away' => 'awayteam_q4',
            ],
            5 => [
                'home' => 'hometeam_overtime',
                'away' => 'awayteam_overtime',
            ],
        ];
        $homescore = 0;
        $awayscore = 0;

        for ($i = 1; $i <= $numericQuarter; $i++) {
            try {
                $homeVar = $scores[$i]['home'];
                $awayVar = $scores[$i]['away'];
                $homescore = +$game->$homeVar;
                $awayscore = +$game->$awayVar;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }

        return [
            'hometeam' => isset($homescore) ? $homescore : 0,
            'awayteam' => isset($awayscore) ? $awayscore : 0,
        ];
    }

    public function get_score($game, $quarter = 'totalscore', $cumulative = true)
    {
        if (!$game) return false;
        $varHome = "hometeam_{$quarter}";
        $varAway = "awayteam_{$quarter}";
        if ($cumulative) {
            switch ($quarter) {
                case 'q1':
                    return [
                        'hometeam_score' => isset($game->hometeam_q1) ? $game->hometeam_q1 : null,
                        'awayteam_score' => isset($game->awayteam_q1) ? $game->awayteam_q1 : null,
                    ];
                    break;
                case 'q2':
                    return [
                        'hometeam_score' => isset($game->$varHome) ? ($game->$varHome + $game->hometeam_q1) : null,
                        'awayteam_score' => isset($game->$varAway) ? ($game->$varAway + $game->awayteam_q1) : null,
                    ];
                    break;
                case 'q3':
                    return [
                        'hometeam_score' => isset($game->$varHome) ? ($game->$varHome + $game->hometeam_q2  + $game->hometeam_q1) : null,
                        'awayteam_score' => isset($game->$varAway) ? ($game->$varAway + $game->awayteam_q2 + $game->awayteam_q1) : null,
                    ];
                    break;
                case 'q4':
                    return [
                        'hometeam_score' => isset($game->$varHome) ? ($game->$varHome + $game->hometeam_q3 + $game->hometeam_q2  + $game->hometeam_q1) : null,
                        'awayteam_score' => isset($game->$varAway) ? ($game->$varAway + $game->awayteam_q4 + $game->awayteam_q2 + $game->awayteam_q1) : null,
                    ];
                    break;
                default:
                    return [
                        'hometeam_score' => isset($game->hometeam_totalscore) ? $game->hometeam_totalscore : null,
                        'awayteam_score' => isset($game->awayteam_totalscore) ? $game->awayteam_totalscore : null,
                    ];
                    break;
            }
        } else {
            return [
                'hometeam_score' => isset($game->$varHome) ? $game->$varHome : null,
                'awayteam_score' => isset($game->$varAway) ? $game->$varAway : null,
            ];
        }
    }

    public function game_started($game, $date = null)
    {
        $date = ($date) ? strtotime($date) : strtotime('now');
        return (strtotime($game->start) < $date);
    }

    public function game_ended($game)
    {
        return ($game->is_final);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function get_by_round_id($roundId = null, $sport = null, $league = null, $queryOnly = false)
    {
        $games = $this->model;
        if ($roundId) {
            $games = $games->where('season_id', $roundId);
        }
        if ($sport) {
            $games = $games->where('sport', $sport);
        }
        if ($league) {
            $games = $games->where('league', $league);
        }
        return ($queryOnly) ? $games : $games->get();
    }

    public function get($filters = [], $queryOnly = false)
    {
        $games = $this->model::where('id', '<>', ''); //calling static on non-static
        //            if($filters){
        foreach ($filters as $key => $filter) {
            $games = $games->where($key, $filter);
        }
        //            }
        return ($queryOnly) ? $games : $games->get();
    }

    public function create($data = [])
    {
        try {
            return $this->model::create($data);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            $update = $this->model::where('id', $id)->update($data);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return true;
    }

    public function get_last_game($sport = null, $league = null, $season = "1st Round")
    {
        return $this->model->where('time', '<', date('Y-m-d H:i:s'))
            ->where('season', $season)
            ->where('sport', $sport)
            ->where('league', $league)
            ->where('status', 'Final') //might not be needed
            ->orderBy('time', 'desc')
            ->first();
    }

    public function get_next_game($sport = null, $league = null, $season = "1st Round")
    {
        return $this->model->where('time', '>', date('Y-m-d H:i:s'))
            ->where('season', $season)
            ->where('sport', $sport)
            ->where('league', $league)
            //                ->where('status', 'Scheduled') //might not be needed
            ->orderBy('time', 'asc')
            ->first();
    }

    public function season_games_played_count($sport = null, $league = null, $round = "1st Round")
    {
        return $this->model->where('round', $round)
            ->where('sport', $sport)
            ->where('league', $league)
            ->where('status', 'Final')->count();
    }

    public function search($term, $take)
    {
        $games = $this->model::orderBy('start', 'desc')
            ->whereRaw("MATCH (tournament_name) AGAINST (? IN BOOLEAN MODE)", $term);
        if ($take) {
            $games->take($take);
        }
        return $games->get();
    }

    public function week_played($week, $season, $seasonYear = '2019/2020', $sport = 'football', $league = 'professional')
    {
        $gameFilters = [
            'sport' => $sport,
            'league' => $league,
            'season_year' => $seasonYear,
            'season' => $season,
            'week' => $week
        ];
        $games = $this->get($gameFilters);
        $totalCount = $games->count();
        $finalCount = 0;
        foreach ($games as $game) {
            if ($game->is_final) {
                $finalCount++;
            }
        }
        return ($totalCount == $finalCount);
    }

    public function week_in_play($week, $season, $seasonYear = '2019/2020', $sport = 'football', $league = 'professional')
    {

        //IF week is played - OR week not started
        if (
            $this->week_played($week, $season, $seasonYear, $sport, $league)
            or
            $this->week_not_started($week, $season, $seasonYear, $sport, $league)
        ) {
            return false;
        }
        return true;
    }

    public function week_not_started($week, $season, $seasonYear = '2019/2020', $sport = 'football', $league = 'professional')
    {
        $gameFilters = [
            'sport' => $sport,
            'league' => $league,
            'season_year' => $seasonYear,
            'season' => $season,
            'week' => $week
        ];
        // Any count of statuses that aren't Not Started will return false
        //            dd($this->get($gameFilters, true)->where('status','<>', 'Not Started')->count()==0);
        return ($this->get($gameFilters, true)->where('status', '<>', 'Not Started')->count() == 0);
    }
}
