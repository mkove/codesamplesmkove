<?php
    namespace App\Repositories;

    use App\Models\Discount;

    class DiscountRepository {
        public function __construct(Discount $model) {
            $this->model = $model;
        }

        public function create($data){
            return $this->model->create($data);
        }

        public function find($id) {
            return $this->model::find($id);
        }

        public function update($id, $data){
            return $this->model->where('id', $id)->update($data);
        }
    }
