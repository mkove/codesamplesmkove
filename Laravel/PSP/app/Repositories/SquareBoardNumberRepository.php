<?php

namespace App\Repositories;

use App\Helpers\SiteHelper;
use App\Helpers\SquareHelper;

class SquareBoardNumberRepository
{
    public $model;

    public function __construct(\App\Models\SquareBoardNumber $model)
    {
        $this->model = $model;
    }

    public function generate(&$squareBoard)
    {
        try {
            $numSets = ($squareBoard->sets) ? $squareBoard->sets : 1;
            $winType = $squareBoard->win_type;
            switch ($winType) {
                case 'final': // we don't need 4 or 2 number sets if they only win once
                    //TODO: update to use square board service to do this shit
                    $numSets = 1;
                    $squareBoard->sets = $numSets;
                    $squareBoard->save();
                    break;
                case 'halftime': //we don't really need more than 2 sets per half-time and final wins
                    $numSets = ($numSets == 1) ? $numSets : 2;
                    $squareBoard->sets = $numSets;
                    $squareBoard->save();
                    break;
            }

            $labels = $this->_gen_labels($numSets);
            $this->delete_all($squareBoard);
            for ($i = 1; $i <= $numSets; $i++) {
                $data = [
                    'squareboard_id' => $squareBoard->id,
                    'count' => $i,
                    'hometeam_numbers' => SquareHelper::generate_random(10),
                    'awayteam_numbers' => SquareHelper::generate_random(10),
                    'label' => $labels[$i],
                ];
                $this->create($data);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return true;
    }



    public function generate_manual(&$squareBoard, $hometeam_manual_numbers, $awayteam_manual_numbers)
    {
        try {
            $numSets = ($squareBoard->sets) ? $squareBoard->sets : 1;
            $winType = $squareBoard->win_type;
            switch ($winType) {
                case 'final': // we don't need 4 or 2 number sets if they only win once
                    //TODO: update to use square board service to do this shit
                    $numSets = 1;
                    $squareBoard->sets = $numSets;
                    $squareBoard->save();
                    break;
                case 'halftime': //we don't really need more than 2 sets per half-time and final wins
                    $numSets = ($numSets == 1) ? $numSets : 2;
                    $squareBoard->sets = $numSets;
                    $squareBoard->save();
                    break;
            }

            $labels = $this->_gen_labels($numSets);
            $this->delete_all($squareBoard);
            for ($i = 1; $i <= $numSets; $i++) {
                $data = [
                    'squareboard_id' => $squareBoard->id,
                    'count' => $i,
                    'hometeam_numbers' => SquareHelper::generate_manual($i - 1, $hometeam_manual_numbers, $numSets),
                    'awayteam_numbers' => SquareHelper::generate_manual($i - 1, $awayteam_manual_numbers, $numSets),
                    'label' => $labels[$i],
                ];
                $this->create($data);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return true;
    }

    public function get_by_quarter($squareBoardId, $quarter = null)
    {
        if (!$quarter) return $this->_get_by_squareboard_id($squareBoardId)->get();
        $allSets = $this->_get_by_squareboard_id($squareBoardId)->get();
        $qtrInteger = SiteHelper::convert_qurater_to_number($quarter, true);
        //dont matter quarter just pull one
        if ($allSets->count() == 1) {
            return $allSets->first();
        } else if ($allSets->count() == 2) {
            if ($qtrInteger <= 2) { //first half of the game
                return $allSets->where('count', 1)->first();
            } else { //second half of the game
                return $allSets->where('count', 2)->first();
            }
        } else {
            //all others
            return $allSets->where('count', $qtrInteger)->first();
        }
    }

    private function _get_by_squareboard_id($squareBoardId)
    {
        return $this->model->where('squareboard_id', $squareBoardId);
    }

    public function delete_all(&$squareBoard)
    {
        $squareBoardId = $squareBoard->id;
        return $this->model->where('squareboard_id', $squareBoardId)->delete();
    }

    private function _gen_labels($numberSet = 1)
    {
        if ($numberSet == 2) {
            return $labels = [
                '1' => "first_half",
                '2' => "second_half",
            ];
        }
        if ($numberSet == 4) {
            return $labels = [
                '1' => "q1",
                '2' => "q2",
                '3' => "q3",
                '4' => "totalscore"
            ];
        }
        return $labels = [
            '1' => "totalscore",
        ];
    }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $numbers = $this->model::where('id', '<>', ''); //calling static on non-static
        //            if($filters){
        foreach ($filters as $key => $filter) {
            $numbers = $numbers->where($key, $filter);
        }
        //            }
        return ($queryOnly) ? $numbers : $numbers->get();
    }

    public function create($data = [])
    {
        try {
            $ct = $this->model::create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function findWith($id, $with = [])
    {
        return $this->model::with($with)->find($id);
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            $up = $this->model::where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }

    public function updateMany($condition, $data)
    {
        try {
            $up = $this->model::where($condition)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function userSquare($id, $userId)
    {
        return $this->model::where('user_id', $userId)->find($id);
    }

    //        public function encode($array)
    //        {
    //            return $array);
    //        }

    public function decode($jsonNumbers, $assoc = true)
    {
        return json_decode($jsonNumbers, $assoc);
    }
}
