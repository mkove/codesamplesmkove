<?php

namespace App\Repositories;


use App\Models\Invoice;

class InvoiceRepository
{
    public function __construct(Invoice $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $invoices = $this->model::where('id', '<>', ''); //calling static on non-static
        //            if($filters){
        foreach ($filters as $key => $filter) {
            $invoices = $invoices->where($key, $filter);
        }
        return ($queryOnly) ? $invoices : $invoices->get();
    }

    public function create($data)
    {
        try {
            $ct = $this->model->create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function update($id, $data)
    {
        try {
            $up = $this->model->where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
