<?php

namespace App\Repositories;

use App\Models\Rate;

/**
 * Class RateRepository
 * @package App\Repositories
 */
class RateRepository
{
    public function __construct(Rate $model)
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model::find($id);
    }

    /**
     * @param array $filters
     * @param false $queryOnly
     * @return mixed
     */
    public function get($filters = [], $queryOnly = false)
    {
        $invoices = $this->model::where('id', '<>', ''); //calling static on non-static
        foreach ($filters as $key => $filter) {
            $invoices = $invoices->where($key, $filter);
        }
        return ($queryOnly) ? $invoices : $invoices->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        try {
            $ct = $this->model->create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        try {
            $up = $this->model->where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
