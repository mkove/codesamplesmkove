<?php
    /**
     * Created by PhpStorm.
     * User: mkova
     * Date: 10/12/2018
     * Time: 9:30 AM
     */

    namespace App\Repositories;


    use App\Models\Chat;

    class ChatRepository {
        public function __construct(Chat $model) {
            $this->model = $model;
        }

        public function find($id)
        {
            return $this->model::find($id);
        }

        public function create($data = [])
        {
            return $this->model::create($data);
        }

        public function findWith($id, $with = [])
        {
            return $this->model::with($with)->find($id);
        }

        public function delete($id)
        {
            return $this->model::find($id)->delete();
        }

        public function update($id, $data)
        {
            return $this->model::find($id)->update($data);
        }

        public function updateMany($condition, $data)
        {
            return $this->model::where($condition)->update($data);
        }

        public function userSquare($id, $userId)
        {
            return $this->model::where('user_id', $userId)->find($id);
        }
    }
