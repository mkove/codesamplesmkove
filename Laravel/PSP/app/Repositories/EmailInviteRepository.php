<?php
    namespace App\Repositories;

    use App\Helpers\SiteHelper;

    class EmailInviteRepository {

        public $model;

        public function __construct(\App\Models\EmailInvite $model) {
            $this->model = $model;
        }

        public function find($id = null){
            if($id) return $this->model->find($id);
            return $this->model->get();
        }

        public function get($filters = [], $queryOnly = false){
            $invites = $this->model::where('id', '<>',''); //calling static on non-static
//            if($filters){
            foreach($filters as $key => $filter){
                $invites = $invites->where($key, $filter);
            }
//            }
            return ($queryOnly) ? $invites : $invites->get();
        }
        public function create($data){
            return $this->model::create($data);
        }

        public function update($id, $data){
            return $this->model::where('id', $id)->update($data);
        }

        public function is_invited($user, $poolId){
            if(isset($user->email)){
                $email = $user->email;
            } else {
                $email = $user;
            }
            return $this->model::where('pool_id', $poolId)->where('email', strtolower(trim($email)))->count();
        }

        public function get_by_email_pool_id($email, $poolId, $count = false){
            if($count){
                return $this->model::where('pool_id', $poolId)->where('email', strtolower(trim($email)))->count();
            }
            return $this->model::where('pool_id', $poolId)->where('email', strtolower(trim($email)))->first();
        }
    }
