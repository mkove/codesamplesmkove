<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 10:37 AM
 */

namespace App\Repositories;


class UserDisplayNameRepository
{
    public $model;

    public function __construct(\App\Models\UserDisplayName $model)
    {
        $this->model = $model;
    }

    public function get($id = null, $filters = [], $queryOnly = false)
    {
        if ($id) return $this->find($id);
        $userDisplayNames = $this->model::where('id', '<>', ''); //calling static on non-static
        foreach ($filters as $key => $filter) {
            $userDisplayNames = $userDisplayNames->where($key, $filter);
        }
        return ($queryOnly) ? $userDisplayNames : $userDisplayNames->get();
    }


    public function find($id)
    {
        return $this->model::find($id);
    }

    public function create($data = [])
    {
        try {
            $ct = $this->model::create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            $up =  $this->model::where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }

    public function set_primary($id, &$displayNames)
    {
        foreach ($displayNames as $displayName) {
            try {
                $displayName->primary = ($displayName->id == $id) ? true : false;
                $displayName->save();
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return true;
    }

    public function updateMany($condition, $data)
    {
        try {
            $up = $this->model::where($condition['column'], $condition['operator'], $condition['value'])->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
