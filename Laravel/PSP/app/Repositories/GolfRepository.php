<?php

namespace App\Repositories;

use App\Models\Golf;

class GolfRepository
{
    public function __construct(Golf $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $golfPools = $this->model::where('id', '<>', ''); //calling static on non-static
        //            if($filters){
        foreach ($filters as $key => $filter) {
            $golfPools = $golfPools->where($key, $filter);
        }
        //            }
        return ($queryOnly) ? $golfPools : $golfPools->get();
    }

    public function create($data)
    {
        try {
            $ct = $this->model->create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function update($id, $data)
    {
        $item = $this->find($id);

        try {
            $item->update($data);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return $item;
        //            $result =  $this->model->where('id', $id)->update($data);
        // dd($ret, $item->getChanges(), $item->getDirty());
    }

    public function update_config(&$golf, $config, $returnUpdates = false)
    {
        $return = true;
        try {
            if ($returnUpdates) {
                $a1 = json_decode(json_encode($config), TRUE);
                $a2 = json_decode(json_encode($golf->config), TRUE);
                $diff = array_diff(array_map('serialize', $a1), array_map('serialize', $a2));
                $multidimensional_diff = array_map('unserialize', $diff);
                if (isset($multidimensional_diff['viewonly'])) {
                    $return = "View Only";
                }
            }
            $golf->config = $config;
            $golf->save();
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $return;
    }
}
