<?php
    namespace App\Repositories;
    use App\Models\DynamicRate;

    class DynamicRateRepository {
        public function __construct(DynamicRate $model) {
            $this->model = $model;
        }

        public function find($id) {
            return $this->model::find($id);
        }

        public function get($filters=[], $queryOnly = false)
        {
            $dynamicRates = $this->model::where('id', '<>',''); //calling static on non-static
//            if($filters){
            foreach($filters as $key => $filter){
                $dynamicRates = $dynamicRates->where($key, $filter);
            }
//            }
            return ($queryOnly) ? $dynamicRates : $dynamicRates->get();
        }

        public function create($data){
            return $this->model->create($data);
        }

        public function update($id, $data){
            return $this->model->where('id', $id)->update($data);
        }
    }
