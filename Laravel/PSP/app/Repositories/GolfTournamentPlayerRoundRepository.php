<?php

namespace App\Repositories;

use App\Models\GolfTournamentPlayerRound;

class GolfTournamentPlayerRoundRepository
{
    public function __construct(GolfTournamentPlayerRound $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $items = $this->model::where('id', '<>', ''); //calling static on non-static
        //            if($filters){
        foreach ($filters as $key => $filter) {
            $items = $items->where($key, $filter);
        }
        //            }
        return ($queryOnly) ? $items : $items->get();
    }

    public function create($data)
    { 
        try {
            $ct = $this->model->create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function update($id, $data)
    { 
        try {
            $up = $this->model->where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
