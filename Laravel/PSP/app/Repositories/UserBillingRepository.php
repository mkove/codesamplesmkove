<?php

namespace App\Repositories;

use App\Models\UserBilling;

class UserBillingRepository
{

    public $model;

    public function __construct(UserBilling $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $userBillings = $this->model::where('id', '<>', ''); //calling static on non-static
        foreach ($filters as $key => $filter) {
            $userBillings = $userBillings->where($key, $filter);
        }
        return ($queryOnly) ? $userBillings : $userBillings->get();
    }

    public function find_by_provider_id($providerId)
    {
        return $this->model->where('provider_id', $providerId)->first();
    }
    public function create($data)
    {
        try {
            $ct = $this->model->create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function update($id, $data)
    {
        try {
            $up = $this->model->where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
