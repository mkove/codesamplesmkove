<?php

namespace App\Repositories;


class SchwindyRepository
{

    public $model;

    public function __construct(\App\Models\Schwindy $model)
    {
        $this->model = $model;
    }

    public function get($id = null)
    {
        if ($id) {
            return $this->model::find($id);
        }
        return $this->model->get();
    }

    public function get_incomplete()
    {
        return $this->model::where('completed', false)->get();
    }

    public function create($data = [])
    {
        try {
            $ct = $this->model::create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null; 
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            if (isset($data['config'])) {
                $item = $this->model->find($id);
                $item->config = $data['config'];
                $item->save();
                unset($data['config']);
            }
            if (count($data)) {
                $this->model::where('id', $id)->update($data);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return $this->model::find($id);
    }

    public function updateMany($condition, $data)
    {
        try {
            $ct = $this->model::where($condition)->update($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null; 
    }

    public function userSquare($id, $userId)
    {
        return $this->model::where('user_id', $userId)->find($id);
    }
}
