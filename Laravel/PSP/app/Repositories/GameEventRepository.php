<?php

namespace App\Repositories;

class GameEventRepository
{

    public $model;

    public function __construct(\App\Models\GameEvent $model)
    {
        $this->model = $model;
    }

    public function get_by_event_id($eventId)
    {
        return $this->model->where('event_id', $eventId)->first();
    }

    public function create($data)
    {
        return $this->model::create($data);
    }

    public function update($id, $data)
    {
        return $this->model::where('id', $id)->update($data);
    }

    public function create_update($eventId, $data)
    {
        //            $clockMax = (int)$data['clock'] + 5;
        //            $clockMin = (int)$data['clock'] - 5;
        //            $clockMin = ($clockMin < 0) ? 0 : $clockMin;
        $entry = $this->model::where('event_id', $eventId)->first();
        //            if(!$entry || !$eventId){ //LEGACY
        //                $entry = $this->model::where('quarter', $data['quarter'])
        //                    ->where('type',$data['type'])
        //                    ->where('player_id', $data['player_id'])
        //                    ->where('game_id', $data['game_id'])
        //                    ->where('by', $data['by'])
        //                    ->where('clock', '>=', $clockMin)
        //                    ->where('clock', '<=', $clockMax);
        //                if($data['player_id']){
        //                    $entry = $entry->where('player_id', $data['player_id']);
        //                }
        //                $entry =  $entry->first();
        //            } 

        try {
            if ($entry) {
                if ($data['action'] != $entry->action && $data['type'] == 'TD') { //so we got updated event only for touch down.
                    $data['pat_pending'] = false;
                }
                if (preg_match_all('/\(.*\)/', $data['action'])) {
                    $data['pat_pending'] = false;
                }
                $this->update($entry->id, $data);
            } else {
                if ($data['type'] == 'TD') {
                    $data['pat_pending'] = true; //it's a TD and let's throw PAT pending
                } else {
                    $data['pat_pending'] = false;
                }
                //                \Log::info("Matching ". "/\(.*\)|extra point|good|failed/i"." to ".$data['action']);
                if (preg_match_all('/\(.*\)|extra point|good|failed/i', $data['action'])) {
                    $data['pat_pending'] = false;
                }

                $entry = $this->create($data);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $entry;
    }

    private function _match_pat($verb)
    {
        $res = false;
        $match = 'extra point';
    }
    public function  events_by_game_id($gameId, $result = true)
    {
        if ($result) return $this->model::where('game_id', $gameId);
        return $this->model::where('game_id', $gameId)->get();
    }
}
