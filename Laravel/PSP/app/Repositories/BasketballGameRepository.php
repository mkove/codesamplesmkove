<?php

namespace App\Repositories;

class BasketballGameRepository
{

    public $model;

    public function __construct(\App\Models\BasketballGame $model)
    {
        $this->model = $model;
    }

    public function get($model = false)
    {
        if ($model) {
            return $this->model;
        } else {
            return $this->model->all();
        }
    }

    public function load_game($id)
    {

        return $this->find($id);
    }

    public function get_game_by_tournament_id($tournamentId)
    {
        return $this->model->where('tournament_id', $tournamentId)->first();
    }

    public function get_next_game($league = 'college')
    {

        $nextGame   = $this->model->where('league', $league)
            //                ->where('status','Scheduled')
            //                ->where('game_date','>', date('Y-m-d H:is'))
            ->orderBy('game_date', 'asc')->first();
        dd($nextGame);
    }

    public function get_next_round($league = 'college')
    {
    }

    public function round_in_progress($round)
    {
    }



    public function get_score($game, $quarter = 'totalscore', $cumulative = true)
    {
        if (!$game) return false;

        try {
            $varHome = "hometeam_{$quarter}";
            $varAway = "awayteam_{$quarter}";
            if ($cumulative) {
                switch ($quarter) {
                    case 'q1':
                        return [
                            'hometeam_score' => isset($game->hometeam_q1) ? $game->hometeam_q1 : null,
                            'awayteam_score' => isset($game->awayteam_q1) ? $game->awayteam_q1 : null,
                        ];
                        break;
                    case 'q2':
                        return [
                            'hometeam_score' => isset($game->$varHome) ? ($game->$varHome + $game->hometeam_q1) : null,
                            'awayteam_score' => isset($game->$varAway) ? ($game->$varAway + $game->awayteam_q1) : null,
                        ];
                        break;
                    case 'q3':
                        return [
                            'hometeam_score' => isset($game->$varHome) ? ($game->$varHome + $game->hometeam_q2  + $game->hometeam_q1) : null,
                            'awayteam_score' => isset($game->$varAway) ? ($game->$varAway + $game->awayteam_q2 + $game->awayteam_q1) : null,
                        ];
                        break;
                    case 'q4':
                        return [
                            'hometeam_score' => isset($game->$varHome) ? ($game->$varHome + $game->hometeam_q3 + $game->hometeam_q2  + $game->hometeam_q1) : null,
                            'awayteam_score' => isset($game->$varAway) ? ($game->$varAway + $game->awayteam_q4 + $game->awayteam_q2 + $game->awayteam_q1) : null,
                        ];
                        break;
                    case 'current':
                        return [
                            'hometeam_score' => isset($game->hometeam_currentscore) ? $game->hometeam_currentscore : null,
                            'awayteam_score' => isset($game->awayteam_currentscore) ? $game->awayteam_currentscore : null,
                        ];
                        break;
                    case 'halftime':
                        return [
                            'hometeam_score' => isset($game->hometeam_halftime) ? $game->hometeam_halftime : null,
                            'awayteam_score' => isset($game->awayteam_halftime) ? $game->awayteam_halftime : null,
                        ];
                        break;
                    default:
                        return [
                            'hometeam_score' => isset($game->hometeam_finalscore) ? $game->hometeam_finalscore : null,
                            'awayteam_score' => isset($game->awayteam_finalscore) ? $game->awayteam_finalscore : null,
                        ];
                        break;
                }
            } else {
                return [
                    'hometeam_score' => isset($game->$varHome) ? $game->$varHome : null,
                    'awayteam_score' => isset($game->$varAway) ? $game->$varAway : null,
                ];
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
            return false;
        }
    }

    public function game_started($game, $date = null)
    {
        $date = ($date) ? strtotime($date) : strtotime('now');
        return (strtotime($game->start) < $date);
    }

    public function game_ended($game)
    {
        return ($game->is_final);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($data = [])
    {
        //            dump($data);
        try {
            $create =  $this->model::create($data);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return true;
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            $update = $this->model::where('id', $id)->update($data);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return true;
    }
}
