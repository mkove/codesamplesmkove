<?php

namespace App\Repositories;

use App\Models\InvoiceDiscount;

class InvoiceDiscountRepository
{
    public function __construct(InvoiceDiscount $model)
    {
        $this->model = $model;
    }

    public function create($data)
    { 
        try {
            $ct = $this->model->create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function update($id, $data)
    {
        try {
            $up = $this->model->where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
