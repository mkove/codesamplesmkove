<?php
    /**
     * Created by PhpStorm.
     * User: mkova
     * Date: 10/12/2018
     * Time: 9:30 AM
     */

    namespace App\Repositories;


    use App\Models\EmailContent;

    class EmailContentRepository {
        public function __construct(EmailContent $model) {
            $this->model = $model;
        }

        public function find($id)
        {
            return $this->model::find($id);
        }

        public function find_by_trigger($trigger){
            return $this->model::where('trigger_label', strtolower($trigger))->first();
        }

        public function get($filters = [], $queryOnly = false){
            $emailContents = $this->model::where('id', '<>',''); //calling static on non-static
//            if($filters){
            foreach($filters as $key => $filter){
                $emailContents = $emailContents->where($key, $filter);
            }
//            }
            return ($queryOnly) ? $emailContents : $emailContents->get();
        }



        public function create($data = [])
        {
            return $this->model::create($data);
        }

        public function delete($id)
        {
            return $this->model::find($id)->delete();
        }

        public function update($id, $data)
        {
            return $this->model::find($id)->update($data);
        }

        public function updateMany($condition, $data)
        {
            return $this->model::where($condition)->update($data);
        }

    }
