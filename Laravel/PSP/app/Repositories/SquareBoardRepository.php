<?php

/**
 * Created by PhpStorm.
 * User: mkova
 * Date: 10/12/2018
 * Time: 9:27 AM
 */

namespace App\Repositories;


class SquareBoardRepository
{

    public $model;

    public function __construct(\App\Models\SquareBoard $model)
    {
        $this->model = $model;
    }

    public function get($id = null, $filters = [], $queryOnly = false)
    {
        if ($id) {
            return $this->model->find($id);
        }
        $squareBoard = $this->model::where('id', '<>', ''); //calling static on non-static
        foreach ($filters as $key => $filter) {
            $squareBoard = $squareBoard->where($key, $filter);
        }
        return ($queryOnly) ? $squareBoard : $squareBoard->get();
    }

    public function generate_numbers(&$squareBoard)
    {
    }

    public function find_by_pool_id($poolId)
    {
        return $this->_load_by_pool_id($poolId)->first();
    }

    public function game_started($squareBoard)
    {
        $gameRepo = resolve('App\Repositories\GameRepository');
        $game = $gameRepo->load_game($squareBoard->game_id);
        return $gameRepo->game_started($game);
    }


    public function create($data = [])
    {
        try {
            $ct = $this->model::create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            $up = $this->model::where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }

    public function is_locked($squareBoard)
    {
        return $squareBoard->locked;
    }

    public function lock(&$squareBoard)
    {
        $data = ['locked' => true, 'allow_picks' => false];
        try {
            $this->update($squareBoard->id, $data);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public function unlock(&$squareBoard)
    {
        $data = ['locked' => false];
        try {
            $this->update($squareBoard->id, $data);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public function is_active($squareBoard)
    {
        return $squareBoard->active;
    }

    public function has_numbers($squareBoard)
    {
        return $squareBoard->numbers()->count();
    }

    private function _load_by_pool_id($poolId)
    {
        return $this->model->where('pool_id', $poolId);
    }
}
