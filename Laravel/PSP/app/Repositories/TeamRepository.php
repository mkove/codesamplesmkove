<?php

namespace App\Repositories;

use \App\Models\Team;

class TeamRepository
{

    public $model;

    public function __construct(Team $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $teams = $this->model::where('id', '<>', ''); //calling static on non-static
        foreach ($filters as $key => $filter) {
            $teams = $teams->where($key, $filter);
        }
        return ($queryOnly) ? $teams : $teams->get();
    }

    public function find_by_provider_id($providerId)
    {
        return $this->model->where('provider_id', $providerId)->first();
    }
    public function create($data)
    {
        try {
            $ct = $this->model->create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function update(&$team, $data)
    {
        try {
            $up = $team->model->where('id', $team->id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }
}
