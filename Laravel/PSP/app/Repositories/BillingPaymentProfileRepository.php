<?php
    namespace App\Repositories;

    use App\Models\BillingPaymentProfile;

    class BillingPaymentProfileRepository {

        public $model;

        public function __construct(BillingPaymentProfile $model) {
            $this->model = $model;
        }

        public function find($id){
            return $this->model->find($id);
        }

        public function get($filters=[], $queryOnly = false)
        {
            $billingPaymentProfile = $this->model::where('id', '<>',''); //calling static on non-static
            foreach($filters as $key => $filter){
                $billingPaymentProfile = $billingPaymentProfile->where($key, $filter);
            }
            return ($queryOnly) ? $billingPaymentProfile : $billingPaymentProfile->get();
        }

        public function create($data){
            return $this->model->create($data);
        }

        public function update($id, $data){
            return $this->model->where('id', $id)->update($data);
        }

    }
