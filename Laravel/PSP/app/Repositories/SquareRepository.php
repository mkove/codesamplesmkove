<?php

namespace App\Repositories;

class SquareRepository
{

    public $model;

    public function __construct(\App\Models\Square $model)
    {
        $this->model = $model;
    }

    public function get_all_by_quarter($squareBoard, $quarter)
    {
        return null;
    }

    public function get_users_squares(&$squareBoard, $userId)
    {
        return $squareBoard->squares->where('user_id', $userId);
    }

    public function get_all($squareBoard)
    {
        return $squareBoard->squares;
    }

    public function get_by_squareboard_id($squareBoardId)
    {
        return $this->_load_by_squareboard_id($squareBoardId)->first();
    }

    public function get_by_coordinates($squareBoardId, $x, $y)
    {
        return $this->_load_by_squareboard_id($squareBoardId)
            ->where('x', $x)
            ->where('y', $y)
            ->first();
    }

    //        public function get_all_by_user_id($squareBoardId, $userId)
    //        {
    //            return $this->_load_by_squareboard_id($squareBoardId)->where('user_id', $userId)->get();
    //        }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($filters = [], $queryOnly = false)
    {
        $squares = $this->model::where('id', '<>', ''); //calling static on non-static
        //            if($filters){
        foreach ($filters as $key => $filter) {
            $squares = $squares->where($key, $filter);
        }
        //            }
        return ($queryOnly) ? $squares : $squares->get();
    }

    public function create($data = [])
    {
        try {
            $ct = $this->model::create($data);
            return $ct;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return null;
    }

    public function findWith($id, $with = [])
    {
        return $this->model::with($with)->find($id);
    }

    public function delete($id)
    {
        return $this->model::find($id)->delete();
    }

    public function update($id, $data)
    {
        try {
            $up = $this->model::where('id', $id)->update($data);
            return $up;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return null;
    }


    private function _load_by_squareboard_id($squareBoardId)
    {
        return $this->model->where('squareboard_id', $squareBoardId);
    }
}
