<?php
    namespace App\Repositories;

    class SquareWinRepository {

        public $model;

        public function __construct(\App\Models\SquareWin $model) {
            $this->model = $model;
        }

        public function delete($id, &$squareBoard){
            $squareIds = $squareBoard->squares->pluck('id');
            if($id){
                return $this->model->find($id)->delete();
            }
            return $this->model->whereIn('square_id',$squareIds)->delete();

        }

        public function get_all_by_type($squareBoard, $type) {
            return $this->model->where('type',$type)->where('squareboard_id', $squareBoard->id)->get();
        }

        public function get_all($squareBoard){
            $squareIds = $squareBoard->squares()->pluck('id')->toArray();
            return $this->model->whereIn('square_id', $squareIds)->get();
        }

        public function delete_by_number_id($numberId){
            return $this->model->where('squareboard_number_id', $numberId)->delete();
        }
    }
