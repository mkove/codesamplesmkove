<?php

namespace App\Helpers;


use App\Models\GolfPrizeConfig;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class GolfHelper
{
    private $liveScore = null;

    public function getPlayers()
    {
        try {
            $client = new Client();
            $response = $client->get('http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/pga_players');
            if ($body = $response->getBody()) {
                $xml = new \SimpleXMLElement($body->getContents());
                return $xml;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return null;
    }

    public function getPlayerData($gsId)
    {
        try {
            $client = new Client();
            $gsId = intval($gsId);
            $response = $client->get('https://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/profile?id=' . $gsId);

            if ($body = $response->getBody()) {
                $xml = new \SimpleXMLElement($body->getContents());
                return $xml;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return null;
    }

    public function getPlayerDataAsObject($gsId)
    {
        $playerData = new \stdClass();

        try {
            $raw = $this->getPlayerData($gsId);
            if (isset($raw->player)) {
                $playerData->player = $raw->player;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $playerData;
    }

    public function getTournaments()
    {
        try {
            $client = new Client();
            $response = $client->get('http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/pga_schedule');
            if ($body = $response->getBody()) {
                $xml = new \SimpleXMLElement($body->getContents());
                return $xml;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return null;
    }

    public function getPlayersAsCollection()
    {
        $playerCollection = new Collection();

        foreach ($this->getPlayers() as $player) {
            try {
                $playerCollection->push([
                    'player_id' => $player['id'],
                    'name' => $player['name'],
                    'country' => $player['country'],
                ]);
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }

        return $playerCollection;
    }

    public function getRankings()
    {
        try {
            $client = new Client();
            $response = $client->get('http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/ranking');
            if ($body = $response->getBody()) {
                $xml = new \SimpleXMLElement($body->getContents());
                return $xml;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return null;
    }

    public function getRankingsAsCollection()
    {
        $playerCollection = new Collection();

        try {
            foreach ($this->getRankings() as $player) {
                $playerCollection->push([
                    'player_id' => intval($player['id']),
                    'name' => strval($player['name']),
                    'pos' => strval($player['pos']),
                    'avg_points' => strval($player['avg_points']),
                    'points_total' => strval($player['points_total']),
                    'events' => strval($player['events']),
                ]);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $playerCollection;
    }


    /**
     * @return Collection
     */
    public function getCurrentScoreAsCollection()
    {
        $players = new Collection();

        try {
            $tournamentInfo = null;
            if ($this->liveScore)
                $tournamentInfo = $this->liveScore;
            if (!$this->liveScore || !$tournamentInfo)
                $tournamentInfo = $this->getCurrentTournament();
            $gsNewId = self::make_external_id($tournamentInfo->tournament['id'], $tournamentInfo->tournament['start_date']);
            foreach ($tournamentInfo->tournament->player as $player) {

                $players->push([
                    'player_id' => $player['id'],
                    'tournament_id' => $gsNewId,
                    'tournament_name' => $tournamentInfo->tournament['name'],
                    'player_name' => $player['name'],
                    'pos' => $player['pos'],
                    'hole' => (isset($player['hole'])) ? strval($player['hole']) : "0",
                    'par' => (isset($player['par'])) ? strval($player['par']) : "0",
                    'today' => $player['today'],
                    'total' => (isset($player['total']) && trim($player['total']) !== "") ? intval($player['total']) : 0,
                    'score_to_par' => (isset($player['scoreToPar']) && trim($player['scoreToPar']) !== "") ? strval($player['scoreToPar']) : 0,
                    'rounds' => $player->rounds
                ]);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $players;
    }

    /**
     * @return Collection
     */
    public function getTournamentsAsCollection()
    {
        $tournaments = new Collection();

        try {
            $tournamentsRaw = $this->getTournaments();
            foreach ($tournamentsRaw->tournament as $tournament) {
                $gsNewId = self::make_external_id($tournament['id'], $tournament['date_start']);
                $tData = [
                    "name" => strval($tournament["name"]),
                    "start_date" => date('Y-m-d', strtotime(strval($tournament["date_start"]))),
                    "end_date" => date('Y-m-d', strtotime(strval($tournament["date_end"]))),
                    "winner" => strval($tournament["winner"]),
                    "winner_id" => intval($tournament["winner_id"]),
                    "purse" => (int)$tournament["purse"],
                    "tournament_id" => $gsNewId,
                ];
                $tournaments->push($tData);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $tournaments;
    }

    /**
     * @return array
     */
    public function getCurrentTournamentAsArray()
    {
        $tournamentInfo = null;
        try {
            $tournamentInfo = $this->getCurrentTournament();
            $externalTourneyId = self::make_external_id(intval($tournamentInfo->tournament['id']), $tournamentInfo->tournament['start_date']);

            return [
                "name" => strval($tournamentInfo->tournament['name']),
                "status" => isset($tournamentInfo->tournament['status']) ? strval($tournamentInfo->tournament['status']) : '',
                "start_date" => strval($tournamentInfo->tournament['start_date']),
                "end_date" => strval($tournamentInfo->tournament['end_date']),
                "country" => strval($tournamentInfo->tournament['country']),
                "venue" => strval($tournamentInfo->tournament['venue']),
                "type" => strval($tournamentInfo->tournament['type']),
                "par" => strval($tournamentInfo->tournament['par']),
                "gender" => strval($tournamentInfo->tournament['gender']),
                "external_id" => $externalTourneyId,
            ];
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return null;
    }

    /**
     * @param null $gsId
     * @return Collection
     */
    public function getCurrentTournamentAsCollection($gsId = null)
    {
        //        $tournament= new Collection();
        $players = new Collection();

        try {
            $tournamentInfo = null;
            if ($gsId) {

                $tournamentInfo = $this->getTournament($gsId);
            } else {
                if ($this->liveScore) {
                    $tournamentInfo = $this->liveScore;
                } else {
                    $tournamentInfo = $this->getCurrentTournament();
                }
            }
            if (isset($tournamentInfo->tournament)) {
                $externalTourneyId = self::make_external_id(intval($tournamentInfo->tournament['id']), $tournamentInfo->tournament['start_date']);

                foreach ($tournamentInfo->tournament->player as $player) {
                    // need to test ***jp
                    // Cut players not marked CUT
                    if (isset($player['today']) && trim($player['today']) === "CUT")
                        $score_to_par = "CUT";
                    else
                        $score_to_par = (isset($player['scoreToPar']) && trim($player['scoreToPar']) !== "") ? strval($player['scoreToPar']) : 0;
                    // need to test ***jp

                    $roundData = [
                        'tournament_id' => $externalTourneyId,
                        'player_id' => intval($player['id']),
                        'tournament_name' => strval($tournamentInfo->tournament['name']),
                        'player_name' => strval($player['name']),
                        'pos' => strval($player['pos']),
                        'tee_time' => (isset($player['tee_time'])) ? strval($player['tee_time']) : null,
                        'par' => (isset($player['par'])) ? strval($player['par']) : "0",
                        'hole' => (isset($player['hole'])) ? strval($player['hole']) : "0",
                        'today' => (isset($player['today'])) ? strval($player['today']) : "-",
                        'po' => (isset($player['po'])) ? strval($player['po']) : "0",
                        'score_to_par' => $score_to_par,
                        'drive_dist_avg' => (isset($player['driveDistAvg']) && trim($player['driveDistAvg']) !== "") ? intval($player['driveDistAvg']) : 0,
                        'drive_accuracy_pct' => (isset($player['driveAccuracyPct']) && trim($player['driveAccuracyPct']) !== "") ? intval($player['driveAccuracyPct']) : 0,
                        'gir' => (isset($player['gir']) && trim($player['gir']) !== "") ? intval($player['gir']) : 0,
                        'putts_gir_avg' => (isset($player['puttsGirAvg']) && trim($player['puttsGirAvg']) !== "") ? intval($player['puttsGirAvg']) : 0,
                        'saves' => (isset($player['saves']) && trim($player['saves']) !== "") ? intval($player['saves']) : 0,
                        'eagles' => (isset($player['eagles']) && trim($player['eagles']) !== "") ? intval($player['eagles']) : 0,
                        'birdies' => (isset($player['birdies']) && trim($player['birdies']) !== "") ? intval($player['birdies']) : 0,
                        'pars' => (isset($player['pars']) && trim($player['pars']) !== "") ? intval($player['pars']) : 0,
                        'bogeys' => (isset($player['bogeys']) && trim($player['bogeys']) !== "") ? intval($player['bogeys']) : 0,
                        'doubles' => (isset($player['doubles']) && trim($player['doubles']) !== "") ? intval($player['doubles']) : 0,
                        'total' => (isset($player['total']) && trim($player['total']) !== "") ? intval($player['total']) : 0,
                    ];
                    $players->push($roundData);
                    //                \Log::info($roundData);

                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $players;
    }

    /**
     * @return Collection
     */
    public function getRoundsAsCollection()
    {
        $players = new Collection();

        try {
            $tournamentInfo = null;
            if ($this->liveScore)
                $tournamentInfo = $this->liveScore;
            if (!$this->liveScore || !$tournamentInfo)
                $tournamentInfo = $this->getCurrentTournament();
            if ($tournamentInfo && isset($tournamentInfo->tournament) && isset($tournamentInfo->tournament->player)) {
                $gsNewId = self::make_external_id(strval($tournamentInfo->tournament['id']), strval($tournamentInfo->tournament['start_date']));
                foreach ($tournamentInfo->tournament->player as $player) {
                    $roundsCollection = new Collection();
                    foreach ($player->rounds as $rounds) {
                        foreach ($rounds as $round) {
                            $roundNumber = intval($round['number']);
                            $holes = $this->_collect_holes($player, $roundNumber);
                            $roundsCollection->push([
                                'number' => $roundNumber,
                                'result' => strval($round['result']),
                                'holes' => $holes
                            ]);
                        }
                    }

                    $players->push([
                        'player_id' => strval($player['id']),
                        'tournament_id' => $gsNewId,
                        'tournament_name' => strval($tournamentInfo->tournament['name']),
                        'player_name' => strval($player['name']),
                        'pos' => strval($player['pos']),
                        'rounds' => $roundsCollection,
                    ]);
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $players;
    }

    /**
     * @return Collection
     */
    public function getHolesAsCollection()
    {
        $players = new Collection();

        try {
            $tournamentInfo = null;
            if ($this->liveScore)
                $tournamentInfo = $this->liveScore;
            if (!$this->liveScore || !$tournamentInfo)
                $tournamentInfo = $this->getCurrentTournament();
            $gsNewId = self::make_external_id(strval($tournamentInfo->tournament['id']), strval($tournamentInfo->tournament['start_date']));
            foreach ($tournamentInfo->tournament->player as $player) {

                $players->push([

                    'player_id' => $player['id'],
                    'tournament_id' => $gsNewId,
                    'tournament_name' => $tournamentInfo->tournament['name'],
                    'player_name' => $player['name'],
                    'pos' => $player['pos'],
                    'holes' => $player->stats->rounds,
                ]);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $players;
    }

    /**
     * @return array
     */
    public function getCurrentTournamentInfo()
    {
        $tournamentInfo = null;
        try {
            if ($this->liveScore)
                $tournamentInfo = $this->liveScore;
            if (!$this->liveScore || !$tournamentInfo)
                $tournamentInfo = $this->getCurrentTournament();
            $gsNewId = self::make_external_id(strval($tournamentInfo->tournament['id']), strval($tournamentInfo->tournament['start_date']));
            return [
                'name' => $tournamentInfo->tournament['name'],
                'tournament_id' => $gsNewId,
                'status' => $tournamentInfo->tournament['status'],
                'par' => $tournamentInfo->tournament['par'],
                'start_date' => $tournamentInfo->tournament['start_date'],
                'end_date' => $tournamentInfo->tournament['end_date']
            ];
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return null;
    }

    /**
     * @return \SimpleXMLElement
     */
    public function getCurrentTournament()
    {
        try {
            $client = new Client();
            $response = $client->get('http://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/live');
            if ($body = $response->getBody()) {
                $xml = new \SimpleXMLElement($body->getContents());
                $this->liveScore = $xml;
                return $xml;
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return null;
    }

    /**
     * @param $gsId
     * @return \SimpleXMLElement|null
     */
    public function getTournament($gsId)
    {
        try {
            $client = new Client();
            $response = $client->get('https://www.goalserve.com/getfeed/d263cc5ab8c44dc196157080c19422a7/golf/' . $gsId);
            if ($response->getBody()->getSize() > 0) {
                if ($response) {
                    if ($body = $response->getBody()) {
                        $xml = new \SimpleXMLElement($body->getContents());
                        $this->liveScore = $xml;
                        return $xml;
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }

        return null;
    }



    public static function getRoundScore($stats, $needle)
    {
        $score = @$stats->{$needle};
        $par = @$stats->par;
        if (is_null($stats)) {
            $res = "--";
        } else if (explode('_', $needle)[0] == 'round' && $score != 0) {
            $res = @$stats->{$needle};
        } else if ($par == 'CUT' || $par == 'WD') {
            $res = $par;
        } else if ($score == 0) {
            $res = '--';
        } else {
            $res = @$stats->{$needle};
        }
        return $res;
    }

    public static function getTotalScore($stats)
    {
        if (@$stats->par === 'CUT' || @$stats->par === 'WD') {
            $res = $stats->par;
        } elseif (@$stats->scoreToPar == 0) {
            $res = 'E';
        } else {
            $res = @$stats->scoreToPar;
        }
        return $res;
    }

    public static function group_number_to_letter($num)
    {
        $groups = [
            '1' => "A",
            '2' => "B",
            '3' => "C",
            '4' => "D",
            '5' => "E",
            '6' => "F",
            '7' => "G",
        ];
        return isset($groups[$num]) ? $groups[$num] : $num;
    }

    public static function group_letter_to_number($letter)
    {
        $letter = strtoupper($letter);
        $groups = [
            "A" => '1',
            "B" => '2',
            "C" => '3',
            "D" => '4',
            "E" => '5',
            "F" => '6',
            "G" => '7',
        ];
        return isset($groups[$letter]) ?: $letter;
    }

    public static function fetch_pga_golfers($local = false)
    {
        if ($local) {
            $file = Storage::get('pga_players.json');
            return json_decode($file, true);
        }
        $client = new \Goutte\Client();
        $players = [];

        try {
            $crawler = $client->request('GET', 'https://www.pgatour.com/players.html');
            $crawler->filter('.player-card > a')->each(function ($node) use (&$players) {
                //                print "<pre>".$node->text().$node->html()."</pre>\n";
                $href = $node->attr('href');
                $firstName = $node->filter('.player-content > .player-name > .player-firstname')->first()->text();
                $lastName = $node->filter('.player-content > .player-name > .player-surname')->first()->text();
                $parts = explode('.', str_replace('/players/player.', '', $href));
                $id = $parts[0];
                $players[] = [
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'id' => $id,
                    'name' => $firstName . " " . $lastName
                ];
            });
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $players;
    }

    public static function fetch_tournament_players($url = null)
    {
        $client = new \Goutte\Client();
        $players = [];
        $crawler = $client->request('GET', $url);
        //https://statdata.pgatour.com/r/473/2020/field.json
        $crawler->filter('.hasCustomSelect')->each(function ($node) use (&$players) {
            //                print "<pre>".$node->text().$node->html()."</pre>\n";
            /*$href = $node->attr('href');
            $firstName = $node->filter('.player-content > .player-name > .player-firstname')->first()->text();
            $lastName = $node->filter('.player-content > .player-name > .player-surname')->first()->text();
            $parts = explode('.', str_replace('/players/player.','',$href));
            $id = $parts[0];
            $players[] = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'id' => $id,
                'name' => $firstName." ".$lastName
            ];*/
        });
        return $players;
    }

    public static function get_golf_distribution_config($key, $default)
    {
        return SiteHelper::conf($key, 'text', $default, true);
    }

    public static function show_picks(&$golf, $adminOverride = true)
    {
        $pool = $golf->pool;
        if (!$pool) return false;
        //only commissioners
        if ($pool->is_commissioner && $adminOverride) {
            return true;
        }
    }

    public static function format_score($score, $status = null)
    {
        if ($status && $status !== 'Active') {
            return $status;
        }
        $score = (int)$score;
        if ($score === 0) {
            return "E";
        }
        return ($score > 0) ? "+" . $score : "-" . $score;
    }

    public static function pretty_prize($player, $dollarSign = "$")
    {
        if ($player->status !== 'Active') {
            return $dollarSign . "0";
        }
        return $dollarSign . number_format($player->prize);
    }

    public static function pretty_score($score, $handicap = null)
    {
        if ($score === "" || $score === null || $score === false) return null;
        $score = intval($score);
        $ast = "";
        if ($handicap) {
            $score = $score + $handicap;
            $ast = "*";
        }
        if ($score === 0) {
            return "E" . $ast;
        }
        if ($score === "" || $score === null) {
            return "E";
        }
        return ($score > 0) ? "+" . $score . $ast : $score . $ast;
    }

    public static function pdf_pick_instructions_sentence(&$golf)
    {
        $sentence = "Select a roster of {total_golfers} golfers: {group_max_list}. \nLowest score of your top {toward_score} golfers will be used to tally your {individual_round} total score.\nIf at least {toward_score} golfers do not make the cut your team will be disqualified for rounds 3 & 4.";
        $configSentence = __c('golf.pdf.content.header_instructions_var', $sentence, [
            'vars' => [
                'total_golfers' => [
                    'key' => "{total_golfers}",
                    'type' => "number",
                    'description' => "total golfers allowed to pick"
                ],
                'group_max_list' => [
                    'key' => "{group_max_list}",
                    'type' => "string",
                    'description' => "formatted (N)A, (N)B ..."
                ],
                'toward_score' => [
                    'key' => "{toward_score}",
                    'type' => "number",
                    'description' => "how many golfers towards score"
                ],
                'individual_round' => [
                    'key' => "{individual_round}",
                    'type' => "string",
                    'description' => "Conditional string, if num of round winners > 0"
                ],
            ]
        ], false, 'text');

        $total = 0;
        $groupPicks = [];
        $groupPicksListString = "";

        try {
            foreach ($golf->config->groups as $group) {
                if ($group->enabled) {
                    $total = $total + $group->max;
                    $groupPicks[] = "({$group->max})$group->name";
                }
            }
            $groupPicksListString = implode(', ', $groupPicks);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        $vars = [
            'total_golfers' => $total,
            'group_max_list' => $groupPicksListString,
            'toward_score' => $golf->toward_score,
            'individual_round' => ($golf->number_of_round_winners > 0) ? "individual round and" : "",
        ];
        foreach ($vars as $key => $value) {

            $configSentence = str_replace("{" . $key . "}", $value, $configSentence);
        }
        return $configSentence;
    }

    public static function do_pos(&$items)
    {
    }

    public function log_feed($type = 'unknown', $item, $localTournament)
    {
        //        \Log::info($item);
        //        \Log::info($localTournament);
    }

    public static function parse_scoring_to_par($scoring, $par = 0)
    {
        $returnVal = null;
        try {
            if ($scoring === 'E') {
                return 0;
            }
            if ($scoring === "-" || $scoring === "") {
                return null;
            }
            if (is_numeric($par)) {
                $scoring = intval($scoring);
                $par = intval($par);
                $returnVal = intval($scoring - $par);
            } else {
                $returnVal = null;
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }

        return $returnVal;
    }

    public static function is_finished($status)
    {
        return ($status === 'Final');
    }

    public static function get_not_started($status)
    {
        $curRound = self::get_current_round($status);
        return (!$curRound || $curRound === 0);
    }

    public static function in_progress($status)
    {
        $statuses = [

            'Round 1 - In Progress' => 1,
            'Round 2 - In Progress' => 2,
            'Round 3 - In Progress' => 3,
            'Round 4 - In Progress' => 4,
            'First Round - In Progress' => 1,
            'Second Round - In Progress' => 2,
            'Third Round - In Progress' => 3,
            'Fourth Round - In Progress' => 4,
        ];
        return (isset($statuses[$status])) ? $statuses[$status] : true;
    }

    public static function get_current_round($status)
    {
        $statuses = [
            'Cancelled' => null,
            'Abandoned' => null,
            'Interrupted' => null,
            'Postponed' => null,
            'Not Started' => 0,
            'Delayed' => null,

            'Round 1 - In Progress' => 1,
            'Round 1 - Play Complete' => 1,
            'Round 1' => 1,
            'Round 2 - In Progress' => 2,
            'Round 2 - Play Complete' => 2,
            'Round 2' => 2,
            'Round 3 - In Progress' => 3,
            'Round 3 - Play Complete' => 3,
            'Round 3' => 3,
            'Round 4 - In Progress' => 4,
            'Round 4' => 4,
            'Round 4 - Play Complete' => 5,

            'First Round - In Progress' => 1,
            'First Round - Play Complete' => 1,
            'Second Round - In Progress' => 2,
            'Second Round - Play Complete' => 2,
            'Third Round - In Progress' => 3,
            'Third Round - Play Complete' => 3,
            'Fourth Round - In Progress' => 4,
            'Fourth Round - Play Complete' => 5,
            'Final' => 5,

        ];
        return (isset($statuses[$status])) ? $statuses[$status] : true;
    }

    public static function is_final($status)
    {
        return (self::get_current_round($status) === 5);
    }

    public static function is_in_progress($status)
    {
        $statuses = [
            'Cancelled' => true,
            'Abandoned' => true,
            'Interrupted' => true,
            'Postponed' => false,
            'Not Started' => false,
            'Round 1 - In Progress' => true,
            'Round 1 - Play Complete' => true,
            'Round 2 - In Progress' => true,
            'Round 2 - Play Complete' => true,
            'Round 3 - In Progress' => true,
            'Round 3 - Play Complete' => true,
            'Round 4 - In Progress' => true,
            'Round 4 - Play Complete' => true,
            'First Round - In Progress' => true,
            'First Round - Play Complete' => true,
            'Second Round - In Progress' => true,
            'Second Round - Play Complete' => true,
            'Third Round - In Progress' => true,
            'Third Round - Play Complete' => true,
            'Fourth Round - In Progress' => true,
            'Fourth Round - Play Complete' => true,
            'Final' => true,
            'Delayed' => false,
        ];
        return isset($statuses[$status]) ? $statuses[$status] : true;
    }



    public static function get_completed_round($status)
    {
        $statuses = [
            'Cancelled' => null,
            'Abandoned' => null,
            'Interrupted' => null,
            'Postponed' => 0,
            'Not Started' => 0,
            'Round 1 - In Progress' => 0,
            'Round 1 - Play Complete' => 1,
            'Round 2 - In Progress' => 1,
            'Round 2 - Play Complete' => 2,
            'Round 3 - In Progress' => 2,
            'Round 3 - Play Complete' => 3,
            'Round 4 - In Progress' => 3,
            'Round 4 - Play Complete' => 4,


            'First Round - In Progress' => 0,
            'First Round - Play Complete' => 1,
            'Second Round - In Progress' => 1,
            'Second Round - Play Complete' => 2,
            'Third Round - In Progress' => 2,
            'Third Round - Play Complete' => 3,
            'Fourth Round - In Progress' => 3,
            'Fourth Round - Play Complete' => 4,

            'Final' => 4,
            'Delayed' => 0,
        ];
        return (isset($statuses[$status])) ? $statuses[$status] : null;
    }

    public static function handicap_score($score, $handicap = 0, $isRound = false, $handicapRound = false)
    {
        $applyHandicapToRounds = __conf('golf.handicaps.handicap_rounds', 'boolean', false) || $handicapRound;
        //        if($handicap === null && $score === null){
        if ($handicap === null) {
            return $score;
        }
        if ($isRound) {
            if ($applyHandicapToRounds) {
                return $score + $handicap;
            }
            return $score;
        }
        return $score + $handicap; //we always handicap total.. do we now?
    }

    public static function make_external_id($gsId, $stringDate)
    {
        $year = date('Y', strtotime($stringDate));
        $newId = $gsId . "-" . $year;
        return $newId;
    }

    public static function reverse_external_id($id)
    {
        $ids = explode('-', $id);
        return ($ids[0]);
    }

    public static function prize_default_hardcoded()
    {
        return [
            '1' => '18',
            '2' =>    '10.9',
            '3' =>    '6.9',
            '4' =>    '4.9',
            '5' =>    '4.1',
            '6' =>    '3.625',
            '7' =>    '3.375',
            '8' =>    '3.125',
            '9' =>    '2.925',
            '10' =>    '2.725',
            '11' =>    '2.525',
            '12' =>    '2.325',
            '13' =>    '2.125',
            '14' =>    '1.925',
            '15' =>    '1.825',
            '16' =>    '1.725',
            '17' =>    '1.625',
            '18' =>    '1.525',
            '19' =>    '1.425',
            '20' =>    '1.325',
            '21' =>    '1.225',
            '22' =>    '1.125',
            '23' =>    '1.045',
            '24' =>    '0.965',
            '25' =>    '0.885',
            '26' =>    '0.805',
            '27' =>    '0.775',
            '28' =>    '0.745',
            '29' =>    '0.715',
            '30' =>    '0.685',
            '31' =>    '0.655',
            '32' =>    '0.625',
            '33' =>    '0.595',
            '34' =>    '0.57',
            '35' =>    '0.545',
            '36' =>    '0.52',
            '37' =>    '0.495',
            '38' =>    '0.475',
            '39' =>    '0.455',
            '40' =>    '0.435',
            '41' =>    '0.415',
            '42' =>    '0.395',
            '43' =>    '0.375',
            '44' =>    '0.355',
            '45' =>    '0.335',
            '46' =>    '0.315',
            '47' =>    '0.295',
            '48' =>    '0.279',
            '49' =>    '0.265',
            '50' =>    '0.257',
            '51' =>    '0.251',
            '52' =>    '0.245',
            '53' =>    '0.241',
            '54' =>    '0.237',
            '55' =>    '0.235',
            '56' =>    '0.233',
            '57' =>    '0.231',
            '58' =>    '0.229',
            '59' =>    '0.227',
            '60' =>    '0.225',
            '61' =>    '0.223',
            '62' =>    '0.221',
            '63' =>    '0.219',
            '64' =>    '0.217',
            '65' =>    '0.215',
            '66' =>    '0.208',
            '67' =>    '0.206',
            '68' =>    '0.204',
            '69' =>    '0.202',
            '70' =>    '0.2',
        ];
    }

    public static function reverse_last_first($name = null)
    {
        if ($name) {
            try {
                $parts = explode(',', $name);
                $count = count($parts);
                if ($count === 3) {
                    $firstName = isset($parts[2]) ? trim($parts[2]) : "";
                    $middleName = isset($parts[1]) ? trim($parts[1]) : "";
                    $lastName = isset($parts[0]) ? trim($parts[0]) : "";
                    $fullName = "{$firstName} {$middleName} {$lastName}";
                    $mobile = substr($firstName, 0, 1) . ". {$middleName} {$lastName}";
                    $short = "{$lastName} ({$middleName})";
                } elseif ($count === 2) {
                    $firstName = isset($parts[1]) ? trim($parts[1]) : "";
                    $middleName = "";
                    $lastName = isset($parts[0]) ? trim($parts[0]) : "";
                    $fullName = "{$firstName} {$lastName}";
                    $mobile = substr($firstName, 0, 1) . ". {$lastName}";
                    $short = "{$lastName}";
                } elseif ($count === 1) {
                    $firstName = $parts[0];
                    $middleName = "";
                    $lastName = "";
                    $fullName = "{$firstName} {$middleName} {$lastName}";
                    $mobile = $firstName;
                    $short = "{$firstName}";
                } else {
                    $firstName = $name;
                    $middleName = "";
                    $lastName = "";
                    $fullName = "";
                    $mobile = "";
                    $short = "";
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
                $firstName = $name;
                $middleName = "";
                $lastName = "";
                $fullName = "";
                $mobile = "";
                $short = "";
            }
        }
        return ['first' => $firstName, 'last' => $lastName, 'middle' => $middleName, 'full' => $fullName, 'mobile' => $mobile, 'short' => $short];
    }

    public static function prize_defaults()
    {
        $defaults = GolfPrizeConfig::orderBy('place', 'asc')->get();
        $ret = [];
        foreach ($defaults as $default) {
            try {
                $stringPlace = (string)$default->place;
                $ret[$stringPlace] = $default->percent;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $ret;
    }

	public static function is_player_out($pos = null){
		return in_array(strtoupper($pos), ['DQ','WD','NS','NC','DNF','NR','CUT', 'MDF']);
	}
	
	public static function get_lock_time($tournamentPlayers = []){
		
		$tees = [];
		foreach($tournamentPlayers as $gtp) {
			$tee = strtotime($gtp->r1_teetime);
			$times = explode(' ',$gtp->r1_teetime);
			if(isset($times[1]) && $times[1] != '00:00:00'){
				$tees[$tee] = $gtp->r1_teetime;
			}
		}
		if(count($tees)){
			sort($tees);
			$first = $tees[0];
			$teeOff = date('Y-m-d H:i:s', strtotime($first));
			
			return $teeOff;
//			return date('Y-m-d G:i a', $firtToGo);
		}
		return null;
		
		
	}
	
    private function _collect_holes(&$player, $number)
    {
        $holes = new Collection();
        try {
            foreach ($player->stats as $stats) {
                foreach ($stats->rounds as $rounds) {
                    foreach ($rounds as $round) {
                        $curNumber = intval($round['number']);
                        if ($curNumber === $number) {
                            if (isset($round->hole)) {
                                foreach ($round->hole as $hole) {
                                    $holes->push(
                                        [
                                            "number" => strval($hole['number']),
                                            "par" => strval($hole['par']),
                                            "score" => strval($hole['score']),
                                            "eagle" => $this->_shitty_true_false_to_bool($hole['eagle']),
                                            "birdie" => $this->_shitty_true_false_to_bool($hole['birdie']),
                                            "bogey" => $this->_shitty_true_false_to_bool($hole['bogey']),
                                            "dbl_bogey_worse" => $this->_shitty_true_false_to_bool($hole['dbl_bogey_worse']),
                                        ]
                                    );
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return $holes;
    }

    private function _shitty_true_false_to_bool($shit)
    {
        $shit = strtolower(strval($shit));
        if ($shit === 'true') {
            return true;
        } else {
            return false;
        }
    }
}
$defaults = [
    '1' => '18',
    '2' =>    '10.9',
    '3' =>    '6.9',
    '4' =>    '4.9',
    '5' =>    '4.1',
    '6' =>    '3.625',
    '7' =>    '3.375',
    '8' =>    '3.125',
    '9' =>    '2.925',
    '10' =>    '2.725',
    '11' =>    '2.525',
    '12' =>    '2.325',
    '13' =>    '2.125',
    '14' =>    '1.925',
    '15' =>    '1.825',
    '16' =>    '1.725',
    '17' =>    '1.625',
    '18' =>    '1.525',
    '19' =>    '1.425',
    '20' =>    '1.325',
    '21' =>    '1.225',
    '22' =>    '1.125',
    '23' =>    '1.045',
    '24' =>    '0.965',
    '25' =>    '0.885',
    '26' =>    '0.805',
    '27' =>    '0.775',
    '28' =>    '0.745',
    '29' =>    '0.715',
    '30' =>    '0.685',
    '31' =>    '0.655',
    '32' =>    '0.625',
    '33' =>    '0.595',
    '34' =>    '0.57',
    '35' =>    '0.545',
    '36' =>    '0.52',
    '37' =>    '0.495',
    '38' =>    '0.475',
    '39' =>    '0.455',
    '40' =>    '0.435',
    '41' =>    '0.415',
    '42' =>    '0.395',
    '43' =>    '0.375',
    '44' =>    '0.355',
    '45' =>    '0.335',
    '46' =>    '0.315',
    '47' =>    '0.295',
    '48' =>    '0.279',
    '49' =>    '0.265',
    '50' =>    '0.257',
    '51' =>    '0.251',
    '52' =>    '0.245',
    '53' =>    '0.241',
    '54' =>    '0.237',
    '55' =>    '0.235',
    '56' =>    '0.233',
    '57' =>    '0.231',
    '58' =>    '0.229',
    '59' =>    '0.227',
    '60' =>    '0.225',
    '61' =>    '0.223',
    '62' =>    '0.221',
    '63' =>    '0.219',
    '64' =>    '0.217',
    '65' =>    '0.215',
    '66' =>    '0.208',
    '67' =>    '0.206',
    '68' =>    '0.204',
    '69' =>    '0.202',
    '70' =>    '0.2',
];
