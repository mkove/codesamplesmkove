<?php

namespace App\Helpers;

class GameHelper
{ 
    public static function prepare_live_games($games)
    {
        $responseData = [];
        foreach ($games as $game) {
            try {
                $responseData['games'][] =
                    [
                        'id'  => $game->id,
                        'clock'  => $game->timer,
                        'status'  => $game->status,
                        'hometeam'  => [
                            'id'  => $game->hometeam_id,
                            'name'  => $game->hometeam_name,
                            'totalscore'  => $game->hometeam_totalscore,
                            'winner'  => ($game->hometeam_totalscore > $game->awayteam_totalscore),
                            'pickcount'  => 0
                        ],
                        'awayteam'  => [
                            'id'  => $game->awayteam_id,
                            'name'  => $game->awayteam_name,
                            'totalscore'  => $game->awayteam_totalscore,
                            'winner'  => ($game->awayteam_totalscore > $game->hometeam_totalscore),
                            'pickcount'  => 0
                        ]
                    ];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $responseData;
    }

    public static function get_weekly_games($games)
    {
        $weeklyGames = [];
        foreach ($games as $game) {
            try {
                if ($game->week > 0 && $game->week < __conf('schwindy.max_week', 'text', '18') + 1) {
                    $weeklyGames[$game->week][$game->contestID] = $game;
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $weeklyGames;
    }

    public static function recount(&$events, $homeTeamId = 0)
    {
        $events = $events->orderBy('order', 'asc');
        $scores = [
            'hometeam_q1' => 0,
            'hometeam_q2' => 0,
            'hometeam_q3' => 0,
            'hometeam_q4' => 0,
            'hometeam_ot' => 0,
            'hometeam_totalscore' => 0,
            'awayteam_q1' => 0,
            'awayteam_q2' => 0,
            'awayteam_q3' => 0,
            'awayteam_q4' => 0,
            'awayteam_ot' => 0,
            'awayteam_totalscore' => 0,
        ];
        foreach ($events as $event) {
            try {
                $quarterHometeam = "hometeam_" . $event->quarter;
                $quarterAwayteam = "awayteam_" . $event->quarter;
                if ($event->scoring_team == 'home') {
                    $scores[$quarterHometeam] += $event->points;
                    $scores['hometeam_totalscore'] += $event->points;
                } else {
                    $scores[$quarterAwayteam] += $event->points;
                    $scores['awayteam_totalscore'] += $event->points;
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $scores;
    }

    public static function get_lose_win_teams(&$game, $quarter = 'totalscore')
    {
        try {
            $htAccessor = "hometeam_{$quarter}";
            $atAccessor = "awayteam_{$quarter}";
            $homeTeamScore = $game->{$htAccessor};
            $awayTeamScore = $game->{$atAccessor};
            if ($homeTeamScore > $awayTeamScore) {
                $winningTeam = $game->home_team;
                $winningScore = $homeTeamScore;
                $losingTeam = $game->away_team;
                $losingScore = $awayTeamScore;
            } else {
                $winningTeam = $game->away_team;
                $winningScore = $awayTeamScore;
                $losingTeam = $game->home_team;
                $losingScore = $homeTeamScore;
            }

            return [
                'losing' => [
                    'team' => $losingTeam,
                    'score' => $losingScore,
                    'quarter' => $quarter
                ],
                'winning' => [
                    'team' => $winningTeam,
                    'score' => $winningScore,
                    'quarter' => $quarter
                ]
            ];
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return null;
    }

    public static function game_finished(&$game)
    {
        $status = trim(strtolower($game->status));
        $finish = ['final', 'after over time'];
        return in_array($status, $finish);
    }

    public static function game_in_play(&$game, $force = false)
    {
        if ($force) return true;
        $status = trim(strtolower($game->status));
        $notInPlay = [
            'postponed', 'cancelled', 'abandoned', 'not started', 'final', 'after over time', 'break time', 'awarded', 'interrupted', 'delayed'
        ];
        $inPlay = ['1st quarter', '2nd quarter', '3rd quarter', '4th quarter', 'overtime', 'walk over'];
        if (in_array($status, $inPlay)) {
            return true;
        }

        if (in_array($status, $notInPlay)) {
            return false;
        }

        return false;
    }
}
