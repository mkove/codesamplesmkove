<?php

namespace App\Helpers;

class GoalServeHelper
{

    //		parse all

    static function process($fileName)
    {
        try {
            $xmlFile = self::get_xml($fileName);
            $xml = simplexml_load_string($xmlFile);
            return self::parse_league($xml);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return null;
    }

    static function parse_league($xml)
    {
        try {
            $data = json_decode(json_encode($xml), true);
            $league = [];
            $league['name'] = self::_get_attributes($data, 'name');
            $league['id'] = self::_get_attributes($data, 'id');
            $league['tournaments'] = self::parse_tournaments($data['tournament']);
            $real = self::flatten($league);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            $real = null;
        }

        return $real;
    }

    static function parse_tournaments($data)
    {
        $tournaments = [];
        foreach ($data as $dataTourney) {
            try {
                $tournament = self::parse_tournament($dataTourney);
                $tournaments[$tournament['id']] = $tournament;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $tournaments;
    }

    static function parse_tournament($data)
    {
        try {
            $tournament['id'] = self::_get_attributes($data, 'id');
            $tournament['name'] = self::_get_attributes($data, 'name');
            $tournament['weeks'] = isset($data['week']) ? self::parse_weeks($data['week']) : [];
            return $tournament;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return null;
    }

    static function parse_weeks($data)
    {
        $weekNum = 1;
        $weeks = [];
        foreach ($data as $week) {
            try {
                $weeks[$weekNum] = self::parse_week($week, $weekNum);
                $weekNum++;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $weeks;
    }

    static function parse_week($data, $num)
    {
        if (isset($data['matches'])) {
            try {
                $matches = self::_fix_single($data['matches']);
                $week = [];
                $week['name'] = self::_get_attributes($data, 'name');
                $week['number'] = $num;
                $week['games'] = self::parse_matches($matches);
                return $week;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return [];
    }

    static function parse_matches($data)
    {
        $games = [];
        foreach ($data as $game) {
            try {
                $timezone = self::_get_attributes($game, 'timezone');
                $games[] = self::parse_games($game, $timezone);
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $games;
    }

    static function parse_games($data, $timezone)
    {
        $games = [];
        $matchCount = 0;
        //			dump($data['match']);
        try {
            //				$game = self::_get_attributes($data, $data['@attributes']);
            $matches = isset($data['match']) ? self::_fix_single($data['match']) : [];
            if (count($matches)) {
                foreach ($matches as $match) {
                    $matchCount++;
                    try {
                        $contestId = self::_get_attributes($match, 'contestID');
                        $games[$contestId]['date'] = self::_get_attributes($match, $match['@attributes']);
                        $games[$contestId]['date']['timezone'] = $timezone;
                        $games[$contestId]['awayteam'] = isset($match['awayteam']) ? self::parse_team($match['awayteam']) : null;
                        $games[$contestId]['hometeam'] = isset($match['hometeam']) ? self::parse_team($match['hometeam']) : null;
                        $odds = isset($match['odds']) ? self::parse_odds($match['odds']) : null;

                        $games[$contestId]['over_under'] = isset($odds['over_under']) ? $odds['over_under'] : '0';
                        $games[$contestId]['hometeam']['handicap'] = isset($odds['handicap']['hometeam']) ? $odds['handicap']['hometeam'] : '0';
                        $games[$contestId]['awayteam']['handicap'] = isset($odds['handicap']['awayteam']) ? $odds['handicap']['awayteam'] : '0';
                        //							echo "<h4>$contestId</h4>";
                        //							dump($match, $contestId, $matchCount);
                        //							echo "<hr>";
                    } catch (\Exception $e) {
                        dd("ERROR", $match, $e->getMessage(), $e->getTrace(), $e->getTrace());
                    }
                }
            }
        } catch (\Exception $e) {
            dump("ERR");
        }
        return $games;
    }

    static function parse_odds($data)
    {
        $odds = [];
        if (isset($data['type']))
            foreach ($data['type'] as $type) {
                try {
                    $typeval = $type['@attributes']['id'];
                    if ($typeval == '3') {

                        $odds['over_under'] = self::parse_over_under($type['bookmaker']);
                    }
                    if ($typeval == '4') {
                        $odds['handicap'] = self::parse_handicap($type['bookmaker']);
                    }
                } catch (\Exception $e) {
                    \Log::error(":" . $e->getMessage());
                }
            }
        return $odds;
    }

    static function parse_team($data)
    {
        return self::_get_attributes($data, $data['@attributes']);
    }

    static function parse_over_under($data)
    {
        $overUnder = "0";
        foreach ($data as $bookmaker) {
            try {
                $bookmakerName = (isset($bookmaker['@attributes']['id'])) ? $bookmaker['@attributes']['id'] : null;
                if ($bookmakerName == '20') {
                    $currentOdd = $bookmaker['total'];
                    $overUnder = isset($currentOdd['@attributes']['name']) ? floatval($currentOdd['@attributes']['name']) : '0';
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $overUnder;
    }

    static function parse_handicap($data)
    {
        $saveOdd = [
            'hometeam' => 0,
            'awayteam' => 0
        ];
        $match = ['1' => 'hometeam', '2' => 'awayteam'];
        foreach ($data as $bookmaker) {
            try {
                $bookmakerName = $bookmaker['@attributes']['id'];
                if ($bookmakerName == '20') {
                    $handicap = $bookmaker['handicap'];
                    $currentOdd = self::_find_by_dp3($handicap);
                    if (isset($currentOdd['odd'][0]['@attributes']['handicap'])) {
                        $saveOdd[$match[$currentOdd['odd'][0]['@attributes']['name']]] = $currentOdd['odd'][0]['@attributes']['handicap'];
                    }
                    if (isset($currentOdd['odd'][1]['@attributes']['handicap'])) {

                        $saveOdd[$match[$currentOdd['odd'][1]['@attributes']['name']]] = $currentOdd['odd'][1]['@attributes']['handicap'];
                    }
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        return $saveOdd;
    }

    static function _get_attributes($data, $attributes)
    {
        $attr = isset($data['@attributes']) ? $data['@attributes'] : [];
        if (is_array($attributes)) {
            $ret = [];
            foreach ($attr as $key => $attribute) {
                try {
                    $ret[$key] = $attribute;
                } catch (\Exception $e) {
                    \Log::error(":" . $e->getMessage());
                }
            }
            return $ret;
        } else {
            try {
                $attr = isset($data['@attributes']) ? $data['@attributes'] : [];
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
            return isset($attr[$attributes]) ? $attr[$attributes] : "";
        }
    }

    static private function _fix_single($data, $test = "@attributes")
    {
        return (isset($data[$test])) ? [$data] : $data;
    }

    static function flatten($league)
    {
        $gameData = [];
        $singeGame = [];
        foreach ($league['tournaments'] as $tournaments) {
            try {
                $singeGame['season'] = $tournaments['name'];
                foreach ($tournaments['weeks'] as $week) {
                    if (count($week))
                        try {
                            $singeGame['week'] = $week['number'];
                            foreach ($week['games'] as $games) {
                                foreach ($games as $game) {
                                    try {

                                        $name = $game['awayteam']['name'] . " at " . $game['hometeam']['name'];
                                        $singeGame['contestID'] = $game['date']['contestID'];
                                        //										echo "<h2>".$singeGame['contestID']."</h2>";
                                        $singeGame['tournament_name'] = $name;
                                        $singeGame['tournament_id'] = "0";

                                        $singeGame['timezone'] = $game['date']['timezone'];
                                        $gameTime = date('Y-m-d H:i:s', strtotime($game['date']['formatted_date'] . " " . $game['date']['time']));;
                                        $singeGame['time'] = $gameTime;
                                        $singeGame['start'] = $gameTime;
                                        $singeGame['gmt'] = self::_to_gmt($gameTime);
                                        $singeGame['end'] = '0000-00-00 00:00:00';
                                        $singeGame['status'] = $game['date']['status'];
                                        $singeGame['over_under'] = $game['over_under'];
                                        $teams = [
                                            'hometeam' => [
                                                "name", "q1", "q2", "q3", "q4", "ot", "totalscore", "id", "handicap"
                                            ],
                                            'awayteam' => [
                                                "name", "q1", "q2", "q3", "q4", "ot", "totalscore", "id", "handicap"
                                            ]
                                        ];
                                        foreach ($teams as $teamStr => $values) {
                                            foreach ($values as $el) {
                                                try {
                                                    $singeGame[$teamStr . '_' . $el] = (isset($game[$teamStr][$el]) && $game[$teamStr][$el]) ? $game[$teamStr][$el] : 0;
                                                } catch (\Exception $e) {
                                                    \Log::error($e->GetMessage());
                                                    //												dd("2",$game);

                                                }
                                            }
                                        }
                                        $gameData[$singeGame['contestID']] = $singeGame;
                                    } catch (\Exception $e) {
                                        //									dd('3',$game);

                                    }
                                }
                                //								dd('4',$singeGame);

                            }
                        } catch (\Exception $e) {
                            //							dd('5',$week);

                        }
                }
            } catch (\Exception $e) {
                //					dd('6',$tournaments);

            }
        }
        return $gameData;
    }

    static function _pick_over_under($listOfData)
    {
    }

    static function _find_by_dp3($odds)
    {

        $tempHold = [];
        foreach ($odds as $odd) {
            try {
                $main = (isset($odd['@attributes']['main'])) ? $odd['@attributes']['main'] : null;
                if (isset($odd['odd'])) {

                    $dp3h = (isset($odd['odd'][0]['@attributes']['dp3'])) ? floatval($odd['odd'][0]['@attributes']['dp3']) : null;
                    $dp3a = (isset($odd['odd'][1]['@attributes']['dp3'])) ? floatval($odd['odd'][1]['@attributes']['dp3']) : null;
                    if ($dp3a && $dp3h) {
                        $dpDiff = abs($dp3a - $dp3h);
                        $tempHold["{$dpDiff}"] = $odd;
                    }
                }
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        ksort($tempHold);
        $currentOdd = current($tempHold);
        return $currentOdd;
    }

    static function _to_gmt($timeString)
    {
        $GMT = new \DateTimeZone("GMT");
        $date = new \DateTime($timeString, $GMT);
        return $date;
    }

    static function get_xml($filePath = 'http://goalserve.com/samples/nfl_schedules.xml')
    {
        try {
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $filePath);
            $contents = curl_exec($c);
            curl_close($c);

            if ($contents) return $contents;
            else return false;
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return false;
    }
}
