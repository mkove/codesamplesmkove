<?php
//    namespace App\Helpers;
//
//    use App\Models\SiteContent;
//
//    class ContentHelper{
//
//        public static function __c($identifier, $vars = []){
//
//            if($content =  SiteContent::where('identifier', $identifier)->first()) {
//                return (count($vars))
//                    ? str_replace(array_keys($vars), array_values($vars), $content->content)
//                    : $content->content;
//            }
//            return "";
//        }
//    }

use App\Helpers\GolfHelper;
use App\Helpers\InvoiceHelper;
use App\Helpers\SiteHelper;



function __warn($message = "", $title = null, $icon = null)
{
    $message = __parse_config_var($message);
    $title = __parse_config_var($title);
    $icon = __parse_config_var($icon);
    return __show_notice('danger', $message, $title, $icon);
}

function __info($message = "", $title = null, $icon = null)
{
    $message = __parse_config_var($message);
    $title = __parse_config_var($title);
    $icon = __parse_config_var($icon);
    return __show_notice('info', $message, $title, $icon);
}

function __alert($message = "", $title = null, $icon = null)
{
    $message = __parse_config_var($message);
    $title = __parse_config_var($title);
    $icon = __parse_config_var($icon);
    return __show_notice('warning', $message, $title, $icon);
}

function __success($message = "", $title = null, $icon = null)
{
    $message = __parse_config_var($message);
    $title = __parse_config_var($title);
    $icon = __parse_config_var($icon);
    return __show_notice('success', $message, $title, $icon);
}

function __parse_config_var($item)
{
    $htmlString = "";
    if (is_array($item)) {
        try {
            $keys = array_keys($item);
            $values = array_values($item);
            $key = isset($keys[0]) ? $keys[0] : null;
            $value = isset($values[0]) ? $values[0] : null;
            return ($key && $value) ? __c($key, $value, [], false) : "";
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }
    return $item;
}

function __format_score($score, $status)
{
    return SiteHelper::format_score($score, $status);
}

function __show_notice($type = 'success', $message = "", $title = null, $icon = null)
{
    $iconHtml = ($icon) ? '<div class="col-md-1 text-center">
                            <div style="display: table; height: 100%; overflow: hidden;" class="text-center">
                                <div style="display: table-cell; vertical-align: middle;" >
                                    <div>
                                        <i class="fa fa-' . $icon . ' fa-2x"></i >
                                    </div>
                                </div>
                            </div>

                        </div>' : "";
    $titleHtml = ($title) ? "<h5>" . $title . "</h5>" : '';
    $html = '<div class="alert alert-' . $type . '">
                    <div class="row">
                        ' . $iconHtml . '
                        <div class="col-md-9">
                            ' . $titleHtml . '
                            <h6>' . $message . '</h6>
                        </div>
                    </div>
                </div>';
    return $html;
}

function __cm($key, $default, $type)
{
    return SiteHelper::add_config_notice($key, $default, $type);
}

function __c($id = null, $default = "", $vars = [], $editable = true, $type = 'string')
{
    return \App\Helpers\SiteHelper::__c($id, $default, $vars, $editable, $type);
}

function __gcd($id, $default, $percent = false)
{
    $fraction = GolfHelper::get_golf_distribution_config($id, $default);
    return ($percent) ? $fraction * 100 : $fraction;
}

function __gcd_sum($groups = 3)
{
}


function __notifications($userId, $page = null)
{
    return \App\Helpers\UserNotificationHelper::__notifications($userId, $page = null);
}

function __n($userId, $text = "", $push  = false, $level = 'info', $type = 'commissioner', $url = '/')
{
    return \App\Helpers\UserNotificationHelper::__n($userId, $text = "", $push  = false, $level = 'info', $type = 'commissioner', $url = '/');
}

function __read_notice($id)
{
    return \App\Helpers\UserNotificationHelper::__read($id);
}

function __invoice_total(&$invoice, $showNegative = false)
{
    return InvoiceHelper::calculate_invoice_total($invoice, $showNegative);
}

function __menu($url, $titleKey, $default = "", $additionalText = "", $active = false, $id = null, $liClass = '')
{

    $menuText = __c($titleKey, $default);
    $liClass = $liClass . " text-uppercase";
    $active = $active ? "active" : "";
    $aClass = "" . $active;
    $id = ($id) ? $id : md5(date('U') . rand(1, 1000));
    $html = "<li class='{$liClass}'><a class='{$aClass}' id='{$id}' href='{$url}' >{$menuText} {$additionalText}</a></li>";
    return $html;
}

function __menu_dd($key, $default = "", $active = false, $subs = [], $liClass = '', $addText = [])
{
    try {
        $parentText = __c($key, $default);
        $textBefore = (isset($addText['before'])) ? $addText['before'] : "";
        $textAfter = (isset($addText['after'])) ? $addText['after'] : "";
        $id = md5($key . date('U'));
        $html =  "<li class='text-uppercase has-dropdown  {$liClass}'>
                    <a
                        class=' dd-menu-toggler'
                        href='#'
                        data-dd='{$id}'
                        >
                        {$textBefore} {$parentText} {$textAfter}</a><br>
                            <div id='{$id}' class='dd-menu' aria-labelledby='{$key}' data-label='{$parentText}'><ul>";
        foreach ($subs as $sub) {
            $default = isset($sub['default']) ? $sub['default'] : $sub['key'];
            if (isset($sub['skip_key']) && $sub['skip_key']) {
                $subText = $default;
            } else {
                $subText = __c($sub['key'], $default);
            }
            $html .= "<li><a  href='{$sub['url']}'  class='text-white'>{$subText}</a></li>";
        }
        $html .= "</ul></div></li>";
        return $html;
    } catch (\Exception $e) {
        \Log::error(":" . $e->getMessage());
    }

    return null;
}

function __sorter($route, $sortBy, $order, $page, $curSortBy, $curOrder, $addClass = "")
{
    $params = [
        'sortby' => $sortBy,
        'order' => $order,
        'page' => $page,
    ];
    $class = ($order == $curOrder && $curSortBy == $sortBy) ? $addClass . " text-brand-blue" : $addClass . " text-white";
    $chevron = ($order == 'asc') ? "fa fa-sort-up" : "fa fa-sort-down";

    return "<a href='" . route($route, $params) . "' class='{$class}' style='display: block;line-height: 1px;position:absolute;padding:3px;margin-right:5px;'><i class='{$chevron}'></i></a>";
}

function __conf($key, $type = 'text', $default = null)
{
    return SiteHelper::conf($key, $type, $default);
}

function __set_conf($key, $value)
{
    return SiteHelper::set_conf($key, $value);
}

function __adm_ma($route, $names = [])
{
    if (in_array($route, $names)) {
        return 'active';
    }
    return '';
}

function __num_to_month($num)
{
    $month = [
        '1' => 'Jan',
        '2' => 'Feb',
        '3' => 'Mar',
        '4' => 'Apr',
        '5' => 'May',
        '6' => 'Jun',
        '7' => 'Jul',
        '8' => 'Aug',
        '9' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec'
    ];
    return (isset($month[$num])) ? $month[$num] : '';
}

function __pool_type_help($poolType = null, $default = null)
{
    if ($poolType) {
        //            1	Squares
        //2	Schwindy
        //3	Empty Squares
        //4	NCAA Tournament Squares
        //5	Golf
        switch ($poolType->id) {

            case 1:
                return 'squareboard';
                break;
            case 2:
                return 'schwindy';
                break;
            case 3:
                return 'empty_squareboard';
                break;
            case 4:
                return 'ncaa';
                break;
            case 5:
                return 'golf';
                break;
            default:
                return 'pool';
                break;
        }
        //            $template = trim(strtolower(str_replace(' ','_', $poolType->template)));
        //            $name = trim(strtolower(str_replace(' ','_', $poolType->name)));
        //            return "{$template}.{$name}";
    }
    return $default ?? "pool";
}

function __invert_color($hex = '#000000')
{
    $arr = str_split(str_replace('#', '', $hex), 2);
    foreach ($arr as &$value) {
        try {
            $c = base_convert($value, 16, 10);
            $value = str_pad(base_convert(255 - $c, 10, 16), 2, '0', STR_PAD_LEFT);
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }
    $newColor = "#" . implode('', $arr);
    return $newColor;
}

function __number_to_letter($num)
{
    return GolfHelper::group_number_to_letter($num);
}

function __short_name($fullName = '')
{
    $parts = explode(' ', $fullName);
    $lastName = isset($parts[1]) ? $parts[1] : "";

    $firstNameInitial = isset($parts[0]) ? substr($parts[0], 0, 1) : "";
    return $firstNameInitial . ". " . $lastName;
}

function __split_name($fullName = "")
{
    $firstName = "";
    $lastName = "";
    $middleName = "";

    $parts = explode(' ', $fullName);
    $count = count($parts);

    try {
        switch ($count) {
            case 1:
                $firstName = $parts[0];
                break;
            case 2:
                $firstName = $parts[0];
                $lastName = $parts[1];
                break;
            case 3:
                $firstName = $parts[0];
                $middleName = $parts[1];
                $lastName = $parts[2];
                break;
            case 4:
                $firstName = $parts[0];
                $middleName = $parts[1] . " " . $parts[2];
                $lastName = $parts[3];
                break;
        }
    } catch (\Exception $e) {
        \Log::error(":" . $e->getMessage());
    }

    return ['first_name' => $firstName, 'middle_name' => $middleName, 'last_name' => $lastName];
}

function __format_money($amount, $dollar = "$")
{
    return "{$dollar}" . number_format($amount, '2');
}

function __fix_rank($rank = null, $nr = "")
{
    if ($rank == '9999') {
        return $nr;
    }
    return $rank;
}

function __dump_time(&$start_time = null, $id = null)
{
    if (!$start_time) {
        $start_time = microtime(true);
    }

    $end_time = microtime(true);
    $execution_time = ($end_time - $start_time);
    //        \Log::info("[{$id}] Script Ran For: ".$execution_time);
}

function __dlog(&$e)
{
    \Log::error($e);
}

function __h($section = "site", $subSection = "general", $action = "generic", $label = "")
{
}

function __gtm_push($event = null, $data = [])
{
    return SiteHelper::generate_gtm_datalayer($event, $data);
}

function __gtm_pull()
{
    return SiteHelper::output_gtm_datalayer();
}
