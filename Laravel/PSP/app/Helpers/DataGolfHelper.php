<?php
	
	namespace App\Helpers;
	
	
	use GuzzleHttp\Client;
	use Illuminate\Support\Collection;
	use Illuminate\Support\Facades\Storage;
	
	class DataGolfHelper
	{
		private $_apiKey;
		private $_playersFeed;
		private $_rankingFeed;
		private $_tourScheduleFeed;
		private $_liveFieldUpdateFeed;
		private $_liveTournamentStats;
		private $_guzzleClient;
		
		
		public function __construct()
		{
			$this->_apiKey = __conf('datagolf.api_key','text','5886b5830c6554b18207af73d021');
			$this->_playersFeed = __conf('datagolf.players_feed','text','https://feeds.datagolf.com/get-player-list')."?key=".$this->_apiKey;
			$this->_rankingFeed = __conf('datagolf.ranking_feed','text','https://feeds.datagolf.com/preds/get-dg-rankings')."?key=".$this->_apiKey;
			$this->_tourScheduleFeed = __conf('datagolf.tour_schedule_feed','text','https://feeds.datagolf.com/get-schedule')."?key=".$this->_apiKey;
			
			$this->_liveFieldUpdateFeed = __conf('datagolf.live_field_update_feed','text','https://feeds.datagolf.com/field-updates')."?key=".$this->_apiKey;
			$this->_liveTournamentStats = __conf('datagolf.live_tournament_stats','text','https://feeds.datagolf.com/preds/live-tournament-stats')."?key=".$this->_apiKey;
			$this->_guzzleClient = new Client();
		}
		
		
		public function get_players(){
			$rawData = $this->_fetch_data($this->_playersFeed);
			return $rawData;
		}
		
		public function get_schedule(){
			$rawData = $this->_fetch_data($this->_tourScheduleFeed);
			return $rawData;
		}
		
		public function get_rankings(){
			$rawData = $this->_fetch_data($this->_rankingFeed);
			return $rawData;
		}
		
		public function get_live_field(){
			$rawData = $this->_fetch_data($this->_liveFieldUpdateFeed);
			return $rawData;
		}
		
		public function get_tournament_stats($round = null){
			if($round){
				$rawData = $this->_fetch_data($this->_liveTournamentStats, ['round' => $round]);
			} else {
				$rawData = $this->_fetch_data($this->_liveTournamentStats);
			}
			return $rawData;
		}
		
		private function _fetch_data($url, $params=[]){
			$content = "{}";
			if(count($params)){
				$url = $url.'&'.http_build_query($params);
			}
			try{
				 if($url){
					 $res = $this->_guzzleClient->get($url);
					 $content = (string) $res->getBody();
				 }
				
			} catch (\Exception $e){
				\Log::error($e);
			}
			return json_decode($content);
		}
	
	}