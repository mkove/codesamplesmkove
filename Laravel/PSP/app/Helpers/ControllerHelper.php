<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ControllerHelper
{

    private $_messages = [];
    private $_isAjax;
    private $_sendTo;
    private $_view_data = [];
    private $_crumb_data = [];

    public function __construct(Request $request)
    {
        $this->_isAjax = ($request) ? $request->ajax() : false;
    }

    public function init(&$controller, $data = [])
    {
        foreach ($data as $key => $value) {
            try {
                $controller->{$key} = $value;
                $this->_view_data[$key] = $value;
                $this->_view_data[camel_case($key)] = $value;
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
    }


    public function set_crumbs($crumbs)
    {
        $this->_crumb_data = $crumbs;
    }

    public function add_notice($key, $default = "", $type = 'default')
    {
        try {
            if ($this->_isAjax) {
                SiteHelper::add_config_notice($key, $default, $type, $this->_messages);
            } else {
                SiteHelper::add_config_notice($key, $default, $type);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
    }

    public function build_response($path = null, $data = [], $encode = false)
    {
        try {
            $this->_sendTo = session()->pull('sendTo');
            //if request is ajax send ajax
            if ($this->_isAjax) {
                return $this->_build_ajax($data, $path);
            } else {
                //if looking for view, load view
                if (\View::exists($path)) {
                    return $this->_build_view($path, $data, $encode);
                } else {
                    //not a view, send to redirect
                    return $this->_build_redirect($path);
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return redirect('/error');
        }
    }

    /**
     * @param false $commissionerOnly
     * @param false $allowGuests
     * @return false|\Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     */
    public function check_and_protect($commissionerOnly = false, $allowGuests = false)
    {

        try {
            //            if(!Auth::user()) return false;

            $pool = isset($this->_view_data['pool']) ? $this->_view_data['pool'] : null;
            $user = isset($this->_view_data['user']) ? $this->_view_data['user'] : null;
            //legacy shit if pool doesn't have an id

            //send them away if pool doesn't exist
            if (!$pool || !$pool->id) {
                $this->add_notice('pool.notifications.system.pool_does_not_exist', "Sorry this pool does not exist", 'danger');
                return $this->build_response('psp.user.pools');
            }
            //if comish only - and not comish send them away
            if ($commissionerOnly) {

                //IS commissioner and allow entrance
                if ($user && $pool->is_commissioner($user->id)) {
                    return false;
                } else {
                    $this->add_notice('pool.notifications.system.not_commissioner', "Sorry, you're not a commissioner for this pool", 'danger');
                    return $this->build_response(route('psp.pool.show', ['pool' => $pool->id]));
                }
            } else {
                if ($allowGuests) {
                    return false;
                } else {
                    //OK this is my pool
                    if (PoolHelper::my_pool($user, $pool)) {
                        return false;
                    } else {
                        $this->add_notice('pool.notifications.system.not_a_member', "Sorry this is private pool. You do not have access to it!", 'danger');
                        return $this->build_response('psp.user.pools');
                    }
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }

        return $this->build_response('/');
    }

    private function _build_view($view, $vars = [], $encode = false)
    {
        $allVars = array_merge($this->_view_data, $vars);
        $allVars['crumbs'] = $this->_crumb_data;
        if ($encode) {
            return view($view,  $allVars)->render();
        }
        return view($view,  $allVars);
    }

    private function _build_redirect($route, $data = [])
    {
        try {
            if ($this->_sendTo) {
                return redirect($route);
            }
            if (\Route::has($route, $data)) {
                return redirect()->route($route, $data);
            } elseif (strpos($route, '/') !== false) {
                return redirect($route);
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return redirect('/');
    }

    private function _build_ajax($data = [], $path = null)
    {
        if ($path && \View::exists($path) && !isset($data['html'])) {
            try {
                $data['html'] = $this->_build_view($path, $data, true);
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
        if (!isset($data['messages'])) {
            $data['messages'] = $this->_messages;
        }

        return response()->json($data);
    }

    private function _build_vars($data = [])
    {

        $this->_view_data = [];

        $vars = [
            'user' => $this->user,
            'pool' => $this->pool,
            'golf' => $this->golf,
            'crumbs' => $this->_crumb_data,
            'invoice' => $this->invoice,
            'chat' => $this->chat,
            'userPicks' => $this->user_picks,
            'tournament' => $this->tournament,
            'handicaps' => $this->handicaps
        ];
        return array_merge(
            $vars,
            $data
        );
    }
}
