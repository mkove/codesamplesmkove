<?php

namespace App\Helpers;

use App\Mail\AdminNotice;
use App\Models\Config;
use App\Models\SiteContent;
use App\Models\User;
use App\Providers\User\Notification;

class UserNotificationHelper
{

    public static function __n($userId, $notification, $poolId, $action, $actionName, $commissionerId, $type, $level, $push, $url)
    {
        try {
            $notificationProvider = resolve(Notification::class);

            $notificationProvider->send_notice(
                $userId,
                $notification,
                $poolId,
                $action,
                $actionName,
                $commissionerId,
                $type,
                $level,
                $push,
                $url
            );
        } catch (\Exception $e) {
            \Log::error("Notice didn't send because " . $e->getMessage());
        }
        return true;
    }

    public static function unread_count($userId)
    {
        $filters = ['user_id' => $userId, 'read' => false];
        return app(Notification::class)->get(null, $filters, true)->count();
    }

    public static function __notifications($userId, $page = false)
    {
        try {
            return app(Notification::class)->get_notices_by_user_id($userId, $page);
        } catch (\Exception $e) {
            \Log::error("Notice didn't send because " . $e->getMessage());
        }
        return [];
    }

    public static function __read($id)
    {
        try {
            return app(Notification::class)->mark_read($id);
        } catch (\Exception $e) {
            \log::error("Notice didn't send because " . $e->getMessage());
        }
        return [];
    }

    public static function send_pool_notice($user, $type, $pool, $commissioner = null, $entity = null, $push = false)
    {
        switch ($type) {
            case 'invited':

                break;
            case 'was_invited':

                break;
        }
    }

    public static function send_square_notice($userId, $action, $pool, $commissionerId = null, $entry = null, $push = false)
    {
        try {
            $commissioner = ($commissionerId) ? $pool->users->where('id', $commissionerId)->first() : null;
            $user = $pool->users->where('id', $userId)->first();
            $url = route('psp.squareboard.pick', ['pool' => $pool->id, 'squareboard' => $pool->squareBoard->id]);
            $level = 'info';
            $type = 'user';
            $text = "";
            $textAll = null;
            $all = false;
            $push = true;

            switch ($action) {

                case 'pick':
                    $actionName = "Picked Square";
                    $actionNameAll = "Picked Square";
                    $text = ($commissionerId) ? $commissioner->pivot->display_name . " Picked " . $entry : "You Picked " . $entry;
                    $textAll = ($commissionerId) ? $commissioner->pivot->display_name . " Picked " . $entry : "You Picked " . $entry;

                    break;

                case 'unpick':
                    $actionName = "Unpicked Square";
                    $actionNameAll = "Unpicked Square";
                    $text = ($commissionerId) ? $commissioner->pivot->display_name . " Picked " . $entry : "You Unpicked " . $entry;
                    $textAll = ($commissionerId) ? $commissioner->pivot->display_name . " Unpicked " . $entry : "You Unpicked " . $entry;
                    break;
            }
            if ($all) {
                foreach ($pool->users as $user) {
                    if ($user->id == $userId) {
                        self::__n($userId, $text, $pool->id, $action, $actionName, $commissionerId, $type, $level, $push, $url);
                    } else {
                        self::__n($user->id, $textAll, $pool->id, $action, $actionNameAll, $commissionerId, $type, $level, $push, $url);
                    }
                }
            } else {
                if ($text != "") {
                    self::__n($userId, $text, $pool->id, $action, $actionName, $commissionerId, $type, $level, $push, $url);
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public static function send_schwindy_notice($userId, $action, $pool, $commissionerId = null, $entity = null, $push = false)
    {
        try {
            $commissioner = ($commissionerId) ? $pool->users->where('id', $commissionerId)->first() : null;
            $user = $pool->users->where('id', $userId)->first();
            $url = route('psp.schwindy.pick', ['schwindy' => $pool->schwindy->id]);
            $level = 'info';
            $type = 'user';
            $text = "";
            $textAll = null;
            $all = false;
            $push = true;

            switch ($action) {
                case 'view':
                    if ($commissioner) {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "viewed", "picks");
                        $textAll = self::_do_sentence($pool, $userId, $commissionerId, "viewed", "picks");
                        $actionName = "picks";
                        $actionNameAll = "picks";
                        $push = true;
                        $all =  true;
                        $pushAll  = true;
                    }
                    break;
                case 'unpick':
                    if ($commissioner) {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "unpicked", $entity->team->name, " for week " . $entity->week);
                        $textAll = self::_do_sentence($pool, $userId, $commissionerId, "unpicked", " team ", " for week " . $entity->week);
                        $actionName = "{$entity->team->name} / week {$entity->week}";
                        $actionNameAll = "team / week {$entity->week}";
                        $level = 'warning';
                        $type = 'commissioner';
                        $push = true;
                        $all = true;
                    } else {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "unpicked", $entity->team->name, " for week " . $entity->week);
                        $actionName = "{$entity->team->name} / week {$entity->week}";
                    }
                    break;
                case 'pick':
                    if ($commissioner) {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "picked", $entity->team->name, " for week " . $entity->week);
                        $textAll = self::_do_sentence($pool, $userId, $commissionerId, "picked", " team ", " for week " . $entity->week);
                        $actionName = "{$entity->team->name} / week {$entity->week}";
                        $actionNameAll = "team / week {$entity->week}";
                        $level = 'warning';
                        $type = 'commissioner';
                        $push = true;
                        $all = true;
                    } else {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "picked", $entity->team->name, " for week " . $entity->week);
                        $actionName = "{$entity->team->name} / week {$entity->week}";
                    }
                    break;
                case 'autopick':
                    $text = $textAll = self::_do_sentence($pool, $userId, false, "autopicked", $entity->team->name, " for week " . $entity->week);
                    $actionName = "{$entity->team->name} / week {$entity->week}";
                    $actionNameAll = "team / week {$entity->week}";
                    $level = 'warning';
                    $type = 'system';
                    $push = true;
                    $all = true;
                    break;
                case 'play':
                    $text =  $textAll = self::_do_sentence($pool, $userId, false, "scored", $entity->team->name, " for week " . $entity->week);
                    $actionName = "{$entity->team->name} / week {$entity->week}";
                    $actionNameAll = "team / week {$entity->week}";
                    $push = true;
                    break;
                case 'update':
                    if ($commissioner) {
                        $textAll = self::_do_sentence($pool, $commissionerId, $commissionerId, "updated", "pool", "settings");
                        $actionName = "update_settings";
                        $actionNameAll = "update_settings";
                        $level = 'info';
                        $type = 'commissioner';
                        $push = true;
                        $all = true;
                    }
                    break;
                default:

                    break;
            }
            if ($all) {
                foreach ($pool->users as $user) {
                    if ($user->id == $userId) {
                        self::__n($userId, $text, $pool->id, $action, $actionName, $commissionerId, $type, $level, $push, $url);
                    } else {
                        self::__n($user->id, $textAll, $pool->id, $action, $actionNameAll, $commissionerId, $type, $level, $push, $url);
                    }
                }
            } else {
                if ($text != "") {
                    self::__n($userId, $text, $pool->id, $action, $actionName, $commissionerId, $type, $level, $push, $url);
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public static function send_golf_notice($userId, $action, $pool, $commissionerId = null, $entityString = null, $push = false)
    {
        try {
            $commissioner = ($commissionerId) ? $pool->users->where('id', $commissionerId)->first() : null;
            $user = ($userId) ?  $pool->users->where('id', $userId)->first() : null;
            $url = route('psp.golf.picks', ['golf' => $pool->golf->id]);
            $level = 'info';
            $type = 'user';
            $text = "";
            $textAll = null;
            $all = true;
            $forEntryString = $entityString ? " <span class='text-brand-orange'>" . $entityString . "</span> " : "";
            $actionName = "";
            $actionNameAll = "";
            switch ($action) {
                case 'view':
                    if ($commissioner) {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "viewed", "picks", $forEntryString);
                        $textAll = self::_do_sentence($pool, null, $commissionerId, "viewed", "picks", $forEntryString);
                    }
                    break;
                case 'unpick':
                    if ($commissioner) {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "unpicked", "picks", $forEntryString);
                        $textAll = self::_do_sentence($pool, null, $commissionerId, "unpicked", "picks", $forEntryString);
                        $level = 'warning';
                    } else {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "unpicked", "pick", $forEntryString);
                        $actionName = "";
                    }
                    break;
                case 'pick':
                    if ($commissioner) {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "picked", "picks", $forEntryString);
                        $textAll = self::_do_sentence($pool, null, $commissionerId, "picked", "picks", $forEntryString);
                        $level = 'warning';
                    } else {
                        $text = self::_do_sentence($pool, $userId, $commissionerId, "picked", "picks", $forEntryString);
                    }
                    break;
                case 'delete':
                    $text = $textAll = self::_do_sentence($pool, $userId, false, "deleted", "picks", $forEntryString);
                    $textAll = self::_do_sentence($pool, null, $commissionerId, "picked", "picks", $forEntryString);
                    $level = 'danger';
                    break;
                case 'create':
                    $text = $textAll = self::_do_sentence($pool, $userId, false, "created", "picks", $forEntryString);
                    $textAll = self::_do_sentence($pool, null, $commissionerId, "picked", "picks", $forEntryString);
                    break;
                case 'play':
                    $text =  $textAll = self::_do_sentence($pool, null, false, "scored", "picks", $forEntryString);
                    $type = 'system';
                    break;
                case 'update':
                    if ($commissioner) {
                        $textAll = self::_do_sentence($pool, $commissionerId, $commissionerId, "updated", " settings: ", $forEntryString);
                        $actionName = "";
                        $actionNameAll = " entry ";
                        $level = 'info';
                        $type = 'user';
                        $push = true;
                        $all = true;
                    }
                    break;
                default:
                    $text =  "";
                    $actionName = "";
                    $actionNameAll = "";
                    $push = false;
                    break;
            }
            if ($all) {
                foreach ($pool->users as $user) {
                    if ($user->id == $userId) {
                        self::__n($userId, $text, $pool->id, $action, $actionName, $commissionerId, $type, $level, $push, $url);
                    } else {
                        self::__n($user->id, $textAll, $pool->id, $action, $actionNameAll, $commissionerId, $type, $level, $push, $url);
                    }
                }
            } else {
                if ($text != "") {
                    self::__n($userId, $text, $pool->id, $action, $actionName, $commissionerId, $type, $level, $push, $url);
                }
            }
        } catch (\Exception $e) {
            \Log::error(":" . $e->getMessage());
        }
        return true;
    }

    public static function send_admin($subject = "NOTICE TO ADMIN", $message = "", $toEmail = [])
    {

        if (!count($toEmail)) {
            $toEmail = User::where('is_admin', 'yes')->get(['email']);
        }
        // $emailSender = \Mail::to($toEmail);
        // $emailSender->send(new AdminNotice($subject, $message));
        $subject = "[" . strtoupper(config('app.env')) . "] " . $subject;
        for ($i = 0; $i < count($toEmail); $i++) {
            try {
                $email = $toEmail[$i]['email'];
                $emailSender = new AdminNotice($subject, $message);
                $emailSender->send_admin_email($email);
            } catch (\Exception $e) {
                \Log::error(":" . $e->getMessage());
            }
        }
    }


    private static function _do_sentence($pool, $userId, $commissionerId, $action, $entityText, $additionalText = null)
    {
        $users = $pool->users;
        $user = ($userId) ? $users->where('id', $userId)->first() : null;
        $userNameText = ($user) ? $user->pivot->display_name : "";
        $comish = $users->where('id', $commissionerId)->first();
        $you = ($action == 'autopicked') ? "SYSTEM " : "You ";
        $userName = ($commissionerId) ? "*" . $comish->pivot->display_name : $you;
        $forUser = ($user && $commissionerId && $commissionerId != $userId) ? " for <span class='text-brand-blue'>" . $userNameText . "</span>" : "";
        $addText = ($additionalText) ? "<span class=''>{$additionalText}</span>" : "";
        return "<span class='text-brand-blue'>{$userName}</span> <span class='text-brand-green'>{$action}</span> <span class=''>{$entityText}</span> {$forUser} {$addText}";
    }
}
