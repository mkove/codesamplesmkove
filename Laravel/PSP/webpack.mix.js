let mix = require('laravel-mix');
mix.pug = require('laravel-mix-pug');
const webpack = require('webpack');



// mix.webpackConfig({
//     resolve: {
//         extensions: ['.js', '.vue', '.json'],
//         alias: {
//             // 'vue$': 'vue/dist/vue.esm.js',
//             '@': __dirname + '/resources/assets'
//         },
//     },
// })
let path = require('path');


module.exports = {
    resolve: {
        alias: {
            // If using the runtime only build
            // vue$: 'vue/dist/vue.runtime.esm.js' // 'vue/dist/vue.runtime.common.js' for webpack 1
            // Or if using full build of Vue (runtime + compiler)
            vue$: 'vue/dist/vue.esm.js',      // 'vue/dist/vue.common.js' for webpack 1
            jquery: path.join(__dirname, 'node_modules/jquery/src/jquery'),
        }
    }
}


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.autoload({
//     jquery: ['$', 'jQuery', 'window.jQuery'],
//     tether: ['Tether', 'windows.Tether']
// })
mix.autoload({
    jquery: [
                '$',
                'window.jQuery',
                'jQuery',
                'window.$',
                'jquery',
                'window.jquery'
    ]
});

mix.webpackConfig({
    plugins: [
        new webpack.ProvidePlugin({
            $               : 'jquery',
            jQuery          : 'jquery',
            'window.jQuery' : 'jquery',
            Popper          : ['popper.js', 'default'],
            Alert           : 'exports-loader?Alert!bootstrap/js/dist/alert',
            Button          : 'exports-loader?Button!bootstrap/js/dist/button',
            Carousel        : 'exports-loader?Carousel!bootstrap/js/dist/carousel',
            Collapse        : 'exports-loader?Collapse!bootstrap/js/dist/collapse',
            Dropdown        : 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
            Modal           : 'exports-loader?Modal!bootstrap/js/dist/modal',
            Popover         : 'exports-loader?Popover!bootstrap/js/dist/popover',
            Scrollspy       : 'exports-loader?Scrollspy!bootstrap/js/dist/scrollspy',
            Tab             : 'exports-loader?Tab!bootstrap/js/dist/tab',
            Tooltip         : "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
            Util            : 'exports-loader?Util!bootstrap/js/dist/util',
            Overhang        : 'export-loader?Overhang!overhang/js/dist/overhang'
        }),
    ],
    resolve: {
        alias: {
            '@': __dirname + '/resources/assets',
            jquery: path.join(__dirname, 'node_modules/jquery/src/jquery'),
        },
    },
});

mix.js('resources/assets/pspfront/js/bootstrap.js', 'public/js');
mix.js('resources/assets/pspfront/js/bootstrap-collapse.js', 'public/js/bootstrap-collapse.js');
mix.js('resources/assets/pspfront/js/bootstrap-tooltip.js', 'public/js/bootstrap-tooltip.js');

mix.js('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js', 'public/js');
mix.js('node_modules/datatables.net/js/jquery.dataTables.js', 'public/js');

mix.js('resources/assets/pspfront/js/common-jquery.js', 'public/js/common-jquery.js');
mix.js('resources/assets/pspfront/js/common-axios.js', 'public/js/common-axios.js');
mix.js('resources/assets/pspfront/js/common-echo.js', 'public/js/common-echo.js');
mix.js('resources/assets/pspfront/js/common.js', 'public/js/common.js');
mix.js('resources/assets/pspfront/js/required.js', 'public/js/required.js');

mix.js('resources/assets/pspfront/js/components/chat/chat.js', 'public/js/chat.js');


//Schwindy Shit
// mix.styles(['node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css'], 'public/css/datatable.css');
mix.js('resources/assets/pspfront/js/components/pool/schwindy/Live/schwindy.live.js', 'public/js/schwindy.live.min.js');
mix.js('resources/assets/pspfront/js/components/notification/app.js', 'public/js/notification.min.js');
mix.js('resources/assets/pspfront/js/components/pool/schwindy/Pick/schwindy.pick.js', 'public/js/schwindy.pick.min.js');
mix.js('resources/assets/pspfront/js/components/pool/schwindy/PickGrid/schwindy.pick.grid.js', 'public/js/schwindy.pick.grid.min.js');
mix.js('resources/assets/pspfront/js/components/pool/schwindy/WeekConfig/schwindy.week-config.js', 'public/js/schwindy.week-config.min.js');

//Squareboard shit
mix.js('resources/assets/pspfront/js/components/pool/squareboard/PayoutConfig/squareboard.payout-config.js', 'public/js/squareboard.payout-config.min.js');
mix.js('resources/assets/pspfront/js/components/pool/squareboard/Squareboard/squareboard.js', 'public/js/squareboard.min.js');
mix.js('resources/assets/pspfront/js/components/pool/squareboard/Live/squareboard.live.js', 'public/js/squareboard.live.min.js');

//Golf shit
//Golf
mix.js('resources/assets/pspfront/js/components/pool/golf/Info/Standings/standings.js', 'public/js/golf.standings.js');
mix.js('resources/assets/pspfront/js/components/pool/golf/golf.grouping.js', 'public/js/golf.grouping.js');

mix.js('resources/assets/pspfront/js/components/pool/golf/Pick/pick.js', 'public/js/golf.pick.min.js');


mix.js('resources/assets/pspfront/js/components/pool/golf/Handicap/handicap.js', 'public/js/golf.handicap.min.js');
mix.js('resources/assets/pspfront/js/components/pool/golf/MultiPick/multipick.js', 'public/js/golf.multipick.min.js');
mix.js('resources/assets/pspfront/js/components/pool/golf/Group/group.js', 'public/js/golf.group.min.js');
mix.js('resources/assets/pspfront/js/components/pool/golf/Live/golf.live.js', 'public/js/golf.live.min.js');

mix.js('resources/assets/pspfront/js/components/pool/golf/Settings/settings.js', 'public/js/golf.settings.min.js');

//Pool

mix.js('resources/assets/pspfront/js/components/pool/common/Roster/roster.js', 'public/js/pool.roster.min.js');


//Generic js files
mix.combine(['resources/assets/pspfront/js/includes/*.js'], 'public/js/app.min.js');




mix.js('resources/assets/pspadmin/js/adminapp.js', 'public/pspadmin/js/admin.js');
mix.js('resources/assets/pspadmin/js/pool.edit.js', 'public/pspadmin/js/pool.edit.js');
mix.js('resources/assets/pspadmin/js/components/pool/schwindy/pick/schwindy-pick-admin.js', 'public/pspadmin/js/schwindy.pick.admin.min.js');


mix.js('node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.min.js', 'public/pspadmin/js/easy-autocomplete.js');
mix.js('node_modules/flatpickr/dist/flatpickr.min.js', 'public/js/');


mix.styles(['node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.min.css'], 'public/pspadmin/css/easy-autocomplete.css');
mix.styles(['node_modules/flatpickr/dist/flatpickr.min.css'], 'public/css/flatpickr.min.css');


// app.scss
// @import "~bootstrap/dist/css/bootstrap.css";
// @import "~bootstrap-vue/dist/bootstrap-vue.css";

mix.sass('resources/assets/pspfront/sass/bootstrap.scss', 'public/css');
mix.sass('resources/assets/pspfront/sass/main.scss', 'public/css');
    // .sass(
    //    'resources/assets/pspfrontend/sass/bootstrap.scss', 'public/css');
    // .pug([
    //     'resources/assets/pspfront/pug/*.pug',
    //     'resources/assets/pspfront/pug/includes/*.pug',
    //     'resources/assets/pspfront/pug/includes/mixins/*.pug',
    // ], 'dist');
//overhang
mix.js('node_modules/overhang/dist/overhang.min.js', 'public/js/overhang.min.js');

mix.styles(['node_modules/overhang/dist/overhang.min.css'], 'public/css/overhang.min.css');


//ADMIN TOOLSz

mix.js('resources/assets/pspfront/js/components/pool/admin/GSLog/logger.js', 'public/js/logger.min.js');


mix.js('resources/assets/pspadmin/js/components/system/debug/broadcasttest.js', 'public/js/broadcasttest.js');

mix.js('resources/assets/pspadmin/js/components/friend/friend.js', 'public/pspadmin/js/friend.min.js');
mix.js('resources/assets/pspfront/js/components/friend/friend.js', 'public/pspfront/js/friend.min.js');
