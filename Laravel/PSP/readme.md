Repo contains several code samples from larger SaaS project.

For Client's Trade Secrets Protection:

Proprietary business logic was removed.
Internal database structure (migrations) removed.
Some models were removed.
Controllers might have been modified.
Non-functional repo